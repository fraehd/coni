function ascending(A)
%mustBe.ascending Validate that A is either a vector with ascending
%elements or a diagonal matrix with ascending elements.
%
% history:
%   -   created on 14.09.2018 by Jakob Gabriel

   if (isrow(A) || iscolumn(A)) && issorted(A(:), 'ascend')
	   % okay
   elseif ismatrix(A) && isdiag(A) && (size(A,1) == size(A,2)) && issorted(diag(A), 'ascend')
	   % okay
   else
      error('Value assigned to Data property must be a vector or a quadratic diagonal matrix and ascending.')
   end
end