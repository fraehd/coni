function descending(A)
%mustBe.descending Validate that A is either a vector with descending
%elements or a diagonal matrix with descending elements.
%
% history:
%   -   created on 14.09.2018 by Jakob Gabriel

   if (isrow(A) || iscolumn(A)) && issorted(A(:), 'descend')
	   % okay
   elseif ismatrix(A) && isdiag(A) && (size(A,1) == size(A,2)) && issorted(diag(A), 'descend')
	   % okay
   else
      error('Value assigned to Data property must be a vector or a quadratic diagonal matrix and descending.')
   end
end