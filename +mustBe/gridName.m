function gridName( name )

name = misc.ensureString( name );

assert( isstring( name ), 'A grid name should be a string-array')

end

