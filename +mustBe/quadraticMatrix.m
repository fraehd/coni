function quadraticMatrix(A)
%mustBe.quadraticMatrix Validate that A is quatratic Matrix or else throw 
% an error.
%
% history:
%   -   created on 14.09.2018 by Jakob Gabriel

   if (~ismatrix(A)) || (size(A,1) ~= size(A,2))
      error('Value assigned to Data property must be 2-dimensional and quadratic.')
   end
end