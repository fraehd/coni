function unique(A)
%mustBe.unique Validates that A is an array or cell-array with unique
%elements.

	if ~misc.isunique(A)
		error('Value assigned to Data property must be unique.');
	end
end