function scalarOrEmpty(A)
%mustBe.scalarOrEmpty Validates if the argument is empty or a scalar value

	if numel(A) ~= 1 && ~isempty(A)
		error('Value assigned to Data property must be a scalar value or empty.');
	end
end