classdef BasicVariable < quantity.Function
%BasicVariable class for desription of basic variables
%	b = quantity.BasicVariable( FUNC, < name-value-pairs > )
%	defines a new BasicVariable based on the function FUNC defined as
%	function-handle. Other parameters can be set by the name-value-pairs
%	argument list.
properties (SetAccess = protected)
	% The constant gain of the Gevrey-function
	K double = 1;
	% Shifts the evaluation of the derivative of the Gevrey-function.
	% Can be used if the series, computed using the gevrey function,
	% should not start at the 0-th derivative.
	diffShift double {mustBeNonnegative, mustBeInteger};
	% Offset to raise the whole Gevrey-function g_ = g + offset.
	offset double;
	% Number of discretization points for the time variable at which
	% the Gevrey-function is evaluated.
	N_t double;
	% Number of derivatives that need to be considered for this gevrey
	% function
	N_diff double;
	% Length of the time interval the Gevrey-function is defined on.
	T double = -1;
end

properties (Dependent)
	dt;
end

properties (Hidden)
	derivatives;
end

methods
%% CONSTRUCTOR
function obj = BasicVariable(valueContinuous, varargin)
	warning("DEPRICATED!: Use signals.BasicVariable.")
	parentVarargin = {};

	if nargin > 0 && ~isempty(varargin)

		% #todo
		error("This constructor must be reworked!")
			% make default grid:
			preParser = misc.Parser();
			preParser.addParameter('T', 1);
			preParser.addParameter('N_t', 101);
			preParser.addParameter('dt', []);
			preParser.addParameter('N_diff', 1);
			preParser.addParameter('t', []);
			preParser.parse(varargin{:});

			if ~isempty(preParser.Results.dt)
				N_t = quantity.BasicVariable.setDt(preParser.Results.T, preParser.Results.dt);
				preResults.T = preParser.Results.T;
				preResults.dt = preParser.Results.dt;
			elseif ~isempty(preParser.Results.t)
				N_t = numel( preParser.Results.t );
				preResults.T = preParser.Results.t(end);
				preResults.dt = preParser.Results.t(2) - preParser.Results.t(1);			
			elseif ~isempty(preParser.Results.N_t)
				N_t = preParser.Results.N_t;
				preResults.T = preParser.Results.T;
				preResults.N_t = preParser.Results.N_t;
			else
				error('No time domain specified!')
			end
			
			preResults.N_diff = preParser.Results.N_diff;

			parentVarargin = [{valueContinuous}, myDomain, ];
		end

		obj@quantity.Function(parentVarargin{:});

		if nargin > 0 && ~isempty(varargin)
			% first parser
			p1 = misc.Parser();
			p1.addParameter('K', 1);
			p1.addParameter('diffShift', 0);
			p1.addParameter('offset', 0);
			p1.parse(varargin{:});
			for parameter = fieldnames(p1.Results)'
				obj.(parameter{1}) = p1.Results.(parameter{1});
			end
			for parameter = fieldnames(preResults)'
				obj.(parameter{1}) = preResults.(parameter{1});
			end
			
			obj.derivatives = obj;
		end
	end
	
	function D = diff(obj, K)
		% DIFF compute the K-th derivative of obj
		%	
		%
		if nargin == 1
			K = 1;
		end
		
		% for each requested derivative:
		for i = 1:numel(K)
			k = K(i);
			
			% for each component of this object:
			for j = 1:numel(obj)
				obj_j = obj(j);
				
				% check if the derivative is not yet already computed:
				if size(obj_j.derivatives, 1) < k+1 || isempty(obj_j.derivatives(k+1,:))
					% create a new object for the derivative of obj:
					Dij = obj_j.copy();
					Dij.offset = 0;
					Dij.name = ['d/dt (' num2str(k) ') '];
					Dij.valueDiscrete = [];
					Dij.diffShift = Dij.diffShift + k;
					
					% sort the created derivative into the list of computed
					% derivatives
					obj_j.derivatives(k+1,j) = Dij;
					
					% call the function to evaluate the numerical values of
					% this derivative
					Dij.valueDiscrete;
					
					Di(j) = Dij;
				else % the derivative is already computed -> take this one:
					Di(j) = obj_j.derivatives(k+1);
				end
			end
			D(i,:) = Di;
		end
		D = reshape(D, [numel(K) size(obj)]);
	end

	%% PROPERTIES
	function dt = get.dt(obj)
		dt = obj.T / obj.N_t;
	end
	function set.dt(obj, dt)
		obj.N_t = quantity.BasicVariable.setDt(obj.T, dt);
	end
	function set.diffShift(obj, n)
		obj.diffShift = n;
		obj.valueDiscrete = [];
	end
	function set.N_t(obj, value)
		obj.N_t = obj.set_N_t(value);
	end
	function N = get.N_t(obj)
		N = obj.get_N_t(obj.N_t);
	end
	function set.T(obj, value)
		obj.T = obj.set_T(value);
	end
	function T = get.T(obj)
		T = obj.get_T(obj.T);
	end

	function b = copy_K(obj, K)
		b = obj.copy();
		b.valueDiscrete = K / obj.K * obj.valueDiscrete();
		b.K = K;
		b.derivatives(1) = b;
		for i = 2:numel(b.derivatives)
			bi = b.derivatives(i).copy;
			bi.valueDiscrete = K / obj.K * bi.valueDiscrete();
			bi.K = K;
			b.derivatives(i) = bi;
		end
	end
end

methods (Access = protected)
	function T = get_T(obj, T)
	end
	function T = set_T(obj, T)
	end
	function n = get_n(obj, n)
	end
	function n = set_n(obj, n)
	end
	function N = get_N_t(obj, N)
	end
	function N = set_N_t(obj, N)
	end
end

methods (Access = protected, Static)
	function Nt = setDt(T, dt)
		t = 0:dt:T;
		Nt = length(t);
		if ~numeric.near(t(end), T)
			warning(['Sampling with dt=' num2str(dt) ' leads to t(end)= ' num2str(t(end))]);
		end
	end
end

methods (Static)


	function b = makeBasicVariable(b0, K)

		for k = 1:numel(K)
			for l = 1:size(b0, 2)
				b(k, l) = b0(l).copy_K(K(k));
			end
		end

	end

end
end
