classdef Symbolic < quantity.Function
	
	properties (SetAccess = protected)
		valueSymbolic sym;
		variable (:, 1) sym;
	end
	
	properties 
		symbolicEvaluation = false;
	end
	
	methods
		
		function obj = Symbolic(valueContinuous, myDomain, varargin)
			
			%TODO Contruktor überarbeiten:
			% # bei matlabFunction( symbolischer ausdruck ) muss auf die
			%   Reihenfolge der variablen geachtet werden!
			% # Manchmal werden mehrere grid variablen an den
			% untergeordneten Konstruktor durchgereicht.
			
			parentVarargin = {};
			
			if nargin > 0
				
				if isa(valueContinuous, 'symfun')
					valueContinuous = simplify( formula(valueContinuous) );
				end
				
				variableParser = misc.Parser();
				variableParser.addParameter('variable', []);
				variableParser.addParameter('symbolicEvaluation', false);
				variableParser.parse(varargin{:});
				
                if ~variableParser.isDefault('variable')
                    warning('Do not set the variable property. It will be ignored.')
                end
				
    			s = size(valueContinuous);
				S = num2cell(s);
				
				fun = cell(S{:});
				symb = cell(S{:});
				for k = 1:numel(valueContinuous)
					
					if iscell(valueContinuous)
						fun{k} = valueContinuous{k};
					elseif isa(valueContinuous, 'sym') || isnumeric(valueContinuous)
						fun{k} = valueContinuous(k);
					else
						error('Type of valueContinuous not supported')
					end
					
					% input check for valueContinuous
					% FIXME: check that varargin does not contain valueSymbolic.
					% FIXME: initialize valueSymbolic with the correct symbolic
					% variable
					if isa(fun{k}, 'sym')
						symb{k} = fun{k};
						fun{k} = quantity.Symbolic.setValueContinuous(symb{k}, [myDomain.name]);
					elseif isa(fun{k}, 'double')
						symb{k} = sym(fun{k});
						fun{k} = quantity.Symbolic.setValueContinuous(symb{k}, [myDomain.name]);						
					elseif isa(fun{k}, 'function_handle')
						symb{k} = sym(fun{k});
					else
						error(['valueContinuous has to be a symbolic function or a function_handle but is a ', class(fun{k})]);
					end
				end
				
% 				
% 				% check if the reuqired gridName fits to the current
% 				% variables:
% 				if ~all(strcmp(sort({myDomain.name}), sort(variableName)))
% 					error('If the gridName(s) are set explicitly, they have to be the same as the variable names!')
% 				end
				parentVarargin = [{fun}, {myDomain}, varargin(:)'];
			end
			% call parent class
			obj@quantity.Function(parentVarargin{:});
			
			if nargin > 0
				
				if isempty(obj)
					obj = quantity.Symbolic.empty(size(obj));
				end
				
				mySymbolicVariable = ...
					quantity.Symbolic.getVariable(valueContinuous);
				
				% special properties
				for k = 1:numel(valueContinuous)
					obj(k).variable = mySymbolicVariable;  %#ok<AGROW>
					obj(k).valueSymbolic = symb{k}; %#ok<AGROW>
					obj(k).symbolicEvaluation = ...
						variableParser.Results.symbolicEvaluation;  %#ok<AGROW>
				end
				
				assert( ~( ~isempty( mySymbolicVariable ) && isempty( myDomain ) ), ...
					'quantity:Symbolic:Initialisation', ...
					'If a quantity.Symbolic is initialized with a symbolic variable, a domain is required.');
			end
		end % Symbolic-Constructor
		
		function isEq = eq(A, B)
			%==  Equal.
			%   A == B does element by element comparisons between A and B and returns
			%   an array with elements set to logical 1 (TRUE) where the relation is
			%   true and elements set to logical 0 (FALSE) where it is not. A and B
			%   must have compatible sizes. In the simplest cases, they can be the same
			%   size or one can be a scalar. Two inputs have compatible sizes if, for
			%   every dimension, the dimension sizes of the inputs are either the same
			%   or one of them is 1.
			%	
			%	Two quantity.Symbolic objects are equal if they have the
			%	same symbolic expression, same gridSize and same
			%	variables.
			if isempty(A)
				if isempty(B)
					if isequal(size(A),size(B))
						isEq = true;
						return
					else
						isEq = false;
						return
					end
				else
					isEq = false;
					return;
				end
			else
				if isempty(B)
					isEq = false;
					return
				end
			
				isEq = all(size(A) == size(B));

				if ~isEq
					return;
				end

				isEq = isEq && isequal([A.valueSymbolic], [B.valueSymbolic]);
				isEq = isEq && isequal(A(1).domain, B(1).domain);
				isEq = isEq && isequal(A.variable, B.variable);
			end
		end % eq()
		
		function res = ne(A,B)
			% ~= not equal.
			res = ~(A==B);
		end
		
		function f = function_handle(obj)
			f = quantity.Symbolic.setValueContinuous(obj.sym, [obj(1).domain.name]);
		end % function_handle
		
		function F = quantity.Function(obj, varargin)
			myParser = misc.Parser();
			myParser.addParameter('domain', obj(1).domain);
			myParser.addParameter('name', obj(1).name);
			myParser.parse(varargin{:});
			
			for k = 1 : numel(obj)
				F(k) = quantity.Function(obj(k).function_handle(), myParser.Results.domain, ...
					'name', myParser.Results.name);			
			end
			F = reshape(F, size(obj));
		end % quantity.Function()
		
	end % methods
	
	%% mathematical operations
	methods (Access = public)
		function c = cat(dim, a, varargin)
			%CAT Concatenate arrays.
			%   CAT(DIM,A,B) concatenates the arrays of objects A and B
			%   from the class quantity.Discrete along the dimension DIM.
			%   CAT(2,A,B) is the same as [A,B].
			%   CAT(1,A,B) is the same as [A;B].
			%
			%   B = CAT(DIM,A1,A2,A3,A4,...) concatenates the input
			%   arrays A1, A2, etc. along the dimension DIM.
			%
			%   When used with comma separated list syntax, CAT(DIM,C{:}) or 
			%   CAT(DIM,C.FIELD) is a convenient way to concatenate a cell or
			%   structure array containing numeric matrices into a single matrix.
			%
			%   Examples:
			%     a = magic(3); b = pascal(3); 
			%     c = cat(4,a,b)
			%   produces a 3-by-3-by-1-by-2 result and
			%     s = {a b};
			%     for i=1:length(s), 
			%       siz{i} = size(s{i});
			%     end
			%     sizes = cat(1,siz{:})
			%   produces a 2-by-2 array of size vectors.
			%     
			%   See also NUM2CELL.

			%   Copyright 1984-2005 The MathWorks, Inc.
			%   Built-in function.
			if nargin == 1
				objArgin = {a};
			else
				% this function has the very special thing that it a does
				% not have to be an quantity.Discrete object. So it has to
				% be checked which of the input arguments is an
				% quantity.Discrete object. This is considered to be give
				% the basic values for the initialization of new
				% quantity.Discrete values
				objCell = [{a}, varargin(:)'];
				isAquantityDiscrete = cellfun(@(o) isa(o, 'quantity.Discrete'), objCell);
				objIdx = find(isAquantityDiscrete, 1);
				obj = objCell{objIdx};
				
				if ~isempty(obj)
					myDomain = obj(1).domain;
				else
					myDomain = quantity.Domain.empty();
				end
				
				for k = 1:numel(objCell)
					
					if isa(objCell{k}, 'quantity.Symbolic')
						o = objCell{k};
					elseif isa(objCell{k}, 'double') || isa(objCell{k}, 'sym')
						o = quantity.Symbolic( objCell{k}, myDomain);
					end
					objCell{k} = o;
				end
			end
			c = cat@quantity.Discrete(dim, objCell{:});
		end % cat()
		
		function solution = subsNumeric(obj, oldDomainName, value)
			% subsNumeric   Numeric substitution.
			%
			% subsNumeric(obj, oldDomainName, value) replaces the old domains of obj specified by the
			% string-array oldDomainName with the numeric values in value.
			%
			% Example: subs(f(z, zeta, t), ["z", "zeta"], [1, 2]) = f(1, 2, t) = g(t).
			
			arguments
				obj quantity.Discrete;
				oldDomainName (:, 1) string;
				value (:, 1) double;
			end % arguments
			
			assert(numel(oldDomainName) == numel(value), "for every domain to be substituted, ", ...
				"there must be defined one numeric scalar value");
						
			remainingDomains = obj(1).domain.remove(oldDomainName);
			solution = subs(obj.sym, misc.str2cell(oldDomainName), value);
			if ~isempty(remainingDomains)
				solution = quantity.Symbolic(solution, remainingDomains, "name", obj(1).name);
			else 
				solution = double(solution);
			end
		end % subsNumeric()
		
		function solution = solveAlgebraic(obj, rhs)
			% solveAlgebraic solves
			%	obj( x ) == rhs
			% for the variable specified x.
			% Both x and rhs must be scalars.
			% The input parameter findBetween specifies minimum and maximum of the
			% values of obj, between which the solution should be searched.
			arguments
				obj (1, 1);
				rhs (1, 1) double;
			end
			assert(obj(1).nargin == 1, "only implemented for quantites on 1 domain");
			
			symbolicSolution = solve(obj(:).sym == rhs(:), obj(1).variable);
			solution = double(symbolicSolution);
		end % solveAlgebraic()
		
		function solution = solveDVariableEqualQuantity(obj, varargin)
			%% solves the first order ODE
			%	dvar / ds = obj(var(s))
			%	var(s=0) = ic
			% for var(s, ic). Herein, var is the (only) continuous variale
			% obj.variable. The initial condition of the IVP is a variable
			% of the result var(s, ic).
			assert(numel(obj(1).domain) == 1, ...
				'this method is only implemented for quanitities with one domain');
			
			myParser = misc.Parser();
			myParser.addParameter('initialValueGrid', obj(1).domain(1).grid);
			myParser.addParameter('variableGrid', obj(1).domain(1).grid);
			myParser.addParameter('newGridName', 's');
			myParser.parse(varargin{:});
			
			s = sym(myParser.Results.newGridName);
			syms ic varNew(s)
			symbolicSolution = sym(zeros(size(obj)));
			for it = 1:numel(obj)
				symbolicSolution(it) = ...
					dsolve(diff(varNew(s), s) == subs(obj(it).valueSymbolic, ...
					obj(it).variable, varNew(s)), varNew(0)==ic);
			end
			solution = quantity.Symbolic(symbolicSolution, ...
				[quantity.Domain(myParser.Results.newGridName, myParser.Results.variableGrid), ...
					quantity.Domain('ic', myParser.Results.initialValueGrid)], ...
				'name', "solve(" +  obj(1).name + ")");
		end % solveDVariableEqualQuantity()
		
		function sym = sym(obj)
			sym = reshape([obj.valueSymbolic], size(obj));
		end % sym()
		
		function A = adj(obj)
			a = misc.adj(obj.valueSymbolic);
			argin = misc.struct2namevaluepair(obj.obj2struct());
			A = quantity.Symbolic(a, argin{:});
			warning('not tested')
		end % adj()
		
		function s = sum(obj, dim)
			% sum Sum of elements
			% s = sum(X) is the sum of all elements of the array X.
			% s = sum(X, DIM) is the sum along the dimensions specified by DIM.
			arguments
				obj;
				dim = 1:ndims(obj);
			end % arguments
			if ndims(obj) <= 2
				s = quantity.Symbolic(sum(obj.sym(), dim), obj(1).domain, ...
					'name', "sum(" + obj(1).name + ")");
			else
				% todo: sym/sum only supports matrices and vectors, but not ndims > 2
				% a nicer implementation should be possible anyways.
				s = sum(quantity.Discrete(obj), dim);
			end
		end % sum()
		
		function P = prod(obj, dim)
			% prod Product of elements
			% P = prod(X) is the product of all elements of the vector X.
			% P = prod(X, DIM) is the product along the dimensions specified by DIM.
			arguments
				obj;
				dim = 1:ndims(obj);
			end % arguments
			
			if ndims(obj) <= 2
				P = quantity.Symbolic(prod(obj.sym(), dim), obj(1).domain, ...
					'name', "prod(" + obj(1).name + ")");
			else
				% todo: sym/prod only supports matrices and vectors, but not ndims > 2
				% a nicer implementation should be possible anyways.
				P = prod(quantity.Discrete(obj), dim);
			end
		end % prod()
		
		function P = power(obj, p)
			% a.^p elementwise power
			P = quantity.Symbolic(power(obj.sym(), p), obj(1).domain, ...
				"name", obj(1).name + ".^{" + num2str(p) + "}");
		end % power()
		
		function P = mpower(obj, p)
			% Matrix power a^p is matrix or scalar a to the power p.
			P = quantity.Symbolic(mpower(obj.sym(), p), obj(1).domain, ...
				"name", obj(1).name + "^{" + num2str(p) + "}");
		end % mpower()
		
		function d = det(obj)
			% det(X) returns the the determinant of the squre matrix X
			if isempty(obj)
				d = quantity.Discrete.empty(1);
			else
				d = quantity.Symbolic(det(obj.sym()), obj(1).domain, ...
					'name', "det(" + obj(1).name + ")");
			end
		end % det()
		
		function y = sqrt(x)
			% quadratic root for scalar and diagonal symbolic quantities
			y = quantity.Symbolic(sqrt(x.sym()), x(1).domain, ...
				'name', "sqrt(" + x(1).name + ")");
		end % sqrt()
		
		function y = sqrtm(x)
			% quadratic root for matrices of symbolic quantities
			y = quantity.Symbolic(sqrtm(x.sym()), x(1).domain, ...
				'name', "sqrtm(" + x(1).name + ")");
		end % sqrtm()
		
		function b = flipDomain(obj, flipDomainName)
			arguments
				obj
				flipDomainName string;
			end
		
			idx = obj(1).domain.index( flipDomainName );
			for k = 1:numel(flipDomainName)
				flippedDomainName(k) = obj(1).domain(idx(k)).upper - sym( flipDomainName(k) );
			end
			
			b = quantity.Symbolic(subs(obj.sym, num2cell( flipDomainName ), ...
				num2cell( flippedDomainName ) ), obj(1).domain, ...
				'name', "flip(" + obj(1).name + ")");
		end % flipDomain()
		
		function thisVariable = gridName2variable(obj, thisDomainName)
			% this method returns the variable thisVariable stored in obj(1).variable
			% in the order specified by thisGridName. If thisGridName is a char, then
			% only one variable is returned. If thisGridName is a cell-array, then a
			% cell array of variables is returned.
			arguments
				obj;
				thisDomainName (:, 1) string;
			end
			variableString = string(obj(1).variable);
			assert(numel(thisDomainName) == numel(intersect(variableString, thisDomainName)), ...
				"not for every thisDomainName there is a variable");
			variableIdx = (1:numel(variableString)).';
			selectVariable = zeros(numel(thisDomainName), 1);
			for it = 1 : numel(thisDomainName)
				selectVariable(it) = variableIdx(strcmp(variableString, thisDomainName(it)));
			end
			thisVariable = obj(1).variable(selectVariable);
			assert(numel(thisDomainName) == numel(thisVariable), "no variable of that name found");
		end % gridName2variable()
		
		function mObj = uminus(obj)
			% unitary minus: C = -A
			mObj = quantity.Symbolic(-obj.sym, obj(1).domain, 'name', "-" + obj(1).name);
		end % uminus()
		
		function mObj = uplus(obj)
			% unitary plus: C = +A
			mObj = copy(obj);
		end % uplus()
 		
		function C = mtimes(A, B)
			% if one input is ordinary matrix, this is very simple.
			if isempty(B) ||  isempty(A)
				% TODO: actually this only holds for multiplication of
				% matrices. If higher dimensional arrays are multiplied
				% this can lead to wrong results.
				C = quantity.Symbolic( mtimes@quantity.Discrete(A, B) );
				return
			end		
			if isnumeric(B)
				C = quantity.Symbolic(A.sym() * B, A(1).domain, ...
					'name', A(1).name + " c");
				return
			end
			if isnumeric(A)
				C = quantity.Symbolic(A * B.sym(), B(1).domain, ...
					'name', "c " + B(1).name);
				return
			end
			if ~(isa(B, 'quantity.Symbolic')) && isa(B, 'quantity.Function')
				C = (B' * A')';
				return;
			end
			
			parameters.name = A(1).name + " " + B(1).name;
			parameters = misc.struct2namevaluepair(parameters);
			
			C = quantity.Symbolic(A.sym() * B.sym(), join(A(1).domain, B(1).domain), ...
				parameters{:});
		end % mtimes()
		
		function C = plus(a, b)
			% minus() see quantity.Discrete.
			assert(isequal(size(a), size(b)), 'terms must be of same size');
			if ~isa(a, 'quantity.Discrete')
				if isnumeric(a)
					a = quantity.Symbolic(a, quantity.Domain.empty(), 'name', "c");
				else
					error('Not yet implemented')
				end					
				parameters = struct(b(1));
			else
				if ~isa(b, 'quantity.Discrete')
					if isnumeric(b)
						b = quantity.Symbolic(b, quantity.Domain.empty(), 'name', "c");
					else
						error('Not yet implemented')
					end					
				end
				parameters = struct(a(1));
			end
			
			% remove the not required parameters:
			if isfield(parameters, 'variable')
				parameters = rmfield(parameters, 'variable');
			end

			
			% set new parameter values:
			parameters.name = a(1).name + "+" + b(1).name;
			parameters = misc.struct2namevaluepair(parameters);
			
			C = quantity.Symbolic(a.sym() + b.sym(), join(a(1).domain, b(1).domain), ...
				parameters{:});
		end % plus()
		
		function absQuantity = abs(obj)
			% abs returns the absolut value of the quantity as a quantity
			absQuantity = quantity.Symbolic(abs(obj.sym()), obj(1).domain, ...
				'name', "|" + obj(1).name + "|");	
		end % abs()
		
		function y = real(obj)
			% real() returns the real part of the obj.
			y = quantity.Symbolic(real(obj.sym()), obj(1).domain, ...
				'name', "real(" + obj(1).name + ")");
		end % real()
		
		function y = imag(obj)
			% imag() returns the real part of the obj.
			y = quantity.Symbolic(imag(obj.sym()), obj(1).domain, ...
				'name', "imag(" + obj(1).name + ")");
		end % imag()
		
		function y = inv(obj)
			% inv inverts the matrix obj.
			y = quantity.Symbolic(inv(obj.sym()), obj(1).domain, ...
				'name', "(" + obj(1).name + ")^{-1}");
		end % inv()
		
		function y = exp(obj)
			% exp() is the exponential function using obj as the exponent.
			y = quantity.Symbolic(exp(obj.sym()), obj(1).domain, ...
				'name', "exp(" + obj(1).name + ")");
		end % exp()
		
		function y = expm(obj)
			% exp() is the matrix-exponential function using obj as the exponent.
			y = quantity.Symbolic(expm(obj.sym()), obj(1).domain, ...
				'name', "expm(" + obj(1).name + ")");
		end % expm()
		
		function y = log(obj)
		%  log    Natural logarithm.
		%     log(X) is the natural logarithm of the elements of X.
		%     Complex results are produced if X is not positive.
			y = quantity.Symbolic(log(obj.sym()), obj(1).domain, ...
				'name', "log(" + obj(1).name + ")");
		end % log()
		
		function y = log10(obj)
		% log10  Common (base 10) logarithm.
		% log10(X) is the base 10 logarithm of the elements of X.   
		% Complex results are produced if X is not positive.
			y = quantity.Symbolic(log10(obj.sym()), obj(1).domain, ...
				'name', "log10(" + obj(1).name + ")");
		end % log10()
		
		function y = ctranspose(obj)
			% ctranspose() or ' is the complex conjugate tranpose
			y = quantity.Symbolic(conj(obj.sym().'), obj(1).domain, ...
				'name', "{" + obj(1).name + "}^{H}");
		end % expm()
			
		function C = cumInt(obj, domainName, lowerBound, upperBound)
			% CUMINT cumulative integration
			%	result = cumInt(obj, domainName, lowerBound, upperBound)
			%	performes the integration over 'obj' for the 'domain' and the specified 'bounds'.
			%	domainName must be a name of one domain of the object. 
			%	'lowerBound' and 'upperBound' define the boundaries of the integration domain. 
			%	These can be either doubles or names of a domain as a string. If they are doubles,
			%	the bound is constant. If it is a string, the bound is variable and the result is a
			%	function dependent of this variable.
			
			arguments
				obj;
				domainName string;
				lowerBound;
				upperBound;
			end
			assert(isnumeric(lowerBound) || isstring(lowerBound), ...
				"integral bounds must be string or numeric");
			assert(isnumeric(upperBound) || isstring(upperBound), ...
				"integral bounds must be string or numeric");
			
			% get the desired domain. This step is necessary, as the input
			% argument DOMAIN can be either a string or a quantity.Domain
			% object.
			domainName = obj(1).domain.find(domainName);
			
			% do the integration element-wise
			if obj.nargin == numel(domainName) && isnumeric(lowerBound) && isnumeric(upperBound)
				% if the integration has fixed upper and lower bounds, and
				% should be applied for all independent variables, use the
				% implementation based on function handles, because it is
				% much faster:
				C = cumInt@quantity.Function(obj, domainName, lowerBound, upperBound);
				
			else
				try
					% try to compute the symbolic integration
					% for some reason, sometimes complex results result -> Hence, "real" is added.
					I = quantity.Symbolic(real(int(obj.sym, domainName.name)), obj(1).domain);
					
					C = subs(I, domainName.name, upperBound) - ...
						subs(I, domainName.name, lowerBound);
				catch ex
					try
						% try to compute the integration by function
						% handles
						C = cumInt@quantity.Function(obj, domainName, lowerBound, upperBound);
						
					catch ex
						% last fall back to compute the integration by
						% discrete values. This should work always
						C = cumInt(quantity.Discrete(obj), domainName, lowerBound, upperBound);
					end
				end % try-catch
				
			end % if-else
		end % cumInt
		
	end % methods (Access = public)
	
	methods (Access = protected)
		
		function v = evaluateFunction(obj, varargin)
			% Evaluates the symbolic expression of this quantity. If the
			% flag symbolicEvaluation is set, the expression is evaluated
			% with subs(...) otherwise a function handle is used.
			if obj.symbolicEvaluation
				v = double(subs(sym(obj), obj.variable, varargin{:}));
			else
				v = evaluateFunction@quantity.Function(obj, varargin{:});
			end
		end		
		
		function p = getCopyParameters(obj)
			p = nameValuePair(obj, 'gridName');
		end
		
		% Override copyElement method:
		function cpObj = copyElement(obj)
			% Make a shallow copy of all properties
			cpObj = copyElement@quantity.Discrete(obj);
			% #TODO insert code here if some properties should not be copied.
		end
		
		
		%TODO change this function to have the possibility to compute it to a
		%certain accuracy
		function Phi = internPeanoBakerSeries(obj, z, z0, N)
			% PEANOBAKERSERIES_SYM Symbolic computation of (partial) peano baker series
			% Detailed description for The Peano-Baker Series can be found in:
			% The Peano-Baker Series, MICHAEL BAAKE AND ULRIKE SCHLAEGEL, 2012
			%
			% [ Phi ] = panobakerseries_sym( A, z0, N )
			%
			% * A:   System-Operator Matrix \in C[s]^(nxn); with n is the number of
			%      states.
			% * z0:  Lower limit of Integration
			% * N:  Number of iterations
			%
			% * Phi: First result of the state transition matrix.(n x n x 1)
			
			I = quantity.Symbolic( zeros(size(obj,1), size(obj,2), N ));
			I(:,:,1) = eye( size(obj,1) );
			Phi = I(:,:,1);
			for k = 1 : N - 1
				I(:, :, k + 1) = int( obj * I(:, :, k), z, z0, z );
				Phi = Phi + I(:, :, k + 1);
			end
		end
		
		function result = diff_inner(obj, k, diffGridName)
			result = obj.copy();
			result.setName(deal("(d_{" + diffGridName +"}" + result(1).name + ")"));
			[result.valueDiscrete] = deal([]);
			for l = 1:numel(obj)
				result(l).valueSymbolic = diff(obj(l).valueSymbolic, diffGridName, k);
				result(l).valueContinuous = obj.setValueContinuous(result(l).valueSymbolic, [obj(1).domain.name]);
			end
		end % diff_inner()
		
		function obj = subsDomainMerge(obj, oldDomainName, newDomain)
			% subsDomainMerge()	Symbolic substitution for which 2 domains need to be merged.
			% Example:
			%	z = quantity.Domain("z", linspace(0, 1, 11));
			%	zLr = quantity.Domain("z", linspace(0, 1, 5));
			%	zeta = quantity.Domain("zeta", linspace(0, 1, 11));
			%	f = z.Discrete() + zeta.Discrete()	% = f(z, zeta)
			%	f.subsDomainMerge("zeta", zLr)		% = f(z) 
			%			-> the finest grid is chosen in the latter case, hence z and not zLr.
			% This is a helper function for subsDomain, hence it is protected.
			
			% find domain to be merged
			[idxOldDomain, idxOldDomainLogical] = index(obj(1).domain, oldDomainName);
			[idxNewDomain, idxNewDomainLogical] = index(obj(1).domain, newDomain.name);
			
			% pick finest grid.
			newDomain = join(join(newDomain, obj(1).domain(idxNewDomain)), ...
				obj(1).domain(idxOldDomain).rename(newDomain.name));
			newDomainComplete = [newDomain, obj(1).domain(~(idxOldDomainLogical | idxNewDomainLogical))];
			
			% create new obj
			obj = quantity.Symbolic(subs(obj.sym, oldDomainName, newDomain.name), ...
				newDomainComplete, "name", obj(1).name);
		end % subsDomainMerge()
		
		function obj = subsDomainRename(obj, oldDomainName, newDomain)
			% subsDomainMerge()	Symbolic substitution for which 2 domains need to be merged.
			% Example:
			%	z = quantity.Domain("z", linspace(0, 1, 11));
			%	zeta = quantity.Domain("zeta", linspace(0, 1, 11));
			%	f = z.Discrete()					% = f(z)
			%	f.subsDomainRename("z", zeta)		% = f(zeta) 
			%			-> the finest grid is chosen in the latter case, hence z and not zLr.
			% This is a helper function for subsDomain, hence it is protected.
			
			oldDomainNameNewGrid = quantity.Domain(oldDomainName, newDomain.grid);
			obj = obj.changeDomain(oldDomainNameNewGrid);
			newDomainComplete = obj(1).domain.rename(newDomain.name, oldDomainName);
			obj = quantity.Symbolic(subs(obj.sym, oldDomainName, newDomain.name), ...
				newDomainComplete, "name", obj(1).name);
		end % subsDomainRename()
		
	end % (Access = protected)
	
	methods (Static)
		
		function P = zeros(valueSize, varargin)
			%ZEROS initializes an zero quantity.Discrete object of the size
			%specified by the input valueSize.
			P = quantity.Symbolic(zeros(valueSize), varargin{:});
		end % zeros()
		
		function P = ones(valueSize, varargin)
			%ONES initializes an ones-quantity.Discrete object
			%	P = ones(VALUESIZE, DOMAIN) creates a matrix of size
			%	VALUESIZE on the DOMAIN with ones as entries.
			P = quantity.Symbolic(ones(valueSize), varargin{:});
		end % ones()
		
		function P = eye(N, varargin)
			%  eye Identity matrix.
			%     eye(N, domain) is the N-by-N identity matrix on the domain domain.
			%  
			%     eye([M,N], domain) is an M-by-N matrix with 1's on the diagonal and zeros 
			%		elsewhere.
			%     eye([M,N], domain, varargin) passes further inputs to the quantity.Discrete
			%     constructor.
			P = quantity.Symbolic(eye(N), varargin{:});
		end % eye()

	end % methods (Static)
	
	methods (Static, Access = protected)
		
		function var = getVariable(symbolicFunction, nVar)
			% nVar is needed for overloaded functionality in PolynomialOperator
			
			if misc.issym(symbolicFunction(:).')
				var = symvar(symbolicFunction(:).');
				if ~isempty(var) && ~strcmp(string(var(1)), "z")
					% fast solution to order the spatial and temporal variable
					% in the common order: (z, t)
					var = flip(var);
				end
			else
				var = [];
			end
		end % getVariable()
		
		function [f, functionArguments] = setValueContinuous(f, symVar)
			% converts f into a matlabFunction.
			
			if isa(f, 'sym')			
				if isempty(symvar(f))
					% convert the name of the symbolic variables into a comma separated list and
					% then create a function handle by building the function string and evaluate it
					% in matlab. That the function returns the right dimension is ensured by the
					% "zero" function.
					functionArguments = strjoin( string(symVar), ",");
					f = eval("@(" + functionArguments + ") " + ...
						"double(f) + quantity.Function.zero(" + functionArguments + ")");

				else
					if iscell( symVar )
						symVar = [symVar{:}];
					end
						
					f = matlabFunction(f, 'Vars', convertStringsToChars( symVar ) );
				end
			end
		end % setValueContinuous()
	end % methods (Static, Access = protected)
end % classdef
