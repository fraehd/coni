classdef (InferiorClasses = {?quantity.Symbolic}) Piecewise < quantity.Discrete
	%UNTITLED Summary of this class goes here
	%   Detailed explanation goes here
	
	properties
		quantities cell;
	end
	
	methods
		function obj = Piecewise(quantities, optionalArgs)
			arguments
				quantities = {};
				% the default value for the domainToJoin is set as a
				% default (empty) quantity.Domain. This is required in
				% order to allow initialization of
				% quantity.Piecewise-arrays
				optionalArgs.domainToJoin (1,1) quantity.Domain = quantity.Domain;
				optionalArgs.name char = '';
				optionalArgs.upperBoundaryIncluded logical = repmat( true, numel(quantities)-1, 1);
			end
% 			
			initArgs = {};
			
			if nargin > 0
				if isempty( optionalArgs.domainToJoin )
					% If the optional arg has not been set, use the grid of
					% the functions
					domainToJoin = quantities{1}(1).domain;
				else
					domainToJoin = optionalArgs.domainToJoin;
				end				

				% ensure all have the same domain
				d = quantities{1}(1).domain;
				assert( all( cellfun(@(q) all( strcmp( [d.name], [q(1).domain.name] ) ), quantities) ) , ...
					'The quantities for the piecewise combination must have the same domain names' );
				assert(length(quantities) >= 2, 'Only one quantity is given for the piecewise combination. At least 2 are required.');
				
				domain2joinIndex = quantities{1}(1).domain.index(domainToJoin);
% 				assert( quantities{1}(1).domain.index(domainToJoin) == 1, ...
% 					'The domain to join must be the first domain!');

				joinedGrid = quantities{1}(1).domain.find(domainToJoin.name).grid;
				joinedValues = quantities{1}.on();
				joinedValues = permute(joinedValues, ...
						[domain2joinIndex, setdiff((1:ndims(joinedValues)), domain2joinIndex)]);

				% ensure the domains fit to each other and then concatenate
				% them
				for k = 2:length(quantities)
					dkm1 = quantities{k-1}(1).domain.find(domainToJoin.name);
					dk = quantities{k}(1).domain.find(domainToJoin.name);

					assert(numeric.near(dkm1.upper, dk.lower), "Domains do not connect to each other");

					% *) join the domains and the discrete values to a common
					tmpGrid = dk.grid;
					tmpValues = quantities{k}.on();
					joinedGrid = [joinedGrid; tmpGrid(2:end,:)]; %#ok<AGROW>
					
					tmpValues = permute(tmpValues, ...
							[domain2joinIndex, setdiff(1:ndims(tmpValues), domain2joinIndex)]);
					if optionalArgs.upperBoundaryIncluded(k-1)
						% the value for the assembling point should be
						% taken from the left values
						joinedValues = cat(1, joinedValues, ...
							reshape(tmpValues(2:end,:), size(tmpValues) - eye(1, ndims(tmpValues))));
					else
						% the value for the assembling point should be
						% taken from the right values
						joinedValues = cat(1, ...
							reshape(joinedValues(1:end-1,:), size(joinedValues) - eye(1, ndims(joinedValues))), ...
							tmpValues);
					end
				end
				joinedValues = ipermute(joinedValues, ...
						[domain2joinIndex, setdiff((1:ndims(joinedValues)), domain2joinIndex)]);
				
				joinedDomain = quantities{1}(1).domain.copy.replace(...
						quantity.Domain(domainToJoin.name, joinedGrid));
				initArgs = {joinedValues, joinedDomain, "name", optionalArgs.name};
			end
			
			obj@quantity.Discrete(initArgs{:});
			
			if nargin > 0
				[obj.quantities] = deal(quantities);
			end
			
		end % Piecewise

		function thisQuantity = quantity.Discrete(obj)
			thisQuantity = reshape(quantity.Discrete( cat(numel(obj(1).domain)+1, obj.valueDiscrete), ...
				obj(1).domain, "name", obj(1).name), size(obj));
		end % quantity.Discrete()
	end % methods
end % classdef

