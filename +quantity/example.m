%% example.m gives an overview of basic mathematical operations using the quantity.Discrete class

%% Create a new quantity
% First, we want to create a scalar function a(z) = 1 + z that depends on a spatial variable 
% z \in [0, 1]. For this, we descirbe the domain with the name "z" and 101 discrete spatial points:
z = quantity.Domain("z", linspace(0, 1, 101))
% z contains the following properties
%	"grid"	: the spatial grid points linspace(0, 1, 101)
%	"name"	: the name given "z"
%	"n"		: the number of grid points numel(z.grid) = 101
%	"lower"	: the lower boundary of z = 0
%	"upper"	: the upper boundary of z = 1

% Then we can create a(z) = 1 + z using numerical data and this domain z:
a = quantity.Discrete(...
	1+z.grid, ...			% required input: numerical data describing 1 + z
	z, ...					% required input: domain
	"name", "a")			% optional input: name of the function
% all those inputs can be accessed by a.[property-name], for instance a.domain. The
% numerical data is stored in a.valueDiscrete and accessible via a.on().

% verify that a(z) is the desired variable by plotting it:
a.plot();

%% Access to numerical data
% to access the numerical three methods are provided:
% The first is the method a.on(). If it is called without an argument it returns the original data:
isequal(a.on(), 1+z.grid);
% additionally, on can be called using quantity.Domain objects. Sampling a with 3 discrete points:
zSample = quantity.Domain("z", linspace(0, 1, 3)); % the new domain must have the same name as the 
		% domain of a.
a.on(zSample)
% on(gridValues) performs an interpolation, when the specified values do not lie on
%  the grid a.grid, for instance
a.on(0.1234567123123123);

% the second method to access the numeric data is to use the "at" method:
a.at(0)
% only single points can be specified. This is advantangeous if a would be a matrix valued function.

% verify that a(z) = 1 + z by evaluating it for different points in z = 0, 0.5, 1:
[a.at(0), a.at(0.5), a.at(1)]
% or more compact:
a.on([0, 0.5, 1])

% the third method to access the numeric data is to use the index. Evaluation of the first and the
% last point would be:
a.atIndex(1)
a.atIndex(end)

%% Lets do some basic math operations
% spatial derivatives
a_dz = a.diff("z", 1); % reads 1st order derivative of a w.r.t. z
a_dzz = a.diff("z", 2); % reads 2nd order derivative of a w.r.t. z

% illustrate result by a plot
plot([a; a_dz; a_dzz]);	% note: as the derivative is implemented numerically using the builtin 
						%	gradient method, numerical errors occur.
% plus and minus
aPlus1 = a + 1;
twoMinusA = 2-a;

% note that quantites can be concatenated using [ ... ] or the methods vertcat, horzcat or cat.
aaa = [a; aPlus1; twoMinusA];
plot(aaa);

%% Sustitution: b = a(zeta)
b = a.subs("z", "zeta");	% Here, a is copied and the domain z is renamed into zeta.
b.setName("b")				% Rename b from b.name = a into b.name = b

%% Multiplication c(z, zeta) = a(z) * b(zeta)
c = a*b;
c.setName("c").plot(); % yet, plot only supports quantites with up to 2 domains.

%% Compose quantites
% it is also possible to compose quantites, for example given g(t) = sin(2*pi*t), t \in [0, 0.5]
% and we want to compute c2(z, t) = c(z, g(t))
t = quantity.Domain("t", linspace(0, 0.5, 21));
g = quantity.Discrete(sin(2*pi*t.grid), t, "name", "g");
c2 = c.compose(g, "domain", c.domain.find("zeta"));
c2.plot();

%% flip grid, i.e. d(z) = a(1-z)
d = a.flipDomain("z").setName("d");
plot([a; d]);

%% Access of numerical data of multidimensional data c(z, zeta)
% use c.on(domain) for obtaining linearly interpolated values of c
zetaSample = quantity.Domain("zeta", linspace(0, 1, 5));
cDisc = c.on([zSample, zetaSample]); % the first dimension i of the double array cDisc(i, j)
% represents z, while the second represents zeta.

% alternatively, on() can also be called with the grid vectors in a cell-array only, then the first 
% grid is used for the first element of c.domain, the second grid for the second element of c.domain
% and so on:
cDisc = c.on({linspace(0, 1, 3), linspace(0, 1, 5)});
misc.subsurf(cDisc, "xLabel", "z", "yLabel", "\zeta", "myTitle", "c");

% if the first dimension i of the double array cDisc(i, j) should represent zeta instead of z, and
% the second dimension j should represent z, this can be achieved using the domains:
cDisc = c.on([zetaSample, zSample]);

% to evaluate a function c(z,zeta) pointwise
z1 = 0.19;
zeta1 = 0.29;
c.at({z1, zeta1})
%can be used

%% Substitution: of multidimensional data c(z, zeta)
% use subs to replace variables z or zeta with a constant value, for instance,
% c2(zeta) = c(0.25, zeta)
c.subs("z", 0.25).plot();
% To rename both grids at once, also subs can be used. For this, the first argument must be a string
% array of the domains that are considered, and then a list of with what those domains should be
% substituted with. For instance, to obtain c3 = c(1, eta) use
c.subs(["z", "zeta"], 1, "eta").plot();
% This can also be used to swap the names of the domain names:
c.subs(["z", "zeta"], "zeta", "z").domain.name
% To evaluate c for z=zeta use
c.subs("z", "zeta").plot();

%% Init matrices
% create new data: K = [z^2+zeta, zeta-z; z, cos(z)]
% when creating multidimensional quantity matrices, the first dimensions of the numerical input data
% must represent the domains, i.e., z, zeta. The ending dimensions represent size of the array K 
% itself, i.e., size(K) == [2, 2].
% For this ndgrid can be used:
[zNdgrid, zetaNdgrid] = ndgrid(z.grid, z.grid);
Kdata = cat(4, ...								% combine column-wise
	cat(3, zNdgrid.^2+zetaNdgrid, zNdgrid), ... % 1. column of K = Kmat(:,:,:,1)
	cat(3, zetaNdgrid-zNdgrid, cos(zNdgrid)));	% 2. column of K = Kmat(:,:,:,2)
% hence, size(KData) == [numel(z.grid), numel(z.grid), 2, 2];
K = quantity.Discrete(Kdata, [z, quantity.Domain("zeta", z.grid)], "name", "K");

% % If you have access to the Symbolic Toolbox, you can use instead
% K = quantity.Symbolic([sym("z")^2+sym("zeta"), sym("zeta")-sym("z"); sym("z"), cos(sym("z"))], ...
% 	[z, quantity.Domain("zeta", z.grid)], "name", "K");

% Also, lets create the vector x(z) = [-1  + 2*z; (-1 + 2*z)^2]:
x = quantity.Discrete(...
	[linspace(-1, 1, numel(z.grid)).', linspace(-1, 1, numel(z.grid)).^2.'], z, "name", "x");

% Note that K and x are array of quantities, hence size(K)==[2,2], size(x) == [2,1].

%% Get data out of quantity-arrays
Kdata = K.on();		% yields original data Kdata of size [z.n, zeta.n, 2, 2].
K11 = K.on({1, 1})	% yields the value of K(1, 1), but size(K11) == [1, 1, 2, 2].
% To obtain a double array of size(K) == [2, 2] when evaluating a specific point, use at:
K11 = K.at({1, 1})

%% Volterra integral of 2nd kind
% After this initialisation of the data, calculations are easy.
% Consider the calculation of y = x + int_0^z K(z, zeta) * x(zeta) dzeta
y = x + cumInt(K*x.subs("z", "zeta"), "zeta", 0, "z");
% cumInt:	1. input: integrand K(z, zeta) * x(zeta), 
%			2. input: integration variable 'zeta'
%			3. input: lower bound of integral 0
%			4. input: upper bound of integral 'z'
y.plot();

%% plotting of quantities
% As has been shown, quantities can be displayed by the plot method. By default, for each quantity a
% new window is opened and each entry of a matrix valued quantity is plotted into a own subplot. 
% To specifiy which figure should be used for a plot the optional name-value pair argument "fig" can 
% be used. For instance, the following line will plot a quantity into figure number 1. 
a.plot("fig", 1)
% If a second quantity should be plotted into the same subplot afterwards, the name-value pair 
% "hold" can be used. For instance, the following plots the first derivative a_dz of a into the same
% subplot:
a_dz.plot("fig", 1, "hold", true)
% To display different quantities in different subplots, use "subplot()" and the "currentFigure"
% name-value argument:
figure(1); clf;
subplot(211);
c.plot("currentFigure", true);
% With "currentFigure", true combined with the "hold" option, all entries of a matrix valued 
% quantity are composed into one axis:
subplot(212);
aaa.plot("currentFigure", true, "hold", true)

%% Commonly used methods
% Similar to many matlab built-in methods the following operations are supported:
% - math opertions: mtimes, inv, exp, expm, log, log10, sqrt, sqrtm, uminus, prod, sum, power, ...
%		mpower, det, transpose, ctranspose, eig, luDecomposition, jordanReal, mldivide, mrdivide,
%		rdivide
% - integration & differential: int, cumInt, diff
% - array operations: compose, cat, vertcat, horzcat, blkdiag, vec2diag, diag2vec
% - real & imaginary parts: imag(), real()
% - misc & norms: min(), max(), mean(), median(), abs(), MAX(), l2norm, norm, quadraticNorm
% - analize: isdiag, iseye, iszero, isnan, issorted, isNumber
% - create: ones, zeros, eye
% 
% For usage please refer to the description accessible via entering, for instance,
%	help quantity.Discrete.cumInt
%
% If the help description is not sufficient, maybe having a look at the implemention helps, and if
% not, please contact us: jakob.gabriel@uni-ulm.de and ferdinand.fischer@uni-ulm.de