classdef EquidistantDomain < quantity.Domain
	%EQUIDISTANTDOMAIN class to handle the discretization of the range of
	%definition of a function. The discretization points are equally
	%distributed over the domain.
	
	properties (SetAccess = protected)
		stepSize;
	end
	
	methods
		function obj = EquidistantDomain(name, lower, upper, optArgs)
			% EQUIDISTANTDOMAIN construct a domain with constant step size
			%	obj = EquidistantDomain(NAME, LOWER, UPPER, optArgs)
			%	constructs a domain with the name NAME from LOWER to UPPER
			%	where 100 points are used for the discretization.
			%	
			%	obj = EquidistantDomain(NAME, LOWER, UPPER, 'stepNumber', STEPNUMBER)
			%	constructs a domain with the name NAME from LOWER to UPPER
			%	with a grid of STEPNUMBER points.
			%
			%	obj = EquidistantDomain(NAME, LOWER, UPPER, 'stepSize', STEPSIZE)
			%	constructs a domain with the name NAME from LOWER to UPPER
			%	with a constant step size STEPSIZE.
			%
			%	obj = EquidistantDomain(NAME, LOWER, UPPER, ...
			%		'stepSize', STEPSIZE, 'exactBoundary', EB)
			%	constructs a domain with the name NAME from LOWER to UPPER
			%	with a grid of STEPNUMBER points. With EB as quantity.DomainBoundary
			%	it can be chosen if the grid shout reach the upper or lower
			%	boundary exactly.

			arguments
				name = '',
				lower = [],
				upper = [],
				optArgs.stepNumber double {mustBeInteger};
				optArgs.stepSize double {mustBePositive};
				optArgs.exactBoundary = quantity.DomainBoundary.lower;
			end
			
			if nargin > 1
				
				if  isfield(optArgs, 'stepSize') && isfield(optArgs, 'stepNumber')
					warning('If both, stepSize and stepNumber is set; steSize is chosen and stepNumber is neglected!')
				end
				
				if isfield(optArgs, 'stepSize')
					if optArgs.exactBoundary == quantity.DomainBoundary.lower
						grid = lower : optArgs.stepSize : upper;
					elseif optArgs.exactBoundary == quantity.DomainBoundary.upper
						grid = flip( upper : -optArgs.stepSize : lower );
					elseif optArgs.exactBoundary == quantity.DomainBoundary.extend
						grid = lower : optArgs.stepSize : upper;
						if grid(end) < upper
							grid = [grid grid(end) + optArgs.stepSize];
						end
					end
					
					stepSize = optArgs.stepSize;
					
				else
					
					if isfield(optArgs, 'stepNumber')
						grid = linspace(lower, upper, optArgs.stepNumber);
					else
						grid = linspace(lower, upper);
					end
					
					stepSize = grid(2) - grid(1);
					
				end
			else
				grid = [];
			end
			
			obj@quantity.Domain(name, grid);
			obj.stepSize = stepSize;
		end
		
		function d = quantity.Domain(obj)
			d = arrayfun(@(o) quantity.Domain(o.name, o.grid), obj);
		end
	end
end

