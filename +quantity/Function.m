classdef Function < quantity.Discrete
	
	properties
		valueContinuous (1, 1);	% function_handle or double
	end
	
	methods
		%--------------------
		% --- Constructor ---
		%--------------------
		function obj = Function(valueOriginal, myDomain, varargin)
			parentVarargin = {};	% by default the parent-class is called 
									% with no input.
			
			% only do something if there are some paremters. otherwise an
			% empty object will be returned. This is important to allow the
			% initialization as object array.
			if nargin > 0
				sizeValue = size(valueOriginal);
				fun = cell(sizeValue);
				
				% modification of some input parameters to allow the
				% initialization of the object array
				for k = 1:numel(valueOriginal)
					if iscell(valueOriginal)
						fun{k} = valueOriginal{k};
					elseif isnumeric(valueOriginal)
						fun{k} = valueOriginal(k);
					elseif isa(valueOriginal, 'function_handle')
						fun{k} = valueOriginal;
					elseif isa(valueOriginal, 'sym')
						fun{k} = matlabFunction(valueOriginal);
					else
						error('Type of valueContinuous not supported')
					end
				end
				
				% verify that all arguments of each 'fun' are consitent with the domain names
				for k = 1:numel(fun)
					names = misc.functionArguments(fun{k});
					
					if numel(myDomain) == 0 && names == ""
						% no test of the assertion is needed. Indeed, the obj quantity.Domain.empty
						% has no name entry. Hence, it returns empty, which thorws an error in the
						% strjoin function of the below assertion. Unfortunately, this function is
						% also validated if the assertion holds. Hence, the special case for empty
						% myDomain is checked with this if - else construct.
					else
						assert( numel(myDomain) == numel(names) & ...
							numel(myDomain) == numel(myDomain.find(names)), ...
							'quantity:Function:domain', ...
							"The argument names of the function must be compatible with the names " + ...
							"of the domains. You defined a function with the arguments " + ...
							strjoin(names, ', ') + " but a domain with the names " + ...
							strjoin([myDomain.name], ', ') + ".");
					end
				end
				
				parentVarargin = [{cell(sizeValue)}, {myDomain}, varargin(:)'];
			end
			
			obj@quantity.Discrete(parentVarargin{:});
			if nargin > 0
				for it = 1:numel(obj)
					obj(it).valueContinuous = fun{it};
				end
			end
		end%Function constructor

		function f = function_handle(obj)
			
			if numel(obj) == 1
				f = @obj.valueContinuous;
			else
				for k = 1:numel(obj)
					F{k} = @(varargin) obj(k).valueContinuous(varargin{:});
				end

				f = @(varargin) reshape((cellfun( @(c) c(varargin{:}), F)), size(obj));
			end
		end
		
		function Q = quantity.Discrete(obj, varargin)
            % QUANTITY.DISCRETE cast obj to quantity.Discrete object
			myParser = misc.Parser();
			myParser.addParameter('domain', obj(1).domain);
			myParser.addParameter('name', obj(1).name);
			myParser.parse(varargin{:});
			Q = quantity.Discrete(obj.on(), myParser.Results.domain, 'name', myParser.Results.name);
		end % quantity.Discrete()
		
		%-----------------------------
		% --- overloaded functions ---
		%-----------------------------
		function value = obj2value(obj, myDomain, recalculate)
			
			% check if the domain for the evaluation has changed. If not
			% we can use the stored values in valueDiscrete:
			if nargin < 3
				recalculate = false;
			end
			if ~recalculate && isequal(myDomain, obj(1).domain)
				value = obj2value@quantity.Discrete(obj);
			else
				% otherwise the function has to be evaluated on the new
				% domain
				value = cell(size(obj));
				ndGrd = myDomain.ndgrid;
				sortedNdGrid = cell(numel(myDomain), 1);
				domainNames = [myDomain.name];
				
				for k = 1:numel(obj)
					
					if isempty( myDomain )
						sortedNdGrid = ndGrd;
					else
						% verify that the order of the grid is the same as of the function arguments:
						functionArguments = misc.functionArguments( obj(k).valueContinuous );
						
						logIdx = cell(numel(functionArguments), 1);
						
						for it = 1:numel(functionArguments)
							logIdx{it} = strcmp(domainNames, functionArguments(it));
							sortedNdGrid{it} = ndGrd{ logIdx{it} };
						end						
					end
					
					tmp = obj(k).evaluateFunction( sortedNdGrid{:} );
					value{k} = tmp(:);
				end
				value = reshape( cell2mat(value), [ gridLength(myDomain), size(obj)]);
			end
		end % obj2value()
		
		function value = at(obj, point)
			% at() evaluates the object at one point and returns it as array
			% with the same size as size(obj).
			value = zeros(size(obj));
			if ~iscell(point)
				for it = 1 : numel(value)
					value(it) = obj(it).valueContinuous(point);
				end
			else
				for it = 1 : numel(value)
					value(it) = obj(it).valueContinuous(point{:});
				end
			end
		end % at()
		
	end % methods
	
	% -------------
	% --- Mathe ---
	%--------------
	methods
		function mObj = uminus(obj)
			mObj = obj.copy();
			
			for k = 1:numel(obj)
				mObj(k).valueContinuous = @(varargin) - obj(k).valueContinuous(varargin{:});
				mObj(k).valueDiscrete = - obj(k).valueDiscrete;
			end
			
			mObj.setName("-" + obj(1).name);
		end % uminus
		
		function p = inner(A, B)
			
			AB = A * B;
			
			n = size(A, 1);
			m = size(B, 2);
			p = zeros(n, m);
			
			z0 = A(1).domain(1).lower;
			z1 = A(1).domain(1).upper;
			
			for k = 1 : n*m
				p(k) = AB(k).int(z0, z1);
			end
			
		end % inner()
		
		function I = cumInt(obj, domainName, lowerBound, upperBound)
			% CUMINT cumulative integration
			%	result = cumInt(obj, domainName, lowerBound, upperBound)
			%	performes the integration over 'obj' for the 'domain' and the specified 'bounds'.
			%	domainName must be a name of one domain of the object. 
			%	'lowerBound' and 'upperBound' define the boundaries of the integration domain. 
			%	These can be either doubles or names of a domain as a string. If they are doubles,
			%	the bound is constant. If it is a string, the bound is variable and the result is a
			%	function dependent of this variable.
			
			arguments
				obj;
				domainName string;
				lowerBound;
				upperBound;
			end
			assert(isnumeric(lowerBound) || isstring(lowerBound), ...
				"integral bounds must be string or numeric");
			assert(isnumeric(upperBound) || isstring(upperBound), ...
				"integral bounds must be string or numeric");

			% get the desired domain. This step is necessary, as the input
			% argument DOMAIN can be either a string or a quantity.Domain
			% object.
			domain = obj(1).domain.find(domainName);
			
			for k = 1:numel(obj)
				I_val = numeric.cumIntegral( ...
					@(varargin)obj(k).evaluateFunction(varargin{:}), domain.grid);
				
				result = quantity.Discrete(I_val, obj(1).domain);

				% int_lowerBound^upperBound f(.) = F(upperBound) - F(lowerBound)
				I(k) = result.subs(domainName, upperBound) - result.subs(domainName, lowerBound);
				if isa(result, 'quantity.Discrete')
					result.setName("int(" + obj(1).name + ")");
				end
			end
			
			I = reshape(I, size(obj));
			
		end % cumInt()

	end % methods (Access = public)
	
	methods (Access = protected)
		function v = evaluateFunction(obj, varargin)
			v = obj.valueContinuous(varargin{:});
		end
	end
	
	methods (Static)%, Access = protected)
		function [o] = zero(varargin)
			% ZERO creates an array containing only zeros with the same
			% dimensions as input ndgrids. This is needed for, function
			% handles that return a constant value and should be able to be
			% called on a ndgrid.
			if nargin >= 1
				o = zeros(size(varargin{1}));
			elseif nargin == 0
				o = 0;
			end
		end
	end
	
end

