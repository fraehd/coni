classdef Settings < handle
	%SETTINGS object to save some settings for the quantity class
	%   

	properties (Access = public)
		plot = struct(...
			'dock', false, ...
			'showTitle', true, ...
			'currentFigure', false, ...
			'useAxisLabel', false, ...
			'useLegend', false);
		progressBar = struct('show', true);
	end
	
	methods (Access = private)
		function obj = Settings()
		end
	end

	methods (Static)
		function obj = instance()
			persistent uniqueObj
			if isempty(uniqueObj)
				obj = quantity.Settings();
				uniqueObj = obj;
			else
				obj = uniqueObj;
			end
		end
	end
	
end

