function [texString] = variables2tex(optionsStruc,varargin)
% variables2tex converts the name value pairs of the input into a string that is latex
% compatbile. Example:
%	option = [];
%	texString = misc.variables2tex(option, 'A', ones(2), 'B', [1; 0])
% > textString = 'A = \left(\begin{array}{cc} 1 & 1\\ 1 & 1 \end{array}\right)\\ B = \left(\begin{array}{c} 1\\ 0 \end{array}\right)\\ '
%
% [texString] = variables2tex(options,...)
%    passes extra options to the function. options must be a structure that can have the
%    following fields:
%      filename (char):
%        saves the resulting string in the existing file specified by filename (contains path 
%        and filename).
%      rational (boolean):
%        if set to 1, numeric data is expressed as fractions, if possible. If set to 0
%        (default), decimal numbers are exported.
%      digits (integer):
%        the number of digits for decimal numbers (default: 4)
% 
% [texString] = variables2tex([],...)
%    creates the string in the matlab command window.
%

if (numel(varargin) == 1) && iscell(varargin{1})
	nameValuePairs = varargin{1};
else
	nameValuePairs = varargin;
end
fieldlist = ["filename", "digits", "rational"];
default = {[],4,0};
for fieldIdx =1:length(fieldlist)
	if isfield(optionsStruc,fieldlist(fieldIdx))
		options.(fieldlist(fieldIdx)) = optionsStruc.(fieldlist(fieldIdx));
	else
		options.(fieldlist(fieldIdx)) = default{fieldIdx};
	end
end

assert(mod(numel(nameValuePairs), 2) == 0, ...
	'input must be a name valuepair with an even number of elements');

texStringTmp = "\begin{align*} ";
for it = 1 : 2 : numel(nameValuePairs)
	tempValueString = misc.latexChar(nameValuePairs{it+1}, options);
	if it == (numel(nameValuePairs)-1)
		texStringTmp = texStringTmp + nameValuePairs{it} + " &= " + tempValueString + "\end{align*}";
	else
		texStringTmp = texStringTmp + nameValuePairs{it} + " &= " + tempValueString + "\\ ";
	end
end
while any(contains(texStringTmp, "\left(\begin{array}{cc"))
	texStringTmp = strrep(texStringTmp, '\left(\begin{array}{cc', '\left(\begin{array}{c');
end
texStringTmp = strrep(texStringTmp, '\left(\begin{array}{c}', '\begin{bmatrix*}');
texStringTmp = strrep(texStringTmp, '\end{array}\right)', '\end{bmatrix*}');

texStringTmp = strrep(texStringTmp, '\', '\\');
texStringTmp = strrep(texStringTmp, '\\\\', '\\\\ \n');
texStringTmp = strrep(texStringTmp, '\\begin{align*}', '\\begin{align*}\n');
texStringTmp = strrep(texStringTmp, '\\end{align*}', '\n\\end{align*}');
texString = sprintf(texStringTmp);

if ~isempty(options.filename)
	filename = optionsStruc.filename;
	% find file ending:
	pointIdx = find(filename=='.',1,'last');
	pathWithoutEnding  = filename(1:pointIdx-1);
	ending = filename(pointIdx:end);
	% savety abort for while loop
	maxNum = 25;
	if isfile(filename)
		l=1;
		while l<maxNum
			newName = [pathWithoutEnding '(' num2str(l) ')' ending];
			if isfile(newName)
				l=l+1;
			else
				break
			end
			if l==maxNum
				error([num2str(maxNum) ' Files already existing. Did not save!']);
			end
		end
		answer1 = 'Yes';
		answer2 = ['No, rename to ...(' num2str(l) ')'];
		answer3 = 'Cancel';
		question = sprintf(['File\n' filename '\n already exists. Would you like to overwrite the file?']);
		answer = questdlg(question,...
			'File already exists',...
			answer1,...
			answer2,...
			answer3,...
			answer3);
		if strcmp(answer,answer1)
			[fID, errmsg] = fopen(filename,'w');
			if fID == -1 % could not save
				error(sprintf(['Could not save: \n ' errmsg])); %#ok<*SPERR>
			end
			fprintf(fID,texStringTmp);
			fclose(fID);
		elseif strcmp(answer,answer2)
			[fID, errmsg] = fopen(newName,'w');
			if fID == -1 % could not save
				error(sprintf(['Could not save: \n ' errmsg]));
			end
			fprintf(fID,texStringTmp);
			fclose(fID);
		else % cancel
			% do nothing.
		end	
	else % simply save
		[fID, errmsg] = fopen(filename,'w');
		if fID == -1 % could not save
			error(sprintf(['Could not save: \n ' errmsg]));
		end
		fprintf(fID,texStringTmp);
		fclose(fID);
	end
end
end

