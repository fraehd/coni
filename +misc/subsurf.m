function subsurf(data, varargin)
%SUBSURF creates a figure with surf-subplots. It is assumed that the last
%two dimensions of data are the spatial ones. The default grids are
%equidistand on [0, 1] and the LineStyle is none. They can be changed with
%name-value-pair input. Further arguments for surf() can be passed via
%varargin. Further, zLim can be set with name-value-input.
%%
% Example:
%		data = reshape(magic(200), [100, 100, 2, 2]);
%		misc.subsurf(data)

%% input management
myParser = misc.Parser();
myParser.addParameter('myFigure', []);
myParser.addParameter('grid1', []);
myParser.addParameter('grid2', []);
myParser.addParameter('xLabel', 'z_1');
myParser.addParameter('yLabel', 'z_2');
myParser.addParameter('xDim', 1);
myParser.addParameter('yDim', 2);
myParser.addParameter('showGrid', true);
myParser.addParameter('zLabel', '');
myParser.addParameter('myTitle', '$$[~~]$$');
myParser.addParameter('LineStyle', 'none');
myParser.addParameter('zLim', [-inf, inf]);
myParser.addParameter('compareWith', []);
myParser.parse(varargin{:});
misc.struct2ws(myParser.Results);

if xDim == yDim
	error('x and y dimension must be different!')
end
remainingInput = misc.struct2namevaluepair(myParser.Unmatched);

%% permute data such that xDim and yDim are at the end.
restDim = 1:ndims(data);
restDim = restDim(restDim~=xDim & restDim~=yDim);
data = permute(data, [restDim, xDim, yDim]);

%% set grid and mesh
if any(strcmp(myParser.UsingDefaults, 'grid1'))
	grid1 = linspace(0, 1, size(data, ndims(data)-1));
end
if any(strcmp(myParser.UsingDefaults, 'grid2'))
	grid2 = linspace(0, 1, size(data, ndims(data)));
end
[z1Grid, z2Grid] = ndgrid(grid1, grid2);

%% Compare?
doCompare = ~isempty(myParser.Results.compareWith);
if doCompare
	dataReference = permute(myParser.Results.compareWith, [restDim, xDim, yDim]);
	assert(isequal(size(dataReference), size(data)), ...
		"size(data)="+num2str(size(data))+"~=size(compareWith)="+num2str(size(dataReference)));
end

%% plot
if isempty(myFigure)
	figure();
end
if ismatrix(data)
	surf(z1Grid, z2Grid, data, ...
		'LineStyle', LineStyle, ...
		'FaceAlpha', 1 / (1 + doCompare), ...
		remainingInput{:});
	if doCompare
		hold on; surf(z1Grid, z2Grid, dataReference, 'LineStyle', '--', 'FaceAlpha', 0);
	end
	zlim(zLim);
	xlabel("$$" + xLabel + "$$", "Interpreter", "latex");
	ylabel("$$" + yLabel + "$$", "Interpreter", "latex");
	zlabel("$$" + zLabel + "$$", "Interpreter", "latex");
	if iscell(myTitle)
		title([myTitle{:}], 'Interpreter','latex');
	else
		title(myTitle, 'Interpreter','latex');
	end
	a = gca();
	a.TickLabelInterpreter = 'latex';
	if ~showGrid
		grid off;
	end
	
elseif ndims(data) == 3
	for it = 1 : size(data, 1)
		subplot(size(data, 1), 1, it, 'parent', myFigure);
		surf(z1Grid, z2Grid, squeeze(data(it, :, :)), ...
			'LineStyle', LineStyle, ...
			'FaceAlpha', 1 / (1 + doCompare), ...
			remainingInput{:});
		if doCompare
			hold on; surf(z1Grid, z2Grid, squeeze(dataReference(it, :, :)), 'LineStyle', '--', 'FaceAlpha', 0);
		end
		zlim(zLim);
		xlabel("$$" + xLabel + "$$", "Interpreter", "latex");
		ylabel("$$" + yLabel + "$$", "Interpreter", "latex");
		zlabel("$$" + zLabel + "$$", "Interpreter", "latex");
		if iscell(myTitle)
			title([myTitle{1}, '$$', '_{', num2str(it), '}', '$$', myTitle{2:end}], 'Interpreter','latex');
		else
			title([myTitle, '$$_{', num2str(it), '}$$'], 'Interpreter','latex');
		end
		a = gca();
		a.TickLabelInterpreter = 'latex';
		if ~showGrid
			grid off;
		end
	end
	
elseif ndims(data) == 4
	subPlotIdx = 1;
	for it = 1 : size(data, 1)
		for jt = 1 : size(data, 2)
			subplot(size(data, 1), size(data, 2), subPlotIdx, 'parent', myFigure);
			surf(z1Grid, z2Grid, squeeze(data(it, jt, :, :)), ...
				'LineStyle', LineStyle, ...
				'FaceAlpha', 1 / (1 + doCompare), ...
				remainingInput{:});
			if doCompare
				hold on; surf(z1Grid, z2Grid, squeeze(dataReference(it, jt, :, :)), 'LineStyle', '--', 'FaceAlpha', 0);
			end
			zlim(zLim);
			xlabel("$$" + xLabel + "$$", "Interpreter", "latex");
			ylabel("$$" + yLabel + "$$", "Interpreter", "latex");
			zlabel("$$" + zLabel + "$$", "Interpreter", "latex");
			if size(data, 1) == 1 % only one row -> ignore index it
				if iscell(myTitle)
					title([myTitle{1}, '$$', '_{', num2str(jt), '}', '$$', myTitle{2:end}], 'Interpreter','latex');
				else
					title([myTitle, '$$_{', num2str(jt), '}$$'], 'Interpreter','latex');
				end
			else
				if iscell(myTitle)
					title([myTitle{1}, '$$', '_{', num2str(it), ',', num2str(jt), '}', '$$', myTitle{2:end}], 'Interpreter','latex');
				else
					title([myTitle, '$$_{', num2str(it), ',', num2str(jt), '}$$'], 'Interpreter','latex');
				end
			end
			a = gca();
			a.TickLabelInterpreter = 'latex';
			subPlotIdx = subPlotIdx +1;
			if ~showGrid
				grid off;
			end
		end
	end
end
end % plot()