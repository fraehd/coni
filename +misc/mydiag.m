function out_vec = mydiag( array,idx_vec1, idx_vec2 )
% MYDIAG evaluate array with pairwise indices
%
% Description:
% out_vec = mydiag(array,idx_vec1,idx_vec2) does nothing else than
% diag(array(idx_vec1,idx_vec2)) but uses linear indexing and thus is
% a lot faster.
%
%    
% Moreover, array may be a multidimensional array and is evaluated at the pairs given by
% (idx_vec1,idx_vec2) in the first, resp. second dimension.
%
% Generally, if array(idx_vec1,idx_vec2) is called, than all
% combinations of indices given by the idx vectors are evaluated.
% mydiag evaluates the indices only pairwise.
%
% Inputs:
%    array       Array of interest
%    idx_vec1    First index
%    idx_vec2    Second index
% Outputs:
%    out_vec     Contains all values of array where the first two
%                indices correspond to idx_vec1 and idx_vec2
%
% Example:
% -------------------------------------------------------------------------
% A(:,:,1) = [1  2  3;...
%             4  5  6;...
%             7  8  9];
% A(:,:,2) = [10 11 12;...
%             13 14 15;...
%             16 17 18];
% misc.mydiag(A,1,2)
% >> ans = [2 11]
% -------------------------------------------------------------------------

% created on 2017 by Simon Kerschbaum 

if nargin <2
	idx_vec1 = 1:size(array,1);
	idx_vec2 = 1:size(array,2);
end

if iscell(array)
	for cellid = 1:length(array)
		if ismatrix(array{cellid})
			idx = sub2ind(size(array{cellid}),idx_vec1,idx_vec2);
			out_vec{cellid} = array{cellid}(idx).';
		else
			sizArray = size(array{cellid});
			arrayResh = reshape(array{cellid},sizArray(1)*sizArray(2),[]); % collapse the first dimensions and all the last dimensions
			idx = sub2ind([sizArray(1) sizArray(2)],idx_vec1,idx_vec2);
			out_vecResh = arrayResh(idx,:);
			out_vec{cellid} = reshape(out_vecResh,[length(idx_vec1) sizArray(3:end)]);
		end
	end			
else
	if ismatrix(array)
		idx = sub2ind(size(array),idx_vec1,idx_vec2);
		out_vec = array(idx).';
	else
		sizArray = size(array);
		arrayResh = reshape(array,sizArray(1)*sizArray(2),[]); % collapse the first dimensions and all the last dimensions
		idx = sub2ind([sizArray(1) sizArray(2)],idx_vec1,idx_vec2);
		out_vecResh = arrayResh(idx,:);
		out_vec = reshape(out_vecResh,[length(idx_vec1) sizArray(3:end)]);
	end
end


