function fc = get_fourier_coefficients(signal, t, T, k_vec)
% GET_FOURIER_COEFFICIENTS calcualtes the fourier coefficients for every k
% specified in k_vec of a periodic signal called signal which is periodic
% over the time spam t.
% fc_k = 1/T * int_0^T [ signal(t) * exp(-1i * k * t * omega) ] dt
% with
% omega = 2*pi / T
% It is assumed, that the first dimension of the matrix signal represents
% the time domain fitting to t.
%	
% Inputs:
% MATRIX			   signal : signal for which the fourier coefficients
%   							are calculated
% VECTOR					t : time span of periodic signal (T not
%								included)
% SCALAR					T : period time signal(k*T) = signal(0), k in Z
% VECTOR				k_vec : indices of fourier coefficients
%   
% OUTPUT PARAMETERS:
% MATRIX				   fc : fourier coefficients
%
% required subprogramms:
%   -
%
% global variables:
%   - 
%
% history:
%	-	created on 15.05.2018 by Jakob Gabriel

%% Input check
if size(signal, 1) ~= numel(t)
	error('signal does not fit to time vector');
end

%% Init variables
omega = 2*pi / T;
signal_size = size(signal);
fc_size = size(signal);
fc_size(1) = numel(k_vec); 

%% Reshape signal for easy indexing
signal_reshaped = reshape(signal, [numel(t), prod(signal_size(2:end))]);
fc_unshaped = zeros(numel(k_vec), size(signal_reshaped, 2));

%% Extend t and signal to consider last time step
t_ext = zeros(1, numel(t)+1); t_ext(end) = T; t_ext(1:end-1) = t;
signal_reshaped_ext = zeros(numel(t)+1, prod(signal_size(2:end)));
signal_reshaped_ext(end,:) = signal_reshaped(1,:);
signal_reshaped_ext(1:end-1,:) = signal_reshaped;


%% Calculate coefficients by evaluation of integral term
for k_idx=1:numel(k_vec)
	fc_unshaped(k_idx, :) = 1/T * (numeric.trapz_fast(t_ext, ...
		(diag(exp(-1i*k_vec(k_idx)*omega*t_ext))*signal_reshaped_ext).'));
end

fc = reshape(fc_unshaped, fc_size);

end

