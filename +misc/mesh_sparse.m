function  mesh_sparse( x,y,Z,num_x,num_y,res_x,res_y,varargin )
% MESH_SPARSE plot a mesh with only a few lines.
%
% mesh_sparse( x,y,Z,num_x,num_y,res_x,res_y)
% Plots a mesh with num_x resp. num_y lines in x resp. y direction. On
% contrast to the usage of mesh with a sparse grid, each line is plottet
% with full (higher) resolution provided by the data X,Y,Z (or res_x).
% to include in latex, use
% 	\pgfplotsset{
%	surf/.append style={shader = faceted,faceted color=white, fill=white},
%	every axis plot post/.append style={shader=faceted}}
% Attention: the grid created by pgfplots is NOT opaque. This is due to the fact that
% pgfplots cannot handle z buffering for multiple plots. If this is unwanted, one needs to
% use a normal mesh or surf plot. To do so, the funtion mesh_sparse can also be used for
% reampling only.

% mesh_sparse( x,y,Z,num_x,num_y,res_x,res_y,'grid',0,...)
% suppresses drawing the grid lines in full resolution and simply draws the mesh with the
% resolution given by res_x/res_y.
% mesh_sparse( x,y,Z,num_x,num_y,res_x,res_y,'color',1,...)
% draws a colored mesh instead of the black/white version.
%
% Inputs:
%     x           Discrete grid points in x direction
%     y           Discrete grid points in y direction
%     Z           Matrix of values in z direction with Z = Z(x,y)
%     num_x       Number of gridpoints in x direction to be ploted
%     num_y       Number of gridpoints in y direction to be ploted
%     res_x       Resolution in x direction
%     res_y       Resolution in y direction
%     varargin    Additional opptions (see above)
%
% Example:
% -------------------------------------------------------------------------
% Z = peaks(250);
% x = linspace(0,5,250);
% y = linspace(0,5,250);
% misc.mesh_sparse(x,y,Z,30,30);
% grid on
% -------------------------------------------------------------------------
%

% TODO: DOKU, WIE Z aufgebaut ist, au�erdem �ber Verwendung von
% res_x,res_y!
% created on 17.01.2017 by Simon Kerschbaum

% ACHTUNG: kann mit
% mesh(tPlot,zPlot,obj.xQuantity(i).on({tPlot,zPlot},{'t','z'}).','meshStyle','row')
% hold on
% mesh(zPlot,tPlot,obj.xQuantity(i).on({zPlot,tPlot},{'t','z'}).','meshStyle','column',...
% 	'faceColor','none');
% wesentlich einfacher, sauberer und effizienter implementiert werden!

arglist = {'color','grid','plotPars'};
numfix=7;
% syntax can be improved! use matlab input parser
count=0;
if nargin>numfix
	% if varargin was passed as a cell array:%
	% varargin may also contain an empty array!
	while ~isempty(varargin) && isa(varargin{1},'cell') && count<10 % abort for savety
		varargin = varargin{1};
		count = count+1;
	end
end
for arg = 1:length(arglist)
	if(nargin > numfix) && ~isempty(varargin)
		for index = 1:2:length(varargin)
			if nargin==index, break, end
			switch lower(varargin{index})                       %#ok<ALIGN>
			case arglist{arg}
				v = genvarname(arglist{arg});
				eval([v ' =  varargin{index+1};']);
			end
		end
	end
end

if ~exist('color','var')
	color=0;
end
if ~exist('grid','var')
	grid=1;
end
if ~exist('plotPars','var')
	plotPars = {};
end

if ~exist('res_x','var')
	res_x = 0;
end
if ~exist('res_y','var')
	res_y = 0;
end

xmin = min((x));
xmax = max((x));
ymin = min((y));
ymax = max((y));

% if res_x ~= 0
% 	[~, x_res] = resample_time(Z,x,res_x);
% end
% if res_y ~= 0
% 	[~, y_res] = resample_time(Z.',y,res_y);
% end
if res_x ~= 0
	x_res = linspace(xmin,xmax,res_x);
else, x_res = x; 
end
if res_y ~= 0
	y_res = linspace(ymin,ymax,res_y);
else, y_res = y; 
end
[X,Y] = meshgrid(x,y);
[X_RES,Y_RES] = meshgrid(x_res,y_res);
Z_RES = interp2(X,Y,Z,X_RES,Y_RES);
	


xinc = (xmax-xmin)/(num_x-1);
yinc = (ymax-ymin)/(num_y-1);
hold on
% undurchsichtiges oder farbiges surf
if color==0
	surf(X_RES,Y_RES,Z_RES,'edgecolor','none','facecolor','white',plotPars{:})
else
	surf(X_RES,Y_RES,Z_RES,'edgecolor','none',plotPars{:})
	shading interp
end
if grid
	for x_idx = 1:num_x
		zline = interp2(X,Y,Z,(x_idx-1)*xinc+xmin,y_res);
		plot3(repmat((x_idx-1)*xinc+xmin,length(y_res),1),y_res(:),zline(:),'k',plotPars{:});
	end

	for y_idx = 1:num_y
		zline = interp2(X,Y,Z,x_res,(y_idx-1)*yinc+ymin);
		plot3(x_res(:),repmat((y_idx-1)*yinc+ymin,length(x_res),1),zline(:),'k',plotPars{:});
	end
end
hold off
axis auto
view(3);

end

