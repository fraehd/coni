function [ V,J,l ] = jordan_complete(A)
% JORDAN_COMPLETE Jordan canonical form with length of jordan blocks
%
% Description:
% JORDAN_COMPLETE(A) computes the Jordan Canonical/Normal Form of the 
% matrix A. The matrix must be known exactly, so its elements must be 
% integers or ratios of small integers.  Any errors in the input matrix 
% may completely change its JCF.
%
% [V,J] = JORDAN_COMPLETE(A) also computes the similarity transformation,
% V, so that V\A*V = J.  The columns of V are the generalized 
% eigenvectors.
%
% [V,J,l] = JORDAN_COMPLETE(A) also gives the lengths of the jordan
% blocks. l is a vector containing the lengths. So length(l) is the
% number of jordan blocks.
%
% Inputs:
%     A       Matrix of interest
% Outputs:
%     V       Similarity matrix of A
%     J       Jordan normal form of A
%     l       Lengths of the jordan blocks
%
% Example:
% -------------------------------------------------------------------------
% A = [ 6  -2   6   1   1;...
%       1  -1   2   1  -2;...
%      -2   0  -1   0  -1;...
%      -1   0  -2   2  -1;...
%      -4   4  -6  -2   3]
% jordan_complete(A)
% >> ans = [1    0    0    0    0
%           0   2-i   1    0    0
%           0    0   2-i   0    0
%           0    0    0   2+i   1
%           0    0    0    0   2+i]
% -------------------------------------------------------------------------
%
%   See also jordan

% created on 13.03.2018 by Simon Kerschbaum

if ~isempty(A)
	[V,J] = jordan(A);
else
	V = [];
	J = [];
end

l=[];
if nargout >2
	% calculate lengths of jordan blocks.
	% complicated algorithm: not necessary!
	% Length of the jordan blocks can be read from jordan form!
% 	L = eig(A,'vector');
	n=size(A,1);
	blocknum=1;
	it=1;
	while it<=n % loop through eigenvalues
		k=1; % start with first generalized eigenvector
		while any((A-J(it,it)*eye(size(A)))^(k-1)*V(:,it+k-1)) ...
				&& J(it,it) == J(it+k-1,it+k-1)
			k=k+1;
			% && (it+k-2 > 1) && 
			% increase k as long as the equation is nonzero and the
			% generalized eigenvector still belongs to the considered
			% eigenvalue
			if it+k-1>n || (J(it+k-2, it+k-1) == 0)
				% Check if end of J-matrix is reached OR if a new jordan block starts.
				break
			end
		end
		l(blocknum) = k-1; %#ok<AGROW>
		% consider next eigenvalue
		it=it+k-1;
		% all the eigenvalues of last jordan block are already done.
		% which belongs to next jordan block
		blocknum=blocknum+1;
	end
end
end

