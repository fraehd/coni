function [d, sizes] = getDimensions(args, names, dim)
% #TODO@ferdinand: write a documentation
arguments
	args;
	names string;
	dim = 1;
end
sizes = zeros(numel(names), 1);
for k = 1:length(names)
	if isfield(args, names(k))
		
		% add plus 1 to dimension if the object is a polynomialOperator:
		tmpDim = dim + isa( args.(names(k)), 'signals.PolynomialOperator');
		
		sizes(k) = size(args.(names(k)), tmpDim);
	end
end

d = max(sizes);

end