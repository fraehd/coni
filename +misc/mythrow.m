function str = mythrow( errmsg )
% MYTHROW get errormessage from errmsg caught by try catch
%
% Inputs:
%     errmsg      MException object containing error details
% Outputs:
%     str         String containing errormessage
%

% created in 2017 by Simon Kerschbaum

if ~isempty(errmsg.stack)
	str = (['Achtung: Fehler "' errmsg.message...
				'" geworfen! In ' errmsg.stack(1,1).name...
				', line ' num2str(errmsg.stack(1,1).line)]);
else
	str = (['Fehler "' errmsg.message...
				'" geworfen!']);
end

end

