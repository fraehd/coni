function [ series ] = pylor( fn, N, var )
% PYLOR Computes the taylor series for the function fn upto the order N
%
% Description:
% [ series ] = pylor( fn, N ) calls the python function taylor.py to
% compute the taylor series of the function fn, upto the order N. The
% symbolic variable of fn has to be s or z.
% The python function can handle more functions than the internal matlab
% taylor series approximation. Thus this wrapper was created to use the
% python function.
%
% Inputs:
%     fn          Function to be approximated
%     N           Oder of approximation
% Outputs:
%     series      Taylorseries
%   
% Example:
% -------------------------------------------------------------------------
%   
% -------------------------------------------------------------------------

    syms z s

     if nargin == 2
         var = sym('s');
     end
%%
    % Symbolische Funktion als String darstellen
    pyfun = char(fn);
        % Die Ausdrücke s^(1/2) und (-s)^(1/2) durch die sqrt()
        % Funktion darstellen, da Python die andere
        % Schreibweise nicht annimmt
%         pyfun    = strrep(pyfun, 's^(1/2)', 'sqrt(s)');     
%         pyfun    = strrep(pyfun, '(-s)^(1/2)', 'sqrt(-s)');
        pyfun    = strrep(pyfun, '^', '**');
        pyfun    = strrep(pyfun, '1i', 'I');
        pyfun = ['sympify( ''' pyfun ''' )'];
        
    % Pythonfunktion mit gewünschter Anzahl der Reihenterme
    % aufrufen:
    [~, pyresult] = system(['python ./+misc/taylor.py ' ...
                                num2str(N) ' ' '"' pyfun '"' ' ' '"' char(var) '"']);
                            
                            %%
    series = eval(['[ ' pyresult(2:end-1) ' ]']);
end

