function varlist = analyseMemory(var)
% analyseMemory analyse the memory of all variables  contained in the variable var

if isobject(var) || isstruct(var)
	if isobject(var)
		names=properties(var);
	else
		names=fieldnames(var);
	end
	varlist = cell(1,length(names));
	sizeArray = zeros(1,length(names));
	for i=1:length(names)
		sizeArray(i) = misc.memOf(var.(names{i}));
	end
	[sizeSorted, sortVec] = sort(sizeArray);
	namesSorted = names(sortVec);
	for i=1:length(names)
		varlist{i} = [namesSorted{i} ': ' expressInBytes(sizeSorted(i))];
	end
else
	name = who;
	r = whos(name{1});
	varlist = [name expressInBytes(r.bytes)];
end
end



function res = expressInBytes(var)
	if var > 1e9
		res = [num2str(round(var/1e8)/10) 'GB'];
	elseif var > 1e6
		res = [num2str(round(var/1e5)/10) 'MB'];
	elseif var > 1e3
		res = [num2str(round(var/1e2)/10) 'KB'];
	else
		res = [num2str(var) 'B'];
	end
end
