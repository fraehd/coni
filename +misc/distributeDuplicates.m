function arrOut = distributeDuplicates(arrIn,dim,minDiff)
% distributeDuplicates remove duplicate entries in an array by shifting them to a
% neighbor
%
% Description:
% arrOut = distributeDuplicates(arrIn) 
% searches for duplicate values among the first dimension of arrIn. If a 
% duplicate value is found, both elements are linearly distributed (inter-
% polated) between their neighbors
%
% arrOut = distributeDuplicates(arrIn,dim) 
% works over dimension dim
%
% arrOut = distributeDuplicates(arrIn,dim,minDiff) 
% treats values which are closer than minDiff as duplicates.
%
% If there is a duplicate at the border, the border value is not shifted.
%
% Inputs:
%     arrIn       Array to be manipulated; must be sorted in dim
%     dim         Dimension in arrIn to be manipulated (1 -> rows, 2 ->
%                 columns ...)
%     minDiff     Maximal difference between values to interpret them as 
%                 duplicates
% Outputs:
%     arrOut      Array with distributed entries
%
% Example:
% -------------------------------------------------------------------------
% A = [1 3 3;...
%      3 6 9;...
%      7 7 9];
% distributeDuplicates(A,2)
% >> ans = [1 2 3
%           3 6 9
%           7 8 9]
% -------------------------------------------------------------------------
%

% created on 04.10.2018 by Simon Kerschbaum


% Input checks
sizIn = size(arrIn);
if nargin<2
	dim=1;
end
if sizIn(dim)<3
	warning('Cannot remove duplicates of a vector with length<3!')
	arrOut = arrIn;
	return
end
if nargin<3
	minDiff = eps;
end
% Permute desired dimension to first
sizPerm = sizIn([dim 1:dim-1 dim+1:length(sizIn)]);
% And collapse all further dimensions
arrInResh = reshape(permute(arrIn,[dim 1:dim-1 dim+1:length(sizIn)]),sizIn(dim),[]);
numColsResh = size(arrInResh,2);

% this could be replaced by an ordering and restoring of the original permutation, but would
% take a lot of computation time!
if ~issorted(arrInResh)
	error('The array arrIn must be sorted in ascending oder!')
end

% calculate differences to detect duplicates
diffIn = diff(arrInResh); % get the differences along dimension dim.
diffSmall = diffIn <= minDiff; % find elements with diff<=minDiff
diffSmallInner = [zeros(1,numColsResh);diffSmall(2:end-1,:);zeros(1,numColsResh)]; % Take only the ones not at the border
flagsInner = diff(diffSmallInner); % The Diff signalizes the starts and ends of the sequences
pos1 = find([zeros(1,numColsResh);flagsInner==1;zeros(1,numColsResh)],1); % linear index of first begin of a sequences
pos2 = find([zeros(1,numColsResh);flagsInner==-1;zeros(1,numColsResh)],1); % lienar indices of first end of a sequences
valueBefore = arrInResh(pos1-1);
valueAfter = arrInResh(pos2+1); 
lengthSequence = pos2-pos1+1;  % This is the length of the sequence

% Now all the duplicates which are not at the borders are removed.
count=0;
while(any(diffSmallInner(:))) % recursively remove all duplicates
	count=count+1;
	% Get the distribution that the duplicates are replaced with
	linSp = linspace(valueBefore,valueAfter,lengthSequence+2);
	arrInResh(pos1:pos2) = linSp(2:end-1); % replace values
	% Now recalculate duplicates:
	diffIn = diff(arrInResh); % get the differences along dimension dim.
	diffSmall = diffIn <= minDiff;
	diffSmallInner = [zeros(1,numColsResh);diffSmall(2:end-1,:);zeros(1,numColsResh)];
	flagsInner = diff(diffSmallInner);
	pos1 = find([zeros(1,numColsResh);flagsInner==1;zeros(1,numColsResh)],1); % linear index of first begin of a sequences
	pos2 = find([zeros(1,numColsResh);flagsInner==-1;zeros(1,numColsResh)],1); % lienar indices of first end of a sequences
	valueBefore = arrInResh(pos1-1);
	valueAfter = arrInResh(pos2+1); 
	lengthSequence = pos2-pos1+1;  
	if count>prod(sizIn)
		warning(['Abort after ' num2str(sizIn(dim)) ' tries! (There was an error!)'])
		break
	end
end
% If there is a duplicate exactly at the border, that means only the first and the second
% element are equal (not the third), this has to be taken into account sepearately:
% (If the the first (last) 3 elements were equal, both the 2nd and the third element have been
% shifted w.r.t. the 1st and 4th element, so that there will be no duplicate anymore!
diffSmallTop = [diffSmall(1,:);zeros(size(arrInResh,1)-1,numColsResh)];
diffSmallBot = [zeros(size(arrInResh,1)-1,numColsResh);diffSmall(end,:)];
arrInResh(logical([zeros(1,numColsResh);diffSmallTop(1:end-1,:)])) =...
	1/2*(arrInResh(logical([zeros(1,numColsResh);diffSmallTop(1:end-1,:)]))...
	+ arrInResh(logical([zeros(2,numColsResh);diffSmallTop(1:end-2,:)])));
arrInResh(logical([diffSmallBot(2:end,:);zeros(1,numColsResh)])) =...
	1/2*(arrInResh(logical([diffSmallBot(2:end,:);zeros(1,numColsResh)]))...
	+ arrInResh(logical([diffSmallBot(3:end,:);zeros(2,numColsResh)])));

% restore original dimensions of array
arrOut = permute(reshape(arrInResh,sizPerm),[2:dim 1 dim+1:length(sizIn)]);


end

