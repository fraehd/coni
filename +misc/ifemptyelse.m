function [ out ] = ifemptyelse( arg, ifvalue, elsevalue )
% IFEMPTYELSE short form for empty check
%
% Description:
% out = ifemtpyelse(arg, ifvalue) 
% Checks the *arg* if it is empty. If it is, the *ifvalue* is returned, 
% if not the *arg* is returned
%
% out = ifemtpyelse(arg, ifvalue, elsevalue)
% Checks the *arg* if it is empty. If true, the *ifvalue* is returned, if 
% not the *elsevalue* is returned
%
% Inputs:
%     arg         Variable to be checked
%     ifvalue     Value if arg is emty
%     elsevalue   Value if arg is not empty
%   
% Outputs:
%     out         Return value; see above
%
% Example:
% -------------------------------------------------------------------------
% x = [];
% misc.ifemptyelse(x==1, 2, 3)
% >> ans = 2
% -------------------------------------------------------------------------
if nargin == 2
    elsevalue = arg;
end

if isempty(arg)
    out = ifvalue;
else
    out = elsevalue;
end
end

