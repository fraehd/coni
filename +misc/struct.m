function myStruct = struct(varargin)
% misc.struct is a better constructor then the constructor of the struct class, since
% it can also deal with fieldnames that contain dots, by creating hierarchical
% structs
%%
% Example:
%		myStruct = misc.struct('topLevel.lowerLevel.data', 42, 'asdf', 123)
if (numel(varargin) == 1) && isstruct(varargin{1})
	myStruct = varargin{1};
else
	myStruct = struct();
	for it = 1 : 2 : numel(varargin)
		if ischar(varargin{it}) || isstring(varargin{it})
			myStruct = misc.setfield(myStruct, varargin{it}, varargin{it+1});
		else
			error('input must be name-value-pair or a struct');
		end
	end
end
end % misc.struct()