function [ out ] = defaultInput( var, value )
% DEFAULTINPUT set default values for input args
%   
% Description:
% [ out ] = defaultInput( var, value )
% Checks if the variable 'var' exists. If it is existent nothing happens.
% If not, it is initialized with the value 'value'.
% ATTENTION: var and value have to be strings! 
% remark: it is not possible to assign a string value as default value,
% this only works for numeric types.
%
% Inputs:
% var     
%
% Examples:
% -------------------------------------------------------------------------
% f=@(t)( misc.defaultInput('t', 'diag([1 2])'));
% f()
% -------------------------------------------------------------------------
if ~evalin('caller', ['exist(''' var ''', ''var'')'])
    
    if nargout == 0    % Check if user wants an output
        evalin('caller', [ var ' = ' value]);
    else
        out = value;
    end
else
    out = evalin('caller', var);
end

end

