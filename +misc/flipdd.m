function mat_out = flipdd( mat_in )
% FLIPDD Flip matrix along antidiagonal
%
% Description:
% mat_out = flipdd(mat_in) flips the matrix mat_in along the
% antidiagonal
%
% Inputs:
%     mat_in      Input matrix
% Outputs:
%     mat_out     Output matrix
%
% Example:
% -------------------------------------------------------------------------
% M = [1 2 3;...
%      4 5 6;...
%      7 8 9];
% flipdd(M)
% >> ans = [9 6 3
%           4 5 6
%           7 8 9]
% -------------------------------------------------------------------------

mat_out = reshape(mat_in(end:-1:1),size(mat_in)).'; % gespiegelt an Nebendiagonale

end

