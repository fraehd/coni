function K = place(A, B, p)
%MISC.PLACE calculates the state feedback K such that all eigenvalues of (A-B*K) will result in
%p, i.e. eig(A-B*K) == p. Diffrent to the built-in place, this method supports eigenvalues of
%multiplicity greater than rank(B);
%
% Example
%	K = misc.place(magic(2), [0; 1], [-1, -1])
%	eig(magic(2) - [0; 1] * K)

arguments
	A (:, :) double;
	B (:, :) double;
	p (:, 1) double = zeros([size(A, 1), 1]);
end

try 
	% try matlab built-in place first, as it is more accurate
	K = place(A, B, p);
catch
	K = zeros(size(B, 2), size(A, 1));
	Atmp = A;
	for it = 1 : size(A, 1)
		[~, D, W] = eig(Atmp, "nobalance", "vector");
		idx = chooseIdx(D, p);
		if ~isempty(idx)
			Ktmp = (W(:, idx)' * B) \ (D(idx) - p(it)) * W(:, idx)';
			K = K + Ktmp;
			Atmp = A - B*K;
		else
			break;
		end
	end
	K = real(K);
end % try...catch
if max(abs(sort(eig(A - B*K)) - sort(p))) > 1e-3
	warning("misc.place is faulty: desired eigenvalues = " + sprintf('%.3f, ', p) ...
		+ "resulting eigenvalues = " + sprintf('%.3f, ', eig(A-B*K)) );
end
end % misc.place

function idx = chooseIdx(D, p)
	% helper function
	toBeMoved = setdiff(round(D, 6), round(p, 6), "stable");
	if isempty(toBeMoved)
		idx = [];
	else
		idx = find(round(D, 6) == toBeMoved(1), 1);
	end
end % chooseIdx