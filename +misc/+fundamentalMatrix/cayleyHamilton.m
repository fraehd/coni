function [ Phi, Psi ] = cayleyHamilton(A_in, B_in)
%Computation of the transition matrix
%   [ Phi, Psi ] = Fundamentalmatrix(A, B) computes the
%   transition matrix Phi(z, zeta, s) for the initial value problem
%       dz Phi(z,zeta,s) = A(z,s) Phi(z,zeta,s);    Phi(z,z,s) = I
%   The system operator matrix A(z,s) has to be of polynomial form 
%       A(z,s) = sum A_k(z) s^k, k = 0, 1, ..., d 
%   and has to be passed as in the form 
%       A = cat( 3, A0, A1, ..., Ad ), dim(A) = (n, n, d) 
%   with n as the dimension of the operator A, the degree of the polynomial 
%   operator d and the coefficient matrices A0, A1, ..., Ad. 
%   B(z) is the spatial distributed input matrix with
%       dim(B) = (n, n)
%   
%   Outputs:
%   * Phi is the symbolic computation of the Peano-Baker series as symbolic
%   matrix with the symbolic variables z, zeta, s. 
%   * Psi is the inhomogeneous part of the general solution
%
%   example:
%   --------------------------
%   syms z
%   A0 = [0 -0.5; 0.5, 0];
%   A1 = [1 0; 0 3];
%   A  = cat(3, A0, A1);
%   Fundamentalmatrix(A)
%   --------------------------   
%   syms z
%   A0 = [0 -0.5; 0.5, 0];
%   A1 = [1 0; 0 3];
%   A  = cat(3, A0, A1);
%   B  = [z 0; 0 z];
%   Fundamentalmatrix(A, B)
%   --------------------------   

%% Create dynamic matrix
syms s z
n = length( A_in( :,1,1 ) );
d = length( A_in( 1,1,: ) );

A = zeros( size( A_in(:,:,1) ) ) + 0*z;
for i=1:1:d
    A = A + A_in(:,:,i)*s^(i-1);
end

%% Calculate Phi

% Calculate eigenvalues
eigenvalues = eig( A );

% prepare vector, which is necessary to calculate coefficients alpha
Y = exp( int( eigenvalues, z, 0, z) );

% prepare matrix, which is necessary to calculate coefficients alpha
% X = [ 
%       1   l1      l1^2        ... l1^(n-1);
%       1   l2      l2^2        ... l2^(n-1);
%                               ...
%       1   l_(n-1) l_(n-1)^2   ... l_(n-1)^(n-1)
%      ]
% with l1 as eigenvalue 1 and l2 as eigenvalue 2
X = zeros(n,n) + 0*z;
for i=1:1:n
    for j=1:1:n
        X( i, j ) = eigenvalues( i )^(j-1);
    end
end

% Check for multiple eigenvalues
Determinante = det(X);
if( Determinante == 0 )
    msg = 'No multiple eigenvalues of the dynamic matrix A are allowed.';
    error(msg)
end

% Calculate alpha
alpha = X^-1*Y;

% Calculate Phi
Phi=alpha(1)*eye(n);
for i=2:1:n
    Phi = Phi + alpha(i)*A^(i-1);
end

%% Calculate Psi
% !!! ACHTUNG !!!
% Die Berechnung von Psi wurde NICHT verifiziert!
% Daniel Burk
if( exist( "B_in", 'var' ) )
    syms tau
    temp(z) = Phi + 0*z;
    B(z) = B_in + 0*z;
    Psi = int( temp(z-tau)*B(tau), tau, 0, z );
else
    Psi = 0;
end

end




