function [F] = peanobakerseriesNum(A, varargin)
%PEANOBAKERSERIES_POLY Computation of the transition matrix based on the
%Peano-Baker series
%   [Phi, F, Psi, P, z, s] = peanobakerseries(A, varargin) computes the
%   transition matrix Phi(z, zeta, s) for the initial value problem
%       dz Phi(z,zeta,s) = A(z,s) Phi(z,zeta,s);    Phi(z,z,s) = I
%   using the Peano-Baker series dependent on a symbolic parameter s. The
%   system operator matrix A(z,s) has to be of polynomial form A(z,s) = sum
%   A_k(z) s^k, k = 0, 1, ..., d and has to be passed as in
%   the form A = cat( 3, A0, A1, ..., Ad ), dim(A) = (n, n, d) with n as
%   the dimension of the operator A, the degree of the polynomial 
%   operator d and the coefficient matrices A0, A1, ..., Ad. 
%   Further options can be passed by name-value-pairs. This are:
%       N: The number of series terms. Default is 10.
%       zeta: second argument of the fundamental matrix, can be either
%       symbolic or numeric. Default is the symbolic variable zeta.
%       s: the symbolic variable for the Laplace variable.
%       B: Input matrix for the intial-value problem of the operater
%       matrix:
%           dz x(z,s) = A(z,s) x(z,s) + B(z) u(s)
%           If this arguement is passed, the inhomogeneous part Psi(z,zeta,s)
%           of the general solution 
%               x(z,s) = Phi(z,zeta,s) x(zeta,s) + Psi(z, zeta, s) u(s)
%               Psi(z,zeta,s) = int_zeta_z Phi(z, xi) B(xi) d xi
%           This is only computed if an arguement for B is passed.
%   
%   Outputs:
%   * Phi is the symbolic computation of the Peano-Baker series as symbolic
%   matrix with the symbolic variables z, zeta, s. 
%   * F contains the series coefficient matrices of the transition matrix
%   as polynomial in s
%       Phi(z,zeta,s) = sum F_k(z,zeta) s^k , k = 0, ..., N
%   as symbolic matrix of dim(F) = (n, n, N), F = cat(3, F0, F1,
%   ..., F_iterations). Dependent on the symbolic variables z and maybe
%   zeta. The output z and s are the symbolic variables used in this
%   function.
%   * Psi is the inhomogeneous part of the general solution
%   * P: are the series coefficients of the inhomogeneous part. Same form
%   as F with a matrix of dim(P) = (n, p, N), whereas p is the number of
%   inputs.
%   * z is the symbolic spatial variable used for the computation
%   * zeta is the second symbolic spatial variable.
%   * s is the symbolic Laplace variable used for the computation
%
%   example:
%   --------------------------
%   syms z
%   A0 = [0 1 + 0.5*sin(z); 1, 0];
%   A1 = [0 1; 0 0];
%   A  = cat(3, A0, A1);
%   misc.peanobakerseries_poly(A)
%   --------------------------   


% replace zeta always by symbolic variable because this is required to
% compute the Psi matrix.
Nz = size(A, 1);
n = size(A, 2);
d = size(A, 4) - 1; % As order of a polynom is zero based

%% input parser:
ip = misc.Parser();
ip.addRequired('A');
ip.addOptional('N', 10);
ip.addOptional('zeta', 0);
ip.addOptional('B', []);
ip.addOptional('z', linspace(0, 1, Nz)');
ip.addOptional('s', sym('s'));
ip.parse(A, varargin{:});
misc.struct2ws(ip.Results)

A_ = zeros(Nz, n, n, d, N);

%% Compute helper matrices A_ for i == 1
for k = 0 : d
    A_(:, :, :, k + 1, 1) = cumtrapz(z, A(:, :, :, k + 1));
end
% Compute helper matrices A_ for i > 1
for i = 2: N
        
    for k = 0 : i * d
        tmp = zeros(Nz, n, n);
        for j = 0 : k
           if k-j + 1 <= (d+1) && j + 1 <= (i-1) * d + 1
                Atmp = quantity.Discrete.MTIMES(A(:, :, :, k-j + 1), A_(:, :, :, j+1, i-1));
                tmp = tmp + Atmp;
           end
        end
       A_(:, :, :, k+1, i) =  cumtrapz(z, tmp);
    end    
end

F = zeros(Nz, n, n, N);
F(:, :, :, 1) = repmat(shiftdim(eye(n), -1), [Nz, 1, 1]); % this is J_0

for i = 1 : N 
    for j = 1 : N
        if i <= d
            F(:, :, :, i) = F(:, :, :, i) + A_(:, :, :, i, j); 
        end
    end
end

% %% compute inhomogeneous transition matrix
% if ~isempty(B)
%     if misc.issym(B)
%        assert(isequal(symvar(B), z)); 
%     end
%     xi = sym('xi');
%     b = size(B, 2);
%     % computation of inhomogeneous part
%     Psi = sym( zeros(n, b) );
%     P   = sym( zeros(n, b, N) );
% 
%     for k = 1 : N
%        P(:,:,k) = int(subs(F(:,:,k), zeta, xi) * subs(B, z, xi), xi, zeta, z);
%        Psi = Psi + P(:,:,k) * s^(k-1);
%     end    
%     P = subs(P, zeta, zetainput);
%     Psi = subs(Psi, zeta, zetainput);
% else
%     P = [];
%     Psi = [];
% end
% 
% Phi = subs(Phi, zeta, zetainput);
% F = subs(F, zeta, zetainput);

end






