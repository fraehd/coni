function [Phi, Psi] = rudolphWoittennekNumeric(A, N, B, z)
%FUNDAMENTALMATRIX_WOITTENNEK 

% A has to be a discrete system operator of the form:
%   A[x] = A0 x + A1 dt x + A2 dt^2 x + ...
% So, A is a matrix f size (Nz, n, n, d). At this, Nz is the number of
% spatial discretization points, n is the number of the states and d is the
% degree of the operator.

Nz = [A(1).domain.n];
n = size(A, 2);
m = size(B, 2);
d = size(A, 3); % As order of a polynom is zero based
% z = A(1).grid{1};

%% initialization of the ProgressBar
counter = N - 1;
pbar = misc.ProgressBar(...
    'name', 'Trajectory Planning (Woittennek)', ...
    'terminalValue', counter, ...
    'steps', counter, ...
    'initialStart', true);

%% computation of transition matrix as power series
% The fundamental matrix is considered in the laplace space as the state
% transition matrix of the ODE with complex variable s:
%   dz w(z,s) = A(z, s) w(z,s) * B(z) v(s),
% Hence, fundamental matrix is a solution of the ODE:
%   dz Phi(z,zeta) = A(Z) Phi(z,zeta)
%   Phi(zeta,zeta) = I
%
%   Psi(z,xi,s) = int_zeta^z Phi(z, xi, s) B(xi) d_xi
%
% At this, A(z, s) can be described by
%   A(z,s) = A_0(z) + A_1(z) s + A_2(z) s^2 + ... + A_d(z) s^d
%
% With this, the recursion formula 
%   Phi_k(z, zeta) = int_zeta^z Phi_0(z, xi) * sum_i^d A_i(xi) *
%   Phi_{k-i}(z, xi) d_xi
%   Psi_k(z, zeta) = int_zeta^z Phi_0(z, xi) * sum_i^d A_i(xi) *
%   Psi_{k-i}(z, xi) + B_k(xi) d_xi


F0 = A.fundamentalMatrix0(z);

Phi(:,:,1) = F0.subs('zeta', 0);
B = B.subs('z', 'zeta');
A = A.subs('z', 'zeta');

Psi(:,:,1) = F0.volterra(B);

for kN = 2 : N
   pbar.raise();
    
   dA = min(d, kN);
   M = quantity.Discrete.zeros([n n], z, 'gridName', 'zeta');
   N = quantity.Discrete.zeros([n m], z, 'gridName', 'zeta');
   
   % compute the temporal matrices:
   for i = 2 : dA
       %M = A_i(z) * F_{kN-i+1}(z, xi); xi = 0;
      M = M + A(:, :, i) * Phi(:, :, kN-i+1).subs('z', 'zeta');
      N = N + A(:, :, i) * Psi(:, :, kN-i+1).subs('z', 'zeta');
   end
   if size(B, 3) >= kN
      N = N + B(:, :, kN); 
   end
   
   % compute the integration of the fundamental matrix with the temporal
   % values:
   %    Phi_k(z) = int_0_z Phi_0(z, xi) * M(xi) d_xi   
   Phi(:, :, kN) = F0.volterra( M );
   Psi(:, :, kN) = F0.volterra( N );
end
%%


    pbar.stop();

end

function mTimesPointWise(A, Vi)

z = Vi(1).grid{Vi.index('z')};
c = zeros(size(A,1), size(Vi, 2), length(z));


for zIdx = 1:numel(z)
	
end
end



    function P = intPhiM(Phi0, M)
        p = size(M,2);
        q = size(M,3);

        if isempty(M)
            P = quantity.Discrete.zeros([n m], z);
        else
            I = zeros(Nz, p, q);
            for kNz = 1:Nz
               % 
               tmp2 = quantity.Discrete.MTIMES(shiftdim(Phi0(kNz, 1:kNz, :, :),1), M( 1:kNz, :,:));
               tmp3 = reshape(tmp2, kNz, p*q);
               tmpI = numeric.trapz_fast_nDim(z(1:kNz), tmp3);
               I(kNz, :, :) = reshape(tmpI, p,q );     
            end

            P = quantity.Discrete(quantity.Discrete.value2cell(I, Nz, [p, q]), 'grid', z);
        end
    end

%TODO change this function to have the possibility to compute it to a
%certain accuracy
function Phi = internPeanoBakerSeries(A, z, xi, z0, N)
% PEANOBAKERSERIES_SYM Symbolic computation of (partial) peano baker series
% Detailed description for The Peano-Baker Series can be found in:
% The Peano-Baker Series, MICHAEL BAAKE AND ULRIKE SCHLAEGEL, 2012
%
% [ Phi ] = panobakerseries_sym( A, z0, N )
% 
% * A:   System-Operator Matrix \in C[s]^(nxn); with n is the number of
%      states. 
% * z0:  Lower limit of Integration
% * N:  Number of iterations
%
% * Phi: First result of the state transition matrix.(n x n x 1)
    I = sym( zeros(size(A,1), size(A,2), N ));   
    I(:,:,1) = int( subs(A, z, xi), xi, z0, z);
    Phi = eye( size(A,1), size(A,2) ) + I(:, :, 1);

    for k = 1 : N - 1
        I(:, :, k + 1) = int( subs( ( A * I(:, :, k) ), z, xi ), xi, z0, z );
        Phi = Phi + I(:, :, k + 1 );
    end
end

