function Psi = odeSolver(A, zdisc)
%fundamentalMatrix Calculate fundamental matrix
%  Psi = fundamentalMatrix(A,zdisc) calculates the fundamental matrix Psi(z,zeta) which is the
%  solution of the problem
%    dz Psi(z,zeta) = A(z) Psi(z,zeta)
%       Psi(z,z)    = I
%  and uses the spatial discretisation zdisc.
%
%  INPUT PARAMETERS:  
%    FUNCTION OR ARRAY       A     : square matrix function describing the right side of the ODE.
%                                    Can either be passed as a function that MUST
%                                    return an n times n matrix at every point, or as the
%                                    discretized array A(z_idx,i,j). In that case, A must be
%                                    discretized at the points of zdisc.
%    VECTOR                  zdisc : Discretisation vector of the spatial variable. The
%                                    fundamental matrix will be evaluated at the points
%                                    described by zdisc in both directions.
%
%  OUTPUT PARAMETERS:
%    ARRAY                  Psi    : Fundamental matrix, discretized in the form 
%                                    Psi(z_idx,zeta_idx,i,j)

% history:
% - created on 03.08.2018 by Simon Kerschbaum

% preliminaries
RelTol = 1e-12;
AbsTol = 1e-12;
ndisc=length(zdisc);
if ~isa(A,'function_handle')
	if size(A,1)~= ndisc
		error('If passed as discretized function, then size(A,1) must equal length(zdisc)!')
	end
	opts = odeset('RelTol', RelTol,'AbsTol', AbsTol,'vectorized','on');
	% if discretized version was passed, the solver can use the vectorized option!
	if size(A,2)~= size(A,3)
		error('Size(A,2) must equal size(A,3), such that A is quadratic!')
	end
	n = size(A,2);
	ADiag(1:ndisc,1:n^2,1:n^2)=0; % preallocate
	for z_idx=1:ndisc
		for i=1:n %number of columns in Psi
			% create diagonal matrix of As
			ADiag(z_idx,(i-1)*n+1:i*n,(i-1)*n+1:i*n) = A(z_idx,:,:);
		end
	end
	if n>1
		AInterp = griddedInterpolant({zdisc,1:n^2,1:n^2},ADiag);
		A_for_vec = @(z) squeeze(AInterp({z(:),1:n^2,1:n^2}));
	else
		AInterp = griddedInterpolant({zdiscRegEq},ADiag);
		A_for_vec = @(z) squeeze(AInterp({z(:)}));
	end		
else
	opts = odeset('RelTol', RelTol, 'AbsTol', AbsTol,'vectorized','off');
	ADisc = misc.eval_pointwise(A,zdisc(1));
	n = size(ADisc,2);
	A_for_vec = @(z) misc.fDiag(A,n,z);
end
Psi_mu_vec(1:ndisc,1:ndisc,1:n^2) = nan; % preallocate

		
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% calculate fundamental matrix:
% as the solution of 
%     dz Psi_mu(z,zeta) = A(z) Psi_mu(z,zeta) z in (0,1),zeta
%     in (0,z)
%     Psi_mu(zeta,zeta) = I
Psi_0 = eye(n);
Psi_vec_0 = Psi_0(:); % vectorized
% Problem: matlab-solver only for vectors.
% --> Matrix must be vectorized
% --> A blockdiagonalized

pbar = misc.ProgressBar(...
    'name', 'fundamental matrix_0: ', ...
    'steps', ndisc - 2, ...
    'initialStart', true);

f=@(z,Psi_vec) A_for_vec(z)*Psi_vec;
tmp = cell(ndisc-2);

q = parallel.pool.DataQueue;
afterEach(q, @pbar.raise);

parfor zeta_idx =1:ndisc-2
    
    send(q, []);
    
	% starting value for z
	[~, Psi_z_zeta_vec]=ode15s(f, zdisc(zeta_idx:ndisc), Psi_vec_0,opts);
    tmp{zeta_idx} = Psi_z_zeta_vec;
end

for zeta_idx =1:ndisc-2
   	Psi_mu_vec(zeta_idx:ndisc,zeta_idx,:) = tmp{zeta_idx}; 
end

% compute the last step extra because if the ode15s solver is computed with
% only two arguments, it thinks this is a distance and returns the values
% on a new grid.
zeta_idx = ndisc-1;
pbar.raise(zeta_idx);
[~, Psi_z_zeta_vec]=ode15s(f, zdisc(zeta_idx:ndisc), Psi_vec_0,opts);
Psi_mu_vec(zeta_idx:ndisc,zeta_idx,:) = [Psi_z_zeta_vec(1, :); Psi_z_zeta_vec(end, :)];

Psi_mu_vec(ndisc,ndisc,:) = Psi_vec_0; 
% put vector in matrix form:
Psi = reshape(Psi_mu_vec,[ndisc ndisc n n]);
% fundamental matrix ready.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		
pbar.stop();
end



















