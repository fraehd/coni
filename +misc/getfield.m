function myValue = getfield(myStruct, myFieldName)
% misc.getfield returns data of a struct specified by its field name.
% In contrast to the matlab built-in function getfield(), this method misc.getfield
% can also read fields of field by specifying myFieldName with dots as seperator, for
% instance myFieldName = "topLevel.lowerLevel.data", see example.
% Additionally, misc.getfield can handle arrays of data, i.e. myFieldName string
% arrays, by concatenating the result.
%%
% Example 1:
%		myStruct = misc.setfield(struct(), "topLevel.lowerLevel.data", 42);
%		result = misc.getfield(myStruct, "topLevel.lowerLevel.data")
% Example 2:
%		myStruct = struct("a", 1, "b", 2);
%		result = misc.getfield(myStruct, ["a", "b"])

arguments
	myStruct struct;
	myFieldName string;
end

if isscalar(myFieldName)
	myFieldNameSplitted = split(myFieldName, '.');
	myValue = myStruct;
	for it = 1 : numel(myFieldNameSplitted)
		if isfield(myValue, myFieldNameSplitted{it}) 
			myValue = myValue.(myFieldNameSplitted{it});
		else
			myValue = [];
			return;
		end
	end
else % array case
	myValue = [];
	for it = 1 : numel(myFieldName)
		myValue = [myValue, misc.getfield(myStruct, myFieldName(it))];
	end
end % if isscalar(myFieldName)
end % getfield()