function result = ifThenElse(condition, trueResult, falseResult)
  % IFTHENELSE Inline if-then-else function
  %
  % Abbreviation for the if then else construct in matlab.
 
  narginchk(3, 3);
  
  if condition
    result = trueResult;
  else
    result = falseResult;
  end