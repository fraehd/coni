function result = iseye(matrix)
%ISSYM checks if the matrix is a quadratic identity matrix.

result = isdiag(matrix) && ismatrix(matrix) ...	% diagonal matrix
	&& (size(matrix, 1) == size(matrix, 2)) ...			% quadratic
	&& all(diag(matrix) == ones(size(matrix, 1), 1));	% diag(matrix) == 1


end

