function cur_idx = animate_state(x,t,n,zdisc,animation_time,fps,linear,exportPath,yLim)
% Draw animation of states. 
%
% Description:
% >> cur_idx = ANIMATE_STATE(x,t,n,zdisc,animation_time,fps)
% Creates a figure with subplots for each of the n states and animates 
% the solution of x(t,z). x and t are the matrix and vector containing 
% the solution from the ode-solver and zdisc is the spatial grid. 
% animation_time is the approximate time taken for the animation.
% fps is the number of pictures displayed per second. If matlab is not
% fast enough, the animation time will be larger than animation_time.
%  
% >> cur_idx = animate_state(x,t,n,zdisc,animation_time,fps,linear)
% uses a linear time-scale
%
% >> animate_state(x,t,n,zdisc,animation_time,fps,linear,exportPath)
% exports every frame to 'exportPath', to be used in LaTex.
%
% >> animate_state(x,t,n,zdisc,animation_time,fps,linear,exportPath,yLim)
% sets the y-range to yLim.  Set exportPath = [] if no export is desired
%
% Inputs:
% x              Matrix containing the solution of x(t,z) in discretized
%                 version
% t              Vector containing discrete time values, corresponding
%                 to the points in x
% n              System order of the DPS
% zdisc          Spatial grid of approximation
% animation_time Approximate time taken for the animation, if matlab is
%                 not fast enaugh, animation time will be larger than 
%                 animation_time
% fps            Number of pictures displayed per second
% linear         Boolean indicating wheter the time grid should be inter-
%                 polated into equidistant timesteps 
% exportPath     Path to destination folder
% yLim           Cell Array containing the y-limits for each distributed
%                 state in the corresponding row (i.e. its a n-dimensional
%                 cell-array where each cell contains a yLim vector)
%
% Outputs:
% cur_idx        -||-
%
%
% Example:
% -------------------------------------------------------------------------
% [t,x] = ode15s(@(t,x) A*x,[0 5],x0);
% n = 2; T = 4; FPS = 10;
% yLim = {[0 2],[0 4]};
% zdisc = 0:(1/ndisc-1):1;
%   
% animate_state(x,t,n,zdisc,T,FPS,1,'C:\MatlabWorkspace\Test',yLim);
% -------------------------------------------------------------------------

% created on 13.10.2017 by Simon Kerschbaum

% TODO: Bessere Doku/Hilfe!
% Ideen: - Geschwindigkeitsregler einbauen (fuer linear und nichtlinear)
%        - Geschwindigkeitsanzeige einabuen (fuer nichtlinear)
%        - warten besser als mit While-Schleife!
import misc.*

if ~exist('linear','var')
	linear=1;
end
if ~exist('exportPath','var')
	exportPath=0;
end

if linear==0 && animation_time*fps>length(t)
	warning('There are not enough frames to achieve the simulation time!')
end

wdth = 1500/n; % width of a figure in pixels
hght = 400; % height of a figure
offx = 10; % hor. offset of position
offy = 50; % vert. offset of position

ndisc=length(zdisc); % number of spatial points
% get data and create figures
fig = figure('Name','Animation x','Position',...
			         [offx, offy, n*wdth hght]);
for i=1:n
	y{i} = x(:,(i-1)*ndisc+1:i*ndisc,:); % store each state separately
	subfig{i} = subplot(1,n,i);
	title(['x_' num2str(i)]);
	ax{i} = gca;
	if size(x,3)==1
		axis([0 1 min(min(y{i})) max(max(y{i}))])
	else
		axis([0 1 0 1 min(y{i}(:)) max(y{i}(:))])
		hold on
	end
end
% create controls
pause_btn = uicontrol('units','normalized','position',[0.39 .01 0.1 0.1],...
				'String','Start','UserData',0,...
				'Style', 'pushbutton','callback',@pausefun,'parent',fig);
reset_btn = uicontrol('units','normalized','position',[0.51 .01 0.1 0.1],...
				'String','Reset','UserData',0,...
				'Style', 'pushbutton','callback',@resetfun,'parent',fig);
endless_box = uicontrol('units','normalized','position',[0.63 .01 0.1 0.1],...
				'String','Endless','UserData',0,...
				'Style', 'checkbox','parent',fig);

anim_res = fps; %fps
% interpolate over linear time-grid
y_anim{n} = [];  %preallocate
if linear % if  wanted
	t_anim = linspace(t(1),t(end),animation_time*anim_res);
	for i=1:n
		if size(x,3) == 1
			y_anim{i} = interp1(t,y{i},t_anim);
		else
			y_anim{i} = misc.translate_to_grid(y{i},t_anim);
		end
	end
else % or not
	if ~ismatrix(x)
		error('Only linear animation is implemented for ndims(x)>2!')
	end
	for i=1:n
		[y_anim{i}, t_anim] = resample_time(y{i}',t,animation_time*fps);
		y_anim{i} = y_anim{i}.';
	end
end

% create animation lines
for i=1:n
	if ismatrix(x)
		h{i} = animatedline(ax{i}); % in correct figure
		addpoints(h{i},zdisc,y_anim{i}(1,:)); % set initial values
		title(ax{i},['t_{sim}=' num2str(t_anim(1)) ',  t=' num2str(0)]) % and title
		if exist('yLim','var')
			set(ax{i},'ylim',yLim{i});
		end
	else
		h{i} = surf(ax{i},linspace(0,1,size(y_anim{i},2)),linspace(0,1,size(y_anim{i},3)),squeeze(y_anim{i}(1,:,:)).');
		% surfSparse doesnt work beacuse zData must be provided completele differen! See later!
% 		h{i} = misc.surfSparse(ax{i},linspace(0,1,size(y_anim{i},3)),linspace(0,1,size(y_anim{i},2)),squeeze(y_anim{i}(1,:,:)));
		title(ax{i},['t_{sim}=' num2str(t_anim(1)) ',  t=' num2str(0)]) % and title
		if exist('yLim','var')
			set(h{i},'zlim',yLim{i});
		end
	end
end

% store all information in a structure to link it to the pushbutton (to be
% available everywhere)
infostruc.pause_btn = pause_btn; %store the button handle itself!
infostruc.endless_box = endless_box;
infostruc.anim_res = anim_res;
infostruc.t_anim = t_anim;
infostruc.y_anim = y_anim;
infostruc.h = h;
infostruc.ax = ax;
infostruc.fig = fig;
infostruc.pause=1;
infostruc.n = n;
infostruc.zdisc = zdisc;
infostruc.pause_btn = pause_btn;
infostruc.elapsedAnimationTime = 0;
infostruc.cur_idx = 1;
infostruc.reset=0;
infostruc.exportPath = exportPath;
if exist('yLim','var')
	infostruc.yLim = yLim;
end
pause_btn.UserData=infostruc; 
reset_struc.pause_btn = pause_btn;
reset_btn.UserData = reset_struc; % store pause_btn handle in reset_button to 
% have the information available

% start animation
cur_idx =1;
[cur_idx, pause_btn.UserData.elapsedAnimationTime] = animation(cur_idx, infostruc); %start animation
pause_btn.UserData.cur_idx = cur_idx;

% nested functions: Pause/Resume
function pausefun(source,event)	
	if source.UserData.pause == 0 % if is currently not paused
		source.UserData.pause = 1;
		source.String = 'Resume';
	else % is is currently paused
		source.UserData.pause = 0;
		source.String = 'Pause';
		% resume/start animation at current position and update position
		[source.UserData.cur_idx, source.UserData.elapsedAnimationTime] = animation(source.UserData.cur_idx,source.UserData);
	end
end

% reset
function resetfun(source,event)	
	if source.UserData.pause_btn.UserData.pause == 0 % if still in animation
		source.UserData.pause_btn.UserData.reset = 1; % mark for animation,
	end % so it can reset when leaving the animation function
	source.UserData.pause_btn.UserData.pause = 1;
	source.UserData.pause_btn.String = 'Start';
	source.UserData.pause_btn.UserData.cur_idx = 1;
	
	% Reset animation:
	infostruc = source.UserData.pause_btn.UserData;
	for j=1:infostruc.n
		if ismatrix(x)
			clearpoints(infostruc.h{j});
			addpoints(infostruc.h{j},infostruc.zdisc,infostruc.y_anim{j}(1,:));
		else
			infostruc.h{j}.ZData = squeeze(infostruc.y_anim{j}(1,:,:)).';
		end
		title(infostruc.ax{j},['t_{sim}=' num2str(infostruc.t_anim(1)) ',  t=' num2str(infostruc.elapsedAnimationTime)])
	end
end

% actual animating function
function [end_index, tot_time] = animation(start_index,infostruc)
	% timers for current frame time and total time
	FrameTimer = tic;
	TotalTimer = tic;
	% loop through all time indices
	exit = 0;
	t_idx = start_index-1;
	while ~exit
		t_idx = t_idx+1;
		% and all states
		for j=1:infostruc.n
			if ismatrix(x)
				clearpoints(infostruc.h{j});
				addpoints(infostruc.h{j},infostruc.zdisc,infostruc.y_anim{j}(t_idx,:));
	% 			if ylim~=0
	% 				ylim(infostruc.yLim);
	% 			end
			else
				infostruc.h{j}.ZData = squeeze(infostruc.y_anim{j}(t_idx,:,:)).';
			end
			title(infostruc.ax{j},['t_{sim}=' num2str(infostruc.t_anim(t_idx)) ',  t=' num2str(toc(TotalTimer)+infostruc.elapsedAnimationTime)])
		end
		ElapsedFrameTime = toc(FrameTimer); % check timer
		while ElapsedFrameTime < (1/infostruc.anim_res) % wait (can't be aborted!)
			ElapsedFrameTime=toc(FrameTimer);
		end
		drawnow % wait-time is over
		
		if infostruc.exportPath~=0
			f = figure;
			for k=1:infostruc.n
				subplot(infostruc.n,1,k)
				if ismatrix(x)
					plot(infostruc.zdisc,infostruc.y_anim{k}(t_idx,:));
					if exist('yLim','var')
						ylim(infostruc.yLim{k});
					end
				else
					surf(linspace(0,1,size(infostruc.y_anim{k},2)),linspace(0,1,size(infostruc.y_anim{k},3)),squeeze(infostruc.y_anim{k}(t_idx,:,:)).');
				end
			end
			matlab2tikz('filename', [infostruc.exportPath '/frame_', num2str(t_idx)],...
				'externalData',true,'showInfo',false,'checkForUpdates',false);
			close(f);
% 			fprintf([infostruc.exportPath '/t_' num2str(t_idx) '.tsv'],'infostruc.t_anim(t_idx)');
			fid=fopen([infostruc.exportPath '/t_' num2str(t_idx) '.tsv'],'w');
% 			fprintf(fid, [ header1 ' ' header2 '\n']);
			fprintf(fid, '%.2f' , infostruc.t_anim(t_idx));
			fclose(fid);
			
		end 
		
		FrameTimer = tic; % reset timer
		if infostruc.pause_btn.UserData.pause == 1 %check if pause was clicked
			if infostruc.pause_btn.UserData.reset == 1
				% return values if pause was evoked by reset
				end_index = 1;
				infostruc.pause_btn.UserData.reset = 0;
			else % return values if pause was evoked by pause
				end_index = t_idx;
			end
			% update total elapsed time
			tot_time = toc(TotalTimer) + infostruc.elapsedAnimationTime;
			return
		end
		if t_idx == length(infostruc.t_anim)% last animation frame shown
			if ~get(infostruc.endless_box,'value')
				exit=1; % stop animation
			else
				t_idx = 0; % start over
			end
		end
	end
	% animation is completed, set return_index to 1, so animation starts
	% from beginning.
	end_index = 1;
	tot_time = toc(TotalTimer) + infostruc.elapsedAnimationTime;
	infostruc.pause_btn.String = 'Restart';
	infostruc.pause_btn.UserData.pause =1;
	% stop animation.
	end
end