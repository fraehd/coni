function wstruct=ws2struct()
% WS2STRUCT Save all workspace variable into a struct
%
%     This function allows to save all the variables from the 'caller'
%     workspace into a struct array Description: Sometimes you need to save
%     the variables from your base workspace, but using "save" function
%     will have them all stored individually so if you reload them into a
%     new workspace it could be a mess, and some variables could be
%     overwritten. With this function, you can save all of them into a
%     struct array, and so they'll be nicely packaged and ready to be saved
%     to a .mat file that, when reloaded, will be easy to identify. 
% Tags: save, struct, base, workspace, structure array
%
% Example:
% -------------------------------------------------------------------------
% a='LALALA';
% b=[1:12:258];
% c={'cell1', 'cell2', 'cell3'};
% d=768;
% e=true(3);
% theworkspace=misc.ws2struct()
% >> ans = a: 'LALALA'
%          b: [1 13 25 37 49 61 73 85 97 109 121 133 145 157 169 181 193 205 217 229 241 253]
%          c: {'cell1'  'cell2'  'cell3'}
%          d: 768
%          e: [3�3 logical]
% -------------------------------------------------------------------------

vars = evalin('caller', 'who');
for k = 1 : size( vars, 1 )
    var = evalin('caller', vars{k});
    wstruct.(vars{k})=var;
end