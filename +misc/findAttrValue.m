function cl_out = findAttrValue(obj,attrName,value)
% findAttrValue(obj,attrName,value) find properties with specific
% attributes.
%
% Description:
% properties = findAttrValue(obj,attrName) finds all properties
% in obj, where attrName is true.
%
% properties = findAttrValue(obj,attrName,varagin) finds all properties
% where attrName == value.
%
% obj can either be an object or a string representing a class name.
% attrName is a cell array containing the names of the properties.
%
% Inputs:
%     obj         Object or string representing the class name
%     attrName    Attribute of interest
%     value       Defines whether properties fullfil attrName == true or
%                   attrName == false
% Outputs:
%     cl_out      Cell array containing properties
%
% Example: 
% -------------------------------------------------------------------------
% props = findAttrValue('ppide_simulation','Dependent')
% returns a cell array containging the names of all Dependent properties
% of the class ppide_similation.
% -------------------------------------------------------------------------
%

% taken from matlab help page Get Information About Properties
% https://de.mathworks.com/help/matlab/matlab_oop/getting-information-about-properties.html
% but modified by Simon Kerschbaum, 2018.
   if ischar(obj)
      mc = meta.class.fromName(obj);
   elseif isobject(obj)
      mc = metaclass(obj);
   end
   ii = 0; numb_props = length(mc.PropertyList);
   cl_array = cell(1,numb_props);
   for  c = 1:numb_props
      mp = mc.PropertyList(c);
      if isempty (findprop(mp,attrName))
         error('Not a valid attribute name')
      end
      attrValue = mp.(attrName);
	  if nargin>2
		if  attrValue == value
			ii = ii + 1;
			cl_array(ii) = {mp.Name};
		end
	  elseif islogical(attrValue)
		  if attrValue
			  ii = ii + 1;
			  cl_array(ii) = {mp.Name};
		  end
	  end
   end
   cl_out = cl_array(1:ii);
end

