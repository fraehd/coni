classdef Gains < handle & matlab.mixin.Copyable
%Gains is class to enease implementation of multiple different gains for 
%signals, for instance control, disturbance, fault or other input gains.
%Only finite inputs are considered, see dps.Inputs for inputs for 
%distributed parameter systems. It is assumed, that the input signal is 
%multiplied with the gain from right side, i.e. gain * u(t).
%For every input there is an Gain-object (singular!), which contains the
%corresponding matrices and information for that input.
	properties (SetAccess = protected)
		gain (1, :) misc.Gain;		% this array contains the data of
		%							% each input
	end

	properties (Dependent = true)
		% data resulting from the input-property
		value (:,:);				% gain value
		
		% data about input
		inputType (:, 1) string;	% inputType of input signal, for instance 
		%							% "control", "disturbance", "fault" etc
		%							% Input inputTypes must be unique, this is
		%							% verified in the constructor and in
		%							% add().
		outputType (:, 1) string;	% type of output signal, for instance 
		%							% "measurement", "ode", ...
		lengthInput (:, 1) double;	% number of columns of gain value
		lengthOutput (:, 1) double;	% number of rows of gain value
		numTypes;					% number of different input-object that
		%							% this Inputs-object contains
		OutputName (:, 1) cell;		% enumerated input type of length lengthInput
		InputName (:, 1) cell;		% enumerated output type of length lengthOutput
	end
	
	methods
		function obj = Gains(varargin)
			%% Constructor
			if nargin > 0
				% all inputs of constructor in varargin are of Gain-inputType
				obj.add(varargin{:});
				assert(misc.isunique(obj.inputType), "gain inputTypes must be unique");
				obj.verifySizes();
			end % if nargin > 0
		end % Gain() Constructor
		
		%% Combining objects:
		% add further input(s) to Gains obj
		function obj = add(obj, varargin)
			for it = 1 : numel(varargin)
				myGain = varargin{it};
				if ~isempty(myGain)
					if(isa(myGain, "misc.Gain"))
						if any(strcmp(obj.inputType, myGain.inputType))
							thisInputIndex = find(strcmp(obj.inputType, myGain.inputType));
							obj.gain(thisInputIndex) = obj.gain(thisInputIndex) + myGain;
						else
							if isempty(obj) || isempty(obj.gain(1))
								obj.gain(1) = myGain;
							else
								obj.gain(end+1) = myGain;
							end
						end
					elseif(isa(myGain, "misc.Gains"))
						for jt = 1 : myGain.numTypes
							obj = obj.add(myGain.gain(jt));
						end
					else
						error("only misc.Gain or misc.Gains can be added to misc.Gains-object");
					end
				end
			end
			assert(misc.isunique(obj.inputType), "input inputTypes must be unique");
			obj.verifySizes();
		end % add()
		
		% exchange input(s) of Gains obj
		function obj = exchange(obj, newGain)
			obj.gain(obj.index(newGain.inputType)) = newGain;
		end % exchange()
		
		% add input or combine inputs with simple + notation
		function c = plus(a, b)
			if isempty(a)
				c = b;
			elseif isempty(b)
				c = a;
			elseif isa(a, "misc.Gains") && isa(b, "misc.Gain")
				c = copy(a);
				c = c.add(b);
			elseif isa(a, "misc.Gains") && isa(b, "misc.Gains")
				c = copy(a);
				for it = 1 : b.numTypes
					c.add(b.gain(it));
				end
			end
			c.verifySizes();
		end %plus()
		
		function newObj = mtimes(obj, a, inputType, outputType)
			% mtimes is the method for left-side multiplication of the constant double-array a.
			arguments
				obj misc.Gains;
				a double;
				inputType string = string([]);
				outputType string = string([]);
			end % arguments
			newObj = misc.Gains();
			if isempty(inputType) || isempty(outputType)
				for it = 1 : obj.numTypes
					newObj.add(mtimes(obj.gain(it), a));
				end
			else
				assert(numel(inputType) == obj.numTypes);
				assert(numel(outputType) == obj.numTypes);
				for it = 1 : obj.numTypes
					newObj.add(mtimes(obj.gain(it), a, inputType(it), outputType(it)));
				end
			end
		end % mtimes()
		
		function obj = remove(obj, inputType)
			% remove gains(s) of Gains obj
			arguments
				obj misc.Gains;
				inputType (:, 1) string;
			end
			for it = 1 : numel(inputType)
				if ~isempty(obj.index(inputType(it)))
					if (obj.numTypes == 1)
						obj.gain = misc.Gain();
					else
						obj.gain = obj.gain((1:1:obj.numTypes) ~= obj.index(inputType(it)));
					end
				end
			end
			obj.verifySizes();
		end % remove()
		
		function obj = removeOutput(obj, outputType)
			selectEmptyGains = false(size(obj.gain));
			for it = 1 : obj.numTypes
				obj.gain(it) = obj.gain(it).removeOutput(outputType);
				selectEmptyGains(it) = isempty(obj.gain(it).value);
			end
			if all(selectEmptyGains)
				obj = misc.Gains();
			else
				obj.gain = obj.gain(~selectEmptyGains);
			end
		end % removeOutput()
		
		function thisGain = gainOfOutput(obj, myOutputs)
			thisGain = misc.Gains();
			for it = 1 : obj.numTypes
				thisGain.add(obj.gain(it).gainOfOutput(myOutputs));
			end
		end % gainOfOutput()
		
		function out = dps.Outputs(obj)
			% convert to dps.Outputs-object
			out = dps.Outputs();
			for it = 1 : obj.numTypes
				out = out.add(dps.Outputs(obj.gain(it)));
			end % for it = 1 : numel(obj.outputType)
		end % dps.Output
		
		function gainSeries = series(gainHead, gainTail, inputGainHead, outputGainTail)
			% series implements the series interconnection of obj and gainTail. The new outputs are
			% those of gainHead, but the inputs are those of gainTail. The outputType outputGainTail
			% of gainTail will be connected to the input inputGainHead of gainHead.
			arguments
				gainHead misc.Gains;
				gainTail misc.Gains;
				inputGainHead string;
				outputGainTail string;
			end
			thisHead = gainHead.get(inputGainHead);
			thisTail = gainTail.gainOfOutput(outputGainTail);
			gainSeries = misc.Gains();
			for it = 1 : thisTail.numTypes
				tempTail = thisTail.gain(it);
				gainSeries.add(misc.Gain(tempTail.inputType, ...
					thisHead.value * tempTail.value, ...
					"outputType", thisHead.outputType, ...
					"lengthOutput", thisHead.lengthOutput));
			end
			otherInputsOfHead = copy(gainHead);
			otherInputsOfHead.remove(inputGainHead);
			gainSeries.add(otherInputsOfHead);
		end % series()
		
		function gainParallel = parallel(obj, varargin)
			gainParallel = copy(obj);
			for it = 1 : (nargin-1)
				gainParallel = gainParallel + varargin{it};
			end
		end % parallel()
		
		function newGains = blkdiag(a, b, varargin)
			% blkdiag combines two (or more) misc.Gain objects by combining them blockdiagonally.
			%                                     |A 0 .. 0|
			%     Y = blkdiag(A,B,...)  produces  |0 B .. 0|
			%                                     |0 0 ..  |
			% All misc.Gain objects must have same inputType and outputType.
			assert(isequal(a.inputType, b.inputType), "for blkdiag the inputTypes must be equal");
			assert(isequal(a.outputType, b.outputType), "for blkdiag the outputTypes must be equal");
			
			newGains = misc.Gains();
			for it = 1 : a.numTypes
				newGains = newGains.add(blkdiag(a.gain(it), b.gain(it)));
			end
			
			if nargin > 2
				newGains = blkdiag(newGains, varargin{:});
			end
		end % blkdiag()
		
		%% Methods for getting properties:
		% find gain by naming its inputType
		function idx = index(obj, inputType)
			idx = [];
			allTypes = obj.inputType;
			for it = 1 : numel(allTypes)
				if isequal(allTypes{it}, inputType)
					idx = it;
				end
			end
		end % index()
		
		% get gain of the inputType specified by the strings contained in varargin
		function thisGain = get(obj, varargin)
			if nargin == 2
				thisGain = obj.gain(obj.index(varargin{1}));
			else
				myType = varargin;
				thisGain = cell(numel(myType), 1);
				for it = 1 : numel(myType)
					thisGain{it} = obj.gain(obj.index(myType{it}));
				end
			end
		end % get()
		
		% create name-value pair of gains
		function nameValuePairs = nameValuePairs(obj)
			nameValuePairs = cell(1, 2*obj.numTypes);
			for it = 1 : obj.numTypes
				nameValuePairs{2*it-1} = [obj.gain(it).inputType, "Gain"];
				nameValuePairs{2*it} = obj.gain(it);
			end
		end % nameValuePairs()
		
		%% get meta data
		function numTypes = get.numTypes(obj)
			numTypes = numel(obj.gain);
		end %get.numTypes()
		
		function inputType = get.inputType(obj)
			inputType = vertcat(obj.gain(:).inputType);
		end %get.inputType()
		
		function outputType = get.outputType(obj)
			outputType = vertcat(obj.gain(:).outputType);
			outputType = unique(outputType, "stable");
		end % get.outputType()
		
		function OutputName = get.OutputName(obj)
			OutputName = {};
			for it = 1 : obj.numTypes
				OutputName = [OutputName(:); obj.gain(it).OutputName(:)];
			end
			OutputName = unique(OutputName, "stable");
		end % get.OutputName()
		
		function InputName = get.InputName(obj)
			InputName = {};
			for it = 1 : obj.numTypes
				InputName = [InputName(:); obj.gain(it).InputName(:)];
			end
			InputName = unique(InputName, "stable");
		end % get.InputName()
		
		function length = lengthOf(obj, inputType)
			length = obj.lengthInput(obj.index(inputType));
			if isempty(length)
				length = 0;
			end
		end % lengthOf()
		
		function result = isempty(obj)
			result = any(size(obj)==0) || isempty(obj.lengthInput) || (sum(obj.lengthInput) == 0);
		end %isempty()
		
		
		%% length-getter
		function lengthInput = get.lengthInput(obj)
			lengthInput = [obj.gain(:).lengthInput].';
		end % get.lengthInput()
		
		function lengthOutput = get.lengthOutput(obj)
			lengthOutput = zeros(1, numel(obj.outputType));
			for kt = 1 : numel(obj.outputType)
				for it = 1 : obj.numTypes
					thisLength = obj.gain(it).lengthOfOutput(obj.outputType{kt});
					if ~isempty(thisLength)
						lengthOutput(kt) = thisLength;
						break;
					end
				end
			end
		end % get.lengthOutput()
		
		%% value matrices getter
		function value = get.value(obj)
			% value = [gain(1).value, gain(2).value, ... ]
			valueCell = cell(numel(obj.outputType), obj.numTypes);
			if sum(obj.lengthOutput) > 0
				for it = 1 : numel(obj.outputType)
					for jt = 1 : obj.numTypes
						valueCell{it, jt} = obj.gain(jt).valueOfOutput(obj.outputType(it));
						if isempty(valueCell{it, jt})
							valueCell{it, jt} = zeros([obj.lengthOutput(it), obj.gain(jt).lengthInput]);
						end
					end
				end
			end
			value = cell2mat(valueCell);
		end % get.value()
		
		function value = valueOf(obj, gainType)
			% returns the name of the gain specified by gainType
			value = obj.gain(obj.index(gainType)).value;
		end % get.valueOf()
		
		function sumblkString = outputType2sumblk(obj, varargin)
			% returns the string [type(1) + "+" + type(2) + "+" + ...]
			% if additionally an output 'append' is given as name-value pair, the
			% string results in 
			% returns the string [type(1), append, '+', type(2), append, '+', ...]
			myParser = misc.Parser();
			myParser.addParameter("append", "", @(v) ischar(v) | isstring(v));
			myParser.parse(varargin{:});
			sumblkString = '';
			if numel(obj.outputType) > 0
				for it = 1 : (numel(obj.outputType)-1)
					sumblkString = [sumblkString, ...
						obj.outputType(it), myParser.Results.append, "+"];
				end
				sumblkString = [sumblkString, char(obj.outputType(end)), char(myParser.Results.append)];
			end
		end % inputType2sumblk()
		
		function sumblkString = inputType2sumblk(obj, varargin)
			% returns the string [type(1) + "+" + type(2) + "+" + ...]
			% if additionally an input "append" is given as name-value pair, the
			% string results in 
			% returns the string [type(1), append, "+", type(2), append, "+", ...]
			myParser = misc.Parser();
			myParser.addParameter("append", "", @(v) ischar(v) | isstring(v));
			myParser.parse(varargin{:});
			sumblkString = "";
			if obj.numTypes > 0
				for it = 1 : (obj.numTypes-1)
					sumblkString = [sumblkString, ...
						obj.inputType{it}, myParser.Results.append, "+"];
				end
				sumblkString = [sumblkString, obj.inputType{end}, myParser.Results.append];
			end
		end % inputType2sumblk()
		
		function mySs = gain2ss(obj, varargin)
			% returns a ss() representing the gain with the
			% properties InputName and OutputName set according to inputType and
			% outputType. With "appendInput" and "appendOutput" an extra string /
			% char-array can be attached at the end of the names.
			ssCell = cell(obj.numTypes, 1);			
			for it = 1 : obj.numTypes
				ssCell{it} = obj.gain(it).gain2ss(varargin{:});
				ssCell{it} = misc.ss.add2signalName(ssCell{it}, "OutputName", ...
					"front", obj.inputType(it) + ".");
			end
			mySums = obj.connectOutput();
			mySs = misc.ss.connect( ...
				obj.InputName, obj.OutputName, ...
				ssCell{:}, mySums{:});
		end % gain2ss()
		
		function mySums = connectOutput(obj)
			mySums = cell(numel(obj.outputType), 1);
			for it = 1 : numel(obj.outputType)
				individualOutputNames = '';
				for jt = 1 : obj.numTypes
					if any(strcmp(obj.gain(jt).outputType, obj.outputType{it}))
						if isempty(individualOutputNames)
							individualOutputNames = [obj.inputType{jt}, '.', obj.outputType{it}];
						else
							individualOutputNames = [individualOutputNames, ' + ', ...
								obj.inputType{jt}, '.', obj.outputType{it}];
						end
					end
				end
				mySums{it} = sumblk([obj.outputType{it}, ' = ', ...
					individualOutputNames], obj.lengthOutput(it));
			end
		end % connectOutput()
		
		function result = isequal(A, B, varargin)
			% isequal compares all parameters of A and B and varargin and returns
			% true if they are equal, and false if not.
			result = (A.numTypes == B.numTypes);
			for it = 1 : A.numTypes
				if result
					result = isequal(A.gain(it), B.gain(it));
				end
			end
			if result && (nargin > 2)
				result = result && B.isequal(varargin{:});
			end
		end % isequal()
		
		function obj = strrepOutputType(obj, oldText, newText)
			if ~isempty(obj)
				% replace strings in type
				for it = 1 : obj.numTypes
					obj.gain(it).strrepOutputType(oldText, newText);
				end % for it
			end
		end % strrepOutputType()
		
		function obj = extendOutputType(obj, position, newText)
			% extendOutputType adds a pre- or a postfix to all obj.outputType{:}
			for it = 1 : obj.numTypes
				obj.gain(it).extendOutputType(position, newText);
			end
		end % extendOutputType()
		
		function obj = strrepInputType(obj, oldText, newText)
			if ~isempty(obj)
				% replace strings in type
				for it = 1 : obj.numTypes
					obj.gain(it).strrepInputType(oldText, newText);
				end % for it
			end
		end % strrepInputType()
		
		function obj = extendInputType(obj, position, newText)
			% extendInputType adds a pre- or a postfix to all obj.inputType{:}
			for it = 1 : obj.numTypes
				obj.gain(it).extendInputType(position, newText);
			end
		end % extendInputType()
		
		%% set gain value
		function set.value(obj, valueTemp)
			for it = 1 : obj.numTypes
				obj.gain(it).value = valueTemp(:, sum(obj.lengthInput(1:it-1))+(1:obj.lengthInput(it)));
			end
		end % set.value()
		function set.outputType(obj, outputTypeNew)
			for it = 1 : obj.numTypes
				obj.gain(it).outputType = outputTypeNew;
			end
		end % set.value()
		
		%% test dimensions
		function verifySizes(obj)
			lengthAllInputs = sum(obj.lengthInput);
			assert(isequal(size(obj.value), [sum(obj.lengthOutput), lengthAllInputs]));
		end % testSizes
		
		
		function texString = print(obj, outputType, inputName, leftHandSide)
			% print Print equation to command window as latex compatible string-array
			
			arguments
				obj;
				outputType string = string([]);
				inputName (:, 1) string = obj.inputType;
				leftHandSide string = "none";
			end % arguments
			
			if isempty(outputType) || (strlength(outputType) == 0)
				texString = outputType;
				return;
			else 
				assert(numel(outputType) == 1, "only implemented for scalar outputType")
			end
			
			% put string together
			if isnumeric(leftHandSide)
				if isscalar(leftHandSide) && obj.lengthOutput > 1
					leftHandSide = leftHandSide * ones(obj.lengthOutput, 1);
				end
				leftHandSide = misc.latexChar(leftHandSide);
			end
			if strcmp(leftHandSide, "none")
				myString = "";
			else
				myString = leftHandSide + " &=";
			end
			plusNeeded = false;
			for it = 1 : numel(inputName)
				if any(strcmp(obj.get(inputName(it)).outputType, outputType))
					if ~plusNeeded
						myString = myString + obj.get(inputName(it)).print(outputType);
						plusNeeded = true;
					else
						myString = myString + " + " + obj.get(inputName(it)).print(outputType);
					end
				end
			end
			
			if nargout > 0
				texString = myString;
			else
				misc.printTex(myString);
			end
			
		end % print()
		
		function [texString, nameValuePairs] = printParameter(obj)
			% Create TeX-code for all non-zero parameter.
			nameValuePairs = {'D', obj.value};
			texString = misc.variables2tex([], nameValuePairs);
		end % printParameter
	end % methods
	
	methods (Access = protected)
		% Override copyElement method:
		function cpObj = copyElement(obj)
			% Make a shallow copy of all properties
			cpObj = copyElement@matlab.mixin.Copyable(obj);
			cpObj.gain = copy(obj.gain);
		end
	end % methods (Access = protected)
	
end

