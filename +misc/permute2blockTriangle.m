function [triangularMatrix, permutationMatrix, permutationIdx] = permute2blockTriangle(A)
% MISC.PERMUTE2BLOCKTRIANGLE permutes the matrix A, if possible, to a lower block triangular matrix.
% The implementation is based on using methods for digraphs, see matlab documentation for digraph.
%	
%	triangularMatrix = misc.permute2blockTriangle(A) calculates a permutation of A
%
%	[triangularMatrix, permutationMatrix] = misc.permute2blockTriangle(A) also returns the
%		permutation matrix such that
%			triangularMatrix = permutationMatrix * A * permutationMatrix.'
%
%	[triangularMatrix, permutationMatrix, permutationIdx] = misc.permute2blockTriangle(A) also 
%		returns the double array permutationIdx, which contains the indexes for the permutation of
%		columns of A.
%
% Note that inv(permutationMatrix) = permutationMatrix.' (which holds for all so-called
% permutation matrices).

% conncomp computes strongly connected components of a graph, i.e. irreducible components. Those
% will result in the block-diagonal elements of the triangularMatrix. Hence, conncomp is used to
% find which elements shall be grouped together..
myDiagraph = digraph(A);
[agentsBlockNumber, blockSize] = myDiagraph.conncomp();

% As agentsBlockNumber defines which agent belongs to which group, the following for loop defines
% how they will be sorted.
permutationIdx = zeros(size(A, 1), 1);
for blockIdx = numel(blockSize) : -1 : 1
	permutationIdx(sum(blockSize(numel(blockSize):-1:(blockIdx+1))) + (1:blockSize(blockIdx)), 1) ...
		= find(agentsBlockNumber - blockIdx == 0);
end

permutationMatrix = full(sparse(1:1:size(A, 1), permutationIdx, 1));
triangularMatrix = permutationMatrix * A * permutationMatrix.';

end % permute2blockTriangle()