function myChar = latexChar(data, option)
% misc.latexChar converts input-arrays into latex compatible char-arrays. data may be
% numeric, symbolic, quantity.Symbolic, quantity.Discrete or function_handle.
% Example:
% -------------------------------------------------------------------------
% option.rational = false;
% option.digits = 4;
% tex = misc.latexChar(ones(2), option)
% -------------------------------------------------------------------------
%	See also misc.variables2tex

arguments
	data;
	option = struct('rational', false, 'digits', 4);
end

if ischar(data) || (iscell(data) && ~isempty(data{1}) && ischar(data{1}))
	data = string(data);
end

if isstring(data)
	if isscalar(data)
		myChar = data;
	else
		myChar = "\begin{bmatrix*} ";
		for it = 1 : size(data, 1)
			for jt = 1 : (size(data, 2)-1)
				myChar = myChar + data(it,jt) + " & ";
			end
			myChar = myChar + data(it,end) + " \\ ";
		end
		myChar = myChar + "\end{bmatrix*}";
	end
		

elseif isnumeric(data)
	if option.rational
		myChar = latex(sym(data));
	else
		myChar = latex(vpa(data, option.digits));
	end

elseif isa(data, 'sym')
	myChar = latex(data);

elseif isa(data, 'quantity.Symbolic')
	myChar = latex(data.sym());

elseif isa(data, 'quantity.Discrete')
	if ~isempty(data(1).name)
		myChar = data(1).name;
	else
		myChar = "numeric";
	end

elseif isa(data, 'function_handle')
	try
		myChar = latex(sym(data));
	catch
		try
			myChar = func2str(data);
		catch
			myChar = 'Unknown';
		end
	end
else 
	error(['The data type ', class(data), ...
		' is not considered in this function']);
end

% some formating:
while any(strfind(myChar, '\left(\begin{array}{cc'))
	myChar = strrep(myChar, '\left(\begin{array}{cc', '\left(\begin{array}{c');
end
myChar = strrep(myChar, '\left(\begin{array}{c}', '\begin{bmatrix*}');
myChar = strrep(myChar, '\end{array}\right)', '\end{bmatrix*}');

myChar = strrep(myChar, '\', '\\');
myChar = strrep(myChar, '\\\\', '\\\\ ');
myChar = strrep(myChar, '\\begin{align*}', '\\begin{align*}\n');
myChar = strrep(myChar, '\\end{align*}', '\n\\end{align*}');
myChar = sprintf(myChar);
end % plot()