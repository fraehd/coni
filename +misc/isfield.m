function itIs = isfield(myStruct, myFieldName)
% misc.isfield returns a logical if a field specified by myFieldName exists.
% In contrast to the matlab built-in function isfield(), this method misc.isfield
% can also read fields of field by specifying myFieldName with dots as seperator, for
% instance myFieldName = 'topLevel.lowerLevel.data', see example.
%%
% Example:
%		myStruct = misc.setfield(struct(), 'topLevel.lowerLevel.data', 42);
%		result = misc.isfield(myStruct, 'topLevel.lowerLevel.data')

arguments
	myStruct struct;
	myFieldName (:, 1) string;
end

itIs = true(size(myFieldName));
for it = 1 : numel(itIs)
	myFieldNameSplitted = split(myFieldName(it), '.');
	myExplorer = myStruct;
	for jt = 1 : numel(myFieldNameSplitted)
		if isfield(myExplorer, myFieldNameSplitted{jt})
			myExplorer = myExplorer.(myFieldNameSplitted{jt});
		else
			itIs(it) = false;
			break;
		end
	end % for jt = 1 : numel(myFieldNameSplitted)
end % for it = 1 : numel(itIs)
end % misc.isfield()