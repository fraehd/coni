function [ inverseKernel ] = invertFredholmKernel(kernel, lowerTriangularKernel, signOfIntegralOfInvertedTransformation)
%INVERT_KERNEL Compute the inverse fredholm kernel using a previously
%caluclated fredholm kernel and its transformation.
%  
% INPUT PARAMETERS:
%	4D-DOUBLE-ARRAY			   kernel : kernel to be inverted. 4-dimensional
%										array with indices (z, zeta, rows, columns)
%	BOOLEAN				lowerTriangularKernel : indicates if kernel is
%								either upper or lower triangular
%	DOULBE (-1 or 1)	signOfIntegralOfInvertedTransformation :
% 								to consider the change of the sign of the 
%								integral term in the fredholm 
%								transformation, this input is used. If the
%								backstepping transformation of the input
% 								"kernel" is of the type 
%								F[x] = x - int_0^1 kernel x dzeta
% 								then its inverse usually uses
% 								F_i[x] = x + int_0^1 inverseKernel x dzeta.
% 								In this case use 
%								signOfIntegralOfInvertedTransformation = +1
%								else if the transformations are of the type
%								F[x] = x + int_0^1 kernel x dzeta
% 								and
% 								F_i[x] = x - int_0^1 inverseKernel x dzeta
%								then one must use
%								signOfIntegralOfInvertedTransformation = -1
% OUTPUT PARAMETERS:
%	4D-DOUBLE-ARRAY		inverseKernel : inverted kernel. 4-dimensional
%										array with indices (z, zeta, rows, columns)
%										and same size as input

%% init parameters
n = size(kernel, 3);
ndisc = size(kernel, 1);
zdisc = linspace(0, 1, ndisc);
if lowerTriangularKernel
	%% Calculation of lower triangular Fredholm-Transformation
	inverseKernel = zeros(size(kernel));
	for it = 1:n % first i, then j - cause it matters!
		for jt_wrong_way_arround = 1:n
			% since for calculation of J_ij some values J_il with
			% l>k are needed, j is iterated from right to left.
			jt = n+1-jt_wrong_way_arround;
			if it-1 <= jt
				inverseKernel(:,:,it,jt) = kernel(:,:,it,jt);
			else
				integralTerm = zeros(ndisc, ndisc);
				for zIdx = 1:ndisc
					for zetaIdx = 1:ndisc
						integrand = zeros(1, ndisc);
						for k=(jt+1):(it-1)
							integrand = integrand + ...
								reshape(inverseKernel(zIdx,:,it,k), [1, ndisc])...
								.* reshape(kernel(:,zetaIdx,k,jt), [1, ndisc]);
						end
						integralTerm(zIdx, zetaIdx) = ...
							numeric.trapz_fast(zdisc, integrand);
					end
				end
				inverseKernel(:,:,it,jt) = kernel(:,:,it,jt) ...
						+ signOfIntegralOfInvertedTransformation*integralTerm;
			end
		end
	end
elseif ~lowerTriangularKernel
	%% Calculation of upper triangular Fredholm-Transformation
	inverseKernel = zeros(size(kernel));
	for jt = 1:n % first j, then i - cause it matters!
		for it = 1:n
			integralTerm = zeros(ndisc);
			for zIdx = 1:ndisc
				for zetaIdx = 1:ndisc
					integrand = zeros(1, ndisc);
					for k = 1:(jt-1)
						integrand = integrand + ...
							reshape(inverseKernel(zIdx,:,it,k), [1, ndisc])...
							.* reshape(kernel(:,zetaIdx,k,jt), [1, ndisc]);
					end
					integralTerm(zIdx, zetaIdx) = numeric.trapz_fast(zdisc, integrand);
				end
			end
			inverseKernel(:,:,it,jt) = kernel(:,:,it,jt) ...
					+ signOfIntegralOfInvertedTransformation*integralTerm;
		end
	end
end
end