function [sol, dzSol, z] = solveGenericBVP(L,M,S,A0,H,B1,G1,B2,G2,zdisc)
% SOLVEGENERICBVP Solve a generic boundary value problem (BVP) with 2nd order
% derivatives
%
% Description:
% sol = solveGenericBVP(L,M,S,A0,H,B1,G1,B2,G2)
% computes the solution of the generic BVP
%     L(z) X'' + M(z) X + XS + A0(z) X(0) = H(z)
%       B1[X] = G1
%       B2[X] = G2.
% There, L(z) is a diagonal matrix, M(z), A0(z) and H(z) are general
% spatially varying matrices and S is a constant matrix that may be
% non-diagonalizable.
% The operators B1 and B2 are arbitrary linear operators which have the
% the argument X at the right end in each component. This can be
% represented by a quadratic dps.outout_operator.
% The spatial variable z is discretized with 101 points.
%
% sol = solveGenericBVP(...,zdisc)
%   returns the solution at the spatial points zdisc.
%
% [sol, dzSol] = ...
%   also returns the first spatial derivative of the solution
%
% [sol, dzSol, z] = ...
%   also returns the discretized spatial vector where the solution is
%   determined.
%
% Inputs:
%     L,M,S,A0,H,B1,G1,B2,G2      Matrices determining the form of the
%                                 BVP (see above)
%     zdisc                       Discrete points at which the Solutin is
%                                 calculated
% Outputs:
%     sol         Solution
%     dzsol       First spatial derivative of sol
%     z           Discretized spatial vector where sol is calculated
%
% Example:
% -------------------------------------------------------------------------
%   
% -----------------------------------------------------------------------

% created on 20.06.2018 by Simon Kerschbaum
import misc.*
import numeric.*
import dps.parabolic.system.*
import dps.parabolic.control.backstepping.*

if ~isa(B1,'dps.output_operator') || ~isa(B2,'dps.output_operator')
	error('B1 and B2 must be output_operators')
end

opts = odeset('RelTol',1e-6,'AbsTol',1e-6,'vectorized','on');
regtimer = tic;
fprintf('Solving generic BVP: ...');

if nargin<10 % no zdisc was passed
	ndisc = 101;
	zdisc = linspace(0,1,ndisc);
	z=zdisc;
else
	ndisc=length(zdisc);
end

if isa(L,'function_handle')
	LDisc = eval_pointwise(L,zdisc);
else
	if size(L,1)~= ndisc % constant matrix
		LDisc(1:ndisc,:,:) = repmat(shiftdim(L,-1),ndisc,1,1);
	else
		LDisc = L;
	end
end
if isa(A0,'function_handle')
	A0Disc = eval_pointwise(A0,zdisc);
else
	if size(A0,1)~= ndisc % constant matrix
		A0Disc(1:ndisc,:,:) = repmat(shiftdim(A0,-1),ndisc,1,1);
	else
		A0Disc = A0;
	end
end
if isa(H,'function_handle')
	HDisc = eval_pointwise(H,zdisc);
else
	if size(H,1)~= ndisc % constant matrix
		HDisc(1:ndisc,:,:) = repmat(shiftdim(H,-1),ndisc,1,1);
	else
		HDisc = H;
	end
end
n = size(LDisc,2);
T1 = [eye(n); zeros(n,n)]; % selection matrix of first solution state
T2 = [zeros(n,n); eye(n,n)]; % selection matrix of second solution state
% ACHTUNG: ERRST MAL DAVON AUSGEHEN DASS ALLE MATRIZEN IN DER RICHTIGEN
% DIMENSION UEBERGEBEN WURDEN!
% UEBERPRUFUNG NOCH EINBAUEN!


if ndisc~= n
	if isa(M,'function_handle') % function
		muSys = eval_pointwise(M,zdisc);
	elseif isequal(size(M,1),n) % constant matrix, two dimensions
		muSys(1:ndisc,:,:) = repmat(shiftdim(M,-1),ndisc,1,1);
	else
		if ~isequal(size(M,1),ndisc) % is discretized
			error('Mu needs to be resampled, not yet implemented!')
		else
			muSys = M;
		end
	end
else
	error('ndisc=n is a very bad choice!')
end


nv = size(S,1);
pr_til = zeros(1,0);
pd_til = zeros(1,nv);
% construct a fictive signal model to process jordan canonical form
% calulcations automatically
sigMod = dps.parabolic.control.outputRegulation.signalModel(S,[],pd_til,pr_til);

% preallocate and starting value
% phi_til belonging to k=0 is zero and so is phi and pi.
% phi(1:ndisc,1:2n,k)
% initialize solution array
%    ndisc,  n,  k+1 (k starts at zero, index at 1 for each block!)
%    +1 index for k=0 for each block
X_j(1:ndisc,1:n,1:nv+length(sigMod.blockLength)) =0; 
Xz_j(1:ndisc,1:n,1:nv+length(sigMod.blockLength)) =0; 

% prepare a vector to only select k>=1 later
selectionVector(1:nv+length(sigMod.blockLength))=0;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2. Calculate modal solution
% loop through all jordan blocks:
for jBlock = 1:length(sigMod.blockLength)
	gamma_for_vec(1:ndisc,1:(2*n)^2,1:(2*n)^2)=0; % preallocate
	gamma(1:ndisc,1:2*n,1:2*n) = 0; %preallocate
	Psi_mu_vec(1:ndisc,1:ndisc,1:(2*n)^2)=0; % preallocate
	Psi_mu(1:ndisc,1:ndisc,1:2*n,1:2*n) = 0; %preallocate
	try
		mu = sigMod.lambda(sum(sigMod.blockLength(1:jBlock-1))+1);
	catch err
		mythrow(err)
	end
	for z_idx = 1:ndisc
		% currently considered eigenvalue
		try
		gamma(z_idx,:,:) = ...
			[zeros(n,n)                                                               eye(n);
			 reshape(LDisc(z_idx,:,:),[n n])\(-reshape(muSys(z_idx,:,:),[n n])-mu*eye(n)) zeros(n,n)];
		catch err
			mythrow(err)
		end
		for i=1:2*n %number of columns in Psi
			% create diagonal matrix of gammas
			gamma_for_vec(z_idx,(i-1)*2*n+1:i*2*n,(i-1)*2*n+1:i*2*n) = gamma(z_idx,:,:);
		end
	end
	
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% calculate fundamental matrix:
	% as the solution of 
	%     dz Psi_mu(z,zeta) = gamma(z) Psi_mu(z,zeta) z in (0,1),zeta
	%     in (0,z)
	%     Psi_mu(zeta,zeta) = I
	gammaInterp = griddedInterpolant({zdisc,1:(2*n)^2,1:(2*n)^2},gamma_for_vec);
	gamma_gen = @(z) squeeze(gammaInterp({z(:),1:(2*n)^2,1:(2*n)^2}));

	Psi_0 = eye(2*n);
	Psi_vec_0 = Psi_0(:); % vectorized
	% Problem: matlab-solver only for vectors.
	% --> Matrix must be vectorized
	% --> Gamma blockdiagonalized
	for zeta_idx =1:ndisc-1
		% starting value for z
		z0 = zdisc(zeta_idx); 
		f=@(z,Psi_vec) gamma_gen(z)*Psi_vec;
		[z,Psi_z_zeta_vec]=ode15s(f,[z0 1],Psi_vec_0,opts);
		PsiInterp = griddedInterpolant({z,1:(2*n)^2},Psi_z_zeta_vec);
		Psi_z_zeta_vec_res = PsiInterp({zdisc(zeta_idx:ndisc),1:(2*n)^2});
		
% 			Psi_z_zeta_vec_res = interp1(z,Psi_z_zeta_vec,zdisc(zeta_idx:ndisc).');
		Psi_mu_vec(zeta_idx:ndisc,zeta_idx,:) = Psi_z_zeta_vec_res;
	end
	Psi_mu_vec(ndisc,ndisc,:) = Psi_vec_0; %#ok<AGROW>
	% put vector in matrix form:
	for j=1:2*n
		%      z,zeta,i,j            z,zeta,i
		Psi_mu(:,:,:,j) = Psi_mu_vec(:,:,(j-1)*2*n+1:j*2*n);
	end
	% fundamental matrix ready.
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% calculate unknown initial value
	% phi_til_k_0
	
	% calculate A0_til which represents the inhomogenity:
	A0_til = zeros(ndisc,2*n,2*n);
	Y = zeros(ndisc,2*n,2*n);
	T1tY = zeros(ndisc,n,2*n);
	T2tY = zeros(ndisc,n,2*n);
	for z_idx=1:ndisc
		temp_prod=zeros(z_idx,2*n,2*n); % preallocate and reset
		for zeta_idx=1:z_idx
			temp_prod(zeta_idx,:,:) = ...
				reshape(Psi_mu(z_idx,zeta_idx,:,:),[2*n 2*n])...
				*T2*(reshape(LDisc(zeta_idx,:,:),[n n])\reshape(A0Disc(zeta_idx,:,:),[n n]))*T1.';
		end
		A0_til(z_idx,:,:) = trapz(zdisc(1:z_idx),temp_prod,1);
		Y(z_idx,:,:) = reshape(Psi_mu(z_idx,1,:,:),[2*n,2*n]) - reshape(A0_til(z_idx,:,:),[2*n 2*n]);
		T1tY(z_idx,:,:) = T1.'*reshape(Y(z_idx,:,:),[2*n 2*n]);
		T2tY(z_idx,:,:) = T2.'*reshape(Y(z_idx,:,:),[2*n 2*n]);
	end
	% loop through all generalized eigenvectors of the current
	% jordan block and calculate pi_j
	for k=1:sigMod.blockLength(jBlock)
		% k is index of genVec in current block
		% overall index of genVec:
		indexOfGenVec = sum(sigMod.blockLength(1:jBlock-1))+k;

		% index in pi:
		indexInPi = sum(sigMod.blockLength(1:jBlock-1)+1)+k+1;

		% current generalized eigenvector:
		try
			varphi_j_k = sigMod.genEig(:,indexOfGenVec);
		catch err
			mythrow(err)
		end
		hjk = zeros(ndisc,n);
		hjk_til = zeros(ndisc,2*n);
		hjk_til_diff = zeros(ndisc,2*n);
		T1hjk_til = zeros(ndisc,n);
		T1hjk_til_diff = zeros(ndisc,n);
		
		g1jk = G1*varphi_j_k;
		g2jk = G2*varphi_j_k;
		
		for z_idx = 1:ndisc
			hjk(z_idx,:) = reshape(HDisc(z_idx,:,:),[n nv])*varphi_j_k;
			temp_prod = zeros(z_idx,2*n,1); % preallocate and reset
			for zeta_idx=1:z_idx
				temp_prod(zeta_idx,:,1) = ...
					reshape(Psi_mu(z_idx,zeta_idx,:,:),[2*n 2*n])*T2...
					*(reshape(LDisc(zeta_idx,:,:),[n n])\(-reshape(X_j(zeta_idx,:,indexInPi-1),[n 1])...
					  + reshape(hjk(zeta_idx,:),[n 1])));
			end
			hjk_til(z_idx,:) = trapz(zdisc(1:z_idx),temp_prod(1:z_idx,:),1);
			hjk_til_diff(z_idx,:) = -T2*reshape(X_j(z_idx,:,indexInPi-1),[n 1])...
				+T2*reshape(hjk(z_idx,:),[n 1]) ...
				+ reshape(gamma(z_idx,:,:),[2*n 2*n])*reshape(hjk_til(z_idx,:),[2*n,1]);
			T1hjk_til(z_idx,:) = T1.'*reshape(hjk_til(z_idx,:),[2*n,1]);
			T1hjk_til_diff(z_idx,:) = T1.'*reshape(hjk_til_diff(z_idx,:),[2*n,1]);
		end
		% Get starting value from boundary conditions
		B1T1Y = B1.use_output_operator(T1tY,zdisc,T2tY);
		B2T1Y = B2.use_output_operator(T1tY,zdisc,T2tY);
% 		B1T1Y_comp = B1.use_output_operator(T1tY,zdisc);
% 		B2T1Y_comp = B2.use_output_operator(T1tY,zdisc);
		B1HTtil = B1.use_output_operator(T1hjk_til,zdisc,T1hjk_til_diff);
		B2HTtil = B2.use_output_operator(T1hjk_til,zdisc,T1hjk_til_diff);
% 		B1HTtil_comp = B1.use_output_operator(T1hjk_til,zdisc);
% 		B2HTtil_comp = B2.use_output_operator(T1hjk_til,zdisc);
		try
			phi0 = [B1T1Y;B2T1Y]\([g1jk;g2jk]-[B1HTtil;B2HTtil]);
		catch err
			misc.mythrow(err)
		end
		for z_idx =1:ndisc
			X_j(z_idx,1:n,indexInPi) = ...
				T1.'*reshape(Y(z_idx,:,:),[2*n 2*n])...
				* phi0...
				+ T1.'*reshape(hjk_til(z_idx,:),[2*n,1]);
			Xz_j(z_idx,1:n,indexInPi) = ...
				T2.'*reshape(Y(z_idx,:,:),[2*n 2*n])...
				* phi0...
				+ T2.'*reshape(hjk_til(z_idx,:),[2*n,1]);
		end
		selectionVector(indexInPi) = 1;
	end % k
end % jBlock

X_j_k_g_0 = X_j(:,:,selectionVector==1);
Xz_j_k_g_0 = Xz_j(:,:,selectionVector==1);

%%% modal solution computed
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 3. Calculate original solution
sol(1:ndisc,1:n,1:nv)=0; %preallocate
dzSol(1:ndisc,1:n,1:nv)=0; %preallocate
for z_idx=1:length(zdisc)
	sol(z_idx,:,:) = reshape(X_j_k_g_0(z_idx,:,:),[n nv])/sigMod.genEig;
	dzSol(z_idx,:,:) = reshape(Xz_j_k_g_0(z_idx,:,:),[n nv])/sigMod.genEig;
end

% Only for debugging
% for i=1:n
% 	for j=1:nv
% 		dzSolComp(:,i,j) = numeric.diff_num(zdisc,sol(:,i,j));
% 	end
% end
% for i=1:n
% 	for j=1:nv
% 		figure('Name',['Ableitung numerisch und analytisch, ' num2str(i) num2str(j)]);
% 		hold on
% 		plot(zdisc,dzSol(:,i,j))
% 		plot(zdisc,dzSolComp(:,i,j))
% 	end
% end

if any(imag(sol)>1e-6)
	error('Solution of the BVP contains imaginary parts!')
else
	sol = real(sol);
	dzSol = real(dzSol);
end
fprintf('Finished! (%0.2fs) \n',toc(regtimer));

% L�sung des Randwertproblems numerisch verifizieren:
% L(z) X'' + M(z) X + XS + A0(z) X(0) = H(z)
%         B1[X] = G1
%		  B2[X] = G2.

lhs = zeros(ndisc,n,nv);
Xzz = zeros(ndisc,n,nv);
grad_op = numeric.get_grad_op(zdisc);
for i=1:n
	for j=1:nv
		% erst mal numerisch... Kann auch noch analytisch ueber rechte
		% Seite angegeben werden!
		Xzz(:,i,j) = grad_op*dzSol(:,i,j);
	end
end
for z_idx=1:ndisc
	lhs(z_idx,:,:) = sd(LDisc(z_idx,:,:))*sd(Xzz(z_idx,:,:))...
		+ sd(muSys(z_idx,:,:))*sd(sol(z_idx,:,:))...
		+ sd(sol(z_idx,:,:))*S ...
		+ sd(A0Disc(z_idx,:,:))*sd(sol(1,:,:));
end
% rhs = H;
% err = lhs-rhs;
% figure('Name','Fehler generic BVP')
% hold on
% for i=1:n
% 	for j=1:nv
% 		plot(zdisc,err(:,i,j),'DisplayName',[num2str(i) num2str(j)])
% 	end
% end
% legend('-DynamicLegend')
 
end
