function [M, rows, columns] = remove(M, row_idx, column_idx)
% REMOVE a row or column in a matrix
%
% Description:
% [M, rows, columns] = remove(M, row_idx, column_idx)
% Removes the row with index row_idx and the column with column
% column_idx in the matrix M. If empty or index is smaller 0, the
% argument is ignored. M is the matrix with the removed values, rows
% containes the removed rows and columns the removed columns.
%
% Inputs:
%     M           Matrix of interest
%     row_idx     Index of row to be removed
%     row_idx     Index of row to be removed
% Outputs:
%     M           Resultant matrix
%     rows        Removed row
%     columns     Removed column
%
% Example:
% -------------------------------------------------------------------------
% A = diag(1:7);
% [M, rows, columns] = misc.remove(A, 1, 2)
% >> M = [0     0     0     0     0     0
%         0     3     0     0     0     0
%         0     0     4     0     0     0
%         0     0     0     5     0     0
%         0     0     0     0     6     0
%         0     0     0     0     0     7]
% >> rows = [1     0     0     0     0     0     0]
% >> columns = [2
%               0
%               0
%               0
%               0
%               0]
% -------------------------------------------------------------------------
    if ~isempty(row_idx) && (islogical(row_idx) || row_idx > 0)
        rows = M(row_idx, :);
        M(row_idx, :) = [];
    else
        rows = [];
    end
    
    if exist('column_idx', 'var') && ~isempty(column_idx)
        columns = M(:, column_idx);
        M(:, column_idx) = [];
    else
        columns = [];
    end

end