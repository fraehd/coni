function valueDiag = diag_nd(value, varargin)
%DIAG_ND returns the diagonal elements of the first two dimensions of the
%n-D matrix "value" for every further dimension.
%% Example
% testValue = rand(4, 4, 2, 2);
% testDiagValue = misc.diag_nd(testValue);
% % The following code is only for verification of testDiagValue
% testDiagValueReferenceResult = zeros(4, 2, 2);
% for it = 1:size(testValue, 3)
% 	for jt = 1:size(testValue, 4)
% 		testDiagValueReferenceResult(:, it, jt) = ...
% 				diag(testValue(:, :, it, jt));
% 	end
% end
% if all(testDiagValue(:) == testDiagValueReferenceResult(:))
% 	fprintf('Success! \n');
% end
% 	
%
% INPUT PARAMETERS:
% 	ARRAY						value : nd matrix
% OPTIONAL INPUT PARAMTERS 
%	varargin				 varargin : not in use yet, but it would be
%										helpful to extend this function
%										with further functionalities, like
%										choosing which dimensions should be
%										considered for diag().
%   
% OUTPUT PARAMETERS:
% 	ARRAY					valueDiag : diagonal elements in the first two
%										dimensions of value

valueSize = size(value);
if valueSize(1) ~= valueSize(2)
	error(['The two dimensions for which the diagonal elements should', ...
		'be calculated must be quadratic but size(value, 1) ~= size(value, 2)']);
end

% just do an iteration over the dimensions for the diagonal evaluation.
valueDiag = zeros([valueSize(2:end), 1]);
for it = 1:valueSize(1)
	valueDiag(it,:) = value(it,it,:);
end

end % diag_nd()
