function result = diagNd(value, dimensionsToBeDiagonalized)
% diagNd returns the diagonal elements of all dimensions specified with
% dimensionsToBeDiagonalized. The result contains the diagonalized
% dimensions on the first dimension. Not-diagonalized dimensions of value
% follow afterwards in the original order.
%% Example 1
% misc.diagNd(ones(3))
% >> ans = [1; 1; 1]
%
%% Example 2
% testValue = ones(2, 2, 2);
% testValue(1,:,:) = 2;
% testValue(2,:,:) = 3;
% misc.diagNd(testValue)
% >> ans = [2; 3]
% misc.diagNd(testValue, [1, 2])
% >> ans = [2, 2; 3, 3]
% misc.diagNd(testValue, [2, 3])
% >> ans = [2, 3; 2, 3]

	arguments
		value;
		dimensionsToBeDiagonalized = 1:ndims(value);
	end % arguments
	
	% save sizes and init result
	sizeValue = size(value);
	sizeResult = [min(sizeValue(dimensionsToBeDiagonalized), [], "all"), ...
		sizeValue(all((1:numel(sizeValue)) ~= dimensionsToBeDiagonalized.')), 1];
	result = zeros(sizeResult);
	
	% the cell array tempValueIdx will be used to read the elements of value.
	tempValueIdx = cell(numel(sizeValue), 1);
	for it = 1 : numel(tempValueIdx)
		if any(it == dimensionsToBeDiagonalized)
			tempValueIdx{it} = 1;
		else
			tempValueIdx{it} = (1:1:sizeValue(it)).';
		end
	end
	
	% iterate over diagonal elements
	for it = 1 : sizeResult(1)
		% set same idx for all elements on diagonal
		for jt = 1 : numel(dimensionsToBeDiagonalized)
			tempValueIdx{dimensionsToBeDiagonalized(jt)} = it;
		end
		% read values from value and set values to result
		tempValue = value(tempValueIdx{:});
		result(it, :) = tempValue(:);
	end
end % diagNd()