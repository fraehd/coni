function export2tikz(folder,varargin)
% EXORT2TIKZ exports figures to tikz
%
% Description:
% Export all currently opened figures to tikz in the specified folder
% (String), passes varargin to matlab2tikz
%
% Some usefull options to pass:
% - 'parseStrings',false 
%   deactivates matlab's string parsing, so Latex-code can directly be
%   passed. (e.g. for labels: $y_s$)
% - The argument followed by 'extraAxisOptions' will directly be passed
%   to the options of \begin{axis}. This can be used to configure the
%   appearance of the axis. (Pass everything as string!)
% - legend style = {
%     at={(1,0)}, --> place at lower right corner
%     anchor=south east, --> anchor at lower right corner
%     font = \tiny  --> font size
%   }
% - legend image post style={xscale=0.5} --> shorter lines in legend
% - every axis y label/.append style={yshift=-2mm} 
% - every axis x label/.append style={yshift=1mm} --> position of labels
% - every tick label/.append style={font=\scriptsize} --> size of
%	                                                        ticklabels
% - 'yticklabel style={/pgf/number format/fixed,/pgf/number format/precision=2}'
%                                                       --> fixed number of
%                                                           decimals
% For more, see documentation of pgfplots.
%  
% annotation: 
% paths for the required libraries should be added using the syntax
% e.g. addpath(genpath([pwd '\' 'M_files'])); to ensure the files stay
% available on changig the current folder.
%
% requires matlab2tikz
%
% Example:
% -------------------------------------------------------------------------
% export2tikz('Plots','width','0.75\linewidth','height','1.25cm',...
%        'parseStrings',false,...
%     	 'extraAxisOptions',...
% 			['every axis label/.append style={font=\scriptsize},'...
% 			'every axis y label/.append style={yshift=-2mm},'...
% 			'every axis x label/.append style={yshift=1mm},'...
% 			'every tick label/.append style={font=\scriptsize},'...
% 			'legend style={font=\tiny,at={(1,0)}, anchor=south east, inner sep=0pt, xshift=0.5mm},'...
% 			'legend image post style={xscale=0.5}',...
%           'yticklabel style={/pgf/number format/fixed,/pgf/number format/precision=2}']);
% -------------------------------------------------------------------------
%


% history:
% created by Simon Kerschbaum, 2014
%  * modified on 03.03.2016 by SK:
%    - added reentering of original path on error
%    - added reordering of export, first opened figure will be exported as 
%      first file

if ~isa(folder,'char')
	error('folder must be a valid string!')
end
if ~exist(folder,'dir')
	mkdir(folder);
end
figs = get(0,'children');
% set(figs, 'Position', [0, 0, 1080, 1920]); 
% set figures to a very big size. Reason for that is the autoscaling of the
% axis in matlab, which achieves smaller borders for bigger figures!
oldFolder = cd(['./' folder]);
try
	for i = 1:length(figs)
		j = length(figs)-(i-1);
		name  = get(figs(i),'Name');
		matlab2tikz([num2str(j) '_' num2str(name) '.tex'],'figurehandle',figs(i),varargin{:});
	end
catch error_msg
	cd(oldFolder);
	rethrow(error_msg)
end
cd(oldFolder);