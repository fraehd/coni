function [M_sorted, permutationMatrix] = sort_diagonal_matrix(M, direction)
% sort_diagonal_matrix permutes the element of the diagnal 
% matrix M (input) to obtain the diagnoal matrix M_sorted (output)
% on whichs diagnal the elements of M are arranged in descending order.
% As second output the permutation matrix which ensures
% M_sorted = permutationMatrix * M * permutationMatrix.'
% and
% permutationMatrix * permutationMatrix.' = identity-matrix
% is returned.
%
% INPUT PARAMETERS:
% MATRIX						   M : matrix to be permuted, diagonal
% STRING				   direction : determines if sorted in descending 
%										or ascending order. Must be either
%										'descend' or 'ascend'
%   
% OUTPUT PARAMETERS:
% MATRIX				M_sorted : permuted matrix, diagonal, elements
%										on diagonal are the ones of M but 
%										rearranged to descend
% Matrix		   permutationMatrix : permutation matrix
%
% Example:
% -------------------------------------------------------------------------
% A = blkdiag(1,6,9,5,4,2);
% M = misc.sort_diagonal_matrix(A,'ascend')
% >> M = [1 0 0 0 0 0
%         0 2 0 0 0 0
%         0 0 4 0 0 0
%         0 0 0 5 0 0
%         0 0 0 0 6 0
%         0 0 0 0 0 9]
% -------------------------------------------------------------------------
%
% history:
%	-	created on 14.09.2018 by Jakob Gabriel
	
	% Input check
	if ~isdiag(M)
		error('M must be diagonal');
	end
	if ~sum(strcmp(direction, {'ascend', 'descend'}))
		error('direction must be either ascend or descend');
	end
	
	% sort entrys of M and permute
	[M_diag_sorted, originalIndex] = sort(diag(M), direction);
	M_sorted = diag(M_diag_sorted);
	
	% calculate permutation matrix
	permutationMatrix = zeros(size(M,1));
	for it = 1:size(M,1)
		permutationMatrix(it, originalIndex(it)) = 1;
	end

	% test
	if sum(sum(abs(M_sorted - permutationMatrix * M * permutationMatrix.'))) ~= 0 ...
			&& (sum(abs(M(:))) ~= 0)
		error('permutation failed');
	end			
end