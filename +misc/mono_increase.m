function y = mono_increase(x,strict)
% MONO_INCREASE check if vector x is monotonicallyincreasing
%
% Description:
% y = mono_increase(x) is true if x is monotonically increasing
%
% y = mono_increase(x,strict) is true if x is strictly monotonically 
% increasing if strict=1
%
% Inputs:
%     x           Vector to be checked
%     strict      Flag, determining whether strcly mono. inc. should be
%                 checked
% Outputs:
%     y           TRUE or FALSE if x is (strictly) mono. inc.
%
% Example:
% -------------------------------------------------------------------------
% x = logspace(0,1,100);
% mono_increase(x,1)
% >> ans = 1
% -------------------------------------------------------------------------

if ~exist('strict','var')
	strict=0;
end

if strict
	if sum(all(diff(x)>0))<=0
		y=false;
	else
		y=true;
	end
else
	if sum(all(diff(x)>=0))<=0
		y=false;
	else
		y=true;
	end
end 
