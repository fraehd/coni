classdef myCell
	%myCell cell-array with custom operations, that are +, abs, max
	
	% created on 31.10.2018 by Simon Kerschbaum
	
	
	properties
		value = cell(1,1); 
	end
	
	methods		
		function obj = plus(a,b)
			% Elementwise addition of kernel elements
			obj = a; % Datenuebernahme
			for i=1:size(obj.value,1)
				for j=1:size(obj.value,2)
					obj.value{i,j} =  a.value{i,j}+b.value{i,j};
				end
			end
		end
		
		function obj = minus(a,b)
			% Elementwise addition of kernel elements
			obj = a; % Datenuebernahme
			for i=1:size(obj.value,1)
				for j=1:size(obj.value,2)
					obj.value{i,j} =  a.value{i,j}-b.value{i,j};
				end
			end
		end
		
		function obj = abs(obj)
			% Elementwise abs of the kernel elements
			for i=1:size(obj.value,1)
				for j=1:size(obj.value,2)
					obj.value{i,j} =  abs(obj.value{i,j});
					obj.value{i,j} =  abs(obj.value{i,j});
				end
			end			
		end
		
		function m = max(obj)
			% max of the cell values
			m = 0;
			for i=1:size(obj.value,1)
				for j=1:size(obj.value,2)
					mij =  max(obj.value{i,j}(:));
					if mij >= m
						m = mij;
					end
				end
			end
		end
		
		function obj = myCell(n,m)
			obj.value = cell(n,m);
		end
	end
	
end

