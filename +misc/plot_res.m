function plot_res(t,y,num,varargin)
% PLOT_RES Plot resampled data
%
% Description:
% PLOT_RES(x,y,numpoints,...)
% plots the data y versus t with num discetization points. The further
% arguments (...) are passed to plot.
%
% Inputs:
%     t           Plot points on abscissa
%     y           Plot points on ordinate
%     num         Desired amount of plot points
%     varargin    Additional plot options
%
% Example:
% -------------------------------------------------------------------------
% syms z real
% t=0:0.0001:1;
% fun = double(subs(exp(-4*z),'z',t));
% misc.plot_res(t,fun,10,'r-.');
% -------------------------------------------------------------------------

%  created on 07.09.16 by Simon Kerschbaum

%  requires resample_time

if ~isnumeric(num) % kein t �bergeben
	% virtuelles lineares t erzeugen
	newvar = [num, {varargin}];
	num = y;
	y=t;
	t_new = 1:length(y);
	[t_res, y_res] = misc.resample_time(t_new,y,num);
else
	[t_res, y_res] = misc.resample_time(t,y,num);
end
plot(t_res,y_res,varargin{:});

end

