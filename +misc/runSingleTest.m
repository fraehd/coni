function runSingleTest(testSuite, testName)
%RUNSINGLETEST runs a single test of a test file
%   runSingleTest(testSuite, testName) runs the testfunction with the name
%   'testName' of the 'testSuite'. A testSuite can be produced by
%   matlab.unittest.TestSuite.from*... or if the tests are defined in a
%   file, this file can be directly used as testSuite. 
%	Example:
%	testSuite = unittests.quantity.testSymbolic
%	testName = 'testCat'
%	runSingletest(testSuite, testName)
testSuite(strcmp({testSuite.ProcedureName}, testName)).run
end

