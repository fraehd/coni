function [str] = ensureString(chr)

if iscell( chr )
	if ~all( cellfun( @ischar, chr ) )
		chr = [chr{:}];
	end
end

if isstring( chr )
	str = chr;
else
	str = convertCharsToStrings( chr );
end

end

