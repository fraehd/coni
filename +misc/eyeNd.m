function I = eyeNd(iSize, dataType)
% eyeNd returns the array I with the size iSize, such that I(i, i, i, ...) = 1 and 
% I(i, j, k, , ...) if i~=j or i~k or likewise. The resulting matrix I must be
% quadratic, i.e. size(I, it) = size(I, jt) for all it, jt.

arguments
	iSize (1, :);
	dataType (1, 1) = "double";
end

%% Example 1
% misc.eyeNd([2, 2])
% ans = [1, 0; 0, 1]
%% Example 2
% misc.eyeNd([2, 2, 2])
% ans(:,:,1) =
%      1     0
%      0     0
% ans(:,:,2) =
%      0     0
%      0     1
assert(all(iSize(1) == iSize), 'iSize must specify an quadratic array');
if numel(iSize) <= 2
	I = eye(iSize, dataType);
		
elseif strcmp(dataType, "double")
	I = zeros(iSize);
	indexCell = cell(numel(iSize), 1);
	indexArray = 1:1:iSize(1);
	for it = 1 : numel(iSize)
		indexCell{it} = indexArray;
	end % for it = 1 : numel(iSize)
	I(sub2ind(iSize, indexCell{:})) = 1;
	
elseif strcmp(dataType, "logical")
	I = false(iSize);
	indexCell = cell(numel(iSize), 1);
	indexArray = 1:1:iSize(1);
	for it = 1 : numel(iSize)
		indexCell{it} = indexArray;
	end % for it = 1 : numel(iSize)
	I(sub2ind(iSize, indexCell{:})) = true;
	
else
	error("dataType must be logical or double");
end
end % misc.eyeNd()