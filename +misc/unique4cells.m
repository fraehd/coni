function uniqueCell = unique4cells(myCell)
% misc.unique4cells is similar to the matlabs original unique. However, matlabs
% unique works only for cells filled with character-arrays. In contrast,
% misc.unique4cells permitts arbitary content in the cell elements, which are
% compared using isequal().
%	uniqueCell = unique4cells(myCell)
%
%% Example:
% myCell = {1, 2, "asdf", 2, "asdf"};
% uniqueCell = misc.unique4cells(myCell)
% >> uniqueCell = {1, 2, "asdf"};

arguments
	myCell cell;
end

numelMyCell = numel(myCell);
isUnique = true(size(myCell));
for it = 1 : numelMyCell
	for jt = (it + 1) : numelMyCell
		if isUnique(jt) && isequal(myCell{it}, myCell{jt})
			isUnique(jt) = false;
		end
	end
end % for it = 1 : numel(myCell)

uniqueCell = myCell(isUnique);
end % unique4cells()

