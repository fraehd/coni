classdef Time < matlab.mixin.SetGetExactNames
%TIME is a class used for fixed-step simulations (for instance for
%hyperbolic systems with finite differences approximations), that contains
%all information of the time domain of the simulation. Furthermore, it is
%used to store the current time, and increase the time step in the
%simulations for loop.
	
	properties (SetAccess = private) % Time can only be defined in the constructor
        % #TODO: was immutable before. Decide if it can stay like this.
		array (1,:) double {mustBe.ascending};						% array of time discretization
	end
		
	properties (SetAccess = protected) % Current time 'now' can only be shifted with 
		nowIdx (1,1) {mustBePositive} = 2; % time starts with index 2,
												% since the first contains the initial values
		isOver (1,1) logical = false;			% flag indicating if simulation is done
	end
	
	properties (Dependent = true)
		now (1,1) double;						% current time
		start (1,1) double;						% start value of time for simulation
		final (1,1) double;						% start value of time for simulation
		duration (1,1) double;					% duration of simulation
		stepSize (1,1) double {mustBePositive};	% time between time steps
		numberOfTimeSteps (1,1) uint8 {mustBePositive}; % number of time steps
	end
	
	methods
		function obj = Time(varargin)
			%% Constructor
			parser = misc.Parser();
			addParameter(parser, 'stepSize', 1e-3);
			addParameter(parser, 'start', 0);
			addParameter(parser, 'duration', 2);
			parser.parse(varargin{:});
			if ~isempty(fieldnames(parser.Unmatched))
				warning('unmatched parameter in input parser');
			end
			misc.struct2ws(parser.Results);
			obj.array = parser.Results.start : parser.Results.stepSize : parser.Results.duration;
		end
		
		function nextStep(obj)
			%% Make a time step by adding 1 to nowIdx
			obj.nowIdx = obj.nowIdx + 1;
			obj.isOver = obj.nowIdx > obj.numberOfTimeSteps;
		end
		
		function value = get.now(obj)
			%% returns current time value in time domain (not the index value)
			value = obj.array(obj.nowIdx);
		end
		
		function value = get.start(obj)
			%% returns first time value in time domain (not the index value)
			value = obj.array(1);
		end
		
		function value = get.final(obj)
			%% returns final / last time value in time domain (not the index value)
			value = obj.array(end);
		end
		
		function value = get.duration(obj)
			%% returns time span of simulation
			value = obj.array(end) - obj.array(1);
		end
		
		function value = get.stepSize(obj)
			%% returns time span of simulation
			value = obj.array(2) - obj.array(1);
		end
		
		function value = get.numberOfTimeSteps(obj)
			%% returns time span of simulation
			value = numel(obj.array);
		end
		
		function value = atIdx(obj, idx)
			%% returns time at the idx-th entry in array
			value = obj.array(idx);
		end
		
		function value = atIdxRelative(obj, idx)
			%% returns time value at the (nowIdx + idx)th entry in array
			value = obj.array(obj.nowIdx + idx);
		end
		
		function reset(obj)
			%% Reset index and flag to start simulation again
			obj.nowIdx = 2;
			obj.isOver = false;
		end
	end
	
end

