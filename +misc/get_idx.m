function idx = get_idx(value,v,ext)
% idx = GET_IDX(OF,IN) returns the closest index of the value "value" in 
% the vector "v". If value is a vector, idx returns the indices belonging 
% to the values of the vector v
%
% idx = get_idx(of,in,ext) allows to find indices for values that lie
% outside of "v", if ext=1
%
% Inputs:
%     value       Values to be found in v
%     v           Vector to be searched
%     ext         Flag in order to find values out of rang of v
% Outputs:
%     idx         Vector containing the respective indices
%
% Example:
% -------------------------------------------------------------------------
% a = [0 1 2 3 4 5 6 7 8 9]';
% values = [3 7];
% get_idx(values,a)
% >> ans = [4 8]
% -------------------------------------------------------------------------

% created on 09/2016 by Simon Kerschbaum
%  * modified on 18.04.2017 by SK:
%    - enable passing of as vector

if nargin<3
	ext=0;
end
% if isvector(value)
% 	idx=zeros(length(value),1); % preallocate
% else
% 	idx=zeros(size(value)); % preallocate
% end
sizVal = size(value);
[~, idxVec] = min(abs(value(:).'-v(:)));
idx = reshape(idxVec,sizVal);
for id = 1:numel(value)
	if ~ext
		% attention: values that are as close to a border value as the
		% maximum difference in "in" are ok!
		if abs(value(id)-v(idx(id))) > max(abs(diff(v)))
			% values outside the range!
			error(['get_idx: value to find does not exist!' ...
							newline 'to find: ' num2str(value(id)) sprintf('\n') 'closest value: ' num2str(v(idx(id))) ...
							sprintf('\n') 'in :' num2str(v(:).')]);
		end
	end
end