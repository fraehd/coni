function xNorm = get_L2_norm(x, varargin)
% GET_L2_norm calculates the quadratic L2-norm 
% xnorm = sqrt(int_domain x.' * x dz)

% x can be an arbitrary dimensional matrix. By default it is assumed, that
% the spatial domain to be integrated is first dimension of x with a
% uniform grid from 0 to 1. For f�rther assumptions on dimensions, see 
% parameter list.
%		   
% INPUT PARAMETERS:
% DOUBLE-ARRAY				  x : variable of which the norm is calculated
% OPTIONAL INPUT PARAMETERS
% VECTOR or CELL-ARRAY of VECTORS
%					 domain : if the spatial domain is not uniform on 
%								[0, 1], the use this parameter to specify
%								the grid. By default it is assumed, that
%								the domain is uniform on [0, 1] with
%								size(x, spatial_dim) grid-points.
% SCALAR		spatial_dim : if the dimension to be integrated is not the
%								first one, then specify the dimension with
%								this parameter. By default it is assumed, 
%								that spatial_dim is the 1st dimension of x.
% SCALAR		 vector_dim : if x contains more dimensions, i.e. if x is
%								time dependent and the norm should be
%								calculated for every time-step, then it is
%								neccessary to define which dimension of x
%								should be used to calculate the product
%								inside the integral. This can be done with
%								this optional parameter. By default it is
%								assumed that vector_dim is the last
%								dimension of x.
%   
% OUTPUT PARAMETERS:
% MATRIX			 x_norm : resulting norm of x
%
% Example:
% -------------------------------------------------------------------------
% syms z real
% x = subs(z^2,'z',0:0.01:1);
% norm = get_L2_norm(x')
% >> norm = 0.4473
% -------------------------------------------------------------------------
%
% required subprogramms:
%   -
%
% global variables:
%   - 
%
% history:
%	-	created on 11.06.2018 by Jakob Gabriel

xSize = size(x);
default_spatial_dim = 1;
default_vector_dim = numel(xSize);

% Read input
p = inputParser;
addParameter(p, 'domain', []);
addParameter(p, 'spatial_dim', default_spatial_dim);
addParameter(p, 'vector_dim', default_vector_dim);
parse(p, varargin{:});

% Check domain
if isempty(p.Results.domain)
	domain = linspace(0, 1, size(x, p.Results.spatial_dim));
else
	domain = p.Results.domain;
end
spatial_dim = p.Results.spatial_dim;
vector_dim = p.Results.vector_dim;

% if x is scalar but both spatial and temporal dependend, size(x) cuts a
% way the "vector_dim" dimension, which would be 1. Hence, this is fixed in
% the following lines:
if numel(xSize) < vector_dim
	xNewSize = ones(1, vector_dim);
	xNewSize(1:numel(xSize)) = xSize;
	xSize = xNewSize;
end

% permute x 
% -> last dimension of x = domain
order_x_permute = 1:1:numel(xSize);
for it = spatial_dim:(numel(xSize)-1)
	order_x_permute_itp1 = order_x_permute(it+1);
	order_x_permute(it+1) = order_x_permute(it);
	order_x_permute(it) = order_x_permute_itp1;
end
% -> 2nd last dimension = vector_dim
for it = vector_dim:(numel(xSize)-2)
	order_x_permute_itp1 = order_x_permute(it+1);
	order_x_permute(it+1) = order_x_permute(it);
	order_x_permute(it) = order_x_permute_itp1;
end
xPermuted = permute(x, order_x_permute);
xPermutedSize = size(xPermuted);

% reshape x -> collect all further dimensions, for instance time, in the
% first array dimension of x_reshaped for efficient parfor
xReshapedNumelFirstDimension = prod(xPermutedSize(1:end-2));
xReshaped = reshape(xPermuted, [xReshapedNumelFirstDimension, ...
	xPermutedSize(end-1), xPermutedSize(end)]);
xNormUnshaped = zeros(xReshapedNumelFirstDimension, 1);
parfor it=1:xReshapedNumelFirstDimension
	% perform scalar product, integration and squareroot
	integrand = diag(permute(xReshaped(it, :, :), [3, 2, 1]) ...
		* permute(xReshaped(it, :, :), [2, 3, 1]));
	xNormUnshaped(it) = sqrt(numeric.trapz_fast(domain, integrand.'));
end

% restore previous dimensions that were previously combined in the first
% dimension of x_reshaped
if numel(xPermutedSize(1:end-2)) < 2
	xNorm = xNormUnshaped;
else
	xNorm = reshape(xNormUnshaped, xPermutedSize(1:end-2));
end

end