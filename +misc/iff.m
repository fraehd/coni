function result = iff(condition,trueResult,falseResult)
% IFF is an inline if function
%
% Description:
% result = iff(condition,trueResult,falseResult)
% Returns either trueResult or falseResult, if condition is TRUE or FALSE
%
% Inputs:
%     condition       Determining condition
%     trueResult      Value to be returned in case condition is TRUE
%     falseResult     Value to be returned in case condition is FALSE
%
% Outputs:
%     result          Either trueRsult or falseResult; see above
%
% Example:
% -------------------------------------------------------------------------
% iff(1 == 0,3,[1 0])
% >> ans = [1 0]
% -------------------------------------------------------------------------
%

  error(nargchk(3,3,nargin));
  if condition
    result = trueResult;
  else
    result = falseResult;
  end