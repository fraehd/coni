function [ adjA ] = adj( A )
% ADJ calculates the adjungate of A - this is not the adjoint!
% 
% Description:
% >> adjA = adj(A) 
% Returns the adjungate matrix with the property A*adj(A)=I*det(A) of A.
%
% See Wikipedia-Artikel: https://en.wikipedia.org/wiki/Adjugate_matrix
%
% Inputs:
% A       Square matrix to be adjungated (symbolic or numeric)
% Outputs:
% adjA    Adjungate of A
%
% Example:
% -------------------------------------------------------------------------
% syms a b c d e f g h k 
% A=[a b c; ...
%    d e f; ...
%    g h k];
% adjA = misc.adj(A)
% >> ans = [ e*k - f*h, c*h - b*k, b*f - c*e]
%          [ f*g - d*k, a*k - c*g, c*d - a*f]
%          [ d*h - e*g, b*g - a*h, a*e - b*d]
% -------------------------------------------------------------------------

%% Calculation of the adjungate matrix
adjA=sym(zeros(size(A)));
for n=1:size(A,1)
   for m=1:size(A,2)
       adjA(m,n)=cofactor(n,m,A);           % By switching indices, the resultant matrix is transposed.
   end
end
end % adj()

% Calculate elements of the cofactor-matrix
function [a] = cofactor(i,j,A)
a=(-1)^(i+j)*det(A([1:i-1, i+1:end],[1:j-1,j+1:end]));
end % cofactor()
