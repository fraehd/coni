function result = isunique(value)
%ISUNIQUE checks all entries of value are unique. This works both for
%double arrays as well as cell-arrays. Example:
% misc.isunique({'bli', 'bla', 'blu'}) -> true
% misc.isunique({'bli', 'bla', 'blu', 'bla'}) -> false
% misc.isunique([1, 1]) -> false
% misc.isunique([0, 1]) -> true

result = numel(unique(value(:))) == numel(value(:));

end

