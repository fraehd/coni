function N = solveVolterraIntegroDifferentialIVP(z, ic, n, m, NameValueArgs)
% MISC.solveVolterraIntegroDifferentialIVP calculates the solution N(z) a volterra
% integro differential initial value problem.
%	N = solveVolterraIntegroDifferentialIVP(z, ic, 'M', M, 'A', A, 'B', B, 'C', C, 'D', D)
%		calculates the desired solution N. With M, A, B, C, D being optional
%		parameter that have to be quantity.Discrete objects.
% The equation to be solved is defined as:
%	M(z) N'(z) + A(z) N(z) + N(z) B(z) + C(z) + int_0^z D(z, zeta) N(zeta) dzeta = 0
%	N(0) = n0 \in R^{nxm}.
% This equation is solved in the form
%	N'(z) = -M(z) \ (A(z) N(z) + N(z) B(z) + C(z) + int_0^z D(z, zeta) N(zeta) dzeta)
%	N(0) = n0 \in R^{nxm}
% by using an Runge-Kutta solver.

arguments
	z (1, 1) quantity.Domain;
	ic (:, :) double;
	n (1, 1) double = size(ic, 1);
	m (1, 1) double = size(ic, 2);
	NameValueArgs.M quantity.Discrete = quantity.Discrete.zeros([n, n], z);
	NameValueArgs.A quantity.Discrete = quantity.Discrete.zeros([n, n], z);
	NameValueArgs.B quantity.Discrete = quantity.Discrete.zeros([m, m], z);
	NameValueArgs.C quantity.Discrete = quantity.Discrete.zeros([n, m], z);
	NameValueArgs.D quantity.Discrete = ...
		quantity.Discrete.zeros([n, n], [z, quantity.Domain("zeta", z.grid)]);
end

% Calculate parameter for use of myOdeFun:
Minv = inv(NameValueArgs.M);
% spatial dimensions are permuted to the end
c1 = permute(on(- Minv * NameValueArgs.A, z), [2, 3, 1]);
c2 = permute(on(- Minv, z), [2, 3, 1]);
c3 = permute(on(NameValueArgs.B, z), [2, 3, 1]);
c4 = permute(on(- Minv * NameValueArgs.C, z), [2, 3, 1]);

if NameValueArgs.D.abs.MAX() >= 1e3*eps
	% integral term needs to be considered
	%	-> more involved and less efficient implementation with for loop.
	c5 = permute(on(- Minv * NameValueArgs.D, [z, quantity.Domain("zeta", z.grid)]), [3, 4, 1, 2]);
	c5b = permute(misc.diagNd(c5, [3, 4]), [2, 3, 1]);

	% Init result
	Ndouble = zeros(n, m, z.n);
	Ndouble(:, :, 1) = ic;

	% Initial value of integral
	%	c4a = int_0^0(zIdx-1) c5(z, zeta) N(zeta) dzeta = dz c5(0, 0) N(0) = 0
	c5a = 0 * (z.grid(2) - z.grid(1)) * c5(:, :, 1, 1) * ic;

	% Simulate
	for zIdx = 2 : z.n
		NdoubleOld = Ndouble(:, :, zIdx-1);
		[~, myN] = ode45(...
					@(zTemp, NTemp) odeWithIntegral( ...
						zTemp, NTemp, z.grid(zIdx-1), n, m, c1(:, :, zIdx), c2(:, :, zIdx), c3(:, :, zIdx), c4(:, :, zIdx), c5a, ...
						c5b(:, :, zIdx)), ...
					z.grid(zIdx - [1; 0]).', ...
					NdoubleOld(:));
		Ndouble(:, :, zIdx) = reshape(myN(end, :), [n, m]);
		% Calculate integral for next iteration:
		%	c4a = int_0^z(zIdx) c5(z, zeta) N(zeta) dzeta
		integrand = misc.mTimesPointwise(...
								permute(c5(:, :, zIdx, 1:zIdx), [4, 1, 2, 3]), ...
								permute(Ndouble(:, :, 1:zIdx), [3, 1, 2]));
		c5a = permute(numeric.trapz_fast_nDim(z.grid(1:zIdx), integrand), [2, 3, 1]);
	end % for zIdx = 2 : z.n

	N = quantity.Discrete(permute(Ndouble, [3, 1, 2]), z, 'name', "N");
	for it = 1 : 2
		% improve by result by successive approximation for which 
		%	Cnew = C(z) + int_0^z D(z, zeta) N(zeta) dzeta
		% is used.
		N = misc.solveVolterraIntegroDifferentialIVP(z, ic, ...
			'M', NameValueArgs.M, 'A', NameValueArgs.A, 'B', NameValueArgs.B, ...
			'C', NameValueArgs.C + cumInt(NameValueArgs.D * N.subs("z", "zeta"), "zeta", 0, "z"));
	end
else
	% the integral term can be neclegted, hence the solution is easily obtained with
	% an ode solver directly.
	c1Interp = numeric.interpolant({1:n, 1:n, z.grid}, c1);
	c2Interp = numeric.interpolant({1:n, 1:n, z.grid}, c2);
	c3Interp = numeric.interpolant({1:m, 1:m, z.grid}, c3);
	c4Interp = numeric.interpolant({1:n, 1:m, z.grid}, c4);
	[~, myN] = ode45(...
				@(zTemp, NTemp) odeWithOutIntegral( ...
					zTemp, NTemp, n, m, c1Interp, c2Interp, c3Interp, c4Interp), ...
				z.grid.', ...
				ic(:));

	N = quantity.Discrete(reshape(myN, [z.n, n, m]), z, "name", "N");
	
end

end % solveVolterraIntegroDifferentialIVP()

function dN = odeWithIntegral(zNow, N, zOld, n, m, c1, c2, c3, c4, c5a, c5b)
	% odeWithIntegral is the helper function for using the ode45 solver. It calculates
	%	dN	= -M(z) \ (A(z) N(z) + N(z) B(z) + C(z) + int_0^z D(z, zeta) N(zeta) dzeta)
	%		= -M(z) \ A(z) N(z) ...
	%			- M(z) \ N(z) B(z) ...
	%			- M(z) \ C(z)...
	%			- int_0^z M(z) \ D(z, zeta) N(zeta) dzeta
	%		= c1(z) N(z) ...
	%			+ c2(z) N(z) c3(z) ...
	%			+ c4(z) ...
	%			+ int_0^z c5(z, zeta) N(zeta) dzeta
	% The integral int_0^z c5(z, zeta) N(zeta) dzeta is split in two parts:
	%	 int_0^z c5(z, zeta) N(zeta) dzeta ...
	%			= int_0^z(zIdx-1) c5(z, zeta) N(zeta) dzeta ...
	%				+ dz * c5(z(zIdx), z(zIdx)) N(zIdx)
	%			= c5a + c5b(z) N(z)
	
	N = reshape(N, [n, m]);
	dN = c1 * N ...		
		+ c2 * N * c3 ... 			
		+ c4 ...
		+ c5a + (zNow - zOld) * c5b * N;
	dN = dN(:);
end % odeWithIntegral()

function dN = odeWithOutIntegral(z, N, n, m, c1Interp, c2Interp, c3Interp, c4Interp)
	% odeWithOutIntegral does the same as odeWithIntegral, but it uses interpolants.
	N = reshape(N, [n, m]);
	dN = c1Interp.evaluate(1:n, 1:n, z) * N ...		
		+ c2Interp.evaluate(1:n, 1:n, z) * N * c3Interp.evaluate(1:m, 1:m, z) ...
		+ c4Interp.evaluate(1:n, 1:m, z);
	dN = dN(:);
end % odeWithOutIntegral()