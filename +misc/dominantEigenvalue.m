function dominantEigenvalue = dominantEigenvalue(A)
% dominantEigenvalue returns the eigenvalue of A with the largest real value. If
% there are multiple eigenvalues with the same real value, then the one with the
% largest absolute imaginary value is returned. A can be a vector of eigenvalues or a
% system matrix.
%% example:
%	misc.dominantEigenvalue(diag([2+i, 3+2*i, -1*10*i, 3+2*i]))
%	ans = 3.0000 + 2.0000i

% get all eigenvalues
if isvector(A)
	eigenValues = A;
else
	eigenValues = eig(A);
end

% find largest realpart of all eigenvalues
maxRealEigenvalue = max(real(eigenValues));

% select all eigenvalues with that real part
maxRealEigenvalues = eigenValues(real(eigenValues) == maxRealEigenvalue);

% find the largest absolute imaginary part of those eigenvalues
maxRealImagEigenvalue = max(abs(imag(maxRealEigenvalues)));

% combine max real part and the max imaginary part
dominantEigenvalue = maxRealEigenvalue + 1i*maxRealImagEigenvalue;

end