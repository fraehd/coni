function value = ensureIsCell(value)
%ENSUREISCELL ensures that the value is a cell.
% c = ensureIsCell(value) checks if value is a cell. If it is not a cell,
% it is converted as a cell.

if ~iscell(value)
	value = {value};
end	
	
end

