classdef simulation_parameters
	%SIMULATION_PARAMETERS 
    %   Summary of all simulation properties. Parameters can be determined
    %   by name-value-pairs while creating an object of this type.
	
	% created by Simon Kerschbaum 18.02.2016
	
	properties
		tsmax = 1e-2;  % max. Schrittweite variable-step solver
		frel=1e-5;     % rel. Toleranz variable-step solver
		fabs=1e-4;     % abs. Toleranz variable-step solver
		ts = 1e-3;     % Schrittweite fixed-step solver
		tStart = 0;    % simulation starts at t = tStart
		tend = 10;     % Simulationsdauer
	end
	
	methods
		function obj = simulation_parameters(varargin)
			found = 0;
			arglist = properties(obj);
			if ~isempty(varargin)
				if mod(nargin, 2) % uneven number
					error('When passing additional arguments, you must pass them pairwise!')
				end
				for index = 1:2:nargin % loop through all passed arguments:
					for arg = 1:length(arglist)
						if strcmp(varargin{index}, arglist{arg})
							obj.(arglist{arg}) = varargin{index+1};
							found=1;
							break
						end 
					end % for arg
					% argument wasnt found in for-loop
					if ~found
						error([varargin{index} ' is not a valid property of class ppide_simulation!']);
					end
					found=0; % reset found
				end % for index
			end % if nargin > numfix
		end
	end
	
end

