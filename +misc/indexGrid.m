function myGrid = indexGrid(mySize)
% INDEXGRID creates a cell array of grid vectors. For every element of the mySize integer array one
% cell contain 1 : 1 : mySize(it) is created.
%% Example
% myGrid = misc.indexGrid([1, 2, 3]);
% myGrid{:}
% ans =
%      1
% ans =
%      1     2
% ans =
%      1     2     3

myGrid = cell(numel(mySize), 1);
for it = 1 : numel(myGrid)
	myGrid{it} = 1 : 1 : mySize(it);
end
end % indexGrid()