classdef ProgressBar < handle
	%   PROGRESSBAR Shows the progress of a loop in the matlab terminal
	%   This class can be used to generate objects that enable the
	%   monitoring of the progress of a program or loop in the matlab
	%   terminal. Actually it is not a progress bar, because it only shows
	%   a percentage value for the progress.
	%
	%   The properties can be assigned by name-value-pairs at the
	%   initialization. The progress is shown in form:
	%       *name* *percent* [Elapsed time ... seconds ]
	%	or alternativly
	%       *name* *currentIteration* of *terminalValue* [Elapsed time seconds ].
	%	By using additional input in start() and raise(), data in each
	%	iteration can be stored.
	%
	%   Example:
	%   --------------------------
	%   progress = misc.ProgressBar('name', '<test>', 'terminalValue', 4321);
	%   progress.start();
	%   for k=1:4321, progress.raise(k), pause(0.002), end
	%   progress.stop();
	%   --------------------------
	%
	%   example for the usage of the ProgressBar in a parfor:
	%   --------------------------
	%     N=100
	%     pbar = misc.ProgressBar('name', 'diff.Parametrization', 'steps', N);
	%     pbar.start();
	%
	%     q = parallel.pool.DataQueue;
	%     afterEach(q, @pbar.raise);
	%
	%     parfor i = 1:N
	%     pause(0.02)
	%     send(q,[]);
	%     end
	%
	%     pbar.stop();
	%   --------------------------
	
	
	properties
		% Name of the procedure for which the progress should be shown.
		% This is the text that is shown in front of the progress value.
		name (1, 1) string = "...";
		
		% Current progress state of the procedure as absolute value.
		progress = 0;
		
		% Number of steps in which the progres percentage should be raised.
		% If the progress that should be monitored has many iteration (>>
		% 100) to the show the progress in each iteration can slow down the
		% loop. Thus, raising the progress only *steps*-times helps.
		steps = 100;
		
		% Maximal value of the progress values, i.e., the number of
		% required iterations that the loop terminates. Or an end value
		% that shows that the progress is completed.
		terminalValue = 100;
		
		% Timer that is started with the beginning of the progress and is
		% used to monitor the required time of the progress.
		timer;
		
		% TODO Documentation
		initialStart (1, 1) logical = false;
		
		% logical to mute ProgressBar
		silent (1, 1) logical = false;
		
		% if true the absolute value of progress is shown, if false the
		% relative value in percent.
		printAbsolutProgress (1, 1) logical = false;
		
		% data store can be used to store intermediate results in every
		% iteration by passing further inputs to raise()
		dataStore cell;
	end
	properties (Access = protected)
		% (protected) step size in which the progress should be counted
		step = [];
		reversemsg = '';        % (protected) last printed message
		
	end
	
	methods
		function obj = ProgressBar(varargin)
			%PROGRESS New ProgressBar, the properties can be assigned by
			%name-value-pairs
			for arg=1:2:length(varargin)
				obj.(varargin{arg}) = varargin{arg + 1};
			end
			
			if obj.initialStart == true
				obj.start();
			end
		end
		
		function raise(obj, currentValue, varargin)
			%RAISE raise the progress counter and if desired show the
			% progress
			
			if nargin == 1 || isempty(currentValue) || currentValue > obj.progress
				obj.progress = obj.progress + obj.step;
				if nargin > 2
					myParser = misc.Parser();
					myParser.addParameter('addMessage', '');
					myParser.addParameter('value', {});
					myParser.parse(varargin{:});
					obj.dataStore{end+1} = myParser.Results;
					obj.dataStore{end}.time = toc(obj.timer);
					if ~obj.silent
						obj.show(obj.dataStore{end}.addMessage);
					end
				elseif ~obj.silent
					obj.show();
				end
			end
			
		end
		
		function show(obj, addMessage)
			%SHOW print the progress value to the command line
			if nargin == 1
				addMessage = '';
			end
			if ~obj.printAbsolutProgress
				percentDone = obj.progress / obj.terminalValue * 100;
				msg = sprintf(['%3.1f ', addMessage], min(percentDone, 100));
			elseif obj.printAbsolutProgress
				msg = sprintf(['%d of max. %d ', addMessage], obj.progress, obj.terminalValue);
			end
			fprintf([obj.reversemsg, msg '\n']);
			obj.reversemsg = repmat(sprintf('\b'), 1, length(msg)+1);
			
		end
		
		function start(obj, varargin)
			%START initialization of the ProgressBar
			obj.step =  obj.terminalValue / obj.steps;
			obj.timer = tic;
			obj.dataStore = {};
			if nargin > 1
				myParser = misc.Parser();
				myParser.addParameter('addMessage', '');
				myParser.addParameter('value', {});
				myParser.parse(varargin{:});
				obj.dataStore{1} = myParser.Results;
				obj.dataStore{1}.time = 0;
				if ~obj.silent
					obj.reversemsg = '';
					fprintf(string(obj.name) + " ");
					obj.show(obj.dataStore{1}.addMessage);
				end
			elseif ~obj.silent
				obj.reversemsg = '';
				fprintf(obj(1).name + " ");
				obj.show();
			end
		end
		
		function stop(obj, varargin)
			%STOP terminate the ProgressBar
			if ~obj.silent
				fprintf(' Elapsed time %3.2f seconds \n', toc(obj.timer));
			elseif obj.silent
				evalc('toc(obj.timer)');
			end
			if nargin > 1
				obj.dataStore{end+1} = struct(varargin{:});
				obj.dataStore{end}.time = toc(obj.timer);
			end
		end
		
	end
end
