function I = interp_mat(z_old,z_new)
% interp_mat get interpolation matrix for 1D-Interpolation
%
% Description:
% I = interp_mat(z_old,z_new)
% Get an interpolation matrix, such that Yq = I*Y, while Y(z_old) and 
% YQ(z_new). z_old and z_new need to be grids which are sorted ascendingly 
% and need to fulfill z_new(1) >= z_old(1) and z_new(end) <= z_old(end) 
% (no extrapolation is used!)
%
% Inputs:
%     z_old       Old discretization grid
%     z_new       New discretization grid
%
% Outputs:
%     I           Interpolation matrix
%
% Example:
% -------------------------------------------------------------------------
% syms z real   
% z_old = linspace(0,1,10)';
% a = subs(z^2,'z',z_old);
% I = interp_mat(z_old,linspace(0,1,100));
% a_new = I*a
% >> a_new = [0 1/891 2/891 ... 874/891 1]
% -------------------------------------------------------------------------
%
%

% created on 15.03.2018 by Simon Kerschbaum

% TODO:
%  - extrapolation
%  - unsorted grids (first sort with saving of indices)
%  - maybe: nonlinear interpolation

% make sure grids are row vectors:
z_old = z_old(:).';
z_new = z_new(:).';

if z_old(1)>z_new(1)
	error('z_new(1) >= z_old(1) is not fulfilled!')
end
if z_old(end) < z_new(end)
	error('z_new(end) <= z_old(end) is not fulfilled!')
end
if any(sort(z_old)-z_old)
	error('z_old is not ascending!')
end
if any(sort(z_new)-z_new)
	error('z_new is not ascending!')
end
ndisc_orig = length(z_old);
ndisc_new = length(z_new);

% Fuer Punktmessung wird linear zwischen den beiden naechstgelegenen
	% Punkten interpoliert!
I = zeros(ndisc_new,ndisc_orig);
for z_idx = 1:ndisc_new
	idx_up = find(z_old>=z_new(z_idx),1);
	idx_down = length(z_old) - find(fliplr(z_old)<=z_new(z_idx),1)+1;
	
	z_up = z_old(idx_up);
	z_down = z_old(idx_down);
	
	diff_z_up = z_up-z_new(z_idx);
	diff_z_down = z_new(z_idx) - z_down;
	diff_ges = z_up-z_down;
	if diff_ges == 0 % es wurde genau ein Abtastpunkt erwischt!
		I(z_idx,idx_up) = 1;
	else
		I(z_idx,idx_down)=diff_z_up/diff_ges;
		I(z_idx,idx_up)=diff_z_down/diff_ges;
	end
end

end

