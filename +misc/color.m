function rgb = color(idx, NameValue)
% misc.color returns the default color that matlab plot would use of the idx-th plot
% Example:
%	figure(); 
%	for idx = 1 : 12
%		subplot(2, 1, 1); plot([0, 1], idx*[1, 1], "Color", misc.color(idx)); hold on;
%		subplot(2, 1, 2); plot([0, 1], idx*[1, 1], "Color", misc.color(idx, "colormap", "viridis")); hold on;
%	end
arguments
	idx (:, 1) double;
	NameValue.colormap (1, 1) string = "default"
	NameValue.span (1, 1) double = 7;
end

if strcmp(NameValue.colormap, "default")
	if NameValue.span > 7
		warning("span for default color set must be less equal than 7, as only 7 colors are " ...
			+ " defined. Hence the span is reset to 7.")
	end
	idx = mod(idx-1, min(NameValue.span, 7))+1;
	rgb = zeros(numel(idx), 3);
	for it = 1 : numel(idx)
		if idx == 1
			rgb(it, :) = [0 0.4470 0.7410];
		elseif idx == 2
			rgb(it, :) = [0.8500 0.3250 0.0980];
		elseif idx == 3
			rgb(it, :) = [0.9290 0.6940 0.1250];
		elseif idx == 4
			rgb(it, :) = [0.4940 0.1840 0.5560];
		elseif idx == 5
			rgb(it, :) = [0.4660 0.6740 0.1880];
		elseif idx == 6
			rgb(it, :) = [0.3010 0.7450 0.9330];
		elseif idx == 7
			rgb(it, :) = [0.6350 0.0780 0.1840];
		end
	end % for it = 1 : numelIdx
elseif strcmp(NameValue.colormap, "viridis")
	idx = mod(idx-1, NameValue.span)+1;
	thisViridis = viridis(NameValue.span);
	rgb = thisViridis(idx, :);
else
	error("colormap not supported")
end

end % color()