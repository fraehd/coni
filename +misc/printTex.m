function returnString = printTex(data, NameValue)
% misc.printTex prints string-array input that contains tex math text to command
% window
% Example:
% -------------------------------------------------------------------------
% misc.printTex(["\dot{x}(t) &= A x(t) + B u(t)"; "y(t) &= C x(t)"])
% -------------------------------------------------------------------------
%	See also misc.variables2tex and misc.latexChar

arguments
	data (:, 1) string
	NameValue.align = true;
end

data = strrep(data, "\", "\\");

if NameValue.align
	myString = "\n\\begin{align*}";
else
	myString = string();
end
for it = 1 : numel(data)
	if it < numel(data)
		myString = myString + "\n" + data(it) + "\\\\";
	else
		myString = myString + "\n" + data(it);
	end
end
if NameValue.align
	myString = myString + "\n\\end{align*}\n";
end
if nargout > 0
	returnString = sprintf(myString);
else
	fprintf(myString);
end
	
end % printTex()