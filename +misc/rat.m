function [approx] = rat(X, tol)
%RAT_K Rational approximation.
%   approx = RAT(X,tol) returns a cell with the rational approximations of
%   real valued X. 
% X is approximated by
%
%                              1
%         d1 + ----------------------------------
%                                 1
%              d2 + -----------------------------
%                                   1
%                   d3 + ------------------------
%                                      1
%                        d4 + -------------------
%                                        1
%                             d5 + --------------
%                                           1
%                                  d6 + ---------
%                                             1
%                                       d7 + ----
%                                             d8
%
%	in order to chose an approximation of an required tolerance, all
%	approximation steps will be returned. So APPROX is a cell of the same
%	dimension as X. Each entry in X is a struct-array with the fields
%	N for the nominator, D for the denominator, j for the index in X and
%	error for the absolute error of this approximation. 
%	For further details, see matlab function rat.
%	
%	see rat

if nargin < 2
    tol = 1.e-6*norm(X(isfinite(X)),1);
end

assert( all( isreal(X(:)) & isfinite(X(:))));

for j = 1:numel(X)
	k = 0;
	C = [1 0; 0 1];
	x = X(j);
	while 1
		k = k+1;
		d = round(x);
		
		x = x - d;
		C = [C*[d;1] C(:,1)];
		
		xError = abs(C(1,1)/C(2,1) - X(j));

		o.N = C(1,1)/sign(C(2,1));
		o.D = abs(C(2,1));
		o.j = j;
		o.error = xError;
		
		rationals(k) = o;		
		
		if (x==0) || (xError <= max(tol,eps(X(j))))
			break
		end
		x = 1/x;
	end
	approx{j} = rationals(:);
end
end

