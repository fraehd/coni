function newStruct = structMerge(myStructs, NameValue)
% misc.structMerge merges all structs to one struct.
%
%	newStruct = misc.StructMerge(struct1, struct2, ...) merges all fields of all structs struct1,
%		struct2, ... into newStruct. An error is thrown, if structs contain same fields that would
%		be overwritten.
%%
% Example
%		misc.structMerge(struct("asdf", 42), struct("blub", 1337));
arguments (Repeating)
	myStructs struct;
end
arguments
	NameValue.noOverwrite (1, 1) logical = false;
end
if nargin == 0
	newStruct = struct();
else
	newStruct = myStructs{1};
	for it = 2 : numel(myStructs)
		tmpFieldnames = misc.fieldnames(myStructs{it});
		for tmpFieldname = tmpFieldnames.'
			if NameValue.noOverwrite
				if contains(misc.fieldnames(newStruct), tmpFieldname)
					error("misc:structMerge:overwriteField", ...
						"setting " + tmpFieldname + " will overwrite an existing field")
				end
			end
			newStruct = misc.setfield(...
				newStruct, tmpFieldname, misc.getfield(myStructs{it}, tmpFieldname));
		end % for tmpFieldname = tmpFieldnames.'
	end % for it = 2 : numel(myStructs)
end % if nargin == 0
end % misc.structMerge()