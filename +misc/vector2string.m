function s = vector2string(v)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
arguments
	v double;
end

if isrow( v )
	s = string( sprintf('[%s\b]', sprintf('%g ', v)) );
elseif iscolumn( v )
	s = string( sprintf('[%s\b]^T', sprintf('%g ', v)) );
elseif ismatrix( v )
	error("Not yet implemented")
end

end

