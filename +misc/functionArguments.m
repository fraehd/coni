function names = functionArguments(func)
%FUNCTIONARGUMENTS extracts the arguments of a function handle
%	names = misc.functionArguments(func) returns the arguments of the function handle func as the
%	string array names
%% Example
% names = functionArguments(@(z, zeta, t) z*zeta+t)

names = split(string(regexp(func2str(func), "(?<=@\().*(?=\))", "match")), ",");
functionArguments = extractBetween(func2str(func), "@(", ")");
names = split(string(functionArguments), ",");


end

