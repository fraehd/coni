function [ inverseKernel ] = invertBacksteppingKernel( kernel, signOfIntegralTermInverse)
%INVERT_KERNEL Compute the inverse backstepping kernel using the reciprocity
%relation and fixpoint iteration
%  
% INPUT PARAMETERS:
%	4D-DOUBLE-ARRAY			   kernel : kernel to be inverted. 4-dimensional
%										array with indices (z, zeta, rows, columns)
%	DOULBE (-1 or 1)	signOfIntegralOfInvertedTransformation :
% 								to consider the change of the sign of the 
%								integral term in the backstepping 
%								transformation, this input is used. If the
%								backstepping transformation of the input
% 								"kernel" is of the type 
%								T[x] = x - int_0^z kernel x dzeta
% 								then its inverse usually uses
% 								T_i[x] = x + int_0^z inverseKernel x dzeta.
% 								In this case use 
%								signOfIntegralOfInvertedTransformation = +1
%								else if the transformations are of the type
%								T[x] = x + int_0^z kernel x dzeta
% 								and
% 								T_i[x] = x - int_0^z inverseKernel x dzeta
%								then one must use
%								signOfIntegralOfInvertedTransformation = -1
% OUTPUT PARAMETERS:
%	4D-DOUBLE-ARRAY		inverseKernel : inverted kernel. 4-dimensional
%										array with indices (z, zeta, rows, columns)
%										and same size as input

zdisc = linspace(0, 1, size(kernel, 1));
use_F_handle = @(inverseKernel) use_F_K(kernel, inverseKernel, zdisc, signOfIntegralTermInverse);
inverseKernel = fixedPointIteration(kernel, use_F_handle, ...
	'tolerance', 1e-6, 'iterationLimit', 200, 'silent', true);

end % invertBacksteppingKernel()


function myData = fixedPointIteration(initialData, myFunction, varargin)
	myParser = misc.Parser();
	myParser.addParameter('tolerance', 1e-6);
	myParser.addParameter('iterationLimit', 200);
	myParser.addParameter('silent', true);
	myParser.parse(varargin{:});
	
	dataOld = initialData;
	progress = misc.ProgressBar('name', 'Successive Calculation', ...
		'steps', myParser.Results.iterationLimit, ...
		'terminalValue', myParser.Results.iterationLimit, ...
		'printAbsolutProgress', true, ...
		'silent', myParser.Results.silent);
	progress.start();
	for it = 1 : progress.steps
		myData = initialData + myFunction(dataOld);

		changeOfThisIteration = max(abs(myData(:)-dataOld(:)));
		if changeOfThisIteration < myParser.Results.tolerance
			break;
		end
		dataOld = myData;
		progress.raise(it, 'addMessage', ...
			['change = ', num2str(changeOfThisIteration)]);
	end
	progress.stop();
end % fixedPointIteration()

function FK = use_F_K(kernel, inverseKernel, zdisc, signOfIntegralOfInvertedTransformation)
	n = size(kernel, 3);
	kernelPermuted = permute(kernel, [3, 4, 1, 2]);
	inverseKernelPermuted = permute(inverseKernel, [3, 4, 1, 2]);
	FK(1:length(zdisc), 1:length(zdisc), 1:n, 1:n) = 0; % preallocation
	for zIdx = 1:length(zdisc)
		for zetaIdx = 1:zIdx
			zeta_s = zdisc(zetaIdx:zIdx);
			temp_prod = zeros(1+zIdx-zetaIdx, n, n);
			for eta_idx = 1:1+zIdx-zetaIdx
				temp_prod(eta_idx,:,:) = ...
					kernelPermuted(:, :, zIdx, zetaIdx+eta_idx-1) ...
						* inverseKernelPermuted(:, :, zetaIdx + eta_idx - 1, zetaIdx);
			end

			FK(zIdx, zetaIdx, :, :) = signOfIntegralOfInvertedTransformation ...
								* numeric.trapz_fast_nDim(zeta_s, temp_prod);
		end
	end
end % use_F_K()

% function FK = use_F_K_alt(Kappa,KI,zdisc)
% % old, slower implementation from simon
% 	import misc.*
% 	import numeric.*
% 	n = size(KI,3);
% 	FK(1:length(zdisc),1:length(zdisc),1:n,1:n) = 0; % preallocate
% 	for z_idx = 1:length(zdisc)
% 		for zeta_idx =1:length(zdisc)
% 			if zeta_idx > z_idx
% 				FK(z_idx,zeta_idx,:,:) = NaN;
% 			else
% 				zeta_s = zdisc(zeta_idx:z_idx);
% 				clear temp_prod
% 				for zetas_idx = 1:length(zeta_s)
% 					temp_prod(zetas_idx,:,:) = reduce_dimension(Kappa(z_idx,zeta_idx+zetas_idx-1,:,:))*reduce_dimension(KI(zeta_idx + zetas_idx - 1,zeta_idx,:,:));
% 				end
% 				for i=1:n
% 					for j=1:n
% 						FK(z_idx,zeta_idx,i,j) = trapz_fast(zeta_s,...
% 										  temp_prod(:,i,j).');
% 						if isnan(temp_prod)
% 							test = 1;
% 						end
% 					end
% 				end
% 			end
% 		end
% 	end
% end