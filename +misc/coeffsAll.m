function [ coefficients ] = coeffsAll( p, s )
% COEFFSALL coefficients of a polynomial
%
% [ coefficients ] = coeffsAll( p, s )
% Contrary to the standard coeffs function, this function returns all
% coefficients, also if they are zero. The result vector contains the
% coefficients from s^0 to the highest degree s^n. The index for the
% coefficient corresponds to the exponent+1:
% index of c2*s^2 is 3.
%
% Inputs:
% p               Symbolic polynomial
% s               Symbolic variable used in p (i.e. p = p(s))
% Outputs:
% [coefficients]  Vector containing the coefficients of p(s) starting at 
%                   index 1 (s^0) up to index n+1 (s^n)
%
% Example:
% -------------------------------------------------------------------------
% syms s
% p = 3 * s^3 + 2 * s^2 + 1;
% misc.coeffsAll(p, s)
% >> ans = [ 1, 0, 2, 3]
% -------------------------------------------------------------------------
varsChanged=false;

if exist('symengine', 'file')
    
    [coefficients, numpow] = coeffs(expand(sym(p)), s);
    num = sym(sym2poly(sum(numpow)));
    num(num~=0) = coefficients;
    coefficients=num*fliplr(eye(size(num,2)));
    
else
    if length(symvar(char(p)))>1
        if findsym(p,1) ~= s
            syms tmp1
            p=subs(p, [findsym(p,1) s], [tmp1 findsym(p,1)]);
            varsChanged=true;
        end
    elseif ~strcmp(symvar(char(p)), char(s))
        coefficients=p;
        return;
    end
    
    coefficients = sym2poly(sym(p));
    
    if isempty(coefficients)
        coefficients=0;
    else
        coefficients=coefficients*fliplr(eye(size(coefficients,2)));
    end
    
    if varsChanged
        coefficients=subs(coefficients, tmp1, findsym(p,1));
    end
    
end



% [coefficients, numpow] = coeffs(expand(sym(poly)), s);
% num = sym(sym2poly(sum(numpow)));
% num(find(num~=0)) = coefficients;
% num=num*fliplr(eye(size(num,2)));

% [~, numpow] = coeffs(expand(poly), s);
% num = sym(sym2poly(sum(numpow)));
% num = num*fliplr(eye(size(num,2)));
% num(find(double(num))) = coeffs(expand(poly), s);