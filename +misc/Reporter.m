classdef Reporter < handle
	%UNTITLED Summary of this class goes here
	%   Detailed explanation goes here
	
	properties
		showMessage (1,1) logical;
		messages;
	end
	
	methods
		function obj = Reporter(optArgs)
			arguments
				optArgs.showMessage (1,1) logical = true;
			end
			obj.showMessage = optArgs.showMessage;
			obj.messages = {};
		end
		function m = report(obj, message)
			arguments
				obj
				message (1,1) misc.Message
			end
			if obj.showMessage
				fprintf(message.print);
			end
			
			obj.messages = [obj.messages(:)', {message}];
			
		end
		function printMessages(obj)
			for k = 1:numel(obj.messages)
				fprintf(obj.messages{k}.print);
			end
		end
	end
end

