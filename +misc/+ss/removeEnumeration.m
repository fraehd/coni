function unenumeratedNames = removeEnumeration(enumeratedNames, makeUnique)
% REMOVEENUMERATION removes braces with numbers from every string that is an
% element of the array enumeratedNames. This is for instance useful, to deal
% with InputName or OutputName properties of large state-space models (ss).
%
% unenumerateNames = removeEnumeration(enumeratedNames) returns the
%  enumeratedNames string-array without enumeration.
%
% unenumerateNames = removeEnumeration(enumeratedNames, true) returns enumeratedNames
%  without enumeration, but only unique names. The order of names is not changed.
%
% Examples:
% misc.ss.removeEnumeration(["a(1)", "a(2)", "b"])
% misc.ss.removeEnumeration(["a(1)", "a(2)", "b"], true)
arguments
	enumeratedNames (:, 1) string;
	makeUnique (1, 1) logical = false;
end

unenumeratedNames = enumeratedNames;
enumeratedIndex = endsWith(enumeratedNames, ")") & contains(enumeratedNames, "(");
if any(enumeratedIndex)
	splittedEnumeratedNames = split(enumeratedNames(enumeratedIndex), "(", 2);
	unenumeratedNames(enumeratedIndex) = splittedEnumeratedNames(:, 1);
end
	

if makeUnique
	unenumeratedNames = unique(unenumeratedNames, "stable");
end
end % removeEnumeration