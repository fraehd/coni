function myStateSpace = setSignalName(myStateSpace, signalType, signalNames, signalLength)
% SETSSSIGNALNAME sets the property 'OutputName' or 'InputName' of the ss 
% myStateSpace to signalNames. Therein, signalLength how many output elements belong 
% to a signalName. Wheather the InputName or the OutputName properties should be set
% is determined by signalType \in {'input', 'output'}
%
% Inputs:
%	myStateSpace	state space whoms OutputName should be modified
%	signalType		must be either 'input' or 'output' and determines of OutputName
%					or InputName are set
%	signalNames		cell array of signal names that should be applied to
%					myStateSpace.OutputName
%	signalLength	cell array of same size as signal names. Each element defines the
%					length of the signal that is named in signal names
% Outputs:
%	myStateSpace	the state space with modified parameters
%
% Example:
% -------------------------------------------------------------------------
% 	mySimulationModel = ss(1, 1, [1; 2; 3], []);
%	mySimulationModel = misc.ss.setSignalName(mySimulationModel, "output", ["a", "b"], [2, 1]);
% -------------------------------------------------------------------------

arguments
	myStateSpace ss;
	signalType (1, 1) string;
	signalNames (:, 1) string;
	signalLength (:, 1) double;
end

% input checks
if strcmp(signalType, "input")
	assert(size(myStateSpace, 2) == sum(signalLength), "signalLength does not fit to state space");
elseif strcmp(signalType, "output")
	assert(size(myStateSpace, 1) == sum(signalLength), "signalLength does not fit to state space");
else
	error("signalType must be 'input' or 'output'");
end
assert(numel(signalNames) == numel(signalLength), "signalNames must have same number as signalLength");

signalNames = signalNames(signalLength>0);
signalLength = signalLength(signalLength>0);
% create name names
myNewSignalNames = signalNames(1);
if signalLength(1) > 1
	myNewSignalNames = myNewSignalNames + "(" + string((1:1:signalLength(1)).') + ")";
end
for it = 2 : numel(signalNames)
	if signalLength(it) > 1
		myNewSignalNames = [myNewSignalNames; ...
			signalNames(it) + "(" + string((1:1:signalLength(it)).') + ")"];
	else
		myNewSignalNames = [myNewSignalNames; signalNames(it)];
	end
end

% set property
if strcmp(signalType, "input")
	myStateSpace.InputName = myNewSignalNames;
else
	myStateSpace.OutputName = myNewSignalNames;
end
end % misc.ss.setSignalName()