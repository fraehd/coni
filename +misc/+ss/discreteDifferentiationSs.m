function myStateSpace = discreteDifferentiationSs(numberSignal, inputName, ...
															stepSize, varargin)
% misc.ss.discreteDifferentiationSs implements numerical differentiation for run time
% use in simulations by implementing the differentiation
%	y_t = (y(t) - y(t-)) / stepSize
% via a time-discrete state-space (see >> doc ss ).
%
% Inputs:
%	numberSignal	number of differentiation signals
%	inputName		name of signal to be differentiated
%	stepSize		spacing between time steps of simulation
% Optional Input:
%	outputName		name of differentiated signal
%
% Outputs:
%	myStateSpace	the state space that differentiates input of signalName
%
% Example:
% -------------------------------------------------------------------------
% exampleSs = ss(0.8, 1, 1, [], 1e-3, 'OutputName', 'y', 'InputName', 'u');
% diffSs = misc.ss.discreteDifferentiationSs(1, 'y', 1e-3, ...
%			'outputName', 'dydt');
% simSs = misc.ss.connect({'u'}, {'y', 'dydt'}, exampleSs, diffSs);
% [simOutput, t, ~] = initial(simSs, [1; 1], 0:1e-3:50*1e-3);
% simData = misc.ss.simulationOutput2Quantity(simOutput, t, simSs.OutputName);
% plot([simData.y.diff(1, 't'); simData.dydt])
% -------------------------------------------------------------------------

% input reading
myParser = misc.Parser();
myParser.addParameter('outputName', [inputName, '_t'], @(v) ischar(v));
myParser.parse(varargin{:});

% input checks
assert(isnumeric(numberSignal));
assert(isnumeric(stepSize) && isscalar(stepSize));
assert(ischar(inputName), 'inputName must be a character-array or string');

% create time discrete signal model that represents the time derivative described
% above
myStateSpace = ss(...
	zeros(numberSignal), ...			% A
	eye(numberSignal), ...				% B
	- eye(numberSignal)/stepSize, ...	% C
	eye(numberSignal)/stepSize, ...		% D
	stepSize);							% Ts

% set signal names
myStateSpace = misc.ss.setSignalName(myStateSpace, 'input', {inputName}, {numberSignal});
myStateSpace = misc.ss.setSignalName(myStateSpace, 'output', ...
		{myParser.Results.outputName}, {numberSignal});
end