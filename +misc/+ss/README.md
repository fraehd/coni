# misc.ss
This folder misc.ss contains functions that are specifically designed for use with state-space models with the Control System Toolbox. For more Information on this, type "doc ss" in your Command Window.

Most of the files are written to deal with large state-spaces, i.e., approximations of pde systems.
