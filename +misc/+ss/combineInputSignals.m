function u = combineInputSignals(myStateSpace, t, varargin)
%MISC.SS.COMBINEINPUTSIGNALS reads the input varargin and searches for name-value-pairs that
%coincide with one of myStateSpace.InputName. If a pair is found, that signal is used
%as elements of u. For inputs that are not found, u-elements are set to zero.
%
% Inputs:
%	myStateSpace	state space for which an input signal is designed
%	t				time for which u shall be defined
%	varargin		name-value-pairs containing input signals with names coinciding
%					with myStateSpace.InputName
% Outputs:
%	u				quantity vector of input signals suiting to myStateSpace for the
%					time t.
%
% Example:
% -------------------------------------------------------------------------
% 	myStateSpace = ss(-1, [1, 2], 1, [], "InputName", {'control', 'disturbance'});
%	t = quantity.Domain("t", linspace(0, 4, 201));
%	disturbanceSignal = quantity.Symbolic(sin(sym("t")), t, "name", "disturbance");
%	u = misc.ss.combineInputSignals(myStateSpace, t.grid, "disturbance", disturbanceSignal);
%	y = quantity.Discrete(lsim(myStateSpace, u.on(), t.grid), t, "name", "y");
%	plot([y; u]);
% -------------------------------------------------------------------------

% read name of input names from myStateSpace and calculate input lengths
myInputName = misc.ss.removeEnumeration(myStateSpace.InputName);
myInputNameUnique = unique(myInputName, 'stable');
myInputNameUniqueValidFieldName = strrep(myInputNameUnique, '.', 'POINT');
myInputLength = zeros(size(myInputNameUnique));
for it = 1 : numel(myInputLength)
	myInputLength(it) = sum(strcmp(myInputNameUnique{it}, myInputName));
end

% search for input signals in varargin or use default value = 0 instead
myParser = misc.Parser();
for it = 1 : numel(myInputNameUnique)
	myParser.addParameter(myInputNameUniqueValidFieldName{it}, ...
		quantity.Discrete(zeros(numel(t), myInputLength(it)), ...
			quantity.Domain("t", t), 'name', myInputNameUnique{it}), ...
		@(v) isa(v, 'quantity.Discrete') & isvector(v) & (numel(v)==myInputLength(it)));
end

% replace '.' in varargin names, to obtain valid fieldnames
newVarargin = varargin;
for it = 1 : numel(newVarargin)
	if isstring(newVarargin{it}) || ischar(newVarargin{it})
		newVarargin{it} = strrep(newVarargin{it}, ".", "POINT");
	end
end
myParser.parse(newVarargin{:});

% finally, create resulting input vector
u = myParser.Results.(myInputNameUniqueValidFieldName{1});
for it = 2 : numel(myInputNameUnique)
	u = [u; myParser.Results.(myInputNameUniqueValidFieldName{it})];
end % for

end % combineInputSignals()