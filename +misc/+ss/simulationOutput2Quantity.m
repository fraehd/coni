function quantityStruct = simulationOutput2Quantity(simOutput, time, outputNames, varargin)
% misc.ss.simulationOutput2Quantity separates the output of a simulation of a state-space 
% simulation model into quantities according to the names of the output
% signals specified by the OutputName-property of the state space
%
% Inputs:
%	myOutput		double array of the size timeSteps x numel(outputNames)
%	time			double array of the time values with numel(time) = timeSteps
%	outputNames		cell array of char-arrays that specify the signal name. For
%					instance, outputNames can be the OutputName propertie of a ss.
% Optional Inputs:
%	'z'				cell-array of name value pairs of signal-names and grids of a 
%					spatial coordinate 'z'. For	instance, if spatially distributed 
%					states x(t, z) and v(t, z) in R^2 are simulated
%					with the spatial grid  grid_1 = linspace(0, 1, 11)
%					belonging to the spatial domain of x_1 and v_1 and 
%					grid_2 = linspace(0, 1, 21) belonging to x_2 and v_2, then
%					add 'z', {{'x', 'v'}, {{grid_1, grid_2}, {grid_1, grid_2}} 
%					to the input, i.e. 
%					'z', {{signalNames}, {gridVectors of signalNames{1}}, ...
%					{gridVectors of signalNames{2}}, ...}
% Outputs:
%	quantityStruct	a struct containg quantity.Discretes with the names specified by
%					outputNames and the time grid time with the gridName 't'.
%
% Example:
% -------------------------------------------------------------------------
% 	mySimulationModel = ss(1, 1, [1; 2], [], 'OutputName', {'a', 'b'});
% 	myIc = 0.5;
% 	time = linspace(0, 1, 51);
% 	myInput = 0 * time;
% 	simulationOutputArray = lsim(mySimulationModel, myInput, time);
% 	simulationOutputQuantity = misc.ss.simulationOutput2Quantity( ...
% 						simulationOutputArray, time, mySimulationModel.OutputName);
% 	plot([simulationOutputQuantity.a; simulationOutputQuantity.b]);
% -------------------------------------------------------------------------

% input checks
assert(iscell(outputNames) && ischar(outputNames{1}), 'outputNames must be a cell-array of char-arrays')
assert(isvector(time) && isnumeric(time) && issorted(time(:), 'ascend'), ...
	'time-vector must be numeric ans a vector and ascending');
assert(isequal(size(simOutput), [numel(time), numel(outputNames)]), ...
	'size of simOutput does not fit to time or outputNames');

% outputName elements are char arrays. If there is only one output named, for
% instance, 'a', then this element only contains 'a'. If there are two (or more) 
% outputs with the same name, then they are enumerated as 'a(1)', 'a(2)', ...
% In the following the enumeration is removed from outputNames, since it hinders
% elegant string operations.
outputNamesUnenumerated = misc.ss.removeEnumeration(outputNames);
signalNames = unique(outputNamesUnenumerated, 'stable');

% input check for optional parameter z that defines the spatial grids
myParser = misc.Parser();
myParser.addParameter('z', {}, @(z) iscell(z));
myParser.addParameter('method', 'linear');
myParser.parse(varargin{:})
if ~isempty(myParser.Results.z)
	signalsDependentOnZ = myParser.Results.z{1};
	gridOfSignalsDependentOnZ = myParser.Results.z{2};
	assert(numel(signalsDependentOnZ) == numel(gridOfSignalsDependentOnZ), ...
		'input z is not defined properly');
	for it = 1 : numel(signalsDependentOnZ)
		assert(any(strcmp(signalNames, signalsDependentOnZ{it})), ...
			'z grid specified for a signal that is not part of outputNames');
	end
else
	signalsDependentOnZ = '';
end
quantityStruct = struct();
% read data and add to struct
t = quantity.Domain("t", time);
for idx =  1 : numel(signalNames)
	% consider z-grid
	zGridSelector = strcmp(signalsDependentOnZ, signalNames{idx});
	if any(zGridSelector)
		thisSignalUnsorted = simOutput(:, strcmp(outputNamesUnenumerated, signalNames{idx}));
		thisGrid = gridOfSignalsDependentOnZ{zGridSelector};
		thisGridLength = cellfun(@(v) numel(v), thisGrid);
		if ~(numel(misc.unique4cells(thisGrid)) == 1)
			% if the grid is different for every cell element, i.e. for every element
			% of the state vector, then they need to be interpolated to the same
			% spatial grid.
			thisGridHr = thisGrid{1}; % select finest grid of thisGrid = thisGridHr
			for it = 2 : numel(thisGrid)
				if thisGridLength(it) > numel(thisGridHr)
					thisGridHr = thisGrid{it};
				end
			end
			% make grid homogeneous
			thisGridHr = linspace(thisGridHr(1), thisGridHr(end), numel(thisGridHr));
			
			% select signal and interpolate
			thisSignalSorted = zeros(numel(time), numel(thisGridHr), numel(thisGrid));
			for it = 1 : numel(thisGrid)
				thisSignalItTemp = thisSignalUnsorted(:, sum(thisGridLength(1:it-1))+(1:thisGridLength(it)));
				thisSignalSorted(:, :, it) = misc.translate_to_grid(thisSignalItTemp, ...
					{time, thisGridHr}, 'gridOld', {time, thisGrid{it}}, ...
					'method', myParser.Results.method);
			end
			z = quantity.Domain("z", thisGridHr);
		else
			% if the grid is equal for every element of the state vector, no
			% interpolation is needed.
			thisSignalSorted = zeros(numel(time), thisGridLength(1), numel(thisGrid));
			for it = 1 : numel(thisGrid)
				thisSignalSorted(:, :, it) = thisSignalUnsorted( ...
					:, sum(thisGridLength(1:it-1))+(1:thisGridLength(it)));
			end % for it = 1 : numel(thisGrid)
			z = quantity.Domain("z", thisGrid{1});
		end
		quantityStruct = misc.setfield(quantityStruct, signalNames{idx}, ...
			quantity.Discrete(thisSignalSorted, [t, z], 'name', signalNames{idx}));
		
	else
		quantityStruct = misc.setfield(quantityStruct, signalNames{idx}, ...
			quantity.Discrete(simOutput(:, strcmp(outputNamesUnenumerated, signalNames{idx})), t, ...
				'name', signalNames{idx}));
	end
end
end

