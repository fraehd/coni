function [u, y, x, W] = planTrajectory(sys, t, optArgs)
% PLANTRAJECTORY Computes a trajectory for a lumped-parameter systems
%	[u, y, x] = planTrajectory(sys, t, varargin) computes a transition
%		u(t) : x(t0) = x0 --> x(t1) = x1
%   for a state-space system sys of the form
%		d/dt x = A x + bu.
%	Initial and end value x0 and x1 can be defined by name value pairs.
%	Default value is zero. The computation of the transition is based on
%	the Gramian controllability matrix
%		W(t0, t1) = int_t0^t1 Phi(t0, tau) b b^T Phi^T(t0, tau) d tau.
%   The used formula is
%		u(t) = -b^T Phi^T(t0, t) W^(-1)(t0, t1) ( x0 - Phi(t0, t1) x1 )
%		x(t) = Phi(t0, t) * W(t0, t) * W0^(-1)(t0, t1) * ( Phi(t0, t1) * x1 - x0)
%		y(t) = C x(t)
%
arguments
	sys ss;
	t quantity.Domain;
	optArgs.x0 double = zeros(size(sys.A, 1), 1);
	optArgs.x1 double = zeros(size(sys.A, 1), 1);
	optArgs.w quantity.Discrete;
	optArgs.method = "Chen3";
	optArgs.weight = quantity.Symbolic.ones(1, t);
	optArgs.Phi
end

x0 = optArgs.x0;
x1 = optArgs.x1;

if optArgs.method == "Bernstein"
	t0 = t.lower;
	t1 = t.upper;
	% Compute the state transition matrix: Phi(t,tau) = expm(A*(t-tau) )
	Phi_t0 = expm(sys.A * (t.Discrete - t.lower));
	invPhi_t1 = expm(sys.A * (t.upper - t.Discrete));
	
	% compute the gramian controllability matrix
	%W1 = misc.ss.gramian(sys, t, t0, optArgs.domainName);
	
	%int_t0^t1 expm(A*(tau-t0)) * b * b^T * expm(A^T(tau-t0)) dtau
	W = cumInt( Phi_t0 * sys.b * sys.b' * expm(sys.A' * (t.Symbolic - t.lower)), t, t0, t.name);
	
	W1_t1  = W.at(t1);
	%Formel aus dem Bernstein:
	u = sys.b.' * invPhi_t1.' / W1_t1  * (x1 - Phi_t0.at(t1) * x0);
	%Berechnung der
	x = Phi_t0 * x0 + W * invPhi_t1' / W1_t1  * (x1 - Phi_t0.at(t1) * x0);
end
%W2 = expm( sys.A * (t.Discrete - t1 ) ) * cumInt( invPhi_t1 * sys.B * sys.B' * invPhi_t1', t, t0, t.name);
if optArgs.method == "Chen3"
	Phi_t_t0 = expm(sys.A * ( t.Symbolic - t.lower ));
	Phi_t1_t = expm(sys.A * (t.upper - t.Symbolic ) );
	Phi_t_t1 = expm(sys.A * (t.Symbolic - t.upper ) );
	
	Wt0t = cumInt( Phi_t1_t * sys.B * sys.B' * Phi_t1_t' * optArgs.weight, t, t.lower, t.name );
	Wt0t1 = Wt0t.at(t.upper);
	u = - sys.B' * Phi_t1_t' / Wt0t1 * ( Phi_t_t0.at(t.upper) * x0 - x1 ) * optArgs.weight;
	x = Phi_t_t0 * x0 - Phi_t_t1 * Wt0t / Wt0t1 * ( Phi_t_t0.at(t.upper) * x0 - x1 );
elseif optArgs.method == "Chen1"
	%% compute the gramian controllability matrix
	
	if ~isfield(optArgs, "Phi")
		Phi_t_t0 = expm(sys.A * ( t.Symbolic - t.lower ));
		Phi_t0_t = expm(sys.A * ( t.lower - t.Symbolic)); % = inv( Phi_t_t0 )
	else
		Phi_t_t0 = optArgs.Phi.t_t0;
		Phi_t0_t = optArgs.Phi.t0_t;
	end
	
	% int  Phi(t,t0) * b * b^T * Phi(t,t0)' dt
	W = cumInt( Phi_t0_t * sys.b * sys.b' * Phi_t0_t' * optArgs.weight, t, t.lower, t.name);
	
	
	% solution for the control (see Chen (5-15))
	%	u(t) = - B' * Phi'(t0, t) * W(t0, t1)^-1 * ( x0 - Phi(t0, t1) * x1)
	u = - sys.b.' * Phi_t0_t.' / W.at(t.upper)  * (x0 - Phi_t0_t.at(t.upper) * x1) * optArgs.weight;
	% solution for the state:
	%	x(t) = Phi(t,t0) * ( x0 - W(t0, t) * W^-1(t0,t1) * (x0 - Phi(t0, t1*x1) );
	x = Phi_t_t0 * ( x0 - W / W.at(t.upper)  * (x0 - Phi_t0_t.at(t.upper) * x1) );
	%
else
	error("Method " + optArgs.method + " not yet implemented")
end

y = sys.C * x + sys.D * u;

%% Alternative Lösung nach Chen:
% W0 = int( invPhi_t0 * sys.B * sys.B' * invPhi_t0', 't', t0, 't');
% W0 = invPhi_t0 * W1 * invPhi_t0';
% u1 = -sys.B' * invPhi_t0' / W0.at(t1) * (x0 - Phi_t1.at(t0) * x1);
% xW0 = Phi_t0 * ( x0 - W0 / W0.at(t1) * (x0 - Phi_t1.at(t0) * x1));
%
% % simulation results have shown, does the average of both soultions leads to a better than each.
% u = (u0 + u1) / 2;
