function [f, a] = setPointChange(phi0, phi1, t0, t1, n, optArgs)
%SETPOINTCHANGE polynomial fit of an initial and end value with a certain smoothness
%
%	[f, a] = setPointChange(PHI0, PHI1, t0, t1, n, optArgs) plans a function polynom f 
%	which connects the initial value PHI0 with the end value PHI1, so that f(t0) = PHI0 and 
%	f(t1) = PHI1 as well as
%		d^k / dt^k f(t)|_{t=t0,t1} = 0,		k = 1, 2, ..., n
%	The name of the variable can be set with "var" as name-value pair.
arguments
	phi0 (:, 1) double;
	phi1 (:, 1) double;
	t0 (1, 1) double;
	t1 (1, 1) double;
	n (1, 1) double;
	optArgs.var = sym("t");
	optArgs.algorithm (1, 1) string = "matrixInverse";
end

assert(numel(phi0) == numel(phi1), "phi0 and phi1 must have same number of elements")
if ~isscalar(phi0)
	f = cell(size(phi0));
	a = cell(size(phi0));
	optArgsNameValue = misc.struct2namevaluepair(optArgs);
	for it = 1 : numel(phi0)
		[f{it}, a{it}] = misc.ss.setPointChange(phi0(it), phi1(it), t0, t1, n, optArgsNameValue{:});
	end % for it = 1 : numel(phi0)
	f = cat(1, f{:});
	a = cat(2, a{:});
	
elseif strcmp(optArgs.algorithm, "matrixInverse")
	% polynom of order 2*n+2
	%	f = sum_{i = 1, ... , (2n+2)} a(i) * t^(i-1)
	N = 2*n+2;

	M0 = zeros(n+1, N);
	M1 = zeros(n+1, N);

	coefficient = @(k, i, t) factorial( i + k -1 ) / factorial( i - 1) * t^(i-1);

	for k = 0:n
		for i = 1 : ( N - k )
			M0(k+1, i + k) = coefficient(k, i, t0);
			M1(k+1, i + k) = coefficient(k, i, t1);
		end
	end

	a = [M0; M1] \ [phi0; zeros(n,1); phi1; zeros(n,1)];

	if misc.issym( optArgs.var )
		t = optArgs.var;
	else
		t = sym( optArgs.var );
	end

	f = sym(0);

	for i = 1 : N
		f = f + a(i) * t^(i-1);
	end
elseif strcmp(optArgs.algorithm, "sum")
	% polynom
	%	f = phi0 + (phi1 - phi0) sum_{i = n+1, ..., 2n+1} p(i) * (t/T)^i
	% This alternative is needed as the matrix inverse in the original solution above is unprecise
	% for large n.
	if misc.issym( optArgs.var )
		t = optArgs.var - t0;
	else
		t = sym( optArgs.var ) - t0;
	end
	p = zeros(2*n+1, 1);
	f = phi0;
	for it = (n+1) : (2*n+1)
		p(it) = (-1).^(it-n-1) * factorial(2*n+1) ...
			./ ( it * factorial(n) * factorial(it-n-1) * factorial(2*n+1-it));
		f = f + (phi1 - phi0) * p(it) * (t / (t1 - t0)).^it;
	end % for it = (n+1) : (2*n+1)
	a = p;
	
else
	error("algorithm " + optArbs.algorithm + " is unknown. " ...
		+ "Use matrixInverse (default) or sum instead.");
	
end % if ~isscalar(phi0)

end % setPointChange()