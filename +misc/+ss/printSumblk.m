function texStringFinal = printSumblk(thisSumblk, NameValue)
%PRINTSUMBLK creates a string containing latex syntax description of a sumblk ss block ase on
%sumblks parameter D, OutputName, InputName.
arguments
	thisSumblk ss;
	NameValue.subscript = false;
end

thisOutputName = string(misc.ss.removeEnumeration(thisSumblk.OutputName, true));
thisInputName = string(misc.ss.removeEnumeration(thisSumblk.InputName, true));
if NameValue.subscript
	thisOutputName = extractBefore(thisOutputName, 2) + "_{" + extractAfter(thisOutputName, 1) + "}";
	thisInputName = extractBefore(thisInputName, 2) + "_{" + extractAfter(thisInputName, 1) + "}";
end

texString = misc.latexChar(thisOutputName + "(t)") ...
		+ " &= " + misc.latexChar(thisSumblk.D) ...
		+ " " + misc.latexChar(thisInputName + "(t)");

if nargout < 1
	misc.printTex(texString, "align", false);
else
	texStringFinal = texString;
end
end

