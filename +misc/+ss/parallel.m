function s12 = parallel(s1, s2, varargin)
% MISC.SS.PARALLEL connects state space models in parallel by connecting all their InputName
% and OutputName that they share.
% Example:
% s1 = ss(ones(2), [1, 2; 3, 4], [2, 3; 4, 5], eye(2), ...
%	'InputName', {'u', 'v'}, 'OutputName', {'x', 'y'});
% s2 = ss(ones(2), [1, 2; 3, 4], [2, 3; 4, 5], eye(2), ...
%	'InputName', {'w', 'u'}, 'OutputName', {'z', 'y'});
% s12 = misc.ss.parallel(s1, s2);

% find shared InputName and OutputName
sharedInputName = intersect(s1.InputName, s2.InputName);
sharedOutputName = intersect(s1.OutputName, s2.OutputName);

% find which index of InputName or OutputName in s1 and s2 belongs to which
% sharedName
inIndex = {zeros(size(sharedInputName)); zeros(size(sharedInputName))};
outIndex = {zeros(size(sharedOutputName)); zeros(size(sharedOutputName))};

for it = 1 : numel(sharedInputName)
	inIndex{1}(it) = find(strcmp(sharedInputName{it}, s1.InputName));
	inIndex{2}(it) = find(strcmp(sharedInputName{it}, s2.InputName));
end
for it = 1 : numel(sharedOutputName)
	outIndex{1}(it) = find(strcmp(sharedOutputName{it}, s1.OutputName));
	outIndex{2}(it) = find(strcmp(sharedOutputName{it}, s2.OutputName));
end

% parallel connection using built-in function
s12 = parallel(s1, s2, inIndex{1}, inIndex{2}, outIndex{1}, outIndex{2});
	
% recursive call for parallel connection of further state space models in varargin
if ~isempty(varargin)
	s12 = misc.ss.parallel(s12, varargin{:});
end

end