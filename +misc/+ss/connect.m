function resultSs = connect(InputName, OutputName, options, varargin)
% connect  Block-diagram interconnections of dynamic systems based on ss/connect.
% Different from the built-in function, InputName and OutputName are asserted to be
% unique. And it is more robust, since feedthroughs are removed before calling
% connect and are added afterwards again.

% remove empty elements of varargin
% note that empty(ss) does not work, since state space models without inputs are
% considered as empty. Hence, size(v, 1) > 0 is used instead.
if isa(options, "ltioptions.connect")
	inputSs = varargin(cellfun(@(v) size(v, 1) > 0, varargin));
else
	if isa(options, "numlti")
		inputSs = [{options}, varargin(cellfun(@(v) size(v, 1) > 0, varargin))];
	elseif ~isempty(options)
		error("third input must be a connectOptions() or a StateSpaceModel");
	end
	options = connectOptions("Simplify", false);
end

% clean state-spaces
for it = 1 : numel(inputSs)
	% remove feedthroughs
	inputSs{it} = misc.ss.removeInputOfOutput(ss(inputSs{it}));
	inputSs{it}.OutputName = misc.ss.removeSingularEnumeration(inputSs{it}.OutputName);
	inputSs{it}.InputName = misc.ss.removeSingularEnumeration(inputSs{it}.InputName);
end

% get InputNames of result
resultInputNames = unique(misc.ss.removeSingularEnumeration(InputName), "stable");
resultOutputNames = unique(misc.ss.removeSingularEnumeration(OutputName), "stable");
if isempty(resultInputNames)
	% as ss/connect does not support empty cell array for inputNames or outputNames, this dummy
	% object is added for those cases.
	resultInputNames = "dummy.input";
	resultOutputNames = [resultOutputNames; "dummy.output"];
	inputSs = [inputSs, ...
		{ss([], [], [], 0, "InputName", "dummy.input", "OutputName", "dummy.output")}];
end

% call built-in function
resultSs = connect(inputSs{:}, resultInputNames, resultOutputNames, options);

end % misc.ss.connect()