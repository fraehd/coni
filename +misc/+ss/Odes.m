classdef Odes < handle & matlab.mixin.Copyable
%misc.ss.Odes (standing for ordinary differential equations) is class to
%implement multiple different state spaces (see doc/ss).
	properties
		ss (1, :) cell;		% cell-array of state spaces
		type (1, :) cell;	% cell array of names of ss
		C (1, :) cell;		% cell of output gains / matrices
		D (1, :) cell;		% cell of feedthrough gains / matrices
	end

	properties (Dependent = true)
		odes (1, 1) ss;		% connection of all state spaces in the ss property.
		%					% The connection is performed by connecting InputName and
		%					% OutputName, hence specification of those properties of
		%					% all ss-cell-elements is essential.
		numTypes (1, 1);	% number of different types
		OutputName (:, 1) cell;	% OutputNames of ss - unique, sorted
		InputName (:, 1) cell;	% InputNames of ss - unique, sorted
		n (1, :) double;	% size(ss(:).A, 1)
	end
	
	methods
		function obj = Odes(stateSpaces, type)
			%% misc.ss.Odes constructs an object containing ss
			% Example:
			%	ss1 = ss(1, 1, 1, 1, 'InputName', 'u1', 'OutputName', 'y1');
			%	ss2 = ss(2, 2, 2, 2, 'InputName', 'u2', 'OutputName', 'y2');
			%	myOdes = misc.ss.Odes(ss1, 'ss1', ss2, 'ss2')
			arguments (Repeating)
				stateSpaces ss;
				type char;
			end
			
			if nargin > 0
				obj.type = type;
				for it = 1 : numel(stateSpaces)
					[obj.ss{it}, obj.C{it}, obj.D{it}] ...
						= obj.separateOutputfromSs(stateSpaces{it}, type{it});
				end % for it = 1 : numel(stateSpaces)
				obj.verifyProperties();
			end % if nargin > 0
		end % misc.ss.Odes() Constructor
		
		function verifyProperties(obj)
			% verification of properties of misc.ss.Odes. Checks length of ss and
			% type and that type is unique.
			assert(misc.isunique(obj.type), 'type of misc.Odes must be unique');
			assert(isequal(size(obj.ss), size(obj.type)), ...
				'number of ss and types are not equal');
			assert(isequal(size(obj.ss), size(obj.C)), ...
				'number of ss and C are not equal');
			assert(isequal(size(obj.ss), size(obj.D)), ...
				'number of ss and D are not equal');
		end % verifyProperties()
		
		function obj = add(obj, varargin)
			% with add() multiple misc.ss.Odes are combined
			% Example: 
			%	ss1 = ss(1, 1, 1, 1, 'InputName', 'u1', 'OutputName', 'y1');
			%	ss2 = ss(2, 2, 2, 2, 'InputName', 'u2', 'OutputName', 'y2');
			%	Ode1 = misc.ss.Odes(ss1, 'ss1');
			%	Ode2 = misc.ss.Odes(ss2, 'ss2');
			%	Odes12 = Ode1.add(Ode2);
			for myOde = varargin{:}
				assert(isa(myOde, 'misc.ss.Odes'), 'input must be an misc.ss.Odes-object');
				for it = 1 : myOde.numTypes
					assert(~any(contains(obj.type, myOde.type{it})), ...
						['The ss-type ', myOde.type{it}, ' is already defined']);
					obj.ss{end+1} = myOde.ss{it};
					obj.C{end+1} = myOde.C{it};
					obj.D{end+1} = myOde.D{it};
					obj.type{end+1} = myOde.type{it};
				end
			end
			obj.verifyProperties();
		end % add()
		
		function obj = exchange(obj, myType, newSs)
			% Exchange one ss with another one
			% Example:
			%	ss1 = ss(1, 1, 1, 1, 'InputName', 'u1', 'OutputName', 'y1');
			%	ss2 = ss(2, 2, 2, 2, 'InputName', 'u2', 'OutputName', 'y2');
			%	Ode = misc.ss.Odes(ss1, 'ss1');
			%	Ode.exchange('ss1', ss2)
			[obj.ss{obj.index(myType)}, obj.C{obj.index(myType)}, obj.D{obj.index(myType)}] ...
				= obj.separateOutputfromSs(newSs, myType);
			obj.verifyProperties();
		end % exchange()
		
		function c = plus(a, b)
			% Example: 
			%	ss1 = ss(1, 1, 1, 1, 'InputName', 'u1', 'OutputName', 'y1');
			%	ss2 = ss(2, 2, 2, 2, 'InputName', 'u2', 'OutputName', 'y2');
			%	Ode1 = misc.ss.Odes(ss1, 'ss1');
			%	Ode2 = misc.ss.Odes(ss2, 'ss2');
			%	Odes12 = Ode1 + Ode2
			if isempty(a)
				c = b;
			elseif isempty(b)
				c = a;
			elseif isa(a, 'misc.ss.Odes') && isa(b, 'misc.ss.Odes')
				c = copy(a);
				c = c.add(b);
			else
				error('input data type not recognized');
			end
			a.verifyProperties();
		end %plus()
		
		function obj = remove(obj, myType)
			% remove specified ss and type
			% Example:
			%	ss1 = ss(1, 1, 1, 1, 'InputName', 'u1', 'OutputName', 'y1');
			%	ss2 = ss(2, 2, 2, 2, 'InputName', 'u2', 'OutputName', 'y2');
			%	myOdes = misc.ss.Odes(ss1, 'ss1', ss2, 'ss2');
			%	myOdes.remove('ss1')
			if ~iscell(myType)
				myType = {myType};
			end
			for it = 1 : numel(myType)
				selectRemaining = (1:1:obj.numTypes) ~= obj.index(myType{it});
				obj.ss = obj.ss(selectRemaining);
				obj.C = obj.C(selectRemaining);
				obj.D = obj.D(selectRemaining);
				obj.type = obj.type(selectRemaining);
			end
			obj.verifyProperties();
		end % remove()
		
		function idx = index(obj, myType)
			% find ss by naming its type and return index-value
			% Example:
			%	ss1 = ss(1, 1, 1, 1, 'InputName', 'u1', 'OutputName', 'y1');
			%	ss2 = ss(2, 2, 2, 2, 'InputName', 'u2', 'OutputName', 'y2');
			%	myOdes = misc.ss.Odes(ss1, 'ss1', ss2, 'ss2');
			%	myOdes.index('ss2')
			idx = [];
			for it = 1 : obj.numTypes
				if isequal(obj.type{it}, myType)
					idx = it;
				end
			end
		end % index()
		
		function thisSs = getSs(obj, myType)
			% get ss specified by type
			% Example:
			%	ss1 = ss(1, 1, 1, 1, 'InputName', 'u1', 'OutputName', 'y1');
			%	ss2 = ss(2, 2, 2, 2, 'InputName', 'u2', 'OutputName', 'y2');
			%	myOdes = misc.ss.Odes(ss1, 'ss1', ss2, 'ss2');
			%	myOdes.getSs('ss2');
			thisSs = obj.ss{obj.index(myType)};
		end % getSs()
		
		function thisC = getC(obj, myType)
			% get ss specified by type
			% Example:
			%	ss1 = ss(1, 1, 1, 3, 'InputName', 'u1', 'OutputName', 'y1');
			%	ss2 = ss(2, 2, 2, 4, 'InputName', 'u2', 'OutputName', 'y2');
			%	myOdes = misc.ss.Odes(ss1, 'ss1', ss2, 'ss2');
			%	myOdes.getC('ss2');
			thisC = obj.C{obj.index(myType)}.D; % yes, .D and not .C is correct
		end % getC()
		
		function thisN = getN(obj, myType)
			% get ss specified by type
			% Example:
			%	ss1 = ss(1, 1, 1, 1, 'InputName', 'u1', 'OutputName', 'y1');
			%	ss2 = ss(2, 2, 2, 2, 'InputName', 'u2', 'OutputName', 'y2');
			%	myOdes = misc.ss.Odes(ss1, 'ss1', ss2, 'ss2');
			%	myOdes.getN('ss2');
			thisN = size(obj.ss{obj.index(myType)}.A, 1);
		end % getN()
		
		function thisD = getD(obj, myType)
			% get ss specified by type
			% Example:
			%	ss1 = ss(1, 1, 1, 1, 'InputName', 'u1', 'OutputName', 'y1');
			%	ss2 = ss(2, 2, 2, 2, 'InputName', 'u2', 'OutputName', 'y2');
			%	myOdes = misc.ss.Odes(ss1, 'ss1', ss2, 'ss2');
			%	myOdes.getD('ss2');
			thisD = obj.D{obj.index(myType)}.D;
		end % getD()
				
		function result = isempty(obj)
			% A misc.ss.Odes is assumed to be empty if numTypes is zero or empty
			% Example:
			%	isempty(misc.ss.Odes())
			result = isempty(obj.numTypes) || (obj.numTypes == 0);
		end %isempty()
				
		function result = isequal(A, B, varargin)
			% isequal compares all parameters of A and B and varargin and returns
			% true if they are equal, and false if not. Two misc.ss.Odes are assumed
			% to be equal, if all ss and type are equal.
			result = (A.numTypes == B.numTypes) && isequal(A.type, B.type);
			if result
				for it = 1 : A.numTypes
					if result
						result = isequal(A.ss(it), B.ss(it));
					end
				end
			end
			if result && (nargin > 2)
				result = result && B.isequal(varargin{:});
			end
		end % isequal()
		
		function B = BofInputName(obj, inputName)
			arguments
				obj misc.ss.Odes;
				inputName (1, 1) string;
			end
			InputNames = misc.ss.removeEnumeration(obj.InputName);
			B = obj.odes.B;
			B = B(:, strcmp(InputNames, inputName));
		end % BofInputName
		
		function [ic, stateName] = getInitialCondition(obj, varargin)
			% getInitialCondition reads varargin if there are initial conditions
			% defined as name-value-pair with the name according to obj.stateName and
			% returns 2 cell arrays: 
			%	1. ic is a cell array of the values of the states initial condition
			%	2. stateName is a cell array of the names of those states.
			
			icStruct = misc.struct(varargin{:});
			
			% Create initial condition for all states specified by obj.type
			% If a value is specified in icStruct, than this value is used.
			% Otherwise, a default value is considered.
			icTemp = cell(obj.numTypes, 1);
			for it = 1 : obj.numTypes
				if misc.isfield(icStruct, obj.type{it})
					icTemp{it} = misc.getfield(icStruct, obj.type{it});
				else
					icTemp{it} = zeros(size(obj.ss{it}.A, 1), 1);
				end
			end
			
			ic = [icTemp(:)];
			stateName = [obj.type(:)];
		end % getInitialCondition()
		
		function [texString, nameValuePairs] = printParameter(obj)
			myOdes = obj.odes;
			nameValuePairs = {
				'A', myOdes.A, ...
				'B', myOdes.B, ...
				'C', myOdes.C, ...
				'D', myOdes.D, ...
				};
			texString = misc.variables2tex([], nameValuePairs);
		end % printParameter()
		
		function texString = print(obj, NameValue)
			arguments
				obj misc.ss.Odes;
				NameValue.stateName string = "v";
			end
			
			% prepare signal names
			thisOutputName = string(misc.ss.removeEnumeration(obj.OutputName, true));
			thisOutputName = thisOutputName.extractBefore(2) + "_{" ...
				+ thisOutputName.extractAfter(1) + "}(t)";
			thisInputName = string(misc.ss.removeEnumeration(obj.InputName, true));
			thisInputName = thisInputName.extractBefore(2) + "_{" ...
				+ thisInputName.extractAfter(1) + "}(t)";
			
			if strlength(NameValue.stateName) > 1
				myOdeString = "\dot{" + NameValue.stateName.extractBefore(2) + "}" ...
					+ "_{" + NameValue.stateName.extractAfter(1) + "}(t) &= ";
			
				NameValue.stateName = NameValue.stateName.extractBefore(2) ...
				+ "_{" + NameValue.stateName.extractAfter(1) + "}(t)";
			else
				myOdeString = "\dot{" + NameValue.stateName + "}(t) &= ";
				NameValue.stateName = NameValue.stateName + "(t)";
			end
			
			% get ODE-lines
			myOdeString = myOdeString + misc.latexChar(obj.odes.A) + NameValue.stateName;
			if any(obj.odes.B ~= 0)
					myOdeString = myOdeString + " + ";
				if ~misc.iseye(obj.odes.B)
					myOdeString = myOdeString + misc.latexChar(obj.odes.B);
				end
				myOdeString = myOdeString + misc.latexChar(thisInputName);
			end
			
			% get output-lines
			myOutputString = misc.latexChar(thisOutputName) + " &= ";
			plusNeeded = false;
			if any(obj.odes.C ~= 0)
				if misc.iseye(obj.odes.C)
					myOutputString = myOutputString + NameValue.stateName;
				else
					myOutputString = myOutputString + ...
						misc.latexChar(obj.odes.C) + NameValue.stateName;
				end
				plusNeeded = true;
			end
			if any(obj.odes.D ~= 0)
				if plusNeeded
					myOutputString = myOutputString + " + ";
				end
				if ~misc.iseye(obj.odes.D)
					myOutputString = myOutputString + misc.latexChar(obj.odes.D);
				end
				myOutputString = myOutputString + misc.latexChar(thisInputName);
			end
			
			myStringArray = [myOdeString; myOutputString];
			% set output parameter or print
			if nargout > 0
				texString = myStringArray;
			else
				misc.printTex(myStringArray);
			end
		end % print()
		
		%% get methods for Dependent properties
		function myOutputName = get.OutputName(obj)
			myData = [];
			for it = 1 : obj.numTypes
				myData = vertcat(myData, obj.ss{it}.OutputName, ...
					obj.C{it}.OutputName, obj.D{it}.OutputName);
			end
			myOutputName = unique(myData);
		end % get.OutputName();
		
		function myInputName = get.InputName(obj)
			myData = [];
			for it = 1 : obj.numTypes
				myData = vertcat(myData, obj.ss{it}.InputName, obj.D{it}.InputName);
			end
			myInputName = unique(myData);
		end % get.InputName();
		
		function myOdes = get.odes(obj)
			if isempty(obj)
				myOdes = [];
			else
				myOdes = misc.ss.connect(obj.InputName, obj.OutputName, ...
					obj.ss{:}, misc.ss.parallel(obj.C{:}, obj.D{:}));
			end % if
		end % get.odes();
		
		function numTypes = get.numTypes(obj)
			numTypes = numel(obj.ss);
		end %get.numTypes()
		
		function n = get.n(obj)
			n = zeros(size(obj.type));
			for it = 1 : numel(n)
				n(it) = size(obj.ss{1}.A, 1);
			end % for it = 1 : numel(n)
		end %get.n()
	end % methods
	
	methods (Static = true)
		
		function [mySs, C, D] = separateOutputfromSs(mySs, type)
			% separateCfromSs() removes the output matrix of a state space model mySs
			% and replaces it with the identity matrix, in order to obtain the states
			% of the ode as output variables. The old output matrix C results in a
			% separate state space
			if all(strcmp(misc.ss.removeEnumeration(mySs.OutputName), type))
				assert(misc.iseye(mySs.C) && isequal(size(mySs.C), size(mySs.A)), ...
					"output of states space model does not include all states");
				C = ss();
				D = ss();
			else
				% get C as state space
				C = ss([], [], [], mySs.C, 'OutputName', mySs.OutputName);
				C = misc.ss.setSignalName(C, 'input', {type}, size(C, 2));
				
				% get D as state space
				if ~isempty(mySs.D) && any(mySs.D(:) ~= 0)
					D = ss([], [], [], mySs.D, ...
						'OutputName', mySs.OutputName, 'InputName', mySs.InputName);
				else
					D = ss();
				end
				
				% change C and D in mySs
				if numel(mySs.A) == 1
					mySsOutputName = type;
				else
					mySsOutputName = type + "(" + (1:1:size(mySs.A, 1)).' + ")";
				end
				mySs = ss(mySs.A, mySs.B, eye(size(mySs.A)), zeros(size(mySs.A, 1), size(mySs.B, 2)), ...
					mySs.Ts, "InputName", mySs.InputName, ...
					"OutputName", mySsOutputName);
			end % if
		end % function [ss, C] = seperateCfromSs(ss)
		
	end % methods (Static = true)
	
	methods (Access = protected)
		% Override copyElement method:
		function cpObj = copyElement(obj)
			% Make a shallow copy of all properties
			cpObj = copyElement@matlab.mixin.Copyable(obj);
		end
	end % methods (Access = protected)
	
end

