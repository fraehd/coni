function r = relativeDegree(mySs, option)
% r = relativeDegree(mySs) returns the relative degree r of a dynamic model, i.e. 
% r = number of transfer poles - number of transfer zeros. If mySs is an array,
% then the result is a double array of the relative degree of every input to every
% output. If there is no connection fo an input to the output, the resulting value in
% r is set to NaN.
% 
% r = relativeDegree(mySs, 'min') is equal to min(relativeDegree(mySs), [], 'all')
% 
% r = relativeDegree(mySs, 'max') is equal to max(relativeDegree(mySs), [], 'all')
%
%% Example 1
% mySs = ss(ones(3), ones(3, 2), ones(1, 3), []);
% r = misc.ss.relativeDegree(mySs)
%% Example 2
% mySs = ss([0, 1; 0, 0], [0, 1; 1, 0], [1, 0; 0, 1], []);
% r = misc.ss.relativeDegree(mySs)

	arguments
		mySs;
		option = "all";
	end % arguments
	
	r = zeros(size(mySs));
	for it = 1 : size(r, 1)
		for jt = 1 : size(r, 2)
			r(it, jt) = numel(pole(mySs(it, jt))) - numel(zero(mySs(it, jt)));
			if r(it, jt) == 0
				% check if transfer function is zero
				[~, gainTemp] = zero(mySs(it, jt));
				if abs(gainTemp) <= 100*eps
					r(it, jt) = nan;
				end
			end
		end
	end
	
	if strcmp(option, "min")
		r = min(r, [], "all");
	elseif strcmp(option, "max")
		r = max(r, [], "all");
	elseif ~strcmp(option, "all")
		error("unknown option");
	end
	
end % relativeDegree()