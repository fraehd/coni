function newNames = removeSingularEnumeration(enumeratedNames)
% REMOVESINGULARENUMERATION removes braces with numbers from string-arrays of singular 
% names that are an element of the array enumeratedNames. 
% This is for instance useful, to deal with InputName or OutputName properties of 
% large state-space models (ss).
%
% Examples:
%	misc.ss.removeSingularEnumeration(["a(1)", "a(2)", "b(1)", "c(1)", "b", "c(2)", "d(1)"])

arguments
	enumeratedNames (:, 1) string;
end

newNames = enumeratedNames;
endWith1Idx = endsWith(enumeratedNames, "(1)");
newNames(endWith1Idx) = strrep(enumeratedNames(endWith1Idx), "(1)", "");
for it = 1 : numel(newNames)
	if endWith1Idx(it) && any(strcmp(newNames, newNames(it) + "(2)"))
		newNames(it) = newNames(it) + "(1)";
	end % if
end

end % misc.ss.removeSingularEnumeration()