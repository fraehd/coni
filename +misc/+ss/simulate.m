function [output, state] = simulate(mySs, u, t, ic, solver, option)
% MISC.SS.SIMULATE Performs simulations of state space models.
%
% simData = misc.ss.simulate(mySs, u, t, ic) simulates the state space mySs for the
% input u, the time t and the initial condition ic. t is a double array, size(u) must
% be equal [numel(t), size(mySs.B, 2)] and ic must be a double vector suiting mySs.
%
% simData = misc.ss.simulate(mySs, u, t, ic, solver) uses the solver specified.
% default: lsim, alternative: ode45, ode15s, ode113, ode23s, ode23t, ode23tb, ode15i
%
% simData = misc.ss.simulate(mySs, u, t, ic, solver, option) considers the options
% for the solver
%
% ----------------------
% Example
% mySs = ss([0, 1; 0, 0], [0; 1], [1, 0], []);				% ramp model
% u = ones(21, 1);											% input = 1
% t = linspace(0, 2, 21);									% time
% ic = [-1; 0];												% initial condition
% outputOde45 = misc.ss.simulate(mySs, u, t, ic, "ode45");	% simulate with ode45
% outputLsim = misc.ss.simulate(mySs, u, t, ic, "lsim", 'foh');%			% simulate with lsim
% figure(); plot(t, outputOde45); hold on; plot(t, outputLsim, '--'); % compare
% ----------------------

arguments
	mySs ss;
	u (:, :) double;
	t (:, 1) double;
	ic (:, 1) double;
	solver (1, 1) string = "lsim";
	option = odeset(); 
end %arguments

if strcmp(solver, "lsim")
	if isstring(option) || ischar(option)
		[output, ~, state] = lsim(mySs, u, t, ic, option);
	else
		assert(nargin<6, "input argument option not valid for lsim solver");
		[output, ~, state] = lsim(mySs, u, t, ic);
	end
else
	% prepare input interpolant
	uDim = 1 : size(mySs.B, 2);
	uInterp = numeric.interpolant({t, uDim}, u);
	
	% perform simulation using solver
	if strcmp(solver, "ode15i")
		odefun = @(t, x, x_t) -x_t + mySs.A * x + mySs.B * uInterp.evaluate(t, uDim).';
		[~, state] = eval(solver + "(odefun, t, ic, mySs.A*ic, option)");
	else
		odefun = @(t, x) mySs.A * x + mySs.B * uInterp.evaluate(t, uDim).';
		[~, state] = eval(solver + "(odefun, t, ic, option)");
	end
	
	% calculate output for every time step
	output = zeros(numel(t), size(mySs.C, 1));
	for it = 1 : numel(t)
		output(it, :) = mySs.C * state(it, :).' + mySs.D * u(it, :).';
	end % for it = 1 : numel(t)
end % if strcmp(solver, "lsim")
end % simulate()