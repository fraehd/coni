function mySs = add2signalName(mySs, signalType, position, newText)
% ADD2SIGNALNAME adds a string to the OutputName or InputName of the state
% space model mySs either at the position 'front' or 'back'.
% Example:
% misc.ss.add2signalName(ss(1, 1, [1; 1], [], 'OutputName', {'a', 'b'}), ...
%		'OutputName', 'front', 'ode.');
arguments
	mySs ss;
	signalType (1, 1) string {mustBeMember(signalType, ["OutputName", "InputName"])};
	position (1, 1) string {mustBeMember(position, ["front", "back"])};
	newText (1, 1) string;
end
assert(any(strcmp(position, ["front", "back"])));
assert(any(strcmp(signalType, ["OutputName", "InputName"])));

if strcmp(position, "front")
	mySs.(signalType) = strcat(newText, string(mySs.(signalType)));
elseif strcmp(position, "back")
	mySs.(signalType) = strcat(string(mySs.(signalType)), newText);
end
end