function W = gramian(sys, t, t0, gridName)
% GRAMIAN controllability matrix. 
% W = gramian(sys, t, t0) computes the gramian controllability matrix of
% the state space system sys. For each timestep t. If t0 is not set, the
% default value is 0. For the computation the result from 
%
%	C. Van Loan, "Computing integrals involving the matrix exponential," in IEEE Transactions on Automatic Control, vol. 23, no. 3, pp. 395-404, June 1978.
%	doi: 10.1109/TAC.1978.1101743
%   URL: http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=1101743&isnumber=241661
% 
% are used. In particular, the formula below (1.4) is applied. The gramian
% is given as:
%
%	W(t0, t1) = int_t0^t1 expm(A*(tau-t0)) * b * b^T * expm(A^T(tau-t0)) dtau
%			  = int_t0^t1 expm(A*(t1 - tau) * b * b^T * expm(A^T(t1 - tau)) dtau
%
arguments
	sys ss;
	t double;
	t0 double = 0;
	gridName = "t";
end

n = size(sys.A, 1);
Q = [-sys.A      sys.B*sys.B'; ... 
	  zeros(n)   sys.A'] * quantity.Discrete( t - t0, 'grid', t, 'gridName', gridName);

temp = expm(Q);
F3 = temp(n+1:end,n+1:end);
G2 = temp(1:n,n+1:end);

W = F3' * G2;

end