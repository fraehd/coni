function myStateSpace = addInput2Output(myStateSpace)
% ADDINPUT2OUTPUTNAME adds all inputs to the outputs of the state space model, by
% designing feedthroughs.
%
% Inputs:
%	myStateSpace	state space which is modified
% Outputs:
%	myStateSpace	the state space with modified parameters
%
% Example:
% -------------------------------------------------------------------------
%  	mySimulationModel = ss(1, [1, 2, 3], [1; 2; 3], [1, 0, 0; 0, 0, 0; 0, 0, 0], ...
% 		'OutputName', {'x', 'y', 'z'}, 'InputName', {'u(1)', 'u(2)', 'a'});
% 	mySimulationModel = misc.ss.addInput2Output(mySimulationModel)
% -------------------------------------------------------------------------
arguments
	myStateSpace ss;
end

% read InputName and OutputName
inputNameAll = misc.ss.removeEnumeration(myStateSpace.InputName);
outputNameAll = misc.ss.removeEnumeration(myStateSpace.OutputName);

% count
inputName = unique(inputNameAll, 'stable');
inputLength = zeros(size(inputName));
for it = 1 : numel(inputName)
	inputLength(it) = sum(strcmp(inputNameAll, inputName(it)));
end
outputName = unique(outputNameAll, 'stable');
outputLength = zeros(size(outputName));
for it = 1 : numel(outputLength)
	outputLength(it) = sum(strcmp(outputNameAll, outputName(it)));
end

% add feedthrough
isFeedThroughNeccessary = false([numel(inputName), 1]);
for it = 1 : numel(inputName)
	if any(strcmp(outputName, inputName(it)))
		% if the input is already part of the output, assert that it has the same
		% size
		assert(inputLength(it) == outputLength(strcmp(outputName, inputName(it))), ...
			'There is already an output with the same name as an input, but they have different size!?');
		isFeedThroughNeccessary(it) = false;
	else
		% Create a feedthrough and  add it to myStateSpace
		% feedthrough:
		if inputLength(it) > 1
			feedthroughTemp = ss([], [], [], eye(inputLength(it)), ...
				"OutputName", inputName(it) + "(" + string(1:1:inputLength(it)).' + ")", ...
				"InputName", inputName(it) + "(" + string(1:1:inputLength(it)).' + ")");
		else
			feedthroughTemp = ss([], [], [], eye(inputLength(it)), ...
				"OutputName", inputName(it), ...
				"InputName", inputName(it));
		end
		% connection:
		% first myStateSpace is appended, then the input of the feedthrough and the
		% state space are interconneted using series()
		myStateSpace = append(myStateSpace, feedthroughTemp);
		isFeedThroughNeccessary(it) = true;
	end
end
% create short cut of feedthrough and input
shortCutMatrix = eye(sum(inputLength));
shortCutMatrixSelector = true(size(shortCutMatrix));
% remove rows that have now feedthrough
for it = 1 : numel(inputName)
	if ~isFeedThroughNeccessary(it)
		shortCutMatrixSelector(sum(inputLength(1:(it-1)))+(1:inputLength(it)), :) = false;
	end
end
shortCutMatrix = reshape(shortCutMatrix(shortCutMatrixSelector), ...
	[sum(inputLength(isFeedThroughNeccessary)), sum(inputLength)]);
shortCut = ss([], [], [], [eye(sum(inputLength)); shortCutMatrix]);
shortCut = misc.ss.setSignalName(shortCut, 'input', inputName, inputLength);
myStateSpace = series(shortCut, myStateSpace);

end % addInput2Output()