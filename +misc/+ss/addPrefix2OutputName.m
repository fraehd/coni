function mySs = addPrefix2OutputName(mySs, prefix)
% ADDPREFIX2OUTPUTNAME adds a prefix string to the OutputName of the given state
% space mode mySs. A dot is the seperator of the prefix and the name, i.e.
% [prefix].[mySs.OutputName{it}].
% Example:
% misc.ss.addPrefix2OutputName(ss(1, 1, [1; 1], [], 'OutputName', {'a', 'b'}), 'ode');
assert(ischar(prefix) || isstring(prefix), 'prefix must be a char-array or string');
myOutputName = mySs.OutputName;
for it = 1 : numel(myOutputName)
	myOutputName{it} = [prefix, '.', myOutputName{it}];
end
mySs.OutputName = myOutputName;
end