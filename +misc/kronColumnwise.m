function C = kronColumnwise(A, B, AcolumnSize, BcolumnSize)
% misc.kronColumnwise calculates a modified Kronecker product for block matrices defined by
%			[kron(A_11, B_1)	kron(A_12, B_2)		...		kron(A_1r, B_r)	]
%		C = [	...					...				...			...			]
%			[kron(A_r1, B_1)	kron(A_r2, B_2)		...		kron(A_rr, B_r)	]
%
% or equally
%		C = [kron(A_1, B_1)		kron(A_2, B_2)		...		kron(A_r, B_r)	]
%
% Therein, 
%		   r: number of blocks, i.e. r = numel(AcolumnSize) = numel(BcolumnSize)
%		 A_i: The sub-matrix of A resulting by evaluating its
%					columns sum(AcolumnSize(i-1) + (1:AcolumnSize(i)
%			 -> A = [ A_1	A_2	...	A_r ]
%		 B_i: The sub-matrix of B resulting by evaluating its
%					columns sum(BcolumnSize(i-1) + (1:BcolumnSize(i)
%			 -> B = [ B_1	B_2	...	B_r ]
%
%	C = misc.kronColumnwise(A, B, AcolumnSize, BcolumnSize) calculates the product defined above. 
%		AcolumnSize, and BcolumnSize resepectivly, define how many columns AcolumnSize(i) belong to
%		the i-th componenent A_i.
%	
%	C = misc.kronColumnwise(A, B) calculates the usual Kronecker product
%	
%%
% Example
%	A = [1, 2; 3, 4]
%	B = [1, 2, 3]
%	C = misc.kronColumnwise(A, B, [1, 1], [1, 2])

arguments
	A (:, :);
	B (:, :);
	AcolumnSize (:, 1) double = size(A, 2);
	BcolumnSize (:, 1) double = size(B, 2);
end % arguments

assert(sum(AcolumnSize) == size(A, 2), ...
	"The sum of AcolumnSize must be equal to the number of columns of A");
assert(sum(BcolumnSize) == size(B, 2), ...
	"The sum of BcolumnSize must be equal to the number of columns of B");
assert(isequal(numel(AcolumnSize), numel(BcolumnSize)), ...
	"AcolumnSize and BcolumnSize must have same number of elements, because there need to be "...
	+ "same number of sub-matrices for A and B");

Ccell = cell(size(AcolumnSize));
for it = 1 : numel(AcolumnSize)
	Ccell{it} = kron(...
		A(:, sum(AcolumnSize(1:it-1)) + (1 : AcolumnSize(it))), ...
		B(:, sum(BcolumnSize(1:it-1)) + (1 : BcolumnSize(it))));
end % for it = 1 : numel(AcolumnSize)
C = cat(2, Ccell{:});
end % misc.kronColumnwise()