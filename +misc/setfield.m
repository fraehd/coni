function myStruct = setfield(myStruct, myFieldName, myValue)
% misc.setfield Set structure field contents. (taken from help setfield
% myStruct = setfield(myStruct,myFieldName,myValue) sets the contents of the specified field to the
% value myValue. For example, setfield(myStruct,'a',myValue) is equivalent to the syntax
% myStruct.a = myValue, and sets the value of field 'a' as myValue. myStruct must be a 1-by-1
% structure. myFieldName can be a character vector or string scalar. The
% changed structure is returned.
% In contrast to the matlab built-in function setfield(), this method misc.setfield
% can also create nested fields by specifying myFieldName with dots as seperator, for
% instance myFieldName= 'topLevel.lowerLevel.data', see example.
%%
% Example:
%		myStruct = struct('asdf', 123);
%		myStruct = misc.setfield(myStruct, 'topLevel.lowerLevel.data', 42)

myFieldNameSplitted = split(myFieldName, '.');

% add myValue to myStructExplorer.
if numel(myFieldNameSplitted) > 1
	% go down the tree of myStruct as far as fields according to myFieldName exist.
	wasAlreadyAField = false(numel(myFieldNameSplitted)-1, 1);
	myStructExplorerTopDown{1} = myStruct;
	for it = 1 : numel(wasAlreadyAField)
		if isfield(myStructExplorerTopDown{it}, myFieldNameSplitted{it}) ...
				&& isstruct(myStructExplorerTopDown{it}.(myFieldNameSplitted{it}))
			wasAlreadyAField(it) = true;
			myStructExplorerTopDown{it+1} = myStructExplorerTopDown{it}.(myFieldNameSplitted{it});
		else
			break
		end
	end
	
	% create the remaining struct fields that are not included in myStruct already
	% from bottom upwards
	myStructBottomUpBuilder = struct(myFieldNameSplitted{end}, myValue);
	for newFieldName = flip(myFieldNameSplitted([~wasAlreadyAField(:).', false]).')
		myStructBottomUpBuilder = struct(newFieldName{:}, myStructBottomUpBuilder);
	end
	lastField = fieldnames(myStructBottomUpBuilder);
	myStructExplorerTopDown{end}.(lastField{1}) = myStructBottomUpBuilder.(lastField{1});
	
	% attach myStructExplorerTopDown to myStruct
	previouslyExistingFieldNames = myFieldNameSplitted(wasAlreadyAField);
	for it = 1 : sum(wasAlreadyAField)
		myStructExplorerTopDown{end-it}.(previouslyExistingFieldNames{end+1-it}) ...
			= myStructExplorerTopDown{end-it+1};
	end
	myStruct = myStructExplorerTopDown{1};
else
	myStruct.(myFieldName) = myValue;
end
end % plot()