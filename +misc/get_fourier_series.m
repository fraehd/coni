function signal = get_fourier_series(fc, t, T, k_vec)
%GET_FOURIER_SERIES calculates the fourier series by evaluating the sum for
% every fourier coefficient with the index k_vec over the period t.
%	signal = sum_{k in k_vec} fc_k * exp(i1 * k *omega * t)
% with
%	omega = 2*pi / T
% It is assumed, that the first dimension of the matrix fc represents
% the fourier number fitting to k_vec.
%		   
% INPUT PARAMETERS:
% 	MATRIX				   fc : fourier coefficients
%	VECTOR					t : time span of periodic signal (T not
%								included)
%	SCALAR					T : period time signal(k*T) = signal(0), k in Z
%	VECTOR				k_vec : indices of fourier coefficients
%   
% OUTPUT PARAMETERS:
%	MATRIX			   signal : signal resulting from the fourier
%								coefficients
%
% required subprogramms:
%   -
%
% global variables:
%   - 
%
% history:
%	-	created on 15.05.2018 by Jakob Gabriel

%% Input check
if size(fc, 1) ~= numel(k_vec)
	error('matrix of fourier coefficients do not fit to the index vector');
end

%% Init variables
omega = 2*pi / T;
fc_size = size(fc);
signal_size = fc_size;
signal_size(1) = numel(t); 

%% Reshape signal for easy indexing
fc_reshaped = reshape(fc, [fc_size(1), prod(fc_size(2:end))]);
signal_unshaped = zeros(numel(t), size(fc_reshaped, 2));

% Triangular cut of spectrum
% triangular_spec = max(0, 1-abs(k_vec)/(k_vec(end)));
% fc_reshaped = diag(triangular_spec) * fc_reshaped;
% Trapezoidal cut of spectrum
trapezoidal_spec = min(max(0, 1-abs(k_vec)/(k_vec(end)))*2, 1);
fc_reshaped = diag(trapezoidal_spec) * fc_reshaped;

for t_idx=1:numel(t)
	if size(fc_reshaped, 1) == 1 % nothing to sum up... hence, sum() would do non-sense
		signal_unshaped(t_idx, :) = diag(exp(1i*k_vec*omega*t(t_idx))) * fc_reshaped;
	else
		signal_unshaped(t_idx, :) = ...
			sum(diag(exp(1i*k_vec*omega*t(t_idx))) * fc_reshaped);
	end
end

signal = reshape(signal_unshaped, signal_size);

end

