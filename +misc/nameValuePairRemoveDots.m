function nameValuePairsCleaned = nameValuePairRemoveDots(varargin)
% misc.nameValuePairRemoveDots removes all names-value-pairs of varargin, which
% contain dots in the names. This is sometimes useful, since inputParser() can not
% deal with parameters that contain dots.
%%
% Example:
%		nameValuePairsCleaned = misc.nameValuePairRemoveDots(...
%			'asdf', 123', 'bli.bla', 'test')
nameValuePairsCleaned = {};
for it = 1 : 2 : numel(varargin)
	if ~contains(varargin{it}, '.')
		nameValuePairsCleaned(end+(1:2)) = varargin(it:(it+1));
	end
end % for it = 1 : 2 : numel(varargin)
end % misc.nameValuePairRemoveDots()