function [ x ] = nan2zero( x )
% NAN2ZERO replaces each nan entry with zero
%
% Description:
% [ x ] = nan2zero( x )
% Each entry in x that is NaN (not a number) is replaced by 0.
%
% Inputs:
%     x       Input variable
% Outputs:
%     x       Courrputed inputvariable
% 
% Example:
% -------------------------------------------------------------------------
% x = [1 2 3; NaN 5 6;7 NaN 9];
% x = misc.nan2zero(x)
% >> ans = [1 2 3
%           0 5 6
%           7 0 9]
% -------------------------------------------------------------------------

x(isnan(x)) = 0;

end

