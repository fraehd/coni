function [ y ] = minmax( x )
% MINMAX returns the minimal and maximal value of a vector
%
% Description:
% [ y ] = minmax( x )
% Returns y = [min(x), max(x)]
%
% Inputs:
%     x       Vector of interest
% Outputs:
%     y       Vector containing min(x) and max(x)
%
% Example:
% -------------------------------------------------------------------------
% x = [8 2 9 1 9];
% y = minmax(x)
% >> ans = [1 9]
% -------------------------------------------------------------------------

y = [min(x), max(x)];

end

