function f1 = plotEigenvalues(A, NameValue)
%MISC.PLOTEIGENVALUES plot eigenvalues in complex plane
% f1 = misc.plotEigenvalues(A) returns a figure f1 displaying the eigenvalues of A or, if A is a vector,
%	then A itself.
%
% misc.plotEigenvalues(A, "title", title) sets the title to the string title
%
% misc.plotEigenvalues(A1, A2, ..., ) plots all A1, A2, ... in separate subplots
%
% misc.plotEigenvalues(A1, A2, ..., "subplot", false) plots all A1, A2, ... the same plot
%
% misc.plotEigenvalues(A1, A2, ..., "title", title) sets the title of each subplot to the
% corresponding value of the string-array title

arguments (Repeating)
	A (:, :) double;
end
arguments
	NameValue.subplot = true;
	NameValue.title string = "";
end

if nargin > 0
	e = cell(size(A));
	realMin = 0;
	realMax = 0;
	imagMin = 0;
	imagMax = 0;
	for it = 1 : numel(A)
		if isvector(A{it})
			e{it} = A{it};
		else
			e{it} = eig(A{it});
		end
		
		realMin = min([real(e{it}); realMin], [], "all");
		realMax = max([real(e{it}); realMax], [], "all");
		imagMin = min([imag(e{it}); imagMin; realMin], [], "all");
		imagMax = max([imag(e{it}); imagMax; abs(imagMin)], [], "all");
	end
	f1 = figure();
	for it = 1 : numel(A)
		if NameValue.subplot
			subplot(1, numel(A), it);
		end
		e = eig(A{it});
		plot(real(e), imag(e), "Marker", "*", "Color", misc.color(it), "LineStyle", "none");
		hold on;
		
		xlim([realMin, realMax] + diff([realMin, realMax]) * [-1, 1]/10)
		ylim([imagMin, imagMax] + diff([imagMin, imagMax]) * [-1, 1]/10)
		xlabel('real')
		ylabel('imag')
		if strlength(NameValue.title) > 0
			if numel(NameValue.title) > 1
				title(NameValue.title(it));
			else
				title(NameValue.title);
			end
		end
	end
end % nargin > 0

end % function f1 = plotEigenvalues()

