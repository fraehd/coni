function b = writeToOutside(a)
%writeToOutside write values to outside of the valid spatial domain 
%   by replacing NaNs with their corresponding upper/lower neighbor

% created on 07.12.2018 by Simon Kerschbaum
% if nargin<2
% 	fast=0;
% end
sizA  =size(a);
if length(sizA)>3
	aResh = reshape(a,sizA(1),sizA(2),[]);
% 	sizAResh = size(aResh);
else
	aResh = a;
% 	sizAResh = sizA;
end

yNaNIdxLow = findfirst(~isnan(aResh),2,1);
yNaNIdxHigh = findfirst(~isnan(aResh),2,1,'last');
 % if the Index is written to 1, nothing is changed.
% yNaNIdxLow(yNaNIdxLow==0) = 1;
% yNaNIdxHigh(yNaNIdxHigh==0) = 1;

for xIdx = 1:sizA(1)
% 	if ~fast
% 		for yIdx = 2:sizA(2)
% 			yInvIdx = sizA(2)-yIdx+1;
% 			for tIdx = 1:size(aResh,3)
% 				if isnan(aResh(xIdx,yIdx,tIdx))
% 					aResh(xIdx,yIdx,tIdx) = aResh(xIdx,yIdx-1,tIdx);
% 				end
% 				if isnan(aResh(xIdx,yInvIdx,tIdx))
% 					aResh(xIdx,yInvIdx,tIdx) = aResh(xIdx,yInvIdx+1,tIdx);
% 				end
% 			end
% 		end
% 	else
		for tIdx = 1:size(aResh,3)
% 			for yIdx = 2:sizA(2)
% 				if isnan(aResh(xIdx,yIdx,tIdx)) && ~isnan(aResh(xIdx,yIdx-1,tIdx))
% 					aResh(xIdx,yIdx:end,tIdx) = aResh(xIdx,yIdx-1,tIdx);
% 					break
% 				end
% 			end
			% unfortunatelly 
% 			yNaNIdxLow = find(~isnan(aResh(xIdx,:,tIdx)),1);
% 			yNanIdxHigh = find(~isnan(aResh(xIdx,:,tIdx)),1,'last');
try
			aResh(xIdx,1:yNaNIdxLow(xIdx,1,tIdx)-1,tIdx) = aResh(xIdx,yNaNIdxLow(xIdx,1,tIdx),tIdx);
			aResh(xIdx,yNaNIdxHigh(xIdx,1,tIdx)+1:end,tIdx) = aResh(xIdx,yNaNIdxHigh(xIdx,1,tIdx),tIdx);
catch errmsg
	here=1;
	error('stopped here!')
end
			
% 			for yInvIdx = size(a,2)-1:-1:1
% 				if isnan(aResh(xIdx,yInvIdx,tIdx)) && ~isnan(aResh(xIdx,yInvIdx+1,tIdx))
% 					aResh(xIdx,1:yInvIdx,tIdx) = aResh(xIdx,yInvIdx+1,tIdx);
% 					break
% 				end
% 			end
		end
		
% 	end
end
if length(sizA)>3
	b = reshape(aResh,sizA);
else
	b = aResh;
end

end

