function [myOde, spectrumIndividual] = getMinimalInternalModel(...
		S, inputVectorMode, numberOfCopies, ...
		stateName, internalModelInputName, outputSignalName, referenceSignalName)
%misc.getMinimalInternalModel returnes the state space model myOde which is an 
%misc.ss.Odes object and represents an internal model based on the signal model 
%dynamics defined by S. 
%For this, S is split into jordan chains and redundancies are removed. The input 
%vector of mySs is either the 1-vector (inputVectorMode="full") or the input vector
%with the least non-zero entries (inputVectorMode="full" (=default)), i.e. it is 
%zero, but 1 for every last row of a jordan block. 
%Lastly, for multiple outputs, multiple copies of the internal model are needed, which is
%specified by the last input parameter numberOfCopies.
%
% mySs = misc.getMinimalInternalModel(S) generates a state space model which can
% create all signal forms  specified by the dynamic matrix S. The input vector mySs.B
% contains as many zeros as possible.
%
% mySs = misc.getMinimalInternalModel(S, inputVectorMode) specifies if mySs.B is the
% 1-vector (inputVectorMode="full") or the input vector with the least non-zero
% entries (inputVectorMode="full" (=default)).
%
% mySs = misc.getMinimalInternalModel(S, inputVectorMode, numberOfCopies) creates
% n=numberOfCopies (default = 1) copies of the internalModel.
%
% mySs = misc.getMinimalInternalModel(S, inputVectorMode, numberOfCopies, stateName)
% the outputName-property of the resulting stateName will be set to stateName
% (default: 'internalModel')
%
% mySs = misc.getMinimalInternalModel(S, inputVectorMode, numberOfCopies, ...
%	stateName, internalModelInputName) the inputName-property of the resulting
%	state space will be set to controlOutputName (default: 'trakcingError').
%
% mySs = misc.getMinimalInternalModel(S, inputVectorMode, numberOfCopies, ...
%	stateName, internalModelInputName, outputSignalName, referenceSignalName) 
%	additional output of the internal model state space will be 
%		internalModelInputName = outputSignalName - referenceSignalName.
%	The InputName are outputSignalName and referenceSignalName instead of internalModelInputName.

arguments
	S (:, :) double;
	inputVectorMode (1, 1) string = "minimal";
	numberOfCopies (1, 1) double = 1;
	stateName string = "internalModel";
	internalModelInputName string = "trackingError";
	outputSignalName string = "controlOutput";
	referenceSignalName string = string();
end % arguments

% Smin: find minimal representation of S
[sMin, lengthBlocks] = minimalDynamicMatrix(S);
spectrumIndividual = eig(sMin);

% B: choose B according to inputVector mode
switch inputVectorMode
	case "full"
		B = ones(size(sMin, 1), 1);
	case "minimal"
		B = zeros(size(sMin, 1), 1);
		B(cumsum(lengthBlocks), 1) = 1;
	otherwise
		error("inputVectorMode must be minimal or full but is " + inputVectorMode);
end %switch

% add additional copies
sMinUnique = sMin;
Bunique = B;
for it = 2 : numberOfCopies
	sMin = blkdiag(sMin, sMinUnique);
	B = blkdiag(B, Bunique);
end

mySs = ss(sMin, B, eye(size(sMin)), []);
mySs = misc.ss.setSignalName(mySs, "output", stateName, size(mySs.C, 1));

if strlength(referenceSignalName) > 0
	% implement tracking error via sum 
	%	internalModelInputName = outputSignalName - referenceSignalName
	mySs = misc.ss.setSignalName(mySs, "input", internalModelInputName, size(mySs.B, 2));
	% get sum to create tracking error signal in simulation, i.e. a sum block:
	trackingSum = sumblk(char(internalModelInputName + ...
		"=" + outputSignalName + "-" + referenceSignalName), ...
		numberOfCopies);
	mySs = misc.ss.connect(trackingSum.InputName, [trackingSum.OutputName; mySs.OutputName], mySs, trackingSum);
	
else
	% feed internal model via outputSignalName
	mySs = misc.ss.setSignalName(mySs, "input", internalModelInputName, size(mySs.B, 2));
end

myOde = misc.ss.Odes(mySs, stateName);

end % getMinimalInternalModel()

function [sMin, lengthBlocks] = minimalDynamicMatrix(S)
% minimalDynamicMatrix reduces the spectrum of the dynamic matrix S, such that still
% all modes can be generated, but - for instance - if there are two integrators in 
% parallel, the result will only contain one integrator. Likewise, if there is an 
% integrator and a series of integrators, then only the largest series of integrators
% will remain. This is done by tranforming S into a Jordan block matrix, then 
% all the Jordan blocks are compared and those are removed whos complete spectrum is
% part of any other cell element. Lastly, if the Jordan blocks non-zero imaginary
% part, i.e. a sine like diag(-i, i), then this converted into a real matrix 
% (for the sine example: [0, 1; -1, 0]). For ease of implementation, this function is
% restricted for matrices with soley imaginary spectrum, i.e. all(real(eig(S))==0).
% But this restriction might be removed, if the conversion from matrices with
% complex elements into the final matrix with real elements is implemented for this
% case.
arguments
	S (:, :) double {mustBe.imaginarySpectrum};
end % arguments

% Calculate jordan blocks and split them into cell-elements
[~, jordanMatrix, lengthJordan] = misc.jordanReal(S);%misc.jordan_complete(S);
jordanMatrixCell = misc.blkdiagInv(jordanMatrix, lengthJordan);

% calculate spectrum of each jordan block
spectrum = cell(size(jordanMatrixCell));
for it = 1 : numel(jordanMatrixCell)
	spectrum{it} = eig(jordanMatrixCell{it});
end % for it = 1 : numel(S)

% select which jordan block is already contained in other blocks, and set selector =
% false for those, which should be removed later.
selector = true(size(jordanMatrixCell));
for it = 1 : numel(jordanMatrixCell)
	for jt = it + 1 : numel(jordanMatrixCell)
		if numel(spectrum{it}) > numel(spectrum{jt})
			if all(ismember(spectrum{jt}, spectrum{it}), "all")
				selector(jt) = false;
			end
		else
			if all(ismember(spectrum{it}, spectrum{jt}), "all")
				selector(it) = false;
			end
		end		
	end % for jt = it + 1 : numel(jordanMatrixCell)
end % for it = 1 : numel(jordanMatrixCell)

% remove redundant dynamics of the jordanMatrixCell with the selector obtained above
lengthBlocks = lengthJordan(selector);
sMin = blkdiag(jordanMatrixCell{selector});
end % minimalDynamicMatrix