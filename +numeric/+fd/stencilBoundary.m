function [stencil, gridSelector] = stencilBoundary(stencilLength, grid, z)
% numeric.fd.stencilBoundary creates the 1d stencils for finite differences (fd)
% approximations at the boundaries of the domain using forward or backward schemes,
% see https://en.wikipedia.org/wiki/Finite_difference_coefficient
%
% [stencil, gridSelector] = numeric.fd.stencilBoundary(stencilLength, grid, z)
% returns a stencil of the length stencilLength, suiting to the discrete grid defined
% by grid for the location z on grid. Grid must be homogenious if stencilLength > 2
% and z must be either grid(1) or grid(2). This is implemented for stencilLength = 2,
% 3, 4, 5, 6, 7. The 2nd output gridSelector can be used to select the gridPoints out
% of grid which the stencil is applied.
%
%% Example:
% testData = linspace(0, 1, 11).'.^2; % testData = x^2;
% [myStencil, myGridSelector] = numeric.fd.stencilBoundary(...
%												2, linspace(0, 1, 11), 1);
% testDataDerivative = myStencil * testData(myGridSelector);
% testDataReferenceResult = gradient(testData, 0.1);
% numeric.near(testDataDerivative, testDataReferenceResult(end), 1e-6)

% validate input
myParser = misc.Parser();
myParser.addRequired('stencilLength', @(v) any(v==(2:7)));
myParser.addRequired('grid', @(v) isnumeric(v) & isvector(v) & issorted(v, 'ascend'));
myParser.addRequired('z', @(v) isscalar(v) & ((v == grid(1)) | (v == grid(end))));
myParser.parse(stencilLength, grid, z);
if ~numeric.near(diff(grid), mean(diff(grid))) && (stencilLength > 2)
	warning('only implemented for homogenious grids');
end

% get grid selector
gridSelector = false(size(grid));
if z == grid(1)
	gridSelector(1:stencilLength) = true;
else
	gridSelector(end+((1-stencilLength):0)) = true;
end

% create stencil based on the forward schemes found here:
% see https://en.wikipedia.org/wiki/Finite_difference_coefficient
switch myParser.Results.stencilLength
	case 2
		stencil = [-1, 1] / mean(diff(grid(gridSelector)));
	case 3
		stencil = [-1.5, 2, -0.5] / mean(diff(grid(gridSelector)));
	case 4
		stencil = [-11/6, 3, -1.5, 1/3] / mean(diff(grid(gridSelector)));
	case 5
		stencil = [-25/12, 4, -3, 4/3, -1/4]/ mean(diff(grid(gridSelector)));
	case 6
		stencil = [-137/60, 5, -5, 10/3, -5/4, 1/5] / mean(diff(grid(gridSelector)));
	case 7
		stencil = [-49/20, 6, -15/2, 20/3, -15/4, 6/5, -1/6] / mean(diff(grid(gridSelector)));
	otherwise
		error('stencilLength is not valid')
end % switch

% for the right boundary, the forward stencils are converted to backward stencils by
% flipping and multiplying with (-1)
if z == grid(end)
	stencil = -flip(stencil);
end
end % numeric.fd.stencilBoundary()