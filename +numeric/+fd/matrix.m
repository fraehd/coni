function D = matrix(scheme, grid, derivative)
%numeric.fd.matrix creates the 1d matrix for approximating derivatives with the
% finite differences method (fd), 
% see	https://en.wikipedia.org/wiki/Finite_difference_coefficient
%		http://web.media.mit.edu/~crtaylor/calculator.html
%
% D = numeric.fd.matrix(scheme, grid, z, derivative)
% returns a stencil of the length scheme, suiting to the discrete grid defined
% by grid for the location z on grid. Grid must be homogenious if scheme > 2
% and z must be either grid(1) or grid(2). This is implemented for scheme = 2,
% 3, 4, 5, 6, 7. The 2nd output gridSelector can be used to select the gridPoints out
% of grid which the stencil is applied.
%
%% Example:
% testData = linspace(0, 1, 21).'.^2; % testData = x^2;
% myD = numeric.fd.matrix('5pointCentral', linspace(0, 1, 21), 1);
% numeric.near(testDataDerivative, 2*linspace(0, 1, 21).', 1e-6)

% list of all valid schemes used for unittests
implementedSchemes = {'5pointCentral', '3pointCentral'};
% validate input
myParser = misc.Parser();
myParser.addRequired('scheme', @(v) any(contains(implementedSchemes, v)));
myParser.addRequired('grid', @(v) isnumeric(v) & isvector(v) & issorted(v, 'ascend'));
myParser.addRequired('derivative', @(v) v==1 | v==2);
myParser.parse(scheme, grid, derivative);

spacingVector = diff(grid);
if max(abs(spacingVector)-median(spacingVector)) > 1e-6
	warning('numeric.fd.matrix does not support spatial varying grids');
end

% create stencil based on the forward schemes found here:
%		https://en.wikipedia.org/wiki/Finite_difference_coefficient
%		http://web.media.mit.edu/~crtaylor/calculator.html

switch myParser.Results.scheme
	case '5pointCentral'
		if derivative == 1
			boundaryLeft = zeros(2, 5);
			boundaryLeft(1, :) = numeric.fd.stencilBoundary(5, 0:4, 0);
			boundaryLeft(2, :) = [-3, -10, +18, -6, 1]/12;
			centralStencil = [1, -8, 0, 8 , -1]/12;
		elseif derivative == 2
			boundaryLeft = zeros(2, 5);
			boundaryLeft(1, :) = [35, -104, +114, -56, 11]/12;
			boundaryLeft(2, :) = [11, -20, 6, 4, -1]/12;
			centralStencil = [-1, 16, -30, 16, -1]/12;
		else
			error(['scheme ', myParser.Results.scheme, ' is not implemented ', ...
				'for derivative ', num2str(derivative), ' yet']);
		end
		
	case '3pointCentral'
		if derivative == 1
			boundaryLeft(1, :) = numeric.fd.stencilBoundary(3, 0:2, 0);
			centralStencil = [-1, 0, 1]/2;
		elseif derivative == 2
			boundaryLeft(1, :) = [1, -2, 1];
			centralStencil = [1, -2, 1];
		else
			error(['scheme ', myParser.Results.scheme, ' is not implemented ', ...
				'for derivative ', num2str(derivative), ' yet']);
		end
			
	otherwise
			error(['scheme ', myParser.Results.scheme, ' is not implemented yet']);
end % switch

% right boundary follows due to symmetry by rotation and a minus if derivative is a
% uneven number
if mod(derivative, 2) == 0
	boundaryRight = fliplr(flipud(boundaryLeft));
elseif mod(derivative, 2) == 1
	boundaryRight = -fliplr(flipud(boundaryLeft));
end

% consider centralStencil all over domain
n = numel(grid);
D = zeros(n);
for it = 1 : numel(centralStencil)
	distance2center = it - ceil(numel(centralStencil)/2);
	D = D + diag(centralStencil(it)*ones(n-abs(distance2center), 1), distance2center);
end

% consider boundary stencils left and right
D(1:size(boundaryLeft, 1), 1:size(boundaryLeft, 2)) = boundaryLeft;
D(end+((1-size(boundaryLeft, 1)):0), end+((1-size(boundaryLeft, 2)):0)) = boundaryRight;

% consider spacing
spacing = diag(gradient(grid));
D = (spacing.^derivative) \ D;
end % numeric.fd.matrix()