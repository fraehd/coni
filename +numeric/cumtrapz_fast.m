function z=cumtrapz_fast(x,y)
% cumtrapz_fast fast cumulative trapezodial integration
% cumtrapz_fast cumulatively integrates the rows of a matrix.
%
% Description:
% z = cumtrapz_fast(y) 
% integrates y over the argument (0,1), where the values of y are assumed 
% to be uniformly distributed.
%
% z = cumtrapz_fast(x,y) 
% integrates y over the space x.
%
% Inputs:
%     x       Discretized space vector
%     y       Discretized integrand function
% Outputs:
%     z       Cumulative integration result
%
% Example:
% -------------------------------------------------------------------------
% t = 0:0.1:pi;
% y = sin(t);
% z = numeric.cumtrapz_fast(t,y);
% plot(t,y,t,z);
% -------------------------------------------------------------------------
%
% see also cumtrapz, cumtrapz_fast_nDim, trapz_fast, trapz_fast_nDim

% created by Simon Kerschbaum on 12.09.2016

if size(x,1)~= 1
	if size(x,2)== 1
		x=x.';
	else
		error('x must be a row vector!')
	end
end

if nargin < 2
	y = x;
	x = linspace(0,1,size(x,2));
end

if size(x,2)~= size(y,2)
	error('size of x and y not consistent! (nD case)')
end

if length(x)==1
	z = zeros(size(y));
else
	z = [zeros(size(y,1),1), (cumsum(repmat(diff(x)/2,size(y,1),1).* (y(:,1:size(y,2)-1) + y(:,2:size(y,2))),2))]; 
end