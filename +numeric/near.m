function [i, m, s] = near(nominalValue, actualValue, tolerance, relative)
%NEAR checks 'equality' for double numerical values.
%   i = near(nominalValue, actualValue, tolerance) checks if
%        m = max( abs(nominalValue - actualValue) ) < tolerance.
%   The argument tolerance is optional and is 10*eps by default.
% [i, m, s] = near(nominalValue, actualValue, tolerance, relative) checks
% if 
%   abs(nominalValue - actualValue) < tolerance.
% with tolerance, the absolute tolerance can be set. By the input arg
% relative, it can be chosen if the relative deviation
%	m = max( | (nominalValue - actualValue) / max(|nominalValue|)
% For this, relative has to be set to true.
% The output parameters are i: logical value if the tolerance holds, m: is
% the maximal deviation and s is a string which is printed if no output is
% requested.

if nargin <= 2
    tolerance = 10*eps;
end

if nargin == 1
	actualValue = zeros(size(nominalValue));
end

if nargin < 4
	relative = 1;
else
	if islogical(relative)
		relative = max(abs(nominalValue(:)));
	end
end
	

absV = abs(nominalValue ./ relative - actualValue ./ relative);
i = all(absV(:) < tolerance);
m = max( absV(:) );

s = sprintf('Is near %i, maximal difference: %d', [i, m]);

if nargout == 0
	i = s;
end

end