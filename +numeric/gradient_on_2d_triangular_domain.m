function [x_dzeta, x_dz] = gradient_on_2d_triangular_domain(x, spacing, domain)
% gradient_on_2d_triangular_domain the gradient of x which is defined only
% on a triangular domain, which repeatedly occurs when dealing with
% backstepping kernels, which are usally defined on
% (z, zeta) \in {(z, zeta) | 0 <= zeta <= z <= 1},
% or, alternatively by specifying domain = 'zeta>=z', on
% (z, zeta) \in {(z, zeta) | 0 <= z <= zeta <= 1}
% It is assumed, that the first dimension of x is the z coordinate, and that
% the second dimension is the zeta coordinate. Since x is defined only
% on half of the (z,zeta) plane, the missing part is filled with mirrored
% values, to avoid problems at the diagonal boundary of the triangular domain.
%
% INPUT PARAMETERS:
%	ARRAY			x : Matrix containg the values to be differentiated.
%						Assumed to be ordered, i. e.
%						x(z, zeta, index, index, index...)
%	SCALAR	  spacing : space between two point of the spatial grid which
%							is assumed to be homogenious.
% OPTIONAL INPUT:
%	CHAR-ARRAY domain : triangular domain on which the derivatives are calculated
%						Either 'zeta<z' (default) or 'zeta>z'.
%
% OUTPUT PARAMETERS:
%	ARRAY	  x_dzeta : spatial derivative in zeta direction
%	ARRAY		 x_dz : spatial derivative in z direction
%
% Example:
% -------------------------------------------------------------------------
% triangle = tril(ones(21), 0);
% x = ones(21, 21, 2); x(:,:,1) = magic(21).*triangle; x(:,:,2) = meshgrid(1:21, 1:21).*triangle;
% spacing = 1/(21-1);
% [~, x_dz] = numeric.gradient_on_2d_triangular_domain(x, spacing);
% dps.hyperbolic.plot.matrix_4D(x); dps.hyperbolic.plot.matrix_4D(x_dz);
% -------------------------------------------------------------------------

if nargin < 3
	domain = 'zeta<=z';
end

% 'zeta<=z' is the default case. The other case is implemented by permuting the
% z and zeta-coordinates.
domain = strrep(domain, ' ', ''); % remove spaces
if strcmp(domain, 'zeta>=z')
	x = permute(x, [2, 1, 3, 4]);
elseif ~strcmp(domain, 'zeta<=z')
	error("domain must be either 'zeta<=z' or 'zeta>=z'")
end

xSize = size(x);
if numel(xSize)>2
	numberOfIndices = prod(xSize(3:end));
else
	numberOfIndices = 1;
end
ndisc = xSize(1);

[zIdx, zetaIdx] = ndgrid(1:1:ndisc, 1:1:ndisc, 1:1:numberOfIndices);
xReshaped = reshape(x, [ndisc, ndisc, numberOfIndices]);
xReshaped(zetaIdx > zIdx) = 0;

% Calculate gradient
[xReshaped_dzeta, xReshaped_dz] = gradient(xReshaped, spacing);
xReshaped_dzeta(zetaIdx >= zIdx) = 0;
xReshaped_dz(zetaIdx >= zIdx) = 0;

for sIdx = 2 : ndisc-1
	xReshaped_dzeta(sIdx, sIdx, :) = (xReshaped(sIdx, sIdx, :) - xReshaped(sIdx, sIdx-1, :))/spacing;
	xReshaped_dz(sIdx, sIdx, :) = (xReshaped(sIdx+1, sIdx, :) - xReshaped(sIdx, sIdx, :))/spacing;
end
xReshaped_dzeta(ndisc, ndisc, :) = (xReshaped(ndisc, ndisc, :) - xReshaped(ndisc, ndisc-1, :))/spacing;
xReshaped_dz(1, 1, :) = (xReshaped(1+1, 1, :) - xReshaped(1, 1, :))/spacing;
for it = 1 : numberOfIndices
	xReshaped_dzeta(1, 1, it) = interp1([2, 3], ...
		[xReshaped_dzeta(2, 2, it), xReshaped_dzeta(3, 3, it)], ...
		1, 'linear', 'extrap');
	xReshaped_dz(ndisc, ndisc, it) = interp1([-2, -1]+ndisc, ...
		[xReshaped_dz(ndisc-2, ndisc-2, it), xReshaped_dz(ndisc-1, ndisc-1, it)], ...
		ndisc, 'linear', 'extrap');
end
% Set values outside of the domain to zero and reshape
x_dzeta = reshape(xReshaped_dzeta, xSize);
x_dz = reshape(xReshaped_dz, xSize);
if strcmp(domain, 'zeta>=z')
	% permute back
	x_dzetaBackup = x_dzeta;
	x_dzeta = permute(x_dz, [2, 1, 3, 4]);
	x_dz = permute(x_dzetaBackup, [2, 1, 3, 4]);
end
end
