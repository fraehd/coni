function [z, dt] = heuristicdiscretization(v, z0, zL, n)
% HEURISTICDISCRETIZATION computes a discretization for spatial-varying
% transportations speeds
%
% Description:
% [z, dt] = heuristicdiscretization(v, z0, zL, n)
% Computes the spatial discretization z = [z0, z1, ..., zL] and the time
% step dt dependent on the spatial-varying transportation speed v, for
% the spatial domain = (z0, zL). The wanted number of discretization
% points is assigned by n. ATTENTION: the number of discretization points
% of the found discretization can be different!
%
% For a good simulation of transport processes there is a preferred 
% spatial ratio for time and space discretization: 
% (I)        v(z_k) * dt / dz_k = 1
% To find an appropriate spatial discretization for a given
% transportation speed, fsolve is used to solve tow nested system of
% equations. The inner equations is to ensure that equation (I) holds and
% the outer solver choses dt so that the length of the spatial domain
%     z(end) = zL - z0
% is preserved.
%
% Example:
% -------------------------------------------------------------------------
% heuristicdiscretization(@(z) 1 + z, 0, 1, 10)
% -------------------------------------------------------------------------

fsolopts = optimoptions('fsolve');
fsolopts.Display = 'off';

% inital guess for dz and dt
z  = linspace(z0, zL, n);              
dt = integral(v, z(1), z(2)) / v(z(2));     

dt = fsolve(@discretize_dt, dt, fsolopts);

    function [F] = discretize_dt(dt)
       z = discretize_dz(z, dt);
       F = z(end) - (zL - z0);
    end

    function [z] = discretize_dz(z, dt)
        k = 1;
        z = 0;
        while z(end) < zL
            fun = @(zeta)(v(zeta) * dt - integral(v, z(k), zeta));
            z(k+1) = fsolve(fun, z(k), fsolopts);     
            k=k+1;
        end        
        
    end
end