function [dy_dx, dy_dx_findiff] = diff_num( x,y,dim )
% DIFF_NUM numerical differentiation
%   
% Description:
% dy_dx = diff_num(x,y) 
% computes the numerical approximation of the derivative dy/dx, where x 
% and y are the discretized vectors with y(i) = f(x(i)). Uses FEM with 
% linear basis functions according to pde_fem.
%
% dy_dx = diff_num(x,y,dim) 
% differentiates the dim-th dimension of y.
%   
% Inputs:
%     x               Discretized space vector
%     y               Discretized function vector
%     dim             Dimension of y to be differntiated
% Outputs:
%     dy_dx           Derivative of y w.r.t. x
%     dy_dx_findiff 
%
% Example:
% -------------------------------------------------------------------------
% t = 0:0.1:pi;
% y = sin(t)';
% dy = numeric.diff_num(t,y);
% plot(t,y,t,dy);
% -------------------------------------------------------------------------
%

% history:
% created on 13.04.2017 by Simon Kerschbaum
% modified on 05.10.2018 by SIMON KERSCHBAUM:
%   * enable handling of multidimensional arrays
%
% uses
% get_grad_op
% multArray

if nargin<3
	dim=1;
end

if ~isvector(x)
	error('x must be a vector!')
end
if numel(x)~= size(y,dim)
	error(['size of x=' num2str(numel(x)) ' not consistent to size(y,' num2str(dim) ')=' num2str(size(y,dim)) '!']);
end

grad_op = numeric.get_grad_op(x);
% y may be multidimensional.
dy_dx = permute(misc.multArray(grad_op,y,2,dim),[2:dim 1 dim+1:ndims(y)]);


if nargout>1
	% x als Zeilenvektor schreiben:
	x = x(:).';

	% Ableitung mit finiten Differenzen bestimmen:
	% mittige Punkte (centered): dy(i)/dx = (y(x(i+1)) - y(x(i-1)))/(x(i+1)-x(i-1))
	% Randpunkte: dy(1)/dx = (y(2)-y(1))/(x(2)-x(1))

	if dim~= 1
		yPerm = permute(y,[dim 1:dim-1 dim+1:ndims(y)]);
		sizYPerm = size(yPerm);
	else
		yPerm=y;
		sizYPerm = size(y);
	end
	if ~ismatrix(y)		
		yResh = reshape(yPerm,size(yPerm,1),[]);
	else
		yResh = y;
	end
	% erst alle Punkte in einer Operation:
	if length(x)>1
		if length(x)>2 % nicht nur Randpunkte
			dy_dx_findiffResh(1:length(x),1:size(yResh,2)) = 0;
			dy_dx_findiffResh(2:end-1,1:size(yResh,2)) = (yResh(3:end,:)-yResh(1:end-2,:))./repmat((x(3:end)-x(1:end-2)).',1,size(yResh,2));
		end
		% Randpunkte gesondert
		for i=1:2
			idx = (i-1)*(length(x)-1)+1;
			dy_dx_findiffResh(idx,1:size(yResh,2)) = (yResh(idx+1-(i-1),:)-yResh(idx-(i-1),:))./(x(idx+1-(i-1))-x(idx-(i-1)));
		end
	else
		dy_dx_findiffResh = zeros(length(x),size(yResh,2));
	end
	if ~ismatrix(y)
		dy_dx_findiffPerm = reshape(dy_dx_findiffResh,sizYPerm);
	else
		dy_dx_findiffPerm = dy_dx_findiffResh;
	end
	if dim~=1
		dy_dx_findiff = permute(dy_dx_findiffPerm,[2:dim 1 dim+1:ndims(y)]);
	else
		dy_dx_findiff = dy_dx_findiffPerm;
	end
end


end

