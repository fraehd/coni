function z=trapz_fast_nDim(x,y,dim,equal,ignoreNaN) %#ok<INUSD>
%trapz_fast_nDim fast trapezoidal integral with arbitrary dimension
%
% z = trapz_fast_nDim(y) integrates y over the argument (0,1), where the
%   values of y are assumed to be uniformly distributed over the argument.
%
% z = trapz_fast_nDim(x,y) integrates the first dimension of y over the argument x, if x
%   is a vector. If x is an array, it integrates over the first dimension of x.
%
% z = trapz_fast_nDim(x,y,dim) integrates the dim-th dimension of y over x, if x is a vector.
%   If x is an array, it integrates over the dim-th dimension of x. Then, the dim-th 
%   dimension of x must have the same length as the dim-th dimension of y.
%   The result z is an array whose dimensions are the concatenated dimensions of:
%   [dimensions of x without the integrated dimension, 
%        dimensions of y where the integrated dimension has length 1].
%
% z = trapz_fast_nDim(x,y,dim,'equal') integrates the dim-th dimension of y over the dim-th
%   dimension of x. x and y must have the same size, such that each dimension element of y is
%   integrated wrt. the respective dimension element of x
%
% z = trapz_fast_nDim(x,y,dim,'equal','ignoreNaN') 
%   ignores NaN entries in x and y.
%     
% Examples:
% -------------------------------------------------------------------------
% Integrate multiple functions over different arguments in one operation
% x = [1 2 3;
%      2 3 4];
% is a matrix which contains two rows of integration variables.
% y = [4 5 6;
%      7 8 9];
% contains the corresponding function values. Now
% z = trapz_fast_nDim(x,y,2,'equal')
% will result in z(1) = trapz(x(1,:),y(1,:))
%                z(2) = trapz(x(2,:),y(2,:))
% -------------------------------------------------------------------------
%
% INPUT PARAMETRS:
%   ARRAY     x        : Integration variable
%   ARRAY     y        : Integrand
%   INTEGER   dim(1)   : Dimension of the integrand that shall be integrated. If x is an
%                        array, the dim-th dimension of x is the integration variable.
%             ~        : if a 4th argument is given, x and y must have the same size, such that
%                        each dimension element of y is integrated wrt. the respective 
%                        dimension element of x
%             ~        : if a 5th argument is given, NaNs in x are ignored.
%
% OUTPUT PARAMETERS:
%   ARRAY     z        : Result of the integration
%
%
% see also doc.multidimensional_operations, trapz, trapz_fast, cumtrapz_fast, cumtrapz_fast_nDim

% created by Simon Kerschbaum 28.09.2018

% to performance:
% it is only effective to store size(a,n) or length() commands in a variable, if used for more
% than about 3-4 times.
% always store scalars if possible.
	import misc.*
	if nargin<3
		dim =1;
	end
	if ~isvector(x)
		sizX = size(x);
		if sizX(dim) ~= size(y,dim)
			error('If x is an array, size(x,dim) must equal size(y,dim)!')
		end
	end
	
	if nargin<2
		y = x;
		x = linspace(0,length(x),1); % integration from 0 to 1
	end
	if isvector(x) && size(x,2) ~= 1 && nargin<4
		x = x(:); % column vector
	end

	sizY = size(y); % here it is better to store the size because it is used so often!
	m = sizY(dim); % discretization points
	if nargin>3
		if sizY ~= size(x)
			error('When passing the "equal"-argument, size(x) must equal size(y)!')
		end
	end
	if dim ~= 1
		yPerm = permute(y,[dim 1:dim-1 dim+1:ndims(y)]); % permute desired dimension to first
		if nargin>3
			xPerm = permute(x,[dim 1:dim-1 dim+1:ndims(x)]); % permute desired dimension to first
		else
			xPerm = x;
		end
		sizYPerm = sizY([dim 1:dim-1 dim+1:ndims(y)]);
	else
		yPerm=y;
		xPerm=x;
		sizYPerm = sizY;
	end
	if nargin>3 && ~ismatrix(x)
		xResh = reshape(xPerm,m,[]); % store all further dimensions in 1
	else
		xResh = xPerm;
	end
	if ~ismatrix(y)
		yResh = reshape(yPerm,m,[]); % store all further dimensions in one
		sizYResh = size(yResh);
	else
		yResh = yPerm;
		sizYResh = sizYPerm;
	end
	
	if isvector(x) && length(x) ~= m
		error(['Number of entries in x not equal to number of entries in dimension ' num2str(dim) ' of y!']);
	end

	if (~isvector(x) && size(x,dim)==1) || (isvector(x) && length(x) == 1) % special case: integrate over point.
		% y is reshaped, such that dim is at first position always. 
		zResh = zeros(1,sizYResh(2:end));
	else
		if ~isvector(x) % y is reshaped such the dim is at first position always!
			if nargin<4 % all combinations of other dimensions are evaluated
				zResh = multArray(diff(x,1,dim)/2,(yResh(1:m-1,:) + yResh(2:m,:)),dim,1); 
			% by performing multArray, the dim-th dimension of x is automatically shifted to be
			% the last dimension. Then it is collapsed by the multiplication.
			else % further dimensions of x belong to further dimensions of y
				% in that case, size(x)=size(y)! That means interesting dimension is 1!
				if nargin > 4 && any(isnan(xResh(:)))
					dx = diff(xResh);
					dx(isnan(dx)) = 0;
					yResh(isnan(xResh)) = NaN;
					yResh(isnan(yResh)) = 0;
					zResh = (sum(dx/2 .* (yResh(1:m-1,:) + yResh(2:m,:)),1));
				else
					zResh = sum((diff(xResh)/2) .* (yResh(1:m-1,:) + yResh(2:m,:)),1).'; % matrix multiplication
				end
			end
		else
			zResh = sum((diff(x)/2) .* (yResh(1:m-1,:) + yResh(2:m,:)),1); % matrix multiplication
		end
	end
	if ~ismatrix(y) % passt noch nicht vermutlich!
		if ~isvector(x) && nargin<4
			zPerm = reshape(zResh,[sizX([1:dim-1 dim+1:ndims(x)]) 1 sizYPerm(2:end)]); % restore all collapsed dimensions, the integrated one is length 1
		else
			zPerm = reshape(zResh,[1 sizYPerm(2:end)]); % restore all collapsed dimensions, the integrated one is length 1
		end
	else
		zPerm = zResh;
	end
	nxNew = ndims(x)-1;
	% restore the integrated dimension with length 1 at correct position dim
	if dim~=1
		if nargin<4 %all combinations
			if ~isvector(x)
				z = permute(zPerm,[1:nxNew,nxNew+2:nxNew+dim,nxNew+1,nxNew+dim+1:nxNew+ndims(y)]); % permute integrated dimension back so it is at dimension dim+ndims(x)
			else
				z = permute(zPerm,[2:dim,1,dim+1:ndims(y)]); % permute integrated dimension back so it is at dimension dim+ndims(x)
			end
		else % only dimensions of y
			z = permute(zPerm,[2:dim 1 dim+1:ndims(y)]);
		end
	else
		% z  = permute(zPerm,[ndims(y)+1 1:ndims(y)]); % create first dimension of length 1
		z = zPerm;
	end
end % function