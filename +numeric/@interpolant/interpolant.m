classdef interpolant
% Class interpolant: This class intends to fix the nasty error of
% griddedInterpolant: "Interpolation requires at least two sample points
%						in each dimension."
% This is done by implementing a griddedInterpolant with squeezed values
% and by accessing values by a new evaluate function. With varargin
% settings can be passed to the griddedInterpolant, for instance, the type
% of interpolation (linear, quadratic, ...) or extrapolation can be allowed
% or forbidden. For details see griddedInterpolant documentation.
%
%% Examples (griddedInterpolant would not work)
% % Example 1: interpolation
% testData = rand(5,1,3);
% testInterpolant = numeric.interpolant({1:5, 1, 1:3}, testData);
% test_2p5_1_1 = testInterpolant.evaluate(2.5, 1, 1);
%
% % Example 2: sampling data
% testData = rand(5,1,3);
% testInterpolant = numeric.interpolant({1:5, 1, 1:3}, testData);
% testDataHigherResolution = testInterpolant.evaluate(1:.5:5, 1, 1:3);
% dps.hyperbolic.plot.matrix_3D(testData);
% dps.hyperbolic.plot.matrix_3D(testDataHigherResolution);

	properties (SetAccess = public)
		reducedGriddedInterpolant {mustBe.griddedInterpolant};
	end
	properties (SetAccess = private)
		reducedDimension (1,:);		% vector of indices that are 
									% used in reducedGriddedInterpolant
		permutationVector (1,:);	% vector of sequence of dimensions
									% used to permute reduced value back to
									% original size
	end
	
	methods
		%% Constructor
		function obj = interpolant(gridOriginal, valueOriginal, varargin)
% INPUT PARAMETERS:
%	CELL-ARRAY		 gridOriginal : grid vectors of valueOriginal in a cell array
%	ARRAY			valueOriginal : sample data
%	VARARGIN			 varargin : optional parameters for
%									griddedInterpolant, e.g. extrapolation
%									or type of interpolation. See
%									documentation of griddedInterpolant.

			sizeOriginal = misc.sizeOf(valueOriginal);
			valueSqueezed = squeeze(valueOriginal);
			if isscalar(valueSqueezed)
				sizeSqueezed = 1;
			else
				sizeSqueezed = size(valueSqueezed);
				sizeSqueezed = sizeSqueezed(sizeSqueezed>1);
			end
			obj.reducedDimension = zeros(1, numel(sizeSqueezed));
			obj.permutationVector = zeros(1, numel(sizeOriginal));
			jt = 1;
			numberOfDeletedDimensions = numel(sizeOriginal) - numel(sizeSqueezed);
			for it=1:numel(sizeOriginal)
				if sizeOriginal(it) == sizeSqueezed(jt)
					obj.reducedDimension(jt) = it;
					obj.permutationVector(it) = jt;
					jt = jt+1;
				else
					obj.permutationVector(it) = numel(sizeOriginal)-numberOfDeletedDimensions+1;
					numberOfDeletedDimensions = numberOfDeletedDimensions - 1;
				end
			end
			if numel(sizeOriginal) == 1 && sizeOriginal == 1
				% consider scalar case
				if sizeOriginal == 1
					gridSqueezed = [0, 1];
					valueSqueezed = [valueSqueezed, valueSqueezed];
					obj.permutationVector = [1, 2];
				end
			else
				if numel(sizeOriginal) == 1
					% consider vector case
					obj.permutationVector = ones(1,2);
					obj.permutationVector = obj.permutationVector ...
								+ (sizeOriginal ~= size(valueOriginal));
				end
				gridSqueezed = gridOriginal(obj.reducedDimension);
				for it = 1 : numel(gridSqueezed)
					if issorted(gridSqueezed{it}, 'descend')
						gridSqueezed{it} = flip(gridSqueezed{it});
						valueSqueezed = flip(valueSqueezed, it);
					end
				end
			end
			obj.reducedGriddedInterpolant = griddedInterpolant(gridSqueezed, valueSqueezed, varargin{:});
		end
		
		function value = evaluate(obj, varargin)
			% Example:
			%	test_2p5_1_1 = testInterpolant.evaluate(2.5, 1:5, 1)
			%
			% INPUT PARAMETERS:
			%		 obj : interpolant object VARARGIN
			%	varargin : sample points for which the values are
			%			   interpolated. Expected to be a list of arrays
			% OUTPUT:
			%	   value : values at the points specified by varargin
			% evaluate is used to get the value of the data at certain
			% points specified by varargin. It is assumed that
			%	numel(varargin) == ndims(valueOriginal)
			reducedGrid = varargin(obj.reducedDimension);
			interpolant = obj.reducedGriddedInterpolant(reducedGrid);
			value = permute(interpolant, obj.permutationVector);
		end
		
		function value = eval(obj, varargin)
		% a new version of evaluate that allows to use the syntax of the griddedInterpolant.
		% That is, when the arguments are passed in braces, e.g.
		%   value = myInterpolant.eval({z,z})
		% then the vectors z,z define a grid on which the interpolant is evaluated. (same as
		% evaluate!)
		%
		% when the arguments are passed as a list, e.g.
		%   value = myInterpolant.eval(z,z)
		% then the vectors (of same length) define a list of pairwise coordinates, which are
		% evaluated.
			if iscell(varargin{1}) % passed as cellArray
				temp = varargin{1};
				reducedGrid = temp(obj.reducedDimension);
				value = permute(obj.reducedGriddedInterpolant(reducedGrid), obj.permutationVector);
			else % passed as list
				reducedGrid = varargin(obj.reducedDimension);
				value = permute(obj.reducedGriddedInterpolant(reducedGrid{:}), obj.permutationVector);
			end
		end
		
	end
end

