function coeffs = nPointWeights(k,dz)
% fivePointWeights get weights for a n-point finite differences scheme to 
% approximate second order derivative
%   
%   

% created on 12.07.2018 by Simon Kerschbaum

% if length(k)~= 4
% 	error('k mus have 4 entries')
% end
if length(k)<2
	error('There must be at least 3 points in total, to obtain a second derivative!')
end
if any(k==0)
	error('The point i, which is represented by k=0 is automatically included and must not be passed!')
end
numpoints = length(k)+1;

M = zeros(numpoints-1,numpoints-1);
for idx=1:numpoints-1
	for j=1:numpoints-1
		M(idx,j) = k(idx)^j;
	end
end
rhs = [0;1;zeros(numpoints-3,1)];
abcd = M.'\rhs;
numels = max(max(abs(k)),max(k)-min(k));
coeffs = zeros(1,numels+1);

[ksorted,idxShape] = sort([0; k(:)]);
abcdEx = [0;(abcd(:))];
abcdNew = abcdEx(idxShape);  % inlcude zero for xi
mink = min(ksorted);

% Eintr�ge f�r die 4 verschiedenen Punkte, die Einfluss haben (festgelegt durch k)
for kidx = 1:length(ksorted)
	col = 1+(ksorted(kidx)-mink); % Falls Punkte nicht dabei sind, kann Vektor breiter sein!
	if ksorted(kidx)~=0
		coeffs(col) = 2/((k.^2)*abcd)/dz^2*abcdNew(kidx); % xi+k1 bis xi+k4
	else
		coeffs(col) = -2/((k.^2)*abcd)/dz^2*sum(abcd); % xi
	end
end

end

