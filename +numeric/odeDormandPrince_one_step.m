function x_k = odeDormandPrince_one_step(fun, dt, x_km1, varargin)
%ode4_one_step calculates one timestep with the Runge-Kutta scheme.
%
% INPUT PARAMETERS:
% 	FUNCTION		fun : rhs of ode
%	SCALAR			 dt : step size
%	VECTOR		  x_km1 : value of state in last time step km1="k minus	1"
%	VARARGIN	varargin: further parameters of fun
%
% OUTPUT PARAMETERS:
%	VECTOR			x_k : value of state in current time step k
%
% Example:
% -------------------------------------------------------------------------
% y(1,1) = 3;
% fun = @(x) -2*x;
% dt = 0.05;
% for it = 1:20
%     y(it+1,1) = numeric.odeDormandPrince_one_step(fun,dt,y(it,1));
% end
% plot(0:dt:1,y);
% -------------------------------------------------------------------------
%
% required subprogramms:
%   - 
%
% global variables:
%   - 
%
% history:
%	-	created on 25.04.2018 by Jakob Gabriel (inspired by wikipedia)
%			- https://en.wikipedia.org/wiki/Dormand-Prince_method

%% Paramters according to wikipedia
a = [ 1/5,          0,          0,          0,        0		       0;
      3/40,         9/40,       0,          0,        0			   0;
      44/45        -56/15,      32/9,       0,        0			   0;
      19372/6561,  -25360/2187, 64448/6561, -212/729, 0,		   0;
      9017/3168,   -355/33,     46732/5247, 49/176,   -5103/18656, 0;
	  35/384,		0,			500/1113,	125/192,  -2187/6784, 11/84];
b = diag([35/384; 0; 500/1113; 125/192; -2187/6784; 11/84; 0]);
% c = [1/5; 3/10; 4/5; 8/9; 1; 1]; not needed since LTI system

k_i_vec = zeros([numel(x_km1), size(b, 1)]);
k_i_vec(:, 1) = feval(fun, x_km1, varargin{:});
for it=2:size(b, 1)
	k_i_vec(:,it) = feval(fun, ...
		x_km1 + dt*sum(k_i_vec(:, 1:it-1) * diag(a(it-1,1:it-1)), 2), varargin{:});
end
x_k = x_km1 + dt * sum(k_i_vec * b, 2);

end