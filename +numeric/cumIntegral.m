function F = cumIntegral(f, grid)
% CUMINTEGRAL cumulative integral for function handles
% F = cumIntegral(f, lower, upper)
arguments
	f function_handle;
	grid (:,1) double;
end

F = zeros(numel(grid), 1);

for i = 2:numel(grid)
    F(i) = F(i-1) + integral(f, grid(i-1), grid(i));
end

end

