function [M, K, L, P, PHI, dphi, Dz] = femMatrices(spatialDomain, optArgs)
%FEMMATRICES computes the discretization matrices obtained by FE-Method
% [M, K, L, P, PHI, DPHI] = femMatrices(SPATIALDOMAIN, OPTARGS) computes the matrices required for
% the approximation of a system using FEM. For instance, the rhs of a pde
%	PDE[ x(z) ] = dz (alpha(z) dz x(z)) + beta(z) dz x(z) + gamma(z) x(z) + b(z) u
%	          y = c(z) x(z)
% is approximated as K x(z) The matrices that will be returned are:
%	The mass-matrix M is computed using
%		M = int phi(z) phi(z)
%	The stiffness matrix K is computed with
%		K0 = int phi(z) gamma(z) phi(z) 
%		K1 = int phi(z) beta(z) (dz phi(z)) 
%		K2 = int - (dz phi(z)) alpha(z) (dz phi(z)) 
%		 K = K0 + K1 - K2
%	The input matrix L:
%		L = int( phi(z) * b(z) )
%	The output matrix P:
%		P = int( c(z) * phi'(z) )
%	Derivative Matrix 
%		Dz = int( phi(z) * dz phi'(z) dz
%
%	PHI is the trial function used for the discretization and DPHI is the derivative of the trial
%	function.
%
%	The argument SPATIALDOMAIN is mandatory and must be a quantity.Domain object. The further system
%	parameters as
%		"alpha", "beta", "gamma", "b", "c"
%	must be quantity.Discrete objects. With the parameter
%		"Ne"
%	as positive integer number one can specify the number of discretization points for one element.
%	The parameter
%		"silent"
%	is a logical flag to supress the progressbar.
%

arguments
	spatialDomain (1,1) quantity.Domain;
	optArgs.Ne (1,1) double {mustBeInteger} = 111; 
	optArgs.alpha quantity.Discrete {mustBe.scalarOrEmpty} = quantity.Discrete.empty(1,0);
 	optArgs.beta quantity.Discrete {mustBe.scalarOrEmpty} = quantity.Discrete.empty(1,0);
	optArgs.gamma quantity.Discrete {mustBe.scalarOrEmpty} = quantity.Discrete.empty(1,0);
	optArgs.b quantity.Discrete = quantity.Discrete.empty(1,0);
	optArgs.c quantity.Discrete = quantity.Discrete.empty(1,0)';
 	optArgs.silent logical = false;
end

% set the system properties to local variables, for better readability and (maybe) faster access
z = spatialDomain.grid;
N = spatialDomain.n;
z0 = spatialDomain.lower;
z1 = spatialDomain.upper;

pbar = misc.ProgressBar('name', 'FEM', 'terminalValue', N-1, 'steps', N-1, ...
	'initialStart', true, 'silent', optArgs.silent);

alf = optArgs.alpha;
bta =  optArgs.beta;
gma = optArgs.gamma;
b = optArgs.b;
c = optArgs.c;

p = size(b, 2); % number of the input values
m = size(c, 1); % number of the output values

% define the properties of one finite element
element = quantity.EquidistantDomain("z", 0, (z1 - z0) / (N - 1), ...
	'stepNumber', optArgs.Ne);

%% verify that grid is homogenious
% Yet, this function only supports homogenious grids for most output parameters. Only, the mass
% matrix M is calculated correct for nonhomogenious grids. The following line ensures, that if the
% grid is not homogenious, only M is returned.
isGridHomogenious = all(diff(z) - element.upper < 10*eps);
if ~isGridHomogenious
	assert(nargout <= 1, 'non-homogenious grids only supported for mass-matrix');
end

%% define the trial function
%         /   z_kp1 - z   \
%         | ------------- |
%         |     deltaZ_k  |
% phi_k = |               |
%         |   z - z_k     |
%         | ------------  |
%         \    deltaZ_k   /

PHI = quantity.Discrete({(element.upper - element.grid) / element.upper; ...
	(element.grid / element.upper) }, ...
	element, "name", "phi");

%% compute the mass matrix
M0 = 1 / 6 * [2, 1; 1, 2];
zDiff = diff(z);

%% compute the stiffness matrix

% initialize the trial functions for the computation for element matrices. for further comments, see
% the description in the loop
pp = reshape(double(PHI * PHI'), element.n, 4);
phi = double(PHI);
dphi = [ -1 / element.upper; ...
	1 / element.upper ];
dphidphi = reshape(dphi * dphi', 1, 4);

% initialize the element matrices
K0 = zeros(2,2);
K1 = zeros(2,2);
K2 = zeros(2,2);
Dz_k = zeros(2,2);
L0 = zeros(2,0);
P0 = zeros(0,2);

% initialize the assembly matrices:
M = zeros(N, N);
K = zeros(N, N);
Dz = zeros(N, N);
L = zeros(N, p);
P = zeros(m, N);

% set the flags for the values which should be computed:
flags.alpha = ~isempty(alf);
flags.beta = ~isempty(bta);
flags.gamma = ~isempty(gma);
flags.b = ~isempty(b);
flags.c = ~isempty(c);

for k = 1:N-1
	pbar.raise(k);
	idxk=[k, k+1];
	zk = linspace(z(k), z(k+1), element.n)';
	
	if flags.gamma
		% compute the elements for K0 = int phi(z) * gamma(z) * phi'(z)
		%	the matrix phi(z) * phi'(z) is the same for each element. Thus, this matrix is computed
		%	a priori. To simplify the multiplication with the scalar value gamma(z), the matrix
		%	phi*phi' is vectorized and the result of phi(z)phi'(z) alpha(z) is reshaped as a matrix.
		k0 = misc.multArray(pp, gma.on(zk), 3, 2, 1);
		K0 = reshape( numeric.trapz_fast(zk, k0'), 2, 2);
	end
	
	if flags.beta
		% compute the elements for K1 = int phi(z) * beta(z) * dz phi'(z))
		%	as the values dz phi(z) are constants, the multiplication can be. simplified.
		k1 = misc.multArray(phi, bta.on(zk) * dphi', 3, 3, 1);
		K1 = reshape( numeric.trapz_fast(zk, reshape(k1, element.n, 4, 1)' ), 2, 2);
	end
	
	if flags.alpha
		% compute the elements for K2 = int dz phi(z) * alpha(z) * dz phi'(z)
		%	as the matrix dzphi*dzphi' is constant the multiplication is simplified by vectorization
		%	of dzphi*dzphi' and a reshape to obtain the correct result.
		k2 = alf.on(zk) * dphidphi;
		K2 = reshape( numeric.trapz_fast(zk, k2'), 2, 2);
	end
	
	if flags.b
		% compute the input matrix L = int phi(z) * b(z)
		l = reshape( misc.multArray(phi, b.on(zk), 3, 2, 1), element.n, 2*p);
		L0 = reshape( numeric.trapz_fast(zk, l'), 2, p);
	end
	
	if flags.c
		% compute the output matrix P = int c(z) * phi(z)'
		c0 = reshape( misc.multArray(c.on(zk), phi, 3, 3, 1), element.n, 2*m);
		P0 = reshape( numeric.trapz_fast(zk, c0'), m, 2);
	end
	
	if nargout >= 7
		Dz_ = misc.multArray(phi, repmat( dphi', [optArgs.Ne,1]), 3, 3, 1);
		Dz_k = reshape( numeric.trapz_fast(zk, reshape(Dz_, element.n, 4, 1)' ), 2, 2);
	end
	
	% assemble the matrices with the elements:
	M(idxk, idxk) = M(idxk, idxk) + M0 * zDiff(k);
	
	K(idxk, idxk) = K(idxk, idxk) + K0 + K1 - K2;
	L(idxk, :) = L(idxk, :) + L0;
	P(:, idxk) = P(:, idxk) + P0;
	
	Dz(idxk, idxk) = Dz(idxk, idxk) + Dz_k;
end


pbar.stop();
end

