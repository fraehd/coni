function grad_op = get_grad_op(zdisc)
% GET_GRAD_OP get gradient operator to a spatial discretisation
%
% Description:
% grad_op=get_grad_op(zdisc) 
% computes the gradient operator to a spatial 1D discretisation using 
% FEM with linear basis functions according to matfem
% grad_op is a length(zdisc) x length(zdisc) matrix. Use it by
%     dz x = grad_op * x,
% where x is column vector, containing values at the discretisation
% points zdisc.
%
% Inputs:
%     zdisc       Spatial discretization
% Outputs:
%     grad_op     Gradient operator
%
% Example:
% -------------------------------------------------------------------------
% t = 0:0.1:pi;
% x = sin(t)';
% D = numeric.get_grad_op(t);
% dx = D*x;
% plot(t,x,t,dx);
% -------------------------------------------------------------------------
%

% Created on 30.06.2015 by Simon Kerschbaum
% benötigte Unterprogramme:
%  - get_lin_con_matrix
%  - get_lin_mass_matrix

if length(zdisc) < 2
	grad_op = 0;
else
	CM = get_lin_con_matrix(zdisc);
	MM = get_lin_mass_matrix(zdisc);
	grad_op = MM\CM;
end
end