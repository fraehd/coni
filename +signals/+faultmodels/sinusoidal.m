classdef sinusoidal < fault.polynomial
    %SINUSOIDAL dscription of sinusoidal functions
    %   With this class sinusoidal functions as   
    %       
    %       f(t) = a0 + a1 sin( w * t) + a2 cos( w * t )
    %
    % can be described. If the property offset is set to false, the
    % function
    %
    %       f(t) = a0 sin( w * t) + a1 cos( w * t )
    %
    % is considered.
    
    properties
        omega;
        sym_t;
        offset = true;
        fnphi  = cell(0);
        omegaoffset = 0;
    end
    
    properties (Access = private)
       e; 
       a;
    end
    
    properties (Dependent)
        sym_phi;        
    end
    
    methods
        
        function obj = sinusoidal(varargin)
           obj@fault.polynomial('n', 3);
           obj.sym_t = sym('t');
           obj.e = sym('e');
           obj.a = sym('a');
            for arg=1:2:length(varargin)
               obj.(varargin{arg}) = varargin{arg + 1}; 
            end            
        end
        
        function p = get.sym_phi(obj)
            p = symfun([obj.a * obj.sym_t^(obj.e), ...
                        sin(obj.sym_t * obj.omega), ...
                        cos(obj.sym_t * obj.omega)], obj.sym_t);             
        end
        
       function p = phi(obj, t, k)
           
           if ~exist('k', 'var')
              k = 0; 
           end
           
           % check if matlab function has  been calculated
           if k >= 0
               if ~iscell(obj.fnphi) || length(obj.fnphi) < k + 1 || isempty(obj.fnphi{k+1, 1})

                    obj.fnphi{k+1,1} = matlabFunction(diff(obj.sym_phi, obj.sym_t, k), ...
                                                'Vars',[obj.sym_t, obj.e, obj.a]);                             
               end
           
           else
               if ~iscell(obj.fnphi) || length(obj.fnphi) < -k + 1 || isempty(obj.fnphi{-k+1,2})
                   
                   tmp = int(obj.sym_phi, obj.sym_t);
                   tmp = subs(tmp, {'a', 'e'}, {1, 0});
                   
                    obj.fnphi{-k+1,2} = matlabFunction(tmp);
               end
           end
               
           if k>= 0
                p = obj.fnphi{k+1,1}(t, k, 0^k);    
           else
                p = obj.fnphi{-k+1,2}(t);
           end
           
           
           
           if obj.offset == false
               p = p(:,2:end);
           end
       end
        
%        function f = get.fnphi(obj)
%           f = obj.fnphi; 
%        end
%        
%        function obj = set.fnphi(obj, f)
%           obj.fnphi = f; 
%        end

        function f = fnc(obj)
            f = obj.sym_phi * obj.theta;
            f = subs(f, {'a', 'e'}, {1, 0});
        end

       function f = reconstruct(obj, t, theta, k, o)

            if ~exist('theta', 'var') || isempty(theta)
               theta = obj.theta; 
            end
            
            if ~exist('k', 'var') || isempty(k)
              k = 0; 
            end

            if ~exist('o', 'var') || isempty(o)
                o = 0;
            end
            
            f = obj.phi(t(:) + o * obj.omegaoffset / obj.omega, k) * theta;
            
            if k < 0
                f = f - obj.phi(0 + o * obj.omegaoffset / obj.omega, k) * theta;
            end
                
            
            % fm1 = int(f0, 't')
            % double(fm1(t) - fm1(0))
            
            %% test for evaluation
%             for l = 1:length(t)
%                ff(l) = obj.phi(t(l), k) * theta(:,l); 
%             end
%             
%             subplot(2,1,1)
%             plot(t, diag(f)', t, ff)
%             subplot(2,1,2)
%             plot(ff - diag(f)')
              
            if size(theta, 2) > 1
                % computation of several theta:
                %   the function handle is a column vector with a function
                %   for each set of theta:
                %       fnf = [f{theta(1)}; f{theta(2)}; ...
                %   with the evaluation of this functions at timesteps t,
                %   the result will be a matrix. Each row contains the
                %   evaluation fk(t) of function fk = f{theta(k)} at 
                %   timesteps t. The main diagonal of f will contain the
                %   evaluation of fk(tk) at the timestep tk.
               f = diag(f);
            end           
       end        
    end
    
end

