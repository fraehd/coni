classdef TaylorPolynomial < quantity.Discrete
	%TAYLORPOLYNOMIAL signal model for a Taylor polynomial
	%   With this class Taylor polynomials of the form
	%		p(t, t*) = sum c_k * (t-t*)^(k-1)
	%	can be described. The object can be created using the constructor
	%	p = signals.faultmodels.TaylorPoynomial( <name-value-pairs> )
	%	The properties can be set using <name-value-pairs>.
    
	properties
		% The coefficients that describe the polynomial.
		coefficients double;	
		
		% Timestep at which the polynomial occurrs. This means, the
		% defined function is zero before the value of the occurrence
		% property and behaves like the defined polynomial afterwards.
		occurrence double;	
		
		% Symbolic variable for the computation of the polynomial
		t sym;	
		
		% Symbolic variable for the expansion point of the polynomial
		tStar sym;
		
		% The length of the detection window. This is required to use the
		% polynomial as fault model for the fault diagnosis.
		T double; 
		
		% The index of the polynomial. If there are several polynomials
		% defined, this index can be used to distinguish them.
		index double;
		
		% The monomials for this polynomial
		monomial quantity.Symbolic;	
	end
	
	properties (Dependent)
		% degree of the ansatz function which describes the fault.
		degree double;		
		
		% local monomials
		localMonomials;
	end	
	
    methods
		
		function obj = TaylorPolynomial(varargin)
			%FAULTMODEL Construct of an fault model
			p = misc.Parser();
			p.addParameter('coefficients', 1, @isnumeric);
			p.addParameter('occurrence', -1, @isnumeric);
			p.addParameter('t', sym('t', 'real'), @misc.issym);
			p.addParameter('tStar', sym('tStar', 'real'), @misc.issym);
			p.addParameter('localGrid', linspace(0, 1)', @isnumeric);
			p.addParameter('globalGrid', linspace(0, 5)', @isnumeric);
			p.addParameter('T', 0, @isnumeric);
			p.addParameter('index', 1);
			p.parse(varargin{:});
			
% 			if p.isDefault('T')
% 				warning('No windwo width T is set!');
% 			end
			
			t = p.Results.t;
			tStar = p.Results.tStar;
			myLocalGrid = p.Results.localGrid;
			myGlobalGrid = p.Results.globalGrid;
			T = p.Results.T;
			index = p.Results.index;
			occurrence = p.Results.occurrence;
			
			% compute the local polynomials:
			degree = length(p.Results.coefficients);
			coefficients = p.Results.coefficients(:);
			
			monomial = signals.faultmodels.TaylorPolynomial.getMonomials(degree, t, tStar, myLocalGrid, T);
			val = signals.faultmodels.TaylorPolynomial.computeGlobalSignal(monomial, ...
				coefficients, ...
				myGlobalGrid, ...
				occurrence);
			
			obj@quantity.Discrete( val, ...
				quantity.Domain("t", myGlobalGrid), ...
				'name', ['f_' num2str(index)]);
			
			% put all the values to local variables
			obj.occurrence = occurrence;
			obj.t = t;
			obj.tStar = tStar;
			obj.T = T;
			obj.index = index;
			obj.monomial = monomial;
			obj.coefficients = coefficients;			
		end

		function set.coefficients(obj, c)
			obj.coefficients = c;
			
			myLocalGrid = obj.monomial(1).domain(1).grid;
			obj.monomial = signals.faultmodels.TaylorPolynomial.getMonomials(...
				length(c), obj.t, obj.tStar, myLocalGrid, obj.T);
			
			% update the global value
			val = obj.computeGlobalSignal(obj.monomial, ...
				c, ...
				obj.domain(1).grid, ...
				obj.occurrence);
			
			obj.valueDiscrete = val;
		end
		
		function d = get.degree(obj)
			d = length(obj.coefficients);
		end
		
		function m = get.localMonomials(obj)
			m = subs(obj.monomial, char(obj.tStar), obj.T);
		end
		
	end	
	
	methods (Static, Access = protected)
		function val = computeGlobalSignal(monomial, coefficients, myGlobalGrid, occurrence)
			% evaluate the polynomial for its definition
			polyValues = monomial' * coefficients;
			val = (myGlobalGrid >= occurrence) .* ...
				polyValues.on({myGlobalGrid, 0}, {'t', 'tStar'});
		end
		function monomials = getMonomials(degree, t, tStar, myLocalGrid, T)
			monomials = quantity.Symbolic.zeros([degree, 1], ...
					quantity.Domain.empty(), ...
					'name', 'varphi_k');
			for k = 1:degree
				monomials(k, 1) = quantity.Symbolic((t-tStar)^(k-1), ...
					[quantity.Domain(char(t), myLocalGrid), quantity.Domain(char(tStar), T)], ...
					'name', 'varphi_k');
			end
		end
	end
	
end

