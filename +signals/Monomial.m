classdef Monomial < signals.BasicVariable
	%MONOMIAL representation for a monomial of the form
	%	t^i / factorial(i)
	% and its derivatives.
	
	properties
		timeDomain (1,1) quantity.Domain;
		order (1,1) double;
		variable (1,1) sym = sym("t");
		numDiff (1,1) double;
	end		
	methods
		function obj = Monomial(timeDomain, order, optArgs)
			% MONOMIAL representation of a monomial
			% obj = Monomial(timeDomain, order, optArgs) creates a monomial of ORDER on the
			% TIMEDOMAIN. The monomial has the form
			%	f(t) = t^i / i!
			% The number of computed derivatives can be specified by the name-value-pair "numDiff".
			% The name of the symbolic variable can be defined by "variable".
			
			arguments
				timeDomain (1,1) quantity.Domain
				order (1,1) double,
				optArgs.variable sym = sym("t");
				optArgs.numDiff (1,1) double = 3;
			end
						
			for k = 0:optArgs.numDiff+1
				derivatives{k+1} = signals.Monomial.privateEvaluateFunction( timeDomain, ...
					optArgs.variable, order, k);
			end
			
			obj@signals.BasicVariable(derivatives{1}, derivatives(2:end));
			
			obj.timeDomain = timeDomain;
			obj.order = order;
			obj.variable = optArgs.variable;	
			obj.numDiff = optArgs.numDiff;
			
		end
	end
	
	methods (Access = protected)
		
		function v = evaluateFunction(obj, domain, k)
			v = obj.privateEvaluateFunction( domain, obj.variable, obj.order, k);
		end
	end
	methods (Static, Access = private)
		function f = privateEvaluateFunction(domain, t, i, k)
			f = diff( t.^i / factorial(i), k);
			f = quantity.Symbolic( f, domain );
		end
	end
	
end

