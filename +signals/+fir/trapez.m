function filterCoefficients = trapez(kernel, tau, optArg)
%TRAPEZ compute a fir filter based on a time-continous kernel with trapzoidal rule
% 
%	filterCoefficients = trapez(KERNEL, TAU, optArgs) computes a fir filter based on the
%	kernel KERNEL on the time domain TAU. The filter coefficients are
%	computed on the basis of a trapezoidal integral approximation. With the name-value-pair argument
%	"flip" it can be specified if the filter coefficients should be computed on the basis of
%		(flip = false)	y(t) = int_0^T k(tau) u(t-tau) dtau	
%	or
%		(flip = true)	y(t) = int_0^T k(tau) u(t+tau-T) dtau
%
%   For a more detailed explanation of the derivation 
%   see [Mose, Matthias "Fehlerdetektion": Implementierung]
%
arguments
	kernel quantity.Discrete;
	tau quantity.Domain;
	optArg.flip = true;
end

T = tau.upper;
k = T / (tau.n - 1);

if kernel(1).domain.upper ~= tau.upper || kernel(1).domain.lower ~= tau.lower
	warning("Domains of n and tau have different upper and/or lower values");
end

if optArg.flip
	kernel = kernel.flipDomain(tau.name);
end

n_hat = k * kernel.on(tau);

n_hat(1,:,:) = n_hat(1,:,:) / 2;
n_hat(end,:,:) = n_hat(end,:,:) / 2;

filterCoefficients = n_hat;

end

