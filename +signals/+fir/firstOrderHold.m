function n_foh = firstOrderHold(n, tau, optArg)
%FIRSTORDERHOLD compute the fir filter with the kernel n
%
%	n_foh = firstOrderHold(n, tau ) computes the fir filter based on the
%	kernel n on the grid defined by tau. The filter coefficients are
%	computed on the basis of a first-order-hold approximation. If the input
%	of a filter is given by a first-order-hold sampling, the evaluation of
%	the filter will be exact. For further details see: [Kiltz,
%	Polygonzug]

%   INPUT PARAMETERS:   n: The quasi continuous kernel n discretized with
%                          the sample period dtau on the interval [0,T]
%                     tau: auxiliary time grid discretized with the
%                          simulation sample period dt on the interval [0,T]
%
%                          dtau >> dt should hold.

arguments
	n quantity.Discrete;
	tau quantity.Domain;
	optArg.flip = true;
	optArg.gridName = tau.name;
end

% the simulation sample period
dt = tau.grid(2) - tau.grid(1);

inv_dt = 1/dt;
% n_schlange(i) = int(n,i*dt,(i+1)*dt) obtained by calculatig the
% cummulative Integral. n_schlange = int(n,0,(i+1)*dt) - int(n,0,i*dt)

N = n.cumInt(optArg.gridName, 0, optArg.gridName);
Nt = N.on(tau);
n_schlange = Nt(2:1:end, :, :)-Nt(1:1:(end-1), :, :);

% n_strich_strich = int(n,0,(i+1)* dt) - (inv_dt * int(n^(-1), 0, (i+1)*dt) - int(n^(-1), 0 , i*dt)
% This expression is obtained by calculating the second cummulative
% integral. For a detailed explanation of the other terms, refer to the
% derivation in the Documentation [Chapter: Implementation].
NN = N.cumInt(optArg.gridName, 0, optArg.gridName);
NNt = NN.on(tau);
n_strich_strich = Nt(2:1:end, :, :) - (inv_dt*(NNt(2:1:end, :, :) ...
	- NNt(1:1:(end-1), :, :)));
n_star = n_schlange - n_strich_strich;
n_underline = n_star(2:1:end, :, :) + n_strich_strich(1:1:(end-1), :, :);

if optArg.flip
	
	n_underline_flip = flip(n_underline,1);
	
	% the filter coefficients n_foh
	n_foh = [n_strich_strich(end);n_underline_flip;n_star(1)];
	
else
	n_foh = [n_star(1, :, :); n_underline; n_strich_strich(end, :, :)];
end

end

