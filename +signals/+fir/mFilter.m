function y = mFilter(F, u)
% MFILTER MIMO FIR filter
%	Y = mFilter(F, U) filters the data in vector U with the filter
%	described by F to create the filtered data Y. The filter has the form
%		y(n) = F(1,:,:) * u(n) + F(2,:,:) * u(n-1) + ... 
% The filter F is a multidimensional array of size
%   size(fil) = (Ntau, n, m); 
%       Ntau: number of filter coefficients
%       n: number of rows
%       m: number of columns
%
%   size(u) = (Nt, m, p)
%       Nt: number of time steps
%       m: number of rows
%       p: number of columns

    assert(size(F, 3) == size(u, 2));

   % dimensions and initialization
   q = size(u, 1);
   n = size(F, 2);
   o = size(u, 2);
   m = size(u, 3);

   y = zeros(q, n, m);

    for k = 1 : n % rows of P
        for l = 1 : m % columns of P
            for p = 1 : o % rows of B or columns of A
                y(:, k, l) = y( :, k, l ) + filter(F(:, k, p), 1, u(:, p, l));
            end
        end
    end

end