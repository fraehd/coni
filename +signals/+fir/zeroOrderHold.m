function n_zoh = zeroOrderHold(n, tau )
%ZEROORDERHOLD compute the fir filter with the kernel n
% 
%	n_zoh = zeroOrderHold(n, tau ) computes the fir filter based on the
%	kernel n on the grid defined by tau. The filter coefficients are
%	computed on the basis of a zero-order-hold approximation. If the input
%	of a filter is given by a zero-order-hold sampling, the evaluation of
%	the filter will be exact. For further details see: [Kiltz,
%	Treppenfunktion]

%   INPUT PARAMETERS:   n: The quasi continuous kernel n discretized with
%                          the sample period dtau on the interval [0,T]
%                     tau: auxiliary time grid discretized with the
%                          simulation sample period dt on the interval [0,T]                
%                          
%                          dtau >> dt should hold.



%   compute the integral of n and evaluate it on the grid tau
%   n_schlange(i) = int(n,i*dt,(i+1)*dt), this integration is achieved with
%   the cummulative Integral. n_schlange = int(n,0,(i+1)*dt) -
%   int(n,0,i*dt)

N = n.cumInt('t',0,'t');
Nt = N.on(tau);
n_schlange = Nt(2:1:end)-Nt(1:1:(end-1));
%   At the end the filter coefficients n_zoh are obtained by fliping n_schlange.
n_zoh = flip(n_schlange,1);

%   For further details see the derivation in the documentation [Chapter: Implementation]
end

