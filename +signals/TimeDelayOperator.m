classdef TimeDelayOperator < handle & matlab.mixin.Copyable
	%TIMEDELAYOPERATOR class to describe an operator of the form
	%	D[h](z,t) = h( t + coefficient(z) )
	% This comes from the inverse Laplace transfromed value of
	%	exp(-s coefficient(z) ) * H(s)   <->   h(t + coefficient(z))
	
	properties
		coefficient (1,1) quantity.Discrete;
	end
	
	properties (Access = protected)
		spatialDomain_inner quantity.Domain;
		timeDomain_inner (1,1) quantity.Domain;
		isZero logical = false;
	end
	
	methods
		function obj = TimeDelayOperator(coefficient, timeDomain, optionalArgs)
			%TIMEDELAYOPERATOR Construct an instance of a time delay
			%operator
			%   obj = TimeDelayOperator(COEFFICIENT, TIMEDOMAIN, VARARGIN)
			%   will construct a time delay operator of the form
			%		OBJ[h](z,t) = h( t + COEFFICIENT(z) )
			%	with the time delay described by COEFFICIENT on a temporal
			%	domain TIMEDOMAIN. Using the name-value-pairs optional
			%	parameters, a spatial domain can be specified by the name
			%	'spatialDomain'.
			arguments
				coefficient = quantity.Discrete;
				timeDomain (1,1) = quantity.Domain;
				optionalArgs.spatialDomain =  coefficient.domain;
				optionalArgs.isZero logical = false;
			end
			
			if nargin > 0
				for k = 1:numel(coefficient)
					obj(k).coefficient = coefficient(k);
				end

				[obj.timeDomain_inner] = deal(timeDomain);
				[obj.spatialDomain_inner] = deal(optionalArgs.spatialDomain);
				[obj.isZero] = deal(optionalArgs.isZero);
			end
		end
		
		function d = applyTo(obj, h, optionalArgs)
			%APPLYON apply the operator on a function
			%   D = applyOn(OBJ, H) computes the application of the
			%   operator OBJ to the function H. 
			%		d = OBJ[h]
			arguments
				obj
				h quantity.Discrete;
 				optionalArgs.domain quantity.Domain = h(1).domain;
			end
			
			assert( length(optionalArgs.domain) == 1);
			
			n = size(obj, 1);
			m = size(obj, 2); assert(m == size(h, 1));
			l = size(h, 2);
			
			d = quantity.Discrete.zeros([n, l], [obj.spatialDomain, obj.timeDomain]);
			
			for i = 1 : n
				for j = 1 : m
					for k = 1 : l
						if ~obj(i, j).isZero
							d(i, k) = d(i, k) + ...
								h(j, k).compose( obj.t + obj(i, j).coefficient, ...
								"domain", optionalArgs.domain );
						end
					end
				end
			end
		end		
		
		function d = compositionDomain( obj, h)
			
			% todo assert all coefficients have the same domain
			
			d = h(1).compositionDomain( obj.t + obj(1).coefficient );
		end
		
		function t = t(obj)
			t = quantity.Symbolic(sym(obj.timeDomain.name), obj.timeDomain);
		end
		
		function d = diag(obj)
			% DIAG Diagonal matrices and diagonals of a matrix.
			%	DIAG(OBJ) puts the entries of the vector OBJ onto the main
			%	diagonal of matrix. The other values are filled with zero.
			
			n = length(obj);
			idx = 1:n;
			
			d(1:(n^2)) = obj.zero(obj.timeDomain);
			d = reshape(d, n, n);
			d(idx == idx') = obj(:);
						
		end
		
		function C = coefficients(obj)		
			C = reshape([obj.coefficient], size(obj));
		end
		
		function sd = spatialDomain(obj)
			sd = join( [obj.spatialDomain_inner] );
		end
		
		function td = timeDomain(obj)
			td = join( [obj.timeDomain_inner] );
		end	

	end
	
	methods (Static)
		function o = zero(timeDomain, namedArgs)
			
			arguments
				timeDomain quantity.Domain;
				namedArgs.spatialDomain quantity.Domain = quantity.Domain.empty();
			end
			
			if isempty( namedArgs.spatialDomain )			
				infValue = inf;
			else
				infValue = inf([obj.spatialDomain.n], 1);
			end
			
			qinf = quantity.Discrete(infValue, namedArgs.spatialDomain);
			o = signals.TimeDelayOperator(qinf, timeDomain, 'isZero', true);			
		end		
	end
	
end

