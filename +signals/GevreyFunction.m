classdef GevreyFunction < signals.BasicVariable
	%GEVREYFUNCTION - 
	% Class to generate a gevrey function and its derivative. The general function has the form
	%
	%          / 0             : t <= 0
	%          |
	%   f(t) = | f0 + K * h(t) : 0 < t < T
	%          | 
	%          \ 0             : t >= T
	%
	% with the additional property int_0^T h(t) = 1.
	% See also signals.GevreyFunction.GevreyFunction, signals.BasicVariable
	
	properties
		% Gevrey-order
		order (1,1) double = 1.99;
		% the constant gain
		gain double = 1;
		% the offset:
		offset (1,1) double = 0;
		% the number of derivatives to be considered
		numDiff (1,1) double = 5;
		% underlying gevery function
		g;
		% derivative shift
		diffShift (1,1) double = 0;
	end
	
	properties (Dependent)
		% the exponent of the exponential function
		sigma;
	end
	
	methods
		function obj = GevreyFunction(optArgs)
			% GEVREYFUNCTION initialization of a gevrey function as signals.BasicVariable
			%	obj = GevreyFunction(optArgs) creates a gevrey function object. The properties can
			%	be set by name-value-pairs and are "order", "gain", "offset", "numDiff",
			%	"timeDomain", "g", "diffShift".
			arguments
				optArgs.order (1,1) double = 1.99;
				optArgs.gain double = 1;
				optArgs.offset (1,1) double = 0;
				optArgs.numDiff (1,1) double = 5;
				optArgs.timeDomain (1,1) quantity.Domain  = quantity.Domain.defaultDomain(100, "t"); 
				optArgs.g = @signals.gevrey.bump.g;
				optArgs.diffShift (1,1) double = 0;
			end
			
			% generate the underlying gevrey functions:
			f_derivatives = cell(optArgs.numDiff + 1, 1);
			for k = 0:optArgs.numDiff			
				f_derivatives{k+1} = ...
					signals.GevreyFunction.evaluateFunction_p(optArgs, optArgs.timeDomain, k);
			end
			
			obj@signals.BasicVariable(f_derivatives{1}, f_derivatives(2:end));	

			% set the parameters to the object properties:
			for k = fieldnames(rmfield(optArgs, "timeDomain"))'
				obj.(k{:}) = optArgs.(k{:});
			end
			
		end
		
		function s = get.sigma(obj)
			s = 1 ./ ( obj.order - 1);
		end
		function set.sigma(obj, s)
			obj.order = 1 + 1/s;
		end
	end
	
	methods (Static, Access = private)
		function v = evaluateFunction_p(optArgs, domain, k)
			
			derivativeOrder = k + optArgs.diffShift;
			% The offset is only added to the zero-order derivative. 
			offset_k = optArgs.offset * 0^derivativeOrder;
			
			functionHandle = eval("@(" + domain.name + ...
				") offset_k + optArgs.gain * optArgs.g(" + domain.name + ...
				", derivativeOrder, domain.upper, 1 ./ ( optArgs.order - 1))");
			
			v = quantity.Function(functionHandle, domain);
		end		
	end
	
	methods (Access = protected)
		function v = evaluateFunction(obj, domain, k)
			v = signals.GevreyFunction.evaluateFunction_p(obj, domain, k);
		end
	end
end

