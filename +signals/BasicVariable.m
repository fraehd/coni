classdef BasicVariable < handle
	%BasicVariable Class to handle quantities with a priori known derivatives
	% This class is used to simplify the usage of quantities for which a lot of computations must be
	% done on its derivatives. So a BasicVariable consists of a basic function and its derivatives,
	% which are precomputed and safed. So computations can be performed without recalculation of the
	% derivatives.
	
	properties ( SetAccess = protected )
		derivatives cell;
		fun (1,1) quantity.Discrete;
	end
	
	properties ( Dependent )
		highestDerivative;  % order of the highest computed derivative
		T;					% the transition time T
		dt;					% step size
	end
	
	methods
		function obj = BasicVariable(fun, derivatives)
			
			if numel(fun) > 1
				
				for i = 1:numel(fun)
					
					for j = 1:numel(derivatives)
						d{j} = derivatives{j}(i);
					end
					
					obj(i) = signals.BasicVariable( fun(i), d ); 
				end
				
				obj = reshape(obj, size(fun));
				
			else
			
				obj.fun = fun;
				obj.derivatives = derivatives;

				% do the pre-initialization, because if it is done implicit, some strange bug causes the
				% functions to be evaluated more often.
				obj.fun.on();
				for i = 1:numel(derivatives)
					obj.derivatives{i}.on();
				end
			end
		end
		function h = get.highestDerivative(obj)
			h = numel(obj(1).derivatives);
		end
		function T = get.T(obj)
			T = obj.domain.upper;
		end
		function dt = get.dt(obj)
			if isa(obj.domain, "quantity.EquidistantDomain")
				dt = obj.domain.stepSize;
			else
				delta_t = diff(obj.domain.grid);
				assert( all( diff( delta_t ) < 1e-15 ), "Grid is not equidistant spaced" );
				dt = delta_t(1);
			end
		end
		function F = quantity.Discrete(obj)
			F (1,:) = [obj.fun];
			
			for i = 1:max([obj.highestDerivative])
							
				for j = 1:numel(obj)
					F_i(j) = obj(j).derivatives{i};
				end
				
				F(i+1,:) = reshape(F_i, size(obj));
			end
		end	
	end
	
	methods ( Access = public)
		function D = diff(obj, domain, n)
			% DIFF get the k-th derivative 
			%	D = diff(obj, domain, n) tries to find the n-th derivative of obj.fun in the saved
			%	derivatives. If it does not exist, it tries to compute it with the evaluateFunction.
			%	If n is a non integer number, the Riemann-Liouville fractional derivative is
			%	computed. For this, a formula is used which requires the next integer + 1 derivative
			%	of the function to avoid the singularity under the integral. Actually, the domain is
			%	clear, but it is used hear to be consistent with the quantity.Discrete/diff.
			arguments
				obj
				domain (1,1) quantity.Domain;
				n (1,1) double = 0;
			end
			
			for l = 1:numel(obj)
				
				assert( domain.isequal( obj(l).domain  ),...
					"The derivative wrt. this domain is not possible.")
				
				if n == 0
					D(l) = obj(l).fun;
					
				elseif ~numeric.near(round(n), n)
					% fractional derivative:
					D(l) = obj(l).fDiff(n);
					
				elseif n > length(obj(l).derivatives)
					% For some functions their maybe a possibility to compute new derivatives on the
					% fly. For this, the evaluateFunction(k) can be used, where k is the order of
					% the derivative.
					D(l) = obj(l).evaluateFunction(obj.domain, n);
					obj(l).derivatives{end+1} = D(l);
				else
					% use the pre-computed derivatives
					D(l) = obj(l).derivatives{n};
				end
			end
			D = reshape(D, size(obj));
		end
		
		function D = diffs(obj, k)
			% DIFFS compute multiple derivatives
			%	D = diffs(obj, k) computes the k-th derivatives of OBJ. At this, k can be a vector
			%	of derivative orders.
			arguments
				obj,
				k (:,1) double {mustBeInteger, mustBeNonnegative} = 0:numel(obj(1).derivatives);
			end
			D_ = arrayfun( @(i) obj.diff(obj.domain, i), k, 'UniformOutput', false);
			
			for i = 1:numel(k)
				D(i,:) = D_{i};
			end
		end
		

		
		function h = productRule(a, b, n)
			% PRODUCTRULE compute the k-derivative of the product of the basic variable a and b
			%	h = productRule(a, b, n) computes the k-th derivative 
			%		h = d_t^n ( a * b ) 
			%	using the product rule. This is usefull if the derivatives of the basic variable a
			%	are known exactly and the derivatives of b, but not the multiplication a*b
			t = b.domain;
			assert( size(a,2) == size(b,1) || all( size(b) == 1) || all( size(a) == 1) )
			
			if size(a,2) == size(b,1)
				h = quantity.Discrete.zeros( [size(a, 1), size(b,2)], t );	
			elseif all( size(a) == 1)
				h = quantity.Discrete.zeros( size(b), t );
			elseif all( size(b) == 1 )
				h = quantity.Discrete.zeros( size(a), t );
			else
				error("The product rule for quantities of the size " + size(a) + " and " + size(b) + " is not possible.")
			end
			
			if numeric.near(round(n), n)
				% for integer order derivatives
				for k = 0:n
					h = h + misc.binomial(n, k) * a.diff(t, n-k) * b.diff(t, k);
				end
			else
				% for fractional order derivatives: [see Podlubny]
				%         p                     \~~ oo  / p \     (k)       p-k
				%        D  (phi(t) f(t)) =  >      |   |    | phi   (t)   D   f(t)
				%      a  t                     /__ k=0 \ k /               a  t				
				h = misc.binomial( n, 0 ) * b * a.fDiff(n);
				h0 = h;
				k = 1;
				% do the iteration on the loop until the difference is very small.
				while max( abs( h0 ) ) / max( abs( h ) ) > 1e-9 
					h0 = misc.binomial(n,k) * b.diff(t, k) * a.fDiff( n - k);
					h = h + h0;
					k = k+1;
				end					
			end
		end
		
		
		function d = domain(obj)
			d = obj(1).fun(1).domain;
		end
		
		function plot(obj, varargin)
			F = quantity.Discrete(obj);			
			F.plot(varargin{:});
		end
		
		function c = diffShift(obj, k)
			
			c = signals.BasicVariable(obj.derivatives{1}, obj.derivatives(2:end));
			
		end
		
		function c = plus( a, b )
			
			arguments
				a signals.BasicVariable
				b signals.BasicVariable
			end
			
			assert( all(size(a) == size(b), 'all') );
			assert( all(size(a) == 1), "Not jet implemented for array-valued basic variables" )
			
			degree = min([a.highestDerivative, b.highestDerivative]);
			
			% do the addition for each derivative:
			A = [a.diffs(0:degree)];
			B = [b.diffs(0:degree)];
			
			C = A + B;
			
			c = signals.BasicVariable(C(1,:), num2cell( C(2:end, :) ));
			
		end
		
		function c = mtimes(a, b)
			% todo write unit test, in particular for the case if a and/or b are matrix/vector
			% valued
			
			% todo: verify that the pre computed values in b are used.
			arguments
				a 
				b 
			end
			
			if isa( a, "signals.BasicVariable")
				I = a.domain;
			elseif isa( b, "signals.BasicVariable")
				I = b.domain;
			end
			if size(a,2) == size(b,1)
				O = quantity.Discrete.zeros( [size(a, 1), size(b,2)], I );
			elseif all( size(a) == 1)
				O = quantity.Discrete.zeros( size(b), I );
			elseif all( size(b) == 1 )
				O = quantity.Discrete.zeros( size(a), I );
			else
				error("The product rule for quantities of the size " + size(a) + " and " + size(b) + " is not possible.")
			end			
			
			if isa( a, "double") && isa( b, "signals.BasicVariable" )
%				assert( size(a,2) == size(b,1), "dimensions of the terms for multiplication do not match")
				assert( numel(size(a)) <= 2 )
				assert( numel(size(b)) <= 2 )

				degree = max([b.highestDerivative]);

				% do the multiplication for each derivative:
				for i = 1:degree+1
					diffs{i} = a * b.diff(I, i-1);
				end

			elseif isa( a, "signals.BasicVariable") && isa( b, "double" )
				c = ( b.' * a.' ).';
				return;
				
			elseif isa( a, "signals.BasicVariable" ) && isa( b, "signals.BasicVariable" )
				
				assert( a(1).domain.isequal(b(1).domain) , "Both BasicVariables must have the same domain");
				
				
				order = max( [a.highestDerivative], [b.highestDerivative] );
				
				for n = 0 : order
					diffs{n+1} = O;
					for k = 0 : n
						diffs{n+1} = diffs{n+1} + nchoosek(n, k) * a.diff(I, k) * b.diff(I, n-k);
					end
				end
			else
				error("The multiplication of " + class(a) + " and " + class(b) + " is not yet implemented");
			end
			
			c = signals.BasicVariable(diffs{1}, diffs(2:end));
			c = reshape(c, size(O) );
		end
			
	end % methods (Access = public)
	
	methods (Access = protected)
		function v = evaluateFunction(obj, domain, k)
			error('conI:signals:BasicVariable:derivative', ['Requested derivative ' num2str(k) ' is not available']);
		end
		
		function D = fDiff(obj, p)
			% FDIFF compute the p-fractional derivative
			%	D = fDiff(obj, p) computes the fractional derivative of order p. To be
			%	concrete the Riemann-Liouville fractional derivative with initial point t0 = lower
			%	end of the considered time domain is computed.
			%	Because the basisc variable must be C^infinity function, we use the form
			%	of the fractional derivative described in eq. (2.80) in the book [Podlubny: Fractional
			%	differential equations]. Then, integration by parts is applied to eliminate the
			%	singularity:
			%	
			
			n = ceil(p);
			tDomain = obj(1).domain;
			t0 = tDomain.lower;
			t = Discrete( tDomain );
			tauDomain = tDomain.rename( tDomain.name + "_Hat" );
			tau = Discrete( tauDomain );
			if p > 0
				
				% TODO Try to implement the polynomial part as function or as symbolic
				D = quantity.Discrete.zeros(size(obj), tDomain);
				for k = 0:n
					f_dk = obj.diff(tDomain, k);
					
					if f_dk.at(t0) ~= 0
						D = D + f_dk.at(t0) * (t - t0).^(k-p) / gamma( k-p + 1);
					end
					
				end
				assert( ~ any( isinf( obj.diff(tDomain, n+1).on() ) ), "conI:BasicVariable:fDiff", ...
					"The function has a singularity! Hence, the fractional derivative can not be computed")
				D = D + cumInt( ( t - tau ).^(n-p) * subs(obj.diff(tDomain, n+1), tDomain.name, tauDomain.name), ...
				tauDomain, tauDomain.lower, tDomain.name) / gamma( n+1-p );
				
			elseif p < 0 && p > -1
				alpha = -p;
				D = (t - t0).^(alpha) / gamma( alpha + 1) * obj.diff(tDomain, 0).at(t0) + ...
					cumInt( (t-tau).^alpha * subs(obj.diff(tDomain, 1), tDomain.name, tauDomain.name), ...
					tauDomain, tauDomain.lower, tDomain.name) / gamma( alpha + 1 );

			elseif p <= -1
				alpha = -p;
				D = cumInt( (t-tau).^(alpha-1) * subs( obj.diff(tDomain, 0), tDomain.name, tauDomain.name), ...
					tauDomain, tauDomain.lower, tDomain.name) / gamma( alpha );
				
			elseif p == 0
				D = [obj.fun()];	
			else
				error("This should not happen")
			end
		end		
		
	end	
	
end
