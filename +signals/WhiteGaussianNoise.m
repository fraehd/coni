classdef WhiteGaussianNoise
    %WhiteGaussianNoise
    
    properties
        sigma;
        mue = 0;
    end
    
    properties (Dependent)
        variance;
    end
    
    
    methods
        function obj = WhiteGaussianNoise(sigma)
			
			if nargin > 0
				for i = 1:numel(sigma)
					obj(i).sigma = sigma(i);
				end
				obj = reshape(obj, size(sigma));
			end
        end
        
        function s = get.variance(obj)
            s = obj.sigma^2;
        end
    end
    
    methods (Access = public)
        
        function X = calculateProbabilityForThreshold(obj, threshold)
            %CALCULATEPROBABILITYFORDETECTION computes the probability for
            %a given threshold
            %
            %   X = calculateProbabilityForThreshold( THRESHOLD ),
            %   calculate the probability X for a threshold THRESHOLD.
            %   Based on a normal distributed random variable with
            %   probability function P for a mean value MUE and a standard
            %   deviation SIGMA.
            %	The probability X is computed by
            %	P(|f(t)| <= fB) = P(-fB < f(t) <= fB) - P( |f(t)| <= fB)
            %					= F(fB) - F(-fB)
            %   F(fB) is the cumulative distribution function for a normal
            %   distribution with mean mue and sigma F(fB) =
            %   0.5(1+erf((fB-mu)/(sqrt(2)*sigma)))
            X = nan(size(obj));
			for i = 1:numel(obj)
				X(i) = 0.5*(1+erf((threshold(i) - obj(i).mue)/(sqrt(2) * obj(i).sigma))) ...
					- 0.5*(1+erf((-threshold(i) - obj(i).mue)/(sqrt(2) * obj(i).sigma))) ;
			end
					
        end
        
        function X = calculateThresholdForProbability(obj, P)
            %CALCULATETREHSHOLDFORPROBABILITY computes the threshold for fault
            %detection for a given probability detection
            %
            %   X = calculateProbabilityforDetection(X, MUE, SIGMA), calculate the
            %   treshold fB for a correct detection of the fault given the probability for
            %   detection P. Based on a normal distributed random variable with probability
            %   function P for a mean value MUE and a standard deviation SIGMA.
            %	The probability X is computed by
            %		P(|f(t)| <= fB) = P(-fB < f(t) <= fB) - P( |f(t)| <= fB)
            %						= F(fB) - F(-fB)
            %   F(fB) is the cumulative distribution function for a normal distribution
            %   with mean mue and sigma X = F(fB) = 0.5(1+erf((fB-mu)/(sqrt(2)*sigma)))
            %   Solving for fB with the inverse erf function and erf(-x) = -erf(x)  yields
            %   fB = sqrt(2) *  sigma * erfinv(P) + mue
            
			X = nan(size(obj));
			for i = 1:numel(obj)
				X(i) = sqrt(2) * obj(i).sigma * erfinv(P(i)) + obj(i).mue;
			end            
        end
        
        function o = noise(obj, t)
            o = randn(size(t)) * sqrt([obj.variance]);
        end
        
        function plotProbabilityDistributionFunction(obj, noise)
            histogram(noise);
        end
        
        function p = probabilityDensityFunction(obj, varargin)
            
            prsr = misc.Parser();
            prsr.addParameter('grid', linspace(- 5* obj.sigma, 5 * obj.sigma)');
            prsr.parse(varargin{:});
            
            x = sym('x', 'real');
			xDomain = quantity.Domain("x", prsr.Results.grid);
			
			p = quantity.Symbolic(zeros(size(obj)), xDomain);
			
			for i = 1:numel(obj)
			
            p(i) = quantity.Symbolic( 1 / sqrt( 2 * pi ) / obj(i).sigma * ...
                exp( - (x - obj(i).mue)^2 / 2 / obj(i).sigma^2 ), xDomain);
			
			end
        end
        
        function P = cumulativeDistributionFunction(obj, varargin)
            p = obj.probabilityDensityFunction(varargin{:});
            P = p.cumInt("x", p(1).domain(1).lower, "x");
        end
        
        function p = probability(obj, z)
            % computes the probability, that a value is inside the domain
            %	abs(x) < z
            
            P = obj.cumulativeDistributionFunction();
            
            p = 2 * P.at(abs(z - obj.mue)) - 1;
            
        end
	end
	
	methods ( Static )
		
		function P = stochasticHypothesis(f_hat, threshold, startIdx)
			%STOCHASTICHYPOTHESIS testing the stochastic hypothesis
			% P = StochasticHypothesis(f_hat, THRESHOLD, STARTIDX) computes
			% the rate of the values |f_hat| which are smaller than the
			% THRESHOLD. The parameter STARTIDX defines the first index of
			% f_hat, which should be considered for the evaluation.
			
			arguments
				f_hat;
				threshold;
				startIdx = 1;
			end
			
			f_hat = abs(f_hat);
			
			f_test = f_hat(startIdx:end);
			
			total_values = numel(f_test);
			
			values_smaller_fB = numel(find(f_test <= threshold));
			
			P = values_smaller_fB/total_values;
		end
		
	end
end

