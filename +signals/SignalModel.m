classdef SignalModel < matlab.mixin.Heterogeneous
	% Description of a signal model 
	%	v_t(t) = S * v(t),		
	%	  v(0) = v_0,
	%	  r(t) = P * v(t),	
	%
	
	properties 
		occurrence = 0;
		time quantity.Domain;
		initialCondition double;
	end
	
	properties (SetAccess = protected)
		sys_i ss;
	end
	
	properties (Dependent)
		n_v;	% dimension of the signal model
		n_p;	% dimension of the signal
		S;		% system matrix of the signal model
		P;		% output matrix of the signal model
		signalName; 
		modelName;
	end
	
	methods
		function obj = SignalModel(S, P, optArgs)
			arguments
				S (:,:) double;
				P (:,:) double;
				optArgs.occurrence (1,1) double = 0;
				optArgs.timeDomain (1,1) quantity.Domain = quantity.Domain.empty;
				optArgs.initialCondition (:,1) double;
				optArgs.signalName string = "r";
				optArgs.modelName string = "v";
			end
									
			obj.sys_i = ss(S, zeros(size(S,1), 1), P, [], ...
				'name', optArgs.modelName, ...
				'OutputName', optArgs.signalName);
			
			if ~isfield(optArgs, "initialCondition")
				optArgs.initialCondition = zeros(size(S,1),1);
			end
			
			obj.occurrence = optArgs.occurrence;
			obj.time = optArgs.timeDomain;
			obj.initialCondition = optArgs.initialCondition;
			
		end
		
		function s = get.modelName(obj)
			s = obj.sys_i.name;
		end
		
		function s = get.signalName(obj)
			s = obj.sys_i.OutputName;
		end
		
		function S = get.S(obj)
			S = obj.sys_i.A;
		end
		
		function P = get.P(obj)
			P = obj.sys_i.C;
		end
		
		function n = get.n_v(obj)
			n = size(obj.sys_i.A, 1);
		end
		
		function n = get.n_p(obj)
			n = size(obj.sys_i.C, 1);
		end

	end
	
	methods ( Access = public, Sealed ) 
		function sys = sys( obj )			
			sys = ss( obj.getS, [], obj.getP, []);
		end
		function n_states = n(obj)
			n_states = size(obj.getS, 1);
		end
		function m_outputs = m(obj)
			m_outputs = size(obj.getP, 1);
		end
		function sysMatrix = getS( obj )
			sysMatrix = blkdiag(obj.S);
		end
		
		function commonOut = getP( obj )
			commonOut = blkdiag(obj.P);
		end
		
		
		function s = on(obj, time)
			arguments
				obj
				time quantity.Domain = obj.time;
			end
			r = obj.simulate('time', time);
			s = r.on();
		end
		function [r, v] = simulate(obj, optArg)
			% SIMULATE a signal model
			% [r, v] = simulate(obj, varargin) will simulate a signal model
			% with r as the output and v as the states
			arguments
				obj
				optArg.time (1,1) quantity.Domain = obj(1).time;
				optArg.plot (1,1) logical = false;
				optArg.figure;
			end
			
			r = quantity.Discrete.empty();
			v = quantity.Discrete.empty();
			t = optArg.time.grid;
			
			for i = 1:numel(obj)
			
				assert(~isempty(t), 'You have set no simulation time for the signal model. Please set a simulation time for the signals.SignalModel object or for the call of the simulate function.');		

				v0 = obj(i).initialCondition;

				sim_t = t( t >= obj(i).occurrence );

				if all(v0 == 0)
					warning('Simulation of the signal model with zero initial conditions');
				end
				
				if numel( sim_t ) > 1
					%Check if there is something to simulate
				[sol.r, ~, sol.v] = initial(obj(i).sys_i, v0, sim_t - obj(i).occurrence);
				end
				% consider occurrence: 
				result.r = zeros(length(t), obj(i).n_p);
				result.v = zeros(length(t), obj(i).n_v);

				if numel( sim_t ) > 1
					% check if there is something to add				
					result.r( t >= obj(i).occurrence, : ) = sol.r;
					result.v( t >= obj(i).occurrence, : ) = sol.v;
				end
				
				r = [r; quantity.Discrete(result.r, optArg.time, "name", obj(i).signalName)];
				v = [v; quantity.Discrete(result.v, optArg.time, "name", obj(i).modelName)];

			end
			
			if nargout == 0 || optArg.plot
				if isfield(optArg, 'figure')
					h = figure(optArg.figure);
				else
					h = figure();
				end

				r.plot('fig', h.Number);
			end
			
		end
	end
		
	methods (Static)
				
		function sm = constant(varargin)	
			S = 0;
			p = 1;
			
			sm = signals.SignalModel(S, p, varargin{:});
		end
		
		function sm = ramp(varargin)
			warning("DEPRICATED! USE signals.RampModel")
			S = [0 1; 0 0];
			p = [1, 0];
			sm = signals.SignalModel(S, p, varargin{:});
		end
		
		function sm = sinusoidal(varargin)
			% SINUSOIDAL creat a sinusoidal signal model
			% sm = sinusoidal(varargin) will create a signal model with
			% sinusoidal form. The parameter to be set is a 'omega' as
			% frequency of the sinusoidal signal. 
			warning("DEPRICATED! USE signals.SinusModel")
			myParser = misc.Parser();
			myParser.addParameter('omega', 2*pi);
			myParser.parse(varargin{:});
			
			S = [0, -myParser.Results.omega; myParser.Results.omega 0];
			p = [0, 1];
			sm = signals.SignalModel(S, p, myParser.UnmatchedNameValuePair{:});
		end
	end
end

