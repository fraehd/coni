classdef RandomStairs
    %RANDOMSTAIRS Class to generate piecewise constant signal
    %   With this class some pseudo-random piecewise constant signals can
    %   be generated. The piecewise signal can be filtered with any
    %   continuous transfer function or FIR filter, so that a smooth
    %   trajectory is obtained.
	%		signals.RandomStairs(name, value, ...)  
    %	You can set the properties for the signal using name/value pairs.
	%		
    %   example:
    %   --------------------------
	% 		s = signals.RandomStairs('steps', 7, ... 
	% 							'seedt', -1, ...
	% 							'seedf', [], ...
	% 							'fmin', -100, ...
	% 							'fmax', 299);
	% 
	% 		% show the created signal
	% 		t = linspace(0, 5, 1e3);
	% 		s.plot(t);
	%		% get the created signals:
	%		u = s.signal(t);
	%		u_smooth = s.smoothSignal(t);
    %   --------------------------
    
    properties
        % Number of the stairs
		steps = [];         
		stepsRatio = [];
		
		% Number of signals to be generated
        dim = 1;
        
        % seed for the random number generator for the timestamps
		% If empty, i.e., [], a random seed will be chosen. This is the
		% default.
		% If -1, the jumping points will be equally distributed over the
		% temporal domain.
        seedt double;         
        
		% seed for the random number generator for the hight of the jumps
		% If empty, i.e., [], a random seed will be chosen. This is the
		% default.
        seedf double;         
		
        fmin double;        % Lower bound for the hight of the jumps
        fmax double;        % Upper bound for the hight of the jumps
        G;                  % IIR-Filter for smoothing of the jumps
        f0;                 % Initial-values for the filtered values.
		name string;		% A name for the signal
		
		normalize logical = false;
    end
        
    methods
        
        function obj = RandomStairs(varargin)
            %RANDOMJUMPS Class for the creation of (pseudo) random input
            % obj = RandomStairs(varargin) will return a signal generator
            % to create piecewise constant (or also filtered) signals. The
            % system properties can be set by name-value-pairs in this
            % constructor.
			
			myParser = misc.Parser();
			myParser.addParameter('steps', []);
			myParser.addParameter('stepsRatio', []);
			myParser.addParameter('seedt', []);
			myParser.addParameter('seedf', []);
			myParser.addParameter('fmin', -1);
			myParser.addParameter('fmax', 1);
			myParser.addParameter('G', []);
			myParser.addParameter('f0', []);
			myParser.addParameter('dim', 1);
			myParser.addParameter('name', "");
			myParser.addParameter('normalize', false);
			myParser.parse2obj(obj, varargin{:});     
			myParser.unmatchedWarning();
			
			if myParser.isDefault('stepsRatio') && myParser.isDefault('steps')
				obj.stepsRatio = 2;
			end
			
		end
		
		function q = Discrete(obj, timeDomain, optArg)
			% QUANTITY.DISCRETE converter of the RandomStairs object to a
			% quantity.Discrete object
			% q = quantity.Discrete(obj, TIMEDOMAIN, optArg) will convert
			% the signals.RandomStairs object to a quantity.Discrete type.
			% This requires a TIMEDOMAIN as a quantity.Domain object. With
			% the name-value-pair "smooth" as logical value, it can be
			% chosen if the smooth stairs should be used. Default value is
			% true.
			arguments
				obj
				timeDomain quantity.Domain;
				optArg.smooth (1,1) logical = true;
			end
			
			if optArg.smooth
				q = quantity.Discrete(obj.smoothSignal(timeDomain.grid), timeDomain, 'name', obj.name);
			else			
				q = quantity.Discrete(obj.signal(timeDomain.grid), timeDomain, 'name', obj.name);
			end
		end

        
        function [f, tk, fk] = signal(obj, t)
            %SIGNAL compute the piecewise constant signal at the desired
            %jumping points of random hight.
            tk = obj.jumpingPoints(t(:));
            
            if ~isempty(obj.seedf)
                rng(obj.seedf);
            else
                rng('shuffle')
			end
			
			mySteps = obj.getMySteps(t);
			
            fk = rand(mySteps, obj.dim) * diag(obj.fmax - obj.fmin) ...
						+ repmat(obj.fmin', mySteps, 1);
    
            fk = [fk; fk(end,:)];   
			
			% if required normalize fk to min max values:
			if obj.normalize
				
				% get all non zero entries
				i = ( fk ~= 0 );
				
				fk(i) = fk(i) ./ fk(i) .* sign(fk(i));
			end

            f = interp1(tk, fk, t(:), 'previous', 'extrap');
		end
		
        function tk = jumpingPoints(obj, t)
           %JUMPINGPOINTS computes the points at which a jump occurs,
           %depent if they should be equally or randomly distributed.
		   
		   mySteps = obj.getMySteps(t);
		   
           if ~isempty(obj.seedt) && obj.seedt < 0       
               % create equally distributed intervals
               tk = linspace(t(1), t(end), mySteps + 1).';
           else
               
               if isempty(obj.seedt)
                  rng('shuffle');
               else
                  % create intervals of random length
                  rng(obj.seedt);
               end
           
              tk = rand(mySteps,1);
              tk = [0; cumsum(tk)];
              tk = tk * (t(end) - t(1)) / tk(end) + t(1);
           end
           
        end
        
        function f = smoothSignal(obj, t, f)
            %  SMOOTHJUMPS filters the signal with the filter *G*
            if nargin == 2
                f = obj.signal(t(:));
            end            
            
            if ~isempty(obj.G)
                if isempty(obj.f0)
                    fo = f(1,:);
                else
                    fo = obj.f0;
                end

                stsp = ss(obj.G);
                u0 = fo.';
                y0 = stsp.C * inv(-stsp.A) * stsp.B * u0;

                x0 = [stsp.A; stsp.C] \ [-stsp.B * u0; y0];

                f = lsim(stsp, f, t - t(1), x0);
			else
                %% gevrey-function filter
                tk = obj.jumpingPoints(t);
                T = mean(diff(tk));
                dt = misc.dt(t);
                tau = 0:dt:T;
                b = signals.gevrey.bump.g(tau, 1, T, 1.1);
                k = tau(end) / length(tau);
                f = filter(k * b, 1, f);
                
            end
        end
        
        function f = plot(obj, t)
            %PLOT show the created random signal for time t.
            
            [f, tk, fk] = obj.signal(t);
            
            clf; hold on;
            plot(t, f);
            plot(tk, fk, 'x')
            plot(t, obj.smoothSignal(t, f));            
        end
        
	end
	
	methods ( Access = protected )
		function mySteps = getMySteps(obj, t)
			if isempty( obj.steps )
				mySteps = ceil( (t(end) - t(1)) * obj.stepsRatio );
			else
				mySteps = obj.steps;
			end
		end
	end
	
    methods (Static)
        function example()
            %EXAMPLE for the usage of RandomStairs
            %   
            
            % create a filter to smooth the signal
            T = 0.07;
            D = 0.7;
            Gpt2 = tf(1, [T^2, 2 * T * D, 1]);
           
            % create the object for the generation of random signal. 
            signal = signals.RandomStairs('steps', 7, ...
                                'seedt', -1, ...
                                'seedf', [], ...
                                'fmin', -100, ...
                                'fmax', 299, ...
                                'G', Gpt2);
            
            % show the created signal
            t = linspace(0, 5, 1e3);
            signal.plot(t);
        end
        
        function s = sample(t, bounds)
            if nargin < 2
               bounds = [-100, 299]; 
            end
                
            pulse = signals.RandomStairs('steps', 7, ...
                                'seedt', -1, ...
                                'seedf', 1, ...
                                'fmin', bounds(1), ...
                                'fmax', bounds(2));
                            
            s = pulse.signal(t);
        end
        
    end
    
end

