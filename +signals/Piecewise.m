classdef Piecewise < quantity.BasicVariable
	%PIECEWISE: Definition of piecewise basic variables with compact
	%support
	%
	%	The function should be defined as
	%					
	%		       |	0    : t <= t0
	%		f(t) = |  phi(t) : t0 < t < t1
	%			   |    0    : t >= t1
	
	properties
		piecewiseFunction;		
		sgn;
		norm;
		t0;
		t1;
	end
	
	methods
		function obj = Piecewise(piecewiseFunction, varargin)
			
			% make default grid:
			preParser = misc.Parser();
			preParser.addParameter('z', linspace(0, 1)');
			preParser.addParameter('t', linspace(0, 1)');
			
			preParser.parse(varargin{:});
		
			prsr = misc.Parser();
			prsr.addParameter('t0', preParser.Results.t(1));
			prsr.addParameter('t1', preParser.Results.t(end));
			prsr.addParameter('grid', {preParser.Results.z, preParser.Results.t});
			prsr.addParameter('gridName', {'z', 't'});
			prsr.addParameter('name', 'phi');
			prsr.addParameter('sgn', 1);
			prsr.addParameter('T', preParser.Results.t(end));
			prsr.addParameter('N_t', numel(preParser.Results.t));
			prsr.addParameter('norm', 0);
			prsr.parse(varargin{:});			
			uargin = misc.struct2namevaluepair(prsr.Unmatched);
			
			argin = [uargin(:)', ...
				{'grid', prsr.Results.grid, ...
				 'gridName', prsr.Results.gridName, ...
				 'name', prsr.Results.name, ...
				 'T', prsr.Results.T, ...
				 'N_T', prsr.Results.N_t}];
			
			 % piecewiseFunction defined as array:
			 %	[f(t), dt f(t), dt^2 f(t) ... ]
% 			 if isa(piecewiseFunction, 'quantity.Discrete')
% 				 fun = @(t)
 			if ~isa(piecewiseFunction, 'quantity.Function')
				piecewiseFunction = quantity.Function(piecewiseFunction);
			end
			fun = piecewiseFunction(:,1).function_handle;
			 
			obj@quantity.BasicVariable(@(tau) ...
				(prsr.Results.t0 < tau  & tau < prsr.Results.t1) .* ...
				fun(tau), argin{:});
			
			obj.piecewiseFunction = piecewiseFunction;
			obj.sgn = prsr.Results.sgn;
			obj.norm = prsr.Results.norm;
			obj.t0 = prsr.Results.t0;
			obj.t1 = prsr.Results.t1;
			
			if obj.norm ~= 0
				obj.K = 1 / double( int(subs(obj, 'z', 0)) ) / obj.norm;
			end
		end
		
		function mObj = uminus(obj)
			mObj = obj.copy();
			
			for k = 1:numel(obj)
				mObj(k).K = -obj.K;
				mObj(k).valueDiscrete = - obj(k).valueDiscrete;
			end
			
			[mObj.name] = deal(["-" + obj.name]);
			[mObj.derivatives] = deal({}); % #FIXME copy derivatives
		end
	end
	
	methods (Access = protected)
		function v = evaluateFunction(obj, z, t)
					
			fun = obj.piecewiseFunction(:, obj.diffShift + 1).function_handle;
			
			fun = @(tau) (obj.t0 < tau & tau < obj.t1) .* fun(tau);
			
			v = obj.K * fun( t + obj.sgn * obj.t0 * z);
		end		
	end	
	
	methods (Static)
		
		function [phi_p, phi_m] = makeVariable(varargin)
			
			phi_p = signals.Piecewise(varargin{:}, 'sgn', 1);
			phi_m = signals.Piecewise(varargin{:}, 'sgn', -1);
			
		end
		
	end	
	
end

