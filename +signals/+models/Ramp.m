classdef Ramp < signals.SignalModel
	%RAMP signal model to describe a ramp
	%	The signal model has the form
	%	dt v = [0 1; 0 0] v
	%	   r = [1 0] v
	%
	%	The slope and offset are defined by the initial conditions.
	%	The ramp-like signal model can be initialized by
	%		obj = Ramp(offset, slope, optArgs) with offset and slope to set the initial conditions
	%		so that a corresponding signal will result. Further parameters can be set via optArgs
	%		name-value-pairs argument. Details about them can be found in signals.SignalModel
	
	properties (Dependent)
		slope (1,1) double;
		offset (1,1) double;
	end
	
	methods
		function obj = Ramp(offset, slope, optArgs)
			% obj = Ramp(offset, slope, optArgs) with offset and slope to set the initial conditions
			% so that a corresponding signal will result. Further parameters can be set via optArgs
			% name-value-pairs argument. Details about them can be found in signals.SignalModel
			arguments
				offset (1,1) double;
				slope (1,1) double;
				optArgs.occurrence (1,1) double = 0;
				optArgs.timeDomain (1,1) quantity.Domain = quantity.Domain.empty;
				optArgs.signalName string = "r";
				optArgs.modelName string = "v";
			end
			
			optArgs.initialCondition = [offset, slope];
			varArgs = namedargs2cell(optArgs);
			
			S = [0, 1; 0 0];
			p = [1, 0];
			obj@signals.SignalModel(S, p, varArgs{:});
		end
		
		function s = get.slope(obj)
			s = obj.initialCondition(2);
		end
		function c = get.offset(obj)
			c = obj.initialCondition(1);
		end
	end
end

