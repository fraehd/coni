classdef Sinus < signals.SignalModel
	%SINUS signal model to describe a sinusoidal signal
	%	
	
	properties (Dependent)
		omega
	end
	
	methods
		function obj = Sinus(omega, varargin)
			% SINUSOIDAL creat a sinusoidal signal model
			% sm = sinusoidal(varargin) will create a signal model with
			% sinusoidal form. The parameter to be set is a 'omega' as
			% frequency of the sinusoidal signal. 
			arguments
				omega (1,1) double;
			end
			arguments (Repeating)
				varargin
			end
			
			S = [0, -omega; omega 0];
			p = [0, 1];
			obj@signals.SignalModel(S, p, varargin{:});
		end		
		
		function o = get.omega(obj)
			o = obj.S(2,1);
		end
	end
end

