classdef Constant < signals.SignalModel
	%RAMP signal model to describe a ramp
	
	properties (Dependent)
		offset (1,1) double;
	end
	
	methods
		function obj = Constant(offset, optArgs)
			% SINUSOIDAL creat a sinusoidal signal model
			% sm = sinusoidal(varargin) will create a signal model with
			% sinusoidal form. The parameter to be set is a 'omega' as
			% frequency of the sinusoidal signal. 
			arguments
				offset (1,1) double;	
				optArgs.occurrence (1,1) double = 0;
				optArgs.timeDomain (1,1) quantity.Domain = quantity.Domain.empty;
				optArgs.signalName string = "r";
				optArgs.modelName string = "v";
			end

			optArgs.initialCondition = offset;
			varArgs = namedargs2cell(optArgs);
			
			S = 0;
			p = 1;
			obj@signals.SignalModel(S, p, varArgs{:});
		end		
		function c = get.offset(obj)
			c = obj.initialCondition(1);
		end
	end
end

