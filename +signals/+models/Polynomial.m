classdef Polynomial < signals.SignalModel
	%POLYNOMIAL signal model to describe a polynomial
	% By this class signals of the form
	%	r(t) = a_0 + a_1 t + a_2/2 t^t + ... = sum_{k=0}^n a_k / k! t^k
	% can be described. This is represented by the signal model
	%		    /0 1 0 0 ... 0\
	%           |0 0 1 0 ... 0|
	% dt v(t) = |     ...     | v(t)
	%           |0 0 0 0 ... 1|
	%           \0 0 0 0 ... 0/
	%    r(t) = [1 0 0 0 ... 0] v(t)
	%
	% The signal is initialized with 
	%	obj = Polynomial(n, coefficients, optArgs)
	% where n is the order of the polynomial and coefficients are the a_k.
	
	properties
		coefficients (:,1) double;
	end
	
	methods
		function obj = Polynomial(n, coefficients, optArgs)
			arguments 
				n (1,1) double;
				coefficients (:,1) double;
				optArgs.occurrence (1,1) double = 0;
				optArgs.timeDomain (1,1) quantity.Domain = quantity.Domain.empty;
				optArgs.signalName string = "r";
				optArgs.modelName string = "v";				
			end
			
			optArgs.initialCondition = coefficients;
			varArgs = namedargs2cell(optArgs);
			
			S = diag(ones(n-1,1), 1);
			p = misc.unitVector(n, 1)';
			obj@signals.SignalModel(S, p, varArgs{:});
			
		end
	end
end

