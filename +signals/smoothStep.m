function S = smoothStep(deg, optArg)
% SMOOTHSTEP compute a smooth transition
%	S = smoothStep(DEG, VARARGIN) computes a smooth transition s(z) from
%		s(0) = 0	->		s(1) = 1
%	It uses a spline of degree DEG to define a polynomial for this
%	transition.
arguments
	deg (1,1) {mustBeInteger} ;
	optArg.domain = quantity.Domain('z', linspace(0, 1, 101));
	optArg.s0 = 0;
	optArg.s1 = 1;
end
% 

warning("DEPRICATED: Use misc.ss.setPointChange instead");

assert( all( size(optArg.s0) == [deg, 1]))
assert( all( size(optArg.s1) == [deg, 1]))

o = zeros(1, deg);

s = spline([optArg.domain.lower, optArg.domain.upper], ...
	[o optArg.s0.', optArg.s1.' o], optArg.domain.grid);

S = quantity.Discrete(s, optArg.domain);

end