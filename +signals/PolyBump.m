classdef PolyBump < quantity.Function
	% POLYBUMP wrapper for a polynomial bump
	%          / 0;								t <= T0
	%		   |
	%	f(t) = | c * (t - T0)^a * (T1 - t)^b,	T0 < t < T1
	%          |
	%          \ 0;								t >= T1
	
	properties (SetAccess = protected)
		a (1,1) double {mustBeInteger};
		b (1,1) double {mustBeInteger};
		c (1,1) double;
		T0 (1,1) double;
		T1 (1,1) double;
		norm logical;
		coeffs;
	end
		
	methods
		function obj = PolyBump(t, optArgs)
			arguments
				t quantity.Domain;
				optArgs.a (1,1) double {mustBeInteger} = 1;
				optArgs.b (1,1) double {mustBeInteger} = 1;
				optArgs.T0 (1,1) double = t.lower;
				optArgs.T1 (1,1) double = t.upper;
				optArgs.c (1,1) double = 1;
				optArgs.norm (1,1) logical = false;
			end
			
			misc.struct2ws(optArgs);
			
			if optArgs.norm
				if c ~= 1
					warning('The gain factor c will be overwritten so set the norm')
				end
				f = signals.PolyBump.polyBumpFunction(a, b, c, T0, T1);
				c = 1 / integral(f, T0, T1);
			end
			
			fun = signals.PolyBump.polyBumpFunction(a, b, c, T0, T1);
			
			obj@quantity.Function(fun, t);
			
			obj.a = a;
			obj.b = b;
			obj.c = c;
			obj.T0 = T0;
			obj.T1 = T1;
			obj.norm = optArgs.norm;
			obj.coeffs = sym2poly( sym( fun ) );
		end
	end
	
	methods ( Access = public )
		
		
	end
	
	methods ( Access = protected )
		
		function v = evaluateFunction(obj, varargin)
			t = varargin{:};
			v = obj.valueContinuous( t ) .* (obj.T0 < t & t < obj.T1);
		end
		
		function result = diff_inner(obj, k, diffGridName)
			result = obj.copy();
			result.setName("d_{" + diffGridName + "} " + result(1).name + ")");
			[result.valueDiscrete] = deal([]);
			
			for l = 1:numel(obj)
				
				dk_coeffs = obj(l).coeffs;
				for i = 1:k
					dk_coeffs = polyder( dk_coeffs );
				end
				result(l).valueContinuous = @(t) polyval(dk_coeffs, t);
			end
		end
		
	end
	
	methods (Static)
		
		function f = polyBumpFunction(a, b, c, T0, T1)
			f = @(t) c * ( t - T0 ).^a .* ( T1 - t ).^b;
		end
	end
end