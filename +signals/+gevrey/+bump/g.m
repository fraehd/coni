function [out] = g(t, k, T, sigma)
%G Wrapper for the Bump-Gevrey function.
%   [out] = g(t, k, T, sigma) computes the k-th derivative of the 
%   Gevreyfunction 
%       exp(-(1.0./T.^2.*(T.*t-t.^2)).^(-sigma))
%   with the Gevrey-order
%       order = 1 + 1 / sigma
%   for the length T at the timesteps t.
%
%   The derivatives are computed a postiori using symbolic algebra
%   tools and saved as matlab function. This function acts as
%   wrapper to this functions.
out = zeros(length(t),1);
domain = t > 0 & t < T;
out(domain) =  feval(['signals.gevrey.bump.dgdt_'  num2str(k)], ...
    T, sigma, t(domain)) / signals.gevrey.bump.dgdt_0(T, sigma, T);

    % Consider the piecwise definition of the Gevrey-function
	out(t <= 0) = 0;
    if k > 0                                                 
        out(t >= T) = 0; 
	elseif k == 0
		out(t >= T) = 1;
    end 
	
	if any(t > T | t < 0)
		warning('Requested values outside of the support of the gevrey function.');
	end
end