function out = dgdt_2(T,si,t)
t1 = 1 ./ T;
t3 = -t .* t1 + 1;
t6 = (t3 .* t .* t1) .^ si;
t7 = 1 ./ t6;
t9 = (T .^ 2);
t18 = exp(-t7);
out = t7 .* si .* (-1 ./ t9 .* t + t3 .* t1) ./ t3 ./ t .* T .* t18;

