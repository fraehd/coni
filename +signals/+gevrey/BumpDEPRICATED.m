classdef Bump < signals.gevrey.BasicVariable
    %BUMP The standard Gevrey-function based on exp-function.
    %   Representation for the gevrey function
    %       phi(t) = integral( exp(-(1.0./T.^2.*(T.*tau-tau.^2)).^(-si)), 0, t )/ |integral( exp(-(1.0./T.^2.*(T.*tau-tau.^2)).^(-si)), 0, T )|;
    %   With this class, the properties of the gevrey function can be
    %   defined. 
    
    properties (Dependent)
        % This factor is used to scale the gevrey function, so that
        %   PHI = integral( exp(-(1.0./T.^2.*(T.*t-t.^2)).^(-si)) )
        % is equal to one. This ensures, that the gevrey function phi
        % describes a set-point change from 0 to 1.
       scaling;
       
       % The value in phi, that corresponds to the gevrey order by
       %    sigma = 1 / (order - 1)
       sigma;
    end
    
    methods
        function obj = Bump(varargin)
            obj@signals.gevrey.BasicVariable();
            for arg = 1:2:length(varargin)
                obj.(varargin{arg}) = varargin{arg + 1};
            end            
        end
        
        function s = get.sigma(obj)
            s = 1 / ( obj.order - 1);
        end
        
        function set.sigma(obj, s)
           obj.order = 1 + 1/s; 
        end        
        
        function s = get.scaling(obj)
            s = 1 / signals.gevrey.bump.dgdt_0(obj.T, obj.sigma, obj.T);
        end
        
        function [dgdt, t, scale] = eval(obj, k, t)
            %EVAL Computes k-th derivative at timesteps t
            %
            % [dgdt, t] = eval(obj, k) computes k-th
            % derivative dgdt with obj.Nt discretization points
            %
            % [dgdt, t] = eval(obj, k, t) computes k-th
            % derivative dgdt at timesteps t.

            assert(all([obj.N] == obj(1).N))
            assert(all([obj.T] == obj(1).T))
            
            lNt = obj(1).N;
            lT  = obj(1).T;
            
            % either t is determined by the number of requested points Nt
            %   or the number of discretization points is given by length
            %   of t
            if ~exist('t', 'var')
                t = obj.maket; 
            else
                lNt = length(t);
            end
            
            if ~exist('k', 'var')
                k = 0;
            end         

            dgdt = zeros(lNt, length(obj));
            
			scale = zeros(length(obj));
            for l = 1:length(obj)
				[tempg, scale(l)] =  obj(l).g(t, k + obj(l).diffShift);
                dgdt(:,l) = obj(l).K .* tempg;
                
                if (k - obj(l).diffShift) == 0
                    dgdt(:,l) = dgdt(:,l) + obj(l).offset;
                end                
            end
        end
        
        function [out, scale] = g(obj, t, k)
        %G Wrapper for the Bump-Gevrey function.
        %   Evaluation of the k-th derivative of the Gevrey-Bump function
        %   with the Gevrey-order
        %       alpha = 1 + 1 / sigma
        %   for the length T at the timesteps t.
        %
        %   The derivatives are computed a postiori using symbolic algebra
        %   tools and saved as matlab function. This function acts as
        %   wrapper to this functions.
        
        outTemp =  feval(['signals.gevrey.bump.dgdt_'  num2str(k)], ...
            obj.T, obj.sigma, t);
		out = outTemp./max(abs(outTemp(:)));
		scale = 1./max(abs(outTemp(:)));

            % Consider the piecwise definition of the Gevrey-function
            if k > 0                                                 
                        out(1) = 0;
                out(length(t)) = 0; 
            else
                        out(1) = 0;   
                out(length(t)) = 1; 
            end % if k > 0
        end % g(obj, t, k)
    end % methods
end

