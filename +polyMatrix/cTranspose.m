function [M_T] = cTranspose(M)
%CTRANSPOSE complex conjugate transpose of M
% [M_T] = cTranspose(M) returns the complex conjugate transpose of a
% polynomial matrix M = sum M_k s^k
%	M^T_k = sum (-1)^k M_k^T s^k
% M has to be given as 3-dimensional array with the dimension 3 as
% polynomial dimension.
M_T = zeros(size(M,2), size(M, 1), size(M, 3));

for k = 1:size(M, 3)
	M_T(:,:,k) = (-1)^(k-1) * M(:,:,k)';
end

