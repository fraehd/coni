function [ S ] = powerSeries2Sum( M, s_var )
% POWERSERIES2SUM Sum of power series in s_var
%
% Description:
% [ S ] = powerSeries2Sum( M, s_var )
% Summation of all entries in *M* as power series with the symbolic
% varivable *s_var*. *M* can be a cell or a three dimensional array. For
% the 3-D array the dimension 1 and 2 are for the matrices that sould be
% summed up and dimension 3 defines the exponent of the series. Its index
% k corresponds to the coefficient for s_var^(k-1).
%
% Inputs:
%     M           Matrix of interest
%     s_var       Symbolic variable in which S should be developed
% Outputs:
%     S           Summation of power Series
%
% Example:
% -------------------------------------------------------------------------
% import misc.*
% syms s
% M = cat(3, [0 1], [2 0], [0 3]);
% powerSeries2Sum(M, s)
% >> ans = [2*s, 3*s^2 + 1]
% -------------------------------------------------------------------------

if iscell(M)

    S=M{1};
    for k=2:size(M,1)
        S=S+M{k}*s_var^(k-1);
    end

else
    
    S = M(:,:,1);
    for k=2:size(M,3)
       S = S + M(:,:,k)*s_var^(k-1);
    end   
end