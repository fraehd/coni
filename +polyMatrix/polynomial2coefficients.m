function [ APoly ] = polynomial2coefficients(A, s_var, N)
% OPERATOR2POLYNOMIAL Returns the coefficient matrices of an operator
%
% Inputs:
%     A       Operator matrix with polynomials A \in C[s]^(n x n)
%     s_var   symbolic variable
%     N       maximal degree to be regarded for the coefficients. If the
%             polynomial contains higher degree entries than *N*, 
%             the values are cut off.
% Outputs:
%     APoly   Coefficients
%
% Example:
% -------------------------------------------------------------------------
% import misc.*
% syms s;
% A = [10, s; 2*s^2, 3*s^3]
% polynomial2coefficients(A, s)
%   
% >> ans(:,:,1) = [10, 0]
%                 [ 0, 0]
% >> ans(:,:,2) = [ 0, 1]
%                 [ 0, 0]
% >> ans(:,:,3) = [ 0, 0]
%                 [ 2, 0]
% >> ans(:,:,4) = [ 0, 0]
%                 [ 0, 3]
% -------------------------------------------------------------------------

if ~exist('N', 'var')
    % determine maximal degree of spatial variable s
    sDegreeMax = -1;
    for i=1:numel(A)
        sDegreeMax = max([sDegreeMax, length(coeffs(A(i), s_var, 'All'))]);
    end
    N=sDegreeMax;
end

% convert system-operator into polynomal expression
APoly = zeros([size(A), N]);
for i=1:size(A,1)
    for j=1:size(A,2)
        tmp = flip( coeffs(A(i,j), s_var, 'All') );
		APoly(i, j, 1:numel(tmp)) = tmp;
    end
end