function [y, w, x] = simulate(stsp, u, optArgs)

arguments
	stsp ss;
	u quantity.Discrete;
	optArgs.timeDomain quantity.EquidistantDomain = u(1).domain;
	optArgs.x0 = zeros(size(stsp.A, 1), 1);
	optArgs.stepSize (1,1) double = u(1).domain.stepSize;
	optArgs.tol = 1.0e-6;
	optArgs.itmax = 500;
	optArgs.spatialDomain = quantity.Domain("z", linspace(0, 1, size(stsp.A, 1)));
	optArgs.silent (1,1) logical = false;
end


if ~exist('MT_FDE_PI1_Im.m', 'file')
	error("The fractional solver MT_FDE_PI1_Im must be in the matlab path. It can be downloaded from https://de.mathworks.com/matlabcentral/fileexchange/66603-solving-multiterm-fractional-differential-equations-fde");
end
	
if isempty( stsp.E )
	E = eye(size(stsp.A,1));
else
	E = stsp.E;
end

assert( stsp.UserData.alpha <= 1 && stsp.UserData.alpha > 0, ...
	'Fractional order must hold 0 < alpha <= 1!');

f_fun = @(t, x) stsp.A * x + stsp.B * u.at(t);
J_fun = @(t, x) stsp.A; % Jacobian matrix of f_fun, calculated by hand. It always has the same structure.

[~, x] = FDE_PI1_Im_Ver_10(stsp.UserData.alpha, f_fun, J_fun, ...
	optArgs.timeDomain.lower, ...
	optArgs.timeDomain.upper, ...
	optArgs.x0, ...
	optArgs.stepSize, ...
	[], ...
	optArgs.tol, ...
	optArgs.itmax, ....
	optArgs.silent);

w = quantity.Discrete(x, [optArgs.spatialDomain, optArgs.timeDomain]);
x = quantity.Discrete(x', optArgs.timeDomain);

y = stsp.C * x + stsp.D * u;

end