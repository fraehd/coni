function [y, m] = monomialFractionalDerivative(t, t0, nu, p, optArg)
%MONOMIALFRACTIONALDERIVATIVE compute fractional derivative of a monomial
% [y, m] = monomialFractionalDerivative(t, a, nu, p, optArg) compute the p-fractional derivative of
% the monomial m = (t-a)^nu. At this, t must be a symbolic variable, t0 is the lower terminal as
% double value, nu is the exponent as double, p is the fractional order as double the type of the
% derivative can be specified by the name-value-pair "type". So far online the "RL" :
% Riemann-Liouville derivative is implemented.
arguments
	t (1,1) sym; % symbolic variable for the monomial
	t0 (1,1) double;				% expansion point of the monome
	nu (1,1) double;			% order of the monome
	p (1,1) double;				% order of the fractional derivative
	optArg.type string = "RL";	% Type of the fractional derivative
end

m = (t-t0)^nu;

switch optArg.type
	
	case "RL"
		y = gamma( 1+nu ) / gamma( 1+nu-p ) * ( t - t0 )^(nu-p);
	otherwise
		error("Not jet implemented, only the RL - Rieman Liouville derivative is available")
end
end