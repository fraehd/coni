function [tests] = testWhiteGaussianNoise()
%TESTWHITEGAUSSIANNOISE 
tests = functiontests(localfunctions);
end

function testcalculateProbabilityForThreshold(testCase)
% create signal with variance = 2
d  = signals.WhiteGaussianNoise(sqrt(2));
% the two methods give the same expected result
P = calculateProbabilityForThreshold(d,sqrt(2));
P1 = probability(d,sqrt(2));

testCase.verifyEqual([P,P1],[0.6827,0.6827],'absTol',1e-4);
end

function testcalculateThresholdForProbability(testCase)

d  = signals.WhiteGaussianNoise(sqrt(2));
fB = calculateThresholdForProbability(d,0.6827);

testCase.verifyEqual(fB,sqrt(2),'absTol',1e-4);
end

function testNoise(testCase)
t = 0 : 1e-3 : 10000;
d  = signals.WhiteGaussianNoise(sqrt(2));
d_form = noise(d,t);
pd = fitdist(d_form.','Normal');
sigma = pd.sigma;

testCase.verifyEqual(sigma,sqrt(2),'absTol',1e-3);
end
