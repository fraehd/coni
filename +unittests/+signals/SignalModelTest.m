function [tests] = SignalModelTest()
tests = functiontests(localfunctions);
end

function initTest(testCase)
t = quantity.Domain("t", linspace(0,1));
s(1) = signals.SignalModel(0, 1, "timeDomain", t, "initialCondition", 1, "occurrence", 0.5);
s(2) = signals.SignalModel(-1, 1, "timeDomain", t, "occurrence", 0, "initialCondition", 1);

[r, v] = s.simulate();

testCase.verifyEqual(r.on(), v.on())
testCase.verifyEqual(r.atIndex(end), [1; exp(-1)], 'AbsTol', 5e-15)
testCase.verifyTrue(all( r(1).valueDiscrete(t.grid < 0.5) == 0) )
testCase.verifyTrue(all( r(1).valueDiscrete(t.grid > 0.5) == 1) )

end

function sinusTest(testCase)
t = quantity.Domain("t", linspace(0,4*pi));
s = signals.models.Sinus(pi, "timeDomain", t, "initialCondition", [1; 0]);
r = s.simulate;
testCase.verifyEqual(r.on(), sin(t.grid * pi), 'AbsTol', 5e-14)
end

function rampTest(testCase)
t = quantity.Domain("t", linspace(0,1));
s = signals.models.Ramp(2, -1, "time", t);
r = s.simulate();
testCase.verifyEqual( r.on(), s.offset + s.slope * t.grid, 'AbsTol', 5e-14)
end

function constantTest(testCase)
t = quantity.Domain("t", linspace(0,1));
s = signals.models.Constant(2, "time", t);
r = s.simulate();
testCase.verifyEqual( r.on(), s.offset + 0* t.grid, 'AbsTol', 5e-14)
end

function concatenateTest(testCase)
t = quantity.Domain("t", linspace(0,4*pi));
s(1) = signals.models.Sinus(pi, "timeDomain", t, "initialCondition", [1; 0]);
s(2) = signals.models.Ramp(2, -1, "time", t);

r = s.simulate();
testCase.verifyEqual(r(1).on(), s(1).on())
testCase.verifyEqual(r(2).on(), s(2).on())
end

function polynomialTest(testCase)

T = quantity.Domain("t", linspace(0, 3));

a_k = rand(4,1);

s = signals.models.Polynomial( 4, a_k, "timeDomain", T);

t = Discrete(T);
r = a_k' * [1; t; t^2/2; t^3/6];

testCase.verifyEqual( r.on(), s.on(), 'AbsTol', 1e-14)

end