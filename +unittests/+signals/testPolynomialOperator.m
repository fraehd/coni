function [tests] = testPolynomialOperator()
%TESTGEVREY Summary of this function goes here
%   Detailed explanation goes here
tests = functiontests(localfunctions);
end

function setupOnce(testCase)
%%
z = sym('z', 'real');
s = sym('s');
Z = quantity.Domain("z", linspace(0, 1, 11));

% init with symbolic values
A = cat(3, [ 0, 0, 0, 0; ...
		     1, 0, 0, 0; ...
			 0, 1, 0, 0; ...
			 0, 0, 1, 0], ...
            [0, 0, 0, -3 + z; ...
			 0, 0, 0,   0; ...
			 0, 0, 0,   0; ...
			 0, 0, 0,   0], ...
			[0, 0, 0, -4; ...
			 0, 0, 0, 0; ...
			 0, 0, 0, 0; ...
			 0, 0, 0, 0]);

B = cat(3, [1, 0, 0, 0]', zeros(4, 1), zeros(4, 1));
		 
for k = 1:3
	a{k} = quantity.Symbolic(A(:,:,k), Z);
end
		 
A = signals.PolynomialOperator(a, 's', s);

testCase.TestData.A = A;
testCase.TestData.a = a;

end

function testInit(testCase)

K = magic(5);
op.K = signals.PolynomialOperator(K);

testCase.verifyEqual( size(op.K), [1 5 5]);

end

function testEmpty(testCase)

	e = signals.PolynomialOperator.empty();
	testCase.verifyTrue( isempty( e ) );

end

function testApplyTo(testCase)

	A = [0 1 0; 0 0 1; - (1:3)];
	B = [0 0 1]';
	C = [1 0 0];
	
	sys = ss(A, B, C, []);
	detsIA = signals.PolynomialOperator(num2cell(flip(charpoly(A))));
	adjB = signals.PolynomialOperator( adjoint(sym('s')*eye(size(A)) - A) * B );
	
	t = sym('t', 'real');
	sol.y = quantity.Symbolic( t.^3 .* (1 - t).^3, ...
		quantity.Domain("t", linspace(0, 1, 1e2)));
	
	sol.u = detsIA.applyTo(sol.y);
	sol.x = adjB.applyTo(sol.y);
	
	[sim.y, sim.t, sim.x] = lsim(sys, sol.u.on(), sol.y.domain.grid, 'foh');
		
	% deviation of final values:
	dev.y = sim.y(end);
	dev.x = sim.x(end, :);
	
end

function testCTranspose(testCase)
	At = testCase.TestData.A';
	testCase.verifyTrue(At(3).coefficient == testCase.TestData.A(3).coefficient');	
end
% 
function testFundamentalMatrixSpaceDependent(testCase)
%
	a = 20;
	z = sym('z', 'real');
	Z = quantity.Domain("z", linspace(0, 1, 5));
	A = signals.PolynomialOperator(...
		{quantity.Symbolic([1, z; 0, a], Z)});
	
	F = A.stateTransitionMatrix();
		
	% compute the exact solution from the peano-baker series paper.
	if numeric.near(a, 1)
		f = 0.5 * z^2 * exp(z);
	else
		f = (exp(z) - exp(a * z) - (1 - a) * z * exp( a * z ) ) / (1 - a)^2; 
	end   

	PhiExact = quantity.Symbolic([exp(z), f; 0, exp(a*z)], Z,  'name', 'Phi_A');
		
	testCase.verifyEqual(double(F), PhiExact.on(), 'RelTol', 1e-6);
	
end
% 
function testStateTransitionMatrix(testCase)
N = 2;
z = quantity.Domain("z", linspace(0,1,11));
A0 = quantity.Symbolic([0 1; 1 0], z);
A1 = quantity.Symbolic([0 1; 0 0], z);
A = signals.PolynomialOperator({A0, A1});
B = signals.PolynomialOperator(quantity.Symbolic([-1 -1; 0 0], z));

[Phi1, F0] = A.stateTransitionMatrix("N", N);
Psi1 = A.inputTransitionMatrix(B, F0, "N", N);
[Phi2, Psi2] = A.stateTransitionMatrixByOdeSystem("N", N, "B", B);

testCase.verifyEqual(double(Phi1), double(Phi2), 'AbsTol', 1e-2);
testCase.verifyEqual(double(Psi1), double(Psi2), 'AbsTol', 1e-2);
end

function testChangeDomain(testCase)
	A = testCase.TestData.A;
	a = testCase.TestData.a;
	A0 = A.subs("z", 0);
	
	for k = 1:3
		testCase.verifyEqual( ...
			a{k}.at(0), squeeze(A0(k).coefficient.on()), 'AbsTol', 10*eps);
	end
end % testChangeDomain

function testMTimes(testCase)

	zGrid = linspace(0, pi, 3)';
	z = quantity.Domain("z", zGrid);
	A0 = quantity.Discrete(reshape((repmat([1 2; 3 4], length(zGrid), 1)), length(zGrid), 2, 2),...
		z, 'name', 'A');
	A1 = quantity.Discrete(reshape((repmat([5 6; 7 8], length(zGrid), 1)), length(zGrid), 2, 2), ...
		z, 'name', 'A');
	A2 = quantity.Discrete(reshape((repmat([9 10; 11 12], length(zGrid), 1)), length(zGrid), 2, 2), ...
		z, 'name', 'A');
	
	A = signals.PolynomialOperator({A0, A1, A2});
	B = signals.PolynomialOperator({A0});
	
	C = A*B;
	
	s = sym('s');
	
	As = A0.at(0) + A1.at(0)*s + A2.at(0)*s^2;
	Bs = A0.at(0);
	
	Cs = polyMatrix.polynomial2coefficients(As*Bs, s);
	
	for k = 1:3
		testCase.verifyTrue( numeric.near(Cs(:,:,k), C(k).coefficient.at(0)) );
	end
	
	
	K = [0 1; 1 0];
	
	C =	K*A;
	
	for k = 1:3
		numeric.near(double(C(k).coefficient), double(K * A(k).coefficient));
	end
	
end

function testSum(testCase)
	z = quantity.Domain("z", linspace(0, pi, 5));
	A0 = quantity.Discrete(reshape((repmat([1 2; 3 4], z.n, 1)), z.n, 2, 2),...
		z, 'name', 'A');
	A1 = quantity.Discrete(reshape((repmat([5 6; 7 8], z.n, 1)), z.n, 2, 2), ...
		z, 'name', 'A');
	A2 = quantity.Discrete(reshape((repmat([9 10; 11 12], z.n, 1)), z.n, 2, 2), ...
	z, 'name', 'A');
	
	A = signals.PolynomialOperator({A0, A1, A2});
	B = signals.PolynomialOperator({A0});
	
	C = A + B;
		
	testCase.verifyTrue( numeric.near(double(C(1).coefficient), double(A0 + A0)) );
	testCase.verifyTrue( numeric.near(double(C(2).coefficient), double(A1)) );
	testCase.verifyTrue( numeric.near(double(C(3).coefficient), double(A2)) );
	
	C = B + A;
	
	testCase.verifyTrue( numeric.near(double(C(1).coefficient), double(A0 + A0)) );
	testCase.verifyTrue( numeric.near(double(C(2).coefficient), double(A1)) );
	testCase.verifyTrue( numeric.near(double(C(3).coefficient), double(A2)) );
	
end
	
function testDet(testCase)
% test the det of OP via the characteristic polynomial of a matrix A
%	det(sI - A) = det(OP) = det( -A + sI )
A = [0 -1; 1 0];
OP = signals.PolynomialOperator( {-A, eye(2)} );
d1 = det(OP);
testCase.verifyEqual( charpoly([0 -1; 1 0]), double( [d1.coefficient] ) );
end

function testAdj(testCase)
% adj(sI - A) / det(sI - A) = (sI - A)^-1
A = [0 -1; 1 0];
OP = signals.PolynomialOperator( {-A, eye(2) } );

invA = sym( adj(OP) ) / sym( det(OP) );

invSymA = inv( OP(1).s * eye(2) - A );

testCase.verifyEqual( invA, invSymA )

end

function testInitialization(testCase)

A = signals.PolynomialOperator( {[0 -1; 1 0], [0  1; 0 0]} );
verifyClass(testCase, A, 'signals.PolynomialOperator');

end

function testUminus(testCase)
%%
syms z s
Z = quantity.Domain('z', linspace(0,1,11));
A0 = quantity.Symbolic( [z, z.^2; z.^3, z.^4], Z);
A1 = quantity.Symbolic( [sin(z), cos(z); tan(z), sinh(z)], Z);
o = signals.PolynomialOperator({A0, A1});
mo = -o;

%%
verifyEqual(testCase, o(1).coefficient.on , -mo(1).coefficient.on);

end