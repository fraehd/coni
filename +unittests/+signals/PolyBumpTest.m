function [tests] = PolyBumpTest()
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
tests = functiontests(localfunctions);
end

function NormTest(testCase)

t = quantity.EquidistantDomain('t', sqrt(2), 2 * sqrt(2), 'stepSize', 0.1);
p = signals.PolyBump(t, 'a', 3, 'b', 7, 'norm', true);
testCase.verifyEqual( int(p, p.T0, p.T1), 1, 'AbsTol', 3e-16)

end

function OnTest(testCase)

t = quantity.EquidistantDomain('t', -1, 2);
p = signals.PolyBump(t, 'T0', 0, 'T1', 1);
val = p.on;

testCase.verifyTrue( all(val(t.grid < 0) == 0) )
testCase.verifyTrue( all(val(t.grid > 1) == 0) )
testCase.verifyTrue( all(val(t.grid > 0 & t.grid < 1) ~= 0) )

end