function [tests] = signalsTest()
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
tests = functiontests(localfunctions);
end

function taylorPolynomialTest(testCase)

t = linspace(0, 12, 11)';
c = [1, 2]';
f = signals.faultmodels.TaylorPolynomial('coefficients', c, ...
	'T', 1, 'globalGrid', t);

testCase.verifyEqual(c(1) + c(2) * t, f.on())
end

function testSmoothStep(testCase)
% computed step by hand:
z = sym('z', 'real');
d = 1;
sig = 3 / d^2 * z^2 - 2 / d^3 * z^3;
Z = quantity.Domain('z', linspace(0, 1, 101));

SI = quantity.Symbolic(sig, Z);

% compute step by function
si = quantity.Symbolic( misc.ss.setPointChange(0, 1, 0, 1, 1, "var", Z.name), Z );

%
testCase.verifyEqual(si.on(), SI.on(), 'AbsTol', 6e-16);

end
































