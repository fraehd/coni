function [tests ] = testPiecewise()
%TESTQUANTITY Unittests for the quantity objects
%   Add all unittests for the quantity files here
tests = functiontests(localfunctions());
end

function testInit(testCase)

t1 = quantity.Domain('t', 1:3);
t2 = quantity.Domain('t', 3:5);
t3 = quantity.Domain('t', 5:7);

f1 = quantity.Discrete(t1.grid, t1);
f2 = quantity.Discrete(flip(t1.grid), t2);
f3 = quantity.Discrete(t1.grid, t3);

F = quantity.Piecewise({ f1 f2 f3});

testCase.verifyEqual(F.valueDiscrete, [1 2 3 2 1 2 3]')

end

function testAssemblingPoint(testCase)

t1 = quantity.Domain('t', 1:3);
t2 = quantity.Domain('t', 3:5);
t3 = quantity.Domain('t', 5:7);

f1 = quantity.Discrete(1 * ones(t1.n,1), t1);
f2 = quantity.Discrete(2 * ones(t2.n,1), t2);
f3 = quantity.Discrete(3 * ones(t3.n,1), t3);

F = quantity.Piecewise({f1 f2 f3}, 'upperBoundaryIncluded', [true, false]);

testCase.verifyEqual(F.at(t1.upper), 1);
testCase.verifyEqual(F.at(t2.upper), 3);

end