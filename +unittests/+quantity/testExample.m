function [tests] = testExample()
tests = functiontests(localfunctions);
end % testExample()

function testNoFailure(tc)
storeCurrentFigures = findobj(0, 'type', 'figure');
set(groot,'defaultFigureVisible','off')
failureHappened = false;
try
	evalc("quantity.example");
catch
	failureHappened = true;
end
set(groot,'defaultFigureVisible','on') 
delete(setdiff(findobj(0, 'type', 'figure'), storeCurrentFigures));
tc.verifyFalse(failureHappened);
end % testNoFailure()