function [tests] = testDomain()
	tests = functiontests(localfunctions);
end

function testReplace(tc)
z = linspace(0, pi, 3);
a = quantity.Domain("a", z);
b = quantity.Domain("b", z);
c = quantity.Domain("c", z);
d = [a b c];

d = d.replace(quantity.Domain("b", linspace(0, 1, 2)));
tc.verifyEqual([d.name], ["a"; "b"; "c"].');
tc.verifyEqual(d(1).grid, linspace(0, pi, 3).');
tc.verifyEqual(d(3).grid, linspace(0, pi, 3).');
tc.verifyEqual(d(2).grid, linspace(0, 1, 2).');

d = d.replace(quantity.Domain("e", linspace(2, 3, 4)));
tc.verifyEqual([d.name], ["a"; "b"; "c"].');
tc.verifyEqual(d(1).grid, linspace(0, pi, 3).');
tc.verifyEqual(d(3).grid, linspace(0, pi, 3).');
tc.verifyEqual(d(2).grid, linspace(0, 1, 2).');
end % testReplace

function setupOnce(tc)

z = linspace(0, pi, 3);
a = quantity.Domain('a', z);
b = quantity.Domain('b', z);
c = quantity.Domain('c', z);
d = [a b c];

tc.TestData.z = z;
tc.TestData.a = a;
tc.TestData.b = b;
tc.TestData.c = c;
tc.TestData.d = d;

end

function testDiscrete(tc)
d = quantity.Domain("a", linspace(0, 1, 3));
tc.verifyEqual(d.Discrete.on(), d.grid);
end % testDiscrete

function testSymbolic(tc)
d = quantity.Domain("a", linspace(0, 1, 3));
tc.verifyEqual(d.Symbolic.on(), d.grid);
end % testDiscrete

function testRename(tc)
d = [quantity.Domain("a", linspace(0, 1, 3)), ...
	quantity.Domain("b", linspace(0, 2, 5)), ...
	quantity.Domain("ab", linspace(0, pi, 4))];
xyz = d.rename(["x", "y", "z"]);
xyz2 = d.rename(["z", "y", "x"], ["ab", "b", "a"]);
tc.verifyEqual([xyz.name], ["x", "y", "z"]);
tc.verifyEqual([xyz.name], [xyz2.name]);

cba = d.rename(["c", "b", "a"], ["a", "b", "ab"]);
tc.verifyEqual([cba.name], ["c", "b", "a"]);
tc.verifyEqual([cba.n], [d.n]);

zZetaDomain = [quantity.Domain("z", linspace(0, 1, 3)), quantity.Domain("zeta", linspace(0, 1, 3))];
tc.verifyEqual([zZetaDomain.rename("a", "z").name], ["a", "zeta"])
end % testRename()

function testParser(tc)%profile on
d = quantity.Domain.parser('blub', 1, 'grid', 1:3, 'gridName', 'test');
a = quantity.Domain.parser('domain', d);
b = quantity.Domain.parser('blabla', 0, 'domain', a);
b = quantity.Domain.parser('blabla', 0, 'domain', a, 'blub', 'a');
%profile off
%profile viewer


tc.verifyEqual(d, a);
tc.verifyEqual(b, a);

end

function testSplit(tc)

a = quantity.Domain('a', -5:15 );

b = a.split([-1, 0, 4]);

tc.verifyEqual(b(1).grid', -5:-1)
tc.verifyEqual(b(2).grid', -1:0)
tc.verifyEqual(b(3).grid', 0:4)
tc.verifyEqual(b(4).grid', 4:15)

tc.verifyWarning(@() a.split([0.1, 2.6]), 'DOMAIN:split')
tc.verifyWarningFree(@() a.split([0.1, 2.6], 'AbsTol', 0.4), 'DOMAIN:split')

c = a.split([0.1, 2.6], 'warning', false);

tc.verifyEqual(c(1).grid', -5:0)
tc.verifyEqual(c(2).grid', 0:3)
tc.verifyEqual(c(3).grid', 3:15)

end

function testContains(tc)

a = quantity.Domain('a', 0:10);
b = quantity.Domain('b', 3:5);

tc.verifyTrue( a.contains(b) )
tc.verifyTrue( a.contains(1) )
tc.verifyFalse( a.contains(11) )
tc.verifyFalse( a.contains(1.5) )

A = [quantity.Domain('a1', 0:10), quantity.Domain('a2', 0:10)];

tc.verifyTrue( A.contains(5) )

end

function testIsequidistant(tc)
tc.verifyTrue(tc.TestData.a.isequidistant);
tc.verifyTrue(all(isequidistant(tc.TestData.d)));
tc.verifyFalse(isequidistant(quantity.Domain("z", [0, 0.2, 0.4, 0.8, 1.6])));
tc.verifyFalse(all(isequidistant(...
	[tc.TestData.a, quantity.Domain("z", [0, 0.2, 0.4, 0.8, 1.6]), tc.TestData.b])));
end % testIsequidistant

function testFind(tc)

z = tc.TestData.z;
a = tc.TestData.a;
b = tc.TestData.b;
c = tc.TestData.c;
d = tc.TestData.d;

tc.verifyEqual( d.find('a'), a)
tc.verifyEqual( d.find('b', 'c'), [b c])
tc.verifyEqual( d.find({'b', 'c'}), [b c])
tc.verifyEqual( d.find(b.name), b);
tc.verifyEqual( d.find({a.name c.name}), [a c])
tc.verifyEqual( d.find([a.name b.name]), [a b])

end

function testRemove(tc)
d = tc.TestData.d;
dWithoutA = d.remove("a");
tc.verifyEqual([dWithoutA.name], ["b" "c"]);

a = d.remove(dWithoutA);
tc.verifyEqual(a.name, "a");
end

function testGet(tc)

z = linspace(0, pi, 3);
a = quantity.Domain('a', z);
b = quantity.Domain('b', z);
c = quantity.Domain('c', z);

d = [a b c];

tc.verifyEqual(d.find('a'), a);
tc.verifyEqual(d.find(b.name), b);
tc.verifyEqual(d.find('c'), c);

end

function testEquality(tc)

d = quantity.Domain('d', 0);
e = quantity.Domain.empty();

dd = d;
ee = e;

tc.verifyNotEqual( d, e );
tc.verifyEqual( dd, d );
tc.verifyEqual( ee, e );
tc.verifyNotEqual( e, d );

d1 = quantity.Domain('d', 1:3);
d2 = quantity.Domain('d', 1:3);
d3 = quantity.Domain('d', 2:4);
tc.verifyEqual(d1, d2);
tc.verifyNotEqual(d1, d3);

end

function testDomainInit(tc)

Z = linspace(0,pi, 3);
d = quantity.Domain('z', Z);
D = [d d];

tc.verifyEqual( ndims(D), 2);
tc.verifyEqual( ndims(d), 1);
end

function testSort(tc)

z = linspace(0, pi, 3);
a = quantity.Domain('a', z);
b = quantity.Domain('b', z);
c = quantity.Domain('c', z);

d = [a b c];

[d_, I_] = d.sort('descend');
[d__, I__] = d.sort('ascend');
d___ = d.sort();

tc.verifyEqual([d_.name], ["c", "b", "a"]);
tc.verifyEqual({d__.name}, {d.name});
tc.verifyEqual({d__.name}, {d___.name});
tc.verifyEqual( I_, flip(I__) );


end

function testDomainGridLength(tc)

Z = linspace(0,pi, 3);
d = quantity.Domain('z', Z);
D = [d d];

tc.verifyEqual( cellfun(@(v) numel(v), {Z, Z}), D.gridLength)

end

function testDomainNumGridElements(tc)

Z = linspace(0,pi, 3);
d = quantity.Domain('z', Z);
D = [d d];

tc.verifyEqual( D.numGridElements, prod([length(Z), length(Z)]));

end

function testEmpty(tc)

d = quantity.Domain();
e = quantity.Domain.empty();
o = quantity.Domain('', 1);

tc.verifyTrue( isempty(d) )
tc.verifyTrue( isempty(e) )
tc.verifyTrue( isempty([d, e]))
tc.verifyTrue( isempty([d, d]))
tc.verifyFalse( isempty(o) )
tc.verifyFalse( isempty([o e]) )
tc.verifyTrue( isempty([o d]) )

end


function testDefaultGrid(testCase)
	g = quantity.Domain.defaultDomain([100, 42]);
	testCase.verifyEqual(g(1).grid, linspace(0, 1, 100).');
	testCase.verifyEqual(g(2).grid, linspace(0, 1, 42).');
    
    g = quantity.Domain.defaultDomain();
    testCase.verifyEqual(g.grid, linspace(0, 1).');
    testCase.verifyEqual(g.name, "x_1")
    
end

function testJoin(tc)
s = linspace(0, 1);
z = linspace(0, 1, 71)';
t = linspace(0, 1, 51);

S = quantity.Domain('s', s);
Z = quantity.Domain('z', z);
T = quantity.Domain('t', t);
Ps = quantity.Domain('p', s);
St = quantity.Domain('s', t);
Pt = quantity.Domain('p', t);

a = [Z, T, S];
b = [Ps, St];
c = Pt;

joinedDomainAB = sort( join(a, b) );
joinedDomainCC = sort( join(c, c) );
joinedDomainBC = sort( join(b, c) );
joinedDomain = sort( join(Z, [Z, T]) );

tc.verifyEqual([joinedDomainAB.name], ["p", "s", "t", "z"]);
tc.verifyEqual({joinedDomainAB.grid}, {s(:), s(:), t(:), z(:)});
tc.verifyEqual(joinedDomainCC.name, "p");
tc.verifyEqual({joinedDomainCC.grid}, {t(:)});
tc.verifyEqual([joinedDomainBC.name], ["p", "s"]);
tc.verifyEqual({joinedDomainBC.grid}, {s(:), t(:)});
tc.verifyEqual({joinedDomain.grid}, {t(:), z(:)});

d1 = [Z Z T];
d2 = quantity.Domain.empty;

tc.verifyEqual( d1.join( d2 ), [Z T] )

end % join()

function testFind_Index(tc)
z = linspace(0, 2*pi, 71)';
t = linspace(0, 3*pi, 51);
s = linspace(0, 1);

S = quantity.Domain('s', s);
Z = quantity.Domain('z', z);
T = quantity.Domain('t', t);

a = [Z, T, S];

idx = a.index('z');

tc.verifyEqual(idx, 1);


idx = a.index({'z', 't'});
tc.verifyEqual(idx, [1 2]);

idx = a.index({'z', 's'});
tc.verifyEqual(idx, [1 3]);

idx = a.index('t');
tc.verifyEqual(idx, 2);

end





















