function [tests] = testFunction()
%TESTQUANTITYFUNCTIONHANDLE Summary of this function goes here
%   Detailed explanation goes here
tests = functiontests(localfunctions);
end

function testCatBug(tc)

z = quantity.Domain("z", linspace(0, 1, 7));
zeta = quantity.Domain("zeta", linspace(0, 1, 5));
A = quantity.Function(@(z)sin(z * pi), z);

x = A * subs(A, "z", "zeta");
C1 = [0*z.Function + zeta.Function; x];
C2 = [zeta.Function + 0*z.Function; x];
tc.verifyEqual(C1.on([z, zeta]), C2.on([z, zeta]));

end

function testChangeDomain(tc)

z = quantity.Domain("z", 0:5);
t = quantity.Domain("t", 0:4);

a = quantity.Function(@(z,t) z + t*0, [z, t]);
b = a.sortDomain();

tc.verifyEqual( a.on([z,t]), b.on([z,t]));

z2 = quantity.Domain("z", 2:7);
c = a.changeDomain( z2 );

grid = ndgrid([z2, t]);
f = c.valueContinuous;
tc.verifyEqual(c.on(), f(grid{1}, grid{2}))

end


function testAt(tc)
z = quantity.Domain("z", linspace(0, 1, 11));
f = quantity.Function({@(z)sin(z), @(z)1+z; @(z)cos(z), @(z)z.^2}, z);
fZZeta = quantity.Function({@(z,zeta)sin(zeta)+z, @(z,zeta)1+zeta-z; @(z,zeta)cos(z), @(z,zeta)zeta.^2}, ...
	[z, z.rename("zeta")]);
fDisc = quantity.Discrete(f);
tc.verifyLessThan(abs(f.at({0.2}) - fDisc.at({0.2})), 10*eps);
fZZetaDisc = quantity.Discrete(fZZeta);
tc.verifyLessThan(abs(fZZeta.at({0.2, 0.4}) - fZZetaDisc.at({0.2, 0.4})), 10*eps);
end % testAt()

function testInt(testCase)

z = quantity.Domain("z", linspace(0,1));

f = quantity.Function( @(z) sin( sin( z ) ), z);

F_fn = int(f, z, 0, "z");
F_dk = int(quantity.Discrete(f), z, 0, "z");

testCase.verifyEqual( F_fn.on, F_dk.on, "AbsTol", 6e-6)

f2 = quantity.Function({@(z)sin(z); @(z)cos(z)}, z); 

F2_fn = int(f2, z, "zeta", "z");
F2_dk = int( quantity.Discrete(f2), z, "zeta", "z");

testCase.verifyEqual( F2_fn.on, F2_dk.on, "AbsTol", 7.5e-6);

end

function testCastFunction2Discrete(testCase)

z = quantity.Domain("z", linspace(0, 2*pi)');

f = quantity.Function({@(z)sin(z); @(z)cos(z)}, z);

d = quantity.Discrete(f);

testCase.verifyTrue(all(size(d) == size(f)));

end


function testTimesSymbolic(testCase)

z = quantity.Domain("z", linspace(0, 2*pi)');
f = quantity.Function({@(z) sin(z), @(z) cos(z)}, z);
s = quantity.Symbolic([sym("z"); 1], z);

fs = f*s;
sf = s' * f';

testCase.verifyTrue( fs.near(sf) );

end

function testOn(testCase)
z = quantity.Domain("z", linspace(0, 2*pi)');
f = quantity.Function({@(z) sin(z), @(z) cos(z)}, z);

F1 = f.on(z);
F2 = f.on();

testCase.verifyTrue( numeric.near(F1, F2) );

end

function testUMinus(testCase)
z = quantity.Domain("z", linspace(0,1).');
f = quantity.Function(@(z) z, z);
mf = -f;
verifyTrue(testCase, all( mf.on() + f.on() == 0));

%%
f1 = @(z) sinh(z * pi);
f2 = @(z) cosh(z * pi);
f3 = @(z) sin(z * pi);

f = quantity.Function({f1, f3 ; f2, f2; f1, f3 }, z, "name", "sinhcosh");
F = -f;
%
z = sym("z", "real");
fsym = sym({f1, f3 ; f2, f2; f1, f3});
fsym = symfun(- fsym, z);

z = f(1).domain.grid;

Fsym = fsym(z);
%%
for k = 1:numel(F)
    verifyTrue(testCase, numeric.near(double(Fsym{k}), F(k).valueDiscrete, 1e-12));    
end

end

function testInner(testCase)

f1 = @(z) sinh(z * pi);
f2 = @(z) cosh(z * pi);

f = quantity.Function({f1 ; f2 }, quantity.Domain("z", linspace(0,1,1e4).'));

F = f.inner(f.');

verifyTrue(testCase, numeric.near( integral(@(z) f1(z).^2, 0, 1), F(1,1), 1e-6));
verifyTrue(testCase, numeric.near( integral(@(z) f2(z).^2, 0, 1 ), F(2, 2), 1e-6));
verifyTrue(testCase, numeric.near( integral(@(z) f1(z) .* f2(z), 0, 1 ) , F(1, 2), 1e-6));
end

function testMTimes(testCase)
%%
f1 = @(z) sinh(z * pi);
f2 = @(z) cosh(z * pi);
f3 = @(z) sin(z * pi);

z = sym("z", "real");
fsym = sym({f1, f3 ; f2, f2; f1, f3});
fsym = symfun(fsym * fsym.', z);

f = quantity.Function({f1, f3 ; f2, f2; f1, f3 }, quantity.Domain.defaultDomain(10, "z"),...
	"name", "sinhcosh");

F = f * f.';

z = f(1).domain.grid;

Fsym = fsym(z);
%%
for k = 1:numel(F)
    verifyTrue(testCase, numeric.near(double(Fsym{k}), F(k).valueDiscrete, 1e-12));    
end

end

function testInit(testCase)
fun = @(z) z.^2;
q = quantity.Function(fun, quantity.Domain.defaultDomain(19, "z"), "name", "test");
verifyEqual(testCase, q.valueContinuous, fun);
verifyEqual(testCase, q.name, "test");

z = quantity.Domain("z", 0:5);
t = quantity.Domain("t", 0:4);

a = quantity.Function(@(z,t) z + t, [z, t]);

b = quantity.Function(@(t,z) z + t, [z, t]);

testCase.verifyError(@() quantity.Function(@(z,t,x) z + t, [z,t]), "quantity:Function:domain")
testCase.verifyError(@() quantity.Function({ @(z,t) z + t, @(z) z, @(z,t,x) z + t + x}, [z,t]), "quantity:Function:domain")

end

function testDimensions(testCase)
%%
f = quantity.Function(...
    {@(z) (sin(z .* pi) ), @(z) z; @(z) 0.*z, @(z)(cos(z .* pi) )}, ...
    quantity.Domain("z", linspace(0, 1, 42)'));

%%
verifyEqual(testCase, f(1).domain.grid, linspace(0,1, 42).');
verifyEqual(testCase, f(1).domain.gridLength, 42);

end

function testEvaluate(testCase)
%%
f1 = @(z) (sin(z .* pi) );
f2 = @(z) z;
f3 = @(z) (cos(z .* pi) );
z = quantity.Domain("z", linspace(0, 1, 42).');
f = quantity.Function({f1, f2; @(z) z.^2, f3}, z);

value = f.on();

%%
verifyTrue(testCase, all(squeeze(value(:,1,1)) == f1(z.grid)));
verifyTrue(testCase, all(squeeze(value(:,1,2)) == f2(z.grid)));
verifyTrue(testCase, all(squeeze(value(:,2,1)) == z.grid.^2));
verifyTrue(testCase, all(squeeze(value(:,2,2)) == f3(z.grid)));


end

function testSize(testCase)
%%
f = quantity.Function(...
    {@(z) (sin(z .* pi) ),   @(z) z,             @(z) 0.*z; ...
     @(z) 0.*z,              @(z)(cos(z .* pi)), @(z) 0.*z}, ...
    quantity.Domain("z", linspace(0, 1, 42).'));

%%
verifyEqual(testCase, size(f), [2, 3]);
verifyEqual(testCase, size(f, 2), 3);

end
