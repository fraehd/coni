function [tests] = testDiscrete()
%testQuantity Summary of this function goes here
%   Detailed explanation goes here
tests = functiontests(localfunctions);
end

function testCompose2(tc)
tf = quantity.Domain("t", linspace(0, 1, 15));
zf = quantity.Domain("z", linspace(0, 1, 21));
zg = quantity.Domain("z", linspace(0, 1, 11));
f = quantity.Discrete(ndgrid(zf.grid, tf.grid), [zf, tf], "name", "f");
g = quantity.Discrete(ndgrid(zg.grid), zg, "name", "g");

fOfG = f.compose(g, "domain", tf);
tc.verifyEqual(fOfG.on(), linspace(0, 1, 21).');
end % testCompose2

function testKron(tc)
z = quantity.Domain("z", linspace(0, 1, 5));
a = quantity.Discrete.ones([2, 2], z, "name", "a");
tc.verifyEqual(at(kron(ones(2, 3), a), 0.2), ones(4, 6))
tc.verifyEqual(at(kron(2, a), 0.2), 2*a.at(0.2))
tc.verifyEqual(at(kron(a, a), 0.2), ones(4, 4))
tc.verifyEqual(at(kron(a(:, 1), a), 0.2), ones(4, 2))
tc.verifyEqual(at(kron(eye(3, 3), a), 0.2), at(blkdiag(a, a, a), 0.2))
end % testKron()

function testLdivide(tc)
z = quantity.Domain("z", linspace(0, 1, 5));
quan = quantity.Discrete.ones([2, 2], z) * diag([2, 3]);
testMatrix = [1, 2; 3, 4];
tc.verifyEqual(quan.at(0.5) .\ testMatrix, at(quan .\ testMatrix, 0.5))
tc.verifyEqual(testMatrix .\ quan.at(0.5), at(testMatrix .\ quan, 0.5))

tc.verifyEqual(quan.at(0.5) .\ testMatrix(1), at(quan .\ testMatrix(1), 0.5))
tc.verifyEqual(testMatrix(1) .\ quan.at(0.5), at(testMatrix(1) .\ quan, 0.5))

tc.verifyEqual(quan(1).at(0.5) .\ testMatrix, at(quan(1) .\ testMatrix, 0.5))
tc.verifyEqual(testMatrix .\ quan(1).at(0.5), at(testMatrix .\ quan(1), 0.5))

tc.verifyEqual(at(quan .\ quan, 0.5), ones([2, 2]));
end

function testRdivide(tc)
z = quantity.Domain("z", linspace(0, 1, 5));
quan = quantity.Discrete.ones([2, 2], z) * diag([2, 3]);
testMatrix = [1, 2; 3, 4];
tc.verifyEqual(quan.at(0.5) ./ testMatrix, at(quan ./ testMatrix, 0.5))
tc.verifyEqual(testMatrix ./ quan.at(0.5), at(testMatrix ./ quan, 0.5))

tc.verifyEqual(quan.at(0.5) ./ testMatrix(1), at(quan ./ testMatrix(1), 0.5))
tc.verifyEqual(testMatrix(1) ./ quan.at(0.5), at(testMatrix(1) ./ quan, 0.5))

tc.verifyEqual(quan(1).at(0.5) ./ testMatrix, at(quan(1) ./ testMatrix, 0.5))
tc.verifyEqual(testMatrix ./ quan(1).at(0.5), at(testMatrix ./ quan(1), 0.5))

tc.verifyEqual(at(quan ./ quan, 0.5), ones([2, 2]));
end

function testIseye(tc)
z = quantity.Domain("z", linspace(0, 1, 11));
zeta = quantity.Domain("zeta", linspace(0, 1, 11));
I = quantity.Discrete.eye([2, 3], [z, zeta]);
tc.verifyFalse(I.iseye());
I = quantity.Discrete.eye([3, 3], [z, zeta]);
tc.verifyTrue(I.iseye());
I = quantity.Symbolic.eye([3, 3], [z, zeta]);
tc.verifyTrue(I.iseye());
I = quantity.Discrete(1+z.grid/100, z);
tc.verifyFalse(I.iseye());
I = quantity.Discrete(1+0*z.grid, z);
tc.verifyTrue(I.iseye());
end % testIseye()

function testSubsDomain(tc)
z = quantity.Domain("z", linspace(0, 1, 11));
zeta = quantity.Domain("zeta", linspace(0, 1, 11));
zLr = quantity.Domain("z", linspace(0, 1, 5));
eta = quantity.Domain("eta", linspace(0, 1, 11));
f = z.Discrete() * zeta.Discrete();
%
fofZEta = f.subsDomain("zeta", eta);	% = f(z, eta)
tc.verifyEqual(fofZEta.on(), f.on());
tc.verifyEqual([fofZEta(1).domain.name], ["z", "eta"]);
%
fofZlrZeta = f.subsDomain("z", zLr);	% = f(z, zeta)
tc.verifyEqual(fofZlrZeta.on(), f.on([zLr, zeta]));
tc.verifyEqual([fofZlrZeta(1).domain.name], ["z", "zeta"]);

%
fofZ = f.subsDomain("zeta", zLr);		% = f(z) 
tc.verifyEqual(fofZ.on(), diag(f.on([z, quantity.Domain("zeta", z.grid)])));
tc.verifyEqual([fofZ(1).domain.name], "z");
end % testSubsDomain()

function testSubsNumeric(tc)
z = quantity.Domain("z", linspace(0, 1, 5));
zeta = quantity.Domain("zeta", linspace(0, 1, 4));
t = quantity.Domain("t", linspace(0, 1, 6));

quanScalar = z.Discrete() + 1 + zeta.Discrete() * t.Discrete();
tc.verifyEqual(quanScalar.subsNumeric(["z", "t"], [0, 0]).on(), ones(zeta.n, 1));
tc.verifyEqual(quanScalar.subsNumeric(["t", "z", "zeta"], [0.1, 0.2, 0.3]), 0.2 + 1 + 0.3 * 0.1);

quanMatrix = quantity.Discrete(quantity.Symbolic(...
	[sym("z") + 1 + sym("zeta") * sym("t"), 1; ...
	sym("z") + sym("t"), -sym("zeta")], [zeta, t, z]));%[quanScalar, 1; z.Discrete()*z.Discrete() + t.Discrete, -zeta.Discrete];
tc.verifyEqual(quanMatrix.subsNumeric(["z", "zeta", "t"], [0.1, 0.2, 0.3]), ...
	[0.1 + 1 + 0.2 * 0.3, 1; 0.1 + 0.3, -0.2]);
tc.verifyEqual(quanMatrix.subsNumeric(["z", "zeta", "t"], [0, 0, 0]), [1, 1; 0, 0]);
tc.verifyEqual(quanMatrix.subsNumeric(["z", "zeta", "t"], [1, 1, 1]), [3, 1; 2, -1]);
tc.verifyEqual(quanMatrix(1, 1).subsNumeric(["z", "zeta", "t"], [0.1, 0.2, 0.3]), 0.1 + 1 + 0.2 * 0.3);
end % testSubsNumeric()

function testSubsParser(tc)
z = quantity.Domain("z", [0, 1, 2]);
zeta = quantity.Domain("zeta", [0, 1, 2]);
[result1, subsDomainName1] = quantity.Discrete.subsParser(["z", "zeta", "eta", "z"], {["t1", "t2"], 1, linspace(0, 1, 11)});
tc.verifyEqual(result1, {"t1", "t2", 1, quantity.Domain("zbackUp", linspace(0, 1, 11)), "z"});
tc.verifyEqual(subsDomainName1, ["z", "zeta", "eta", "z", "zbackUp"]);

[result2, subsDomainName2] = quantity.Discrete.subsParser(["z", "zeta", "eta", "z"], {["t1"; "t2"], [z; zeta]});
tc.verifyEqual(result2, {"t1", "t2", z.rename("etabackUp"), zeta, "z"});
tc.verifyEqual(subsDomainName2, ["z", "zeta", "eta", "z", "etabackUp"]);

tc.verifyError(@() quantity.Discrete.subsParser(["z", "zeta"], {1}), "");
tc.verifyError(@() quantity.Discrete.subsParser(["z", "zeta"], {1, ones(2)}), "");

end % testSubsParser()

function testSortAndIssorted(tc)
z = quantity.Domain("z", linspace(0, 1, 4));
quanSymbolic = quantity.Symbolic([-sym("z")-0.5, 2 + sym("z"), 3.2, -2, 0, 3.5], z);
quanDisc = quantity.Discrete(quanSymbolic);
[quanSorted, I] = quanDisc.sort();
[quan0Sorted, I0] = sort(quanSymbolic.at(0));
tc.verifyEqual(I, I0);
tc.verifyEqual(quanSorted.at(0), quan0Sorted);
tc.verifyEqual(quanSorted.on(), quanSymbolic.sort.on());
tc.verifyEqual(quanDisc(I).on(), quanSorted.on());
tc.verifyEqual(quanSymbolic(I).on(), quanSorted.on());
tc.verifyEqual(quanDisc.sort('ascend').on(), flip(quanDisc.sort('descend').on(), 3))
tc.verifyTrue(quanSorted.issorted('ascend'));
tc.verifyTrue(quanSorted.issorted('strictascend'));
tc.verifyTrue(quanDisc.sort('descend').issorted('descend'));
tc.verifyTrue(quanSorted.issorted('monotonic'));

% 2 domains
t = quantity.Domain("t", linspace(-1, 2, 5));
quan3 = diag([3, 1]) * quantity.Discrete.ones([2, 1], [z, t]) + z.Discrete()*[1; 0] - t.Discrete()*[0; 1];
tc.verifyEqual(quan3.sort.on(), quan3([2; 1]).on());
tc.verifyEqual(quan3.sort("descend").on(), quan3.on());

% values must be distinct pointwise
quan2 = quantity.Symbolic([1-sym("z"), 0.5], z);
tc.verifyError(@() quan2.sort(), "");
quan4 = quantity.Symbolic([1-sym("z"), 1], z);
tc.verifyTrue(quan4.sort('descend').issorted('descend'));
tc.verifyFalse(quan4.sort('descend').issorted('strictdescend'));
end % testSort()

function testEye(tc)
myDomain = [quantity.Domain("z", linspace(0, 1, 11)), quantity.Domain("zeta", linspace(0, 1, 5))];
tc.verifyEqual(MAX(abs(quantity.Discrete.eye(1, myDomain) - 1)), 0);
tc.verifyEqual(MAX(abs(quantity.Discrete.eye(2, myDomain) - eye(2))), 0);
tc.verifyEqual(MAX(abs(quantity.Discrete.eye([2, 1], myDomain) - eye([2, 1]))), 0);
tc.verifyEqual(MAX(abs(quantity.Discrete.eye([2, 2], myDomain(2)) - eye([2, 2]))), 0);
tc.verifyEqual(MAX(abs(quantity.Discrete.eye([4, 4], myDomain) - eye(4))), 0);
end % testEye()

function testLuDecomposition(tc)
myDomain = [quantity.Domain("z", linspace(0, 1, 11)), quantity.Domain("zeta", linspace(0, 1, 5))];
syms z zeta
quanSymbolic = quantity.Symbolic([z*zeta, z+1, 2; z^2, zeta^2, -2; 1, 2, 3], myDomain);
quan = quantity.Discrete(quanSymbolic);
% discrete case
[l, u] = quan.luDecomposition();
tc.verifyEqual(MAX(abs(l*u - quan)), 0, "AbsTol", 1e3*eps);
tc.verifyEqual(MAX(abs(l.*triu(ones(size(l)), 1))), 0, "AbsTol", 1e3*eps);
tc.verifyEqual(MAX(abs(u.*tril(ones(size(u)), -1))), 0, "AbsTol", 1e3*eps);
% symbolic case
[l, u] = quanSymbolic.luDecomposition();
tc.verifyEqual(MAX(abs(l*u - quanSymbolic)), 0);
tc.verifyEqual(MAX(abs(l.*triu(ones(size(l)), 1))), 0);
tc.verifyEqual(MAX(abs(u.*tril(ones(size(u)), -1))), 0);
tc.verifyTrue(isa(quanSymbolic, "quantity.Symbolic"));
end % testLuDecomposition()

function testCatBug(tc)
z = quantity.Domain("z", linspace(0, 1, 7));
zeta = quantity.Domain("zeta", linspace(0, 1, 5));
A = quantity.Discrete(sin(z.grid * pi), z);

x = A * subs(A, "z", "zeta");
C1 = [0*z.Discrete + zeta.Discrete; x];
C2 = [zeta.Discrete + 0*z.Discrete; x];
tc.verifyEqual(C1.on([z, zeta]), C2.on([z, zeta]));

x = A * subs(A, "z", "zeta");
C1 = [0*z.Symbolic + zeta.Symbolic; x];
C2 = [zeta.Symbolic + 0*z.Symbolic; x];
tc.verifyEqual(C1.on([z, zeta]), C2.on([z, zeta]));
end
function testEigScalar(tc)
z = quantity.Domain("z", linspace(-1, 1, 11));
[Vz, Dz, Wz] = z.Discrete.eig();
tc.verifyEqual(z.grid, Dz.on());
tc.verifyEqual(ones(z.n, 1), Vz.on());
tc.verifyEqual(ones(z.n, 1), Wz.on());
end % testEigScalar()

function testEigMat(tc)
z = quantity.Domain("z", linspace(-1, 1, 11));
zSym = sym("z");
quan = quantity.Discrete(quantity.Symbolic([1+zSym^2, 2; -zSym, zSym], z));
% all output arguments
[Vq, Dq, Wq] = quan.eig();
tc.verifyEqual(diag(eig(quan.at(0.4))), Dq.at(0.4), "AbsTol", 1e-15);
tc.verifyEqual(on(quan * Vq), on(Vq * Dq), "AbsTol", 1e-15);
tc.verifyEqual(on(Wq' * quan), on(Dq * Wq'), "AbsTol", 1e-15);
% 2 output arguments
[Vq, Dq] = quan.eig();
tc.verifyEqual(diag(eig(quan.at(0.4))), Dq.at(0.4), "AbsTol", 1e-15);
tc.verifyEqual(on(quan * Vq), on(Vq * Dq), "AbsTol", 1e-15);
% 1 output argument
Eq = quan.eig();
tc.verifyEqual(eig(quan.at(0.4)), Eq.at(0.4), "AbsTol", 1e-15);
tc.verifyEqual(on(Eq), on(Dq.diag2vec()), "AbsTol", 1e-15);
end % testEigMat()

function testJordanRealMat(tc)
z = quantity.Domain("z", linspace(-1, 1, 11));
zSym = sym("z");
quan = quantity.Discrete(quantity.Symbolic([1+zSym^2, 2; -zSym, zSym], z));
% all output arguments
[V, J, l] = quan.jordanReal();
[V0p4, J0p4, l0p4] = misc.jordanReal(quan.at(0.4));
tc.verifyEqual(J0p4, J.at(0.4), "AbsTol", 1e-15);
tc.verifyEqual(V0p4, V.at(0.4), "AbsTol", 1e-15);
tc.verifyEqual([l0p4; 0], l.at(0.4), "AbsTol", 1e-15);
tc.verifyEqual(on(quan * V), on(V * J), "AbsTol", 1e-15);
end % testJordanRealMat()

function testPower(tc)
%% scalar case
z = quantity.Domain("z", linspace(0, 1, 11));
A = quantity.Discrete(sin(z.grid * pi), z);
aa = sin(z.grid * pi) .* sin(z.grid * pi);
AA = A.^2;

tc.verifyEqual( aa, AA.on());
tc.verifyEqual( on(A.^0), ones(z.n, 1));
tc.verifyEqual( on(A.^3), on(AA * A), "AbsTol", 1e-15);

%% matrix case
zeta = quantity.Domain("zeta", linspace(0, 1, 21));
B = [0*z.Discrete + zeta.Discrete, A + 0*zeta.Discrete; ...
	z.Discrete+zeta.Discrete, A * subs(A, "z", "zeta")];
BBB = B.^3;
BBB_alt = B .* B .* B;
tc.verifyEqual(BBB.on(), BBB_alt.on(), "AbsTol", 1e-14);
tc.verifyEqual(BBB.at({0.5, 0.6}), (B.at({0.5, 0.6})).^3, "AbsTol", 1e-15);
tc.verifyEqual(on(B.^0), ones([B(1).domain(1).n, B(1).domain(2).n, 2, 2]), ...
	"AbsTol", 1e-15);
end % testPower()

function setupOnce(testCase)
syms z zeta t sy
testCase.TestData.sym.z = z;
testCase.TestData.sym.zeta = zeta;
testCase.TestData.sym.t = t;
testCase.TestData.sym.sy = sy;
end

function testNorm(tc)
z = quantity.Domain("z", linspace(0, 1, 11));
quan = Discrete(z) * [-1; 2; 3];
tc.verifyEqual(quan.norm.on(), on(z.Discrete*sqrt(1+4+9)), "AbsTol", 1e-15);
tc.verifyEqual(quan.norm.on(), on(quan.norm(2)), "AbsTol", 1e-15);
end

function testLog(tc)
z = quantity.Domain("z", linspace(0, 1, 11));
quan = Discrete(z) * ones(2, 2) + [1, 1; 0, 0]+0.1*ones(2,2);

tc.verifyEqual(log(quan.on()), on(log(quan)), "AbsTol", 1e-15);
tc.verifyEqual(on(quan), on(log(exp(quan))), "AbsTol", 1e-15);
end % testLog()

function testLog10(tc)
z = quantity.Domain("z", linspace(0, 1, 11));
quan = Discrete(z) * ones(2, 2) + [1, 1; 0, 0]+0.1*ones(2,2);

tc.verifyEqual(log10(quan.on()), on(log10(quan)), "AbsTol", 1e-15);
end % testLog10()

function testPower2(testCase)
t = quantity.Domain("t", linspace(0,1));

p(1,1) = Discrete(t);
p(2,1) = Discrete(t)^2;

p2 = p.^2;

testCase.verifyEqual(p2(1).on(), t.grid.^2)
testCase.verifyEqual(p2(2).on(), t.grid.^4, "AbsTol", 1e-15)

p25 = p.^2.5;

testCase.verifyEqual(p25(1).on(), t.grid.^2.5);
testCase.verifyEqual(p25(2).on(), t.grid.^5, "AbsTol", 1e-15);

end % testPower2()

function testTimes(testCase)

	t = quantity.Domain.defaultDomain(13, "t");
	
	a(1) = quantity.Discrete(t.grid, t);
	a(2) = quantity.Discrete(t.grid.^2, t);
	a(3) = quantity.Discrete(t.grid.^3, t);
	
	p = a .* a;
	
	p_(1) = quantity.Discrete(t.grid.^2, t);
	p_(2) = quantity.Discrete(t.grid.^4, t);
	p_(3) = quantity.Discrete(t.grid.^6, t);
	
	testCase.verifyEqual( p.on(), p_.on(),"AbsTol", 1e-15)
	
end % testTimes()

function testFindZeros(testCase)

x = quantity.Domain('x', linspace(0,1));
f = quantity.Discrete( sin( x.grid * 4 * pi ), x);

[zIdx, zGrid] = f.findZeros();

testCase.verifyEqual(zIdx{:}, [1 25, 50, 75, 100]');
testCase.verifyEqual(zGrid{:}, [0, 0.25, .5, .75, 1]', "AbsTol", 0.01);

end

function testMean(tc)
	z = quantity.Domain("z", linspace(0, 1, 11));
	x = quantity.Discrete(z.grid, z);
	tc.verifyEqual(mean(x), mean(x.on()));
	tc.verifyEqual(mean([1, 0; 0, 2] * x), 0.5*[1, 0; 0, 2]);
	tc.verifyEqual(mean([1, 0; 0, 2] * x, 'all'), 3/4/2);
end % testMean()

function testIszero(tc)
	z = quantity.Domain("z", linspace(0, 1, 11));
	zeta = quantity.Domain("zeta", linspace(0, 1, 11));
	zDiscrete = quantity.Discrete(z.grid, z);
	zetaDiscrete = quantity.Discrete(0*zeta.grid, zeta);
	result = logical([1, 0; 1, 0]);
	nonZeroQuan = (result-1) * (zDiscrete + zetaDiscrete);
	zeroQuan = (result-1) * (zDiscrete * zetaDiscrete);
	tc.verifyEqual(nonZeroQuan.iszero(), result);
	tc.verifyEqual(nonZeroQuan.iszero(true), false);
	tc.verifyEqual(zeroQuan.iszero(), true(size(zeroQuan)));
	tc.verifyEqual(zeroQuan.iszero(true), true);
end % testIszero()

function testDet(tc)
	z = quantity.Domain("z", linspace(0, 1, 11));
	zDiscrete = quantity.Discrete(z.grid, z);
	myMatrix2x2 = [1, 2; 3, 4] * zDiscrete;
	myMatrix3x3 = [1, 2, 3; 2, -3, 4; 3, 4, 5] * zDiscrete;
	myMatrix5x5 = [1, 2, 3, 4, 0; ...
					2, 3, 4, -5, 6; ...
					3, 4, 5, 6, 7; ...
					4, -6, 7, 8, 9; ...
					1, 1, 1, 1, 1] * zDiscrete;
	tc.verifyEqual(zDiscrete.det.on(), zDiscrete.on(), "AbsTol", 10*eps);
	tc.verifyEqual(myMatrix2x2.det.at(0.5), det(myMatrix2x2.at(0.5)), "AbsTol", 10*eps);
	tc.verifyEqual(myMatrix3x3.det.at(0.5), det(myMatrix3x3.at(0.5)), "AbsTol", 10*eps);
	tc.verifyEqual(myMatrix5x5.det.at(0.5), det(myMatrix5x5.at(0.5)), "AbsTol", 10*eps);
end % testDet()

function testSum(tc)
	z = quantity.Domain("z", linspace(0, 1, 11));
	myOnes = quantity.Discrete(ones(11, 3, 4), z);
	tc.verifyEqual(on(sum(myOnes)), 3*4*ones(11, 1), "AbsTol", 10*eps);
	tc.verifyEqual(on(sum(myOnes, 2)), 4*ones(11, 3), "AbsTol", 10*eps);
	tc.verifyEqual(on(sum(myOnes, 1)), 3*ones(11, 1, 4), "AbsTol", 10*eps);
	tc.verifyEqual(on(sum(myOnes, [1, 2])), 3*4*ones(11, 1), "AbsTol", 10*eps);

	myTwos3D = quantity.Discrete(2*ones(11, 2, 3, 4), z);
	tc.verifyEqual(on(sum(myTwos3D)), 2*3*4*ones(11, 1)*2, "AbsTol", 10*eps);
	tc.verifyEqual(on(sum(myTwos3D, 2)), 3*ones(11, 2, 1, 4)*2, "AbsTol", 10*eps);
	tc.verifyEqual(on(sum(myTwos3D, [1, 3])), 2*4*ones(11, 1, 3)*2, "AbsTol", 10*eps);
end % testSum()

function testProd(tc)
	z = quantity.Domain("z", linspace(0, 1, 11));
	myTwos3D = quantity.Discrete(2*ones(11, 2, 3, 4), z);
	tc.verifyEqual(on(prod(myTwos3D)), 2^(2*3*4)*ones(11, 1), "AbsTol", 10*eps);
	tc.verifyEqual(on(prod(myTwos3D, 2)), 2^3*ones(11, 2, 1, 4), "AbsTol", 10*eps);
	tc.verifyEqual(on(prod(myTwos3D, [1, 3])), 2^(2*4)*ones(11, 1, 3), "AbsTol", 10*eps);
end % testProd()

function testHashData(testCase)

	spatialDomain = quantity.Domain("z", linspace(0, 1, 11));
	M1 = reshape(repmat(diag([2, -3]), spatialDomain.n, 1, 1), spatialDomain.n, 2, 2);
	L1 = quantity.Discrete(M1, spatialDomain, 'name', 'Lambda');

	M2 = reshape(repmat(diag([1, -1]), spatialDomain.n, 1, 1), spatialDomain.n, 2, 2);
	L2 = quantity.Discrete(M2, spatialDomain, 'name', 'Lambda');
	
	testCase.verifyNotEqual(L1.hash, L2.hash);
				
end

function testIsdiag(tc)
	z = quantity.Domain("z", linspace(0, 1, 11));

	myOnes = quantity.Discrete(ones(11, 4, 4), z);
	tc.verifyFalse(isdiag(myOnes));

	myScalar = quantity.Discrete(ones(11, 1), z);
	myScalar2 = quantity.Discrete(zeros(11, 1), z);
	myScalar3 = quantity.Discrete(z.grid, z);
	tc.verifyTrue(isdiag(myScalar));
	tc.verifyTrue(isdiag(myScalar2));
	tc.verifyTrue(isdiag(myScalar3));

	myEye3x3 = 2*quantity.Discrete(repmat(reshape(eye(3), [1, 3, 3]), [z.n, 1]), z);
	tc.verifyTrue(isdiag(myEye3x3));
	myEye3x2 = 3*quantity.Discrete(repmat(reshape(eye(3, 2), [1, 3, 2]), [z.n, 1]), z);
	tc.verifyTrue(isdiag(myEye3x2));
end % testIsdiag(tc)

function testL2Norm(tc)

	Z = tc.TestData.sym.z;
	T = tc.TestData.sym.t;
	ZETA = tc.TestData.sym.zeta;

	t = quantity.Domain("t", linspace(0, 2, 101));
	z = quantity.Domain("z", linspace(0, 1, 51));
	x = quantity.Symbolic([Z * T; T], [z, t]);

	xNorm = sqrt(int(x.' * x, "z"));
	tc.verifyEqual(MAX(abs(xNorm - x.l2norm('z'))), 0, "AbsTol", 1e-12);

	x = quantity.Discrete(x);
	xNorm = sqrt(int(x.' * x, 'z'));
	tc.verifyEqual(MAX(abs(xNorm - x.l2norm('z'))), 0, "AbsTol", 1e-12);

	tc.verifyTrue( l2norm(x) == l2norm(x, {'z', 't'}) );

end % testL2Norm()

function testCompose(testCase)

t = quantity.Domain('t', linspace(0, 2));
f = quantity.Discrete(sin(t.grid * 2 * pi) , t, 'name', 'f' );
g = quantity.Discrete( 0.5 * t.grid, t, 'name', 'g');
h = quantity.Discrete( 2 * t.grid, t, 'name', 'h');

% verify simple evaluation of f( g(t) )
testCase.verifyEqual( on( f.compose(g) ), sin(t.grid * pi), "AbsTol", 3e-3 )

% verify that a warning is thrown, if the domain of definition is exceeded
testCase.verifyWarning(@()f.compose(h), 'quantity:Discrete:compose')

% verify f(tau) = sin( tau * 2pi ); g(z, t) = t + z^2
% f( g(z, t ) ) = sin( (t + z^2 ) * 2pi )
tau = quantity.Domain('tau', linspace(-1, 3));
z = quantity.Domain('z', linspace(0, 1, 51));

f = quantity.Discrete( sin( tau.grid * 2 * pi ), tau );
g = quantity.Discrete( ( z.grid.^2 ) + t.grid', [z, t] );

fg = f.compose(g);
F = quantity.Function(@(z,t) sin( ( t + z.^2 ) * 2 * pi ), [z, t], ...
    'name', 'f(g) as function handle');
testCase.verifyEqual(fg.on, F.on, "AbsTol", 1e-2)

% verify f(z, tau) °_tau g(z, t) -> f(z, g(z,t))
%	f(z, tau) = z + tau;
%	g(z, t) = z + t
%	f(z, g(z,t) ) = 2*z + t

z = quantity.Domain('z', linspace(0, 1, 11));
t = quantity.Domain('t', linspace(0, 1, 21));
tau = quantity.Domain('tau', linspace(0, 2, 31)); 

f = quantity.Discrete( z.grid + tau.grid', [z, tau]);
g = quantity.Discrete( z.grid + t.grid', [z, t]);

fog = f.compose(g, 'domain', tau);
FoG = quantity.Discrete( 2 * z.grid + t.grid', [z, t]);
testCase.verifyEqual( fog.on, FoG.on, "AbsTol", 5e-16)

% verify the matrix valued case
% #TODO: by this, an object array can be created where different quantities
% with different grids respectively empty quantities are in the same
% matrix.
M(1:2,1:2) = f;

Mog = M.compose(g, 'domain', tau);
MoG(1:2, 1:2) = FoG;

testCase.verifyEqual( MoG.on, MoG.on, "AbsTol", 5e-16)

% verify the case if one of the domains is a quantity.EquidistantDomain
z = quantity.EquidistantDomain("z", 0, 1, 'stepNumber', 11);
tau = quantity.EquidistantDomain("tau", 0, 2, 'stepNumber', 31);
f = quantity.Discrete( z.grid + tau.grid', [z, tau]);

fog = f.compose(g, 'domain', tau);
testCase.verifyEqual( fog.on, FoG.on, "AbsTol", 5e-16)

end

function testCastDiscrete2Function(testCase)
z = quantity.Domain("z", linspace(0, 2*pi)');
t = quantity.Domain("t", 0:5);
dom = [z,t];

d = quantity.Discrete({sin(dom.ndgrid{1}); cos(dom.ndgrid{1})}, dom);
f = quantity.Function(d);

testCase.verifyTrue(all(size(f) == size(d)));
testCase.verifyEqual(f.on(), d.on());

end

function testQuadraticNorm(tc)
blub = quantity.Discrete(ones(11, 4), quantity.Domain("z", linspace(0, 1, 11)), 'name', 'b');
blubQuadraticNorm = blub.quadraticNorm();
blubQuadraticWeightedNorm = blub.quadraticNorm('weight', 4*eye(4));

tc.verifyEqual(blubQuadraticNorm.on(), 2*ones(11,1));
tc.verifyEqual(blubQuadraticWeightedNorm.on(), 4*ones(11,1));
end % testQuadraticNorm()

function testSortDomain(tc)

t = linspace(0, pi, 7)';
domA = quantity.Domain('a', t);
domB = quantity.Domain('b', t);
q = quantity.Discrete(sin(t) .* cos(t'), [domA, domB]);

q1V = q.valueDiscrete;

q.sortDomain('descend');
q2V = q.sortDomain('descend').valueDiscrete;
tc.verifyEqual(q1V , q2V.');
end % testSortDomain

function testBlkdiag(tc)
z = quantity.Domain('z', linspace(0, 1, 21));
zeta = quantity.Domain('zeta', linspace(0, 1, 41));

A = quantity.Discrete(cat(4, ...
	cat(3, 1 + z.grid .* zeta.grid', - z.ones .* zeta.grid'), ...
	cat(3, -z.grid .* zeta.ones', z.grid .^2 .* zeta.ones') ), ...
	[z, zeta], 'name', "q");

B = 2*A(:, 1);
C = 3*A(1);

% 1 input
tc.verifyEqual(A.on, A.blkdiag.on());

% 2 x same input
AA = blkdiag(A, A);
tc.verifyEqual(AA(1:2, 1:2).on(), A.on());
tc.verifyEqual(AA(3:4, 3:4).on(), A.on());
tc.verifyEqual(AA(1:2, 3:4).on(), 0*A.on());
tc.verifyEqual(AA(3:4, 1:2).on(), 0*A.on());

% 3 different sized quantites
ABCB = blkdiag(A, B, C, B);
tc.verifyEqual(ABCB(1:2, 1:2).on(), A.on());
tc.verifyEqual(ABCB(3:4, 3).on(), B.on());
tc.verifyEqual(ABCB(5, 4).on(), C.on());
tc.verifyEqual(ABCB(6:7, 5).on(), B.on());

zeroElements = ~logical(blkdiag(true(size(A)), true(size(B)), true(size(C)), true(size(B))));
tc.verifyEqual(ABCB(zeroElements(:)).on(), 0*ABCB(zeroElements(:)).on());


% test different grid
E = Discrete(quantity.Domain("z", linspace(0, 1, 11))) + Discrete(zeta);
AE = blkdiag(A, E);
tc.verifyEqual(AE(1:2, 1:2).on([z, zeta]), A.on([z, zeta]));
tc.verifyEqual(AE(3, 3).on([z, zeta]), E.on([z, zeta]));
tc.verifyTrue(all(AE(3, 1:2).on([z, zeta]) == 0, "all"));
tc.verifyTrue(all(AE(1:2, 3).on([z, zeta]) == 0, "all"));
end % testBlkdiag()

function testSetName(tc)
blub = quantity.Discrete(ones(11, 2), quantity.Domain("z", linspace(0, 1, 11)));
blub.setName("asdf");
tc.verifyEqual(blub(1).name, "asdf");
tc.verifyEqual(blub(2).name, "asdf");
end

function testCtranspose(tc)
z = tc.TestData.sym.z;
zeta = tc.TestData.sym.zeta;

Z = quantity.Domain("z", linspace(0, 1, 21));
Zeta = quantity.Domain("zeta", linspace(0, 1, 41));

qSymbolic = quantity.Symbolic( [1+z*zeta, -zeta; -z, z^2], [Z, Zeta], 'name', "q" );
qDiscrete = quantity.Discrete(qSymbolic);
qDiscreteCtransp = qDiscrete';
tc.verifyEqual(qDiscrete(1,1).on(), qDiscreteCtransp(1,1).on());
tc.verifyEqual(qDiscrete(2,2).on(), qDiscreteCtransp(2,2).on());
tc.verifyEqual(qDiscrete(1,2).on(), qDiscreteCtransp(2,1).on());
tc.verifyEqual(qDiscrete(2,1).on(), qDiscreteCtransp(1,2).on());

qDiscrete2 = quantity.Discrete(ones(11, 1) + 2i, quantity.Domain("z", linspace(0, 1, 11)), ...
	'name', "im");
qDiscrete2Ctransp = qDiscrete2';
tc.verifyEqual(qDiscrete2.real.on(), qDiscrete2Ctransp.real.on());
tc.verifyEqual(qDiscrete2.imag.on(), -qDiscrete2Ctransp.imag.on());
end % testCtranspose

function testTranspose(tc)
z = tc.TestData.sym.z;
zeta = tc.TestData.sym.zeta;

qSymbolic = quantity.Symbolic(...
	[1+z*zeta, -zeta; -z, z^2], ...
	[quantity.Domain("z", linspace(0, 1, 21)), quantity.Domain("zeta", linspace(0, 1, 41))], ...
	'name', "q");
qDiscrete = quantity.Discrete(qSymbolic);
qDiscreteTransp = qDiscrete.';

tc.verifyEqual(qDiscrete(1,1).on(), qDiscreteTransp(1,1).on());
tc.verifyEqual(qDiscrete(2,2).on(), qDiscreteTransp(2,2).on());
tc.verifyEqual(qDiscrete(1,2).on(), qDiscreteTransp(2,1).on());
tc.verifyEqual(qDiscrete(2,1).on(), qDiscreteTransp(1,2).on());

E = qDiscrete * ones(2,0);

tc.verifyEqual( size(E'), size( ( ones(size(qDiscrete)) * ones(2,0) )' ))
tc.verifyEqual( size(E.'), size( ( ones(size(qDiscrete)) * ones(2,0) )' ))

end % testTranspose

function testFlipGrid(tc)
z = tc.TestData.sym.z;
zeta = tc.TestData.sym.zeta;

Z = quantity.Domain("z", linspace(0, 1, 11));
Zeta = quantity.Domain("zeta", linspace(0, 1, 11));
f = quantity.Discrete(quantity.Symbolic([1+z+zeta; 2*zeta+sin(z)] + zeros(2, 1)*z*zeta, [Z, Zeta]));
% % flip one grid
fReference = quantity.Discrete(quantity.Symbolic([1+z+(1-zeta); 2*(1-zeta)+sin(z)] + zeros(2, 1)*z*zeta, ...
	[Z, Zeta]));
fFlipped = f.flipDomain('zeta');
tc.verifyEqual(fReference.on(), fFlipped.on(), "AbsTol", 10*eps);

% flip both grids
fReference2 = quantity.Discrete(quantity.Symbolic([1+(1-z)+(1-zeta); 2*(1-zeta)+sin(1-z)] + zeros(2, 1)*z*zeta, ...
	[Z, Zeta]));
fFlipped2 = f.flipDomain(["z", "zeta"]);
fFlipped3 = f.flipDomain({'zeta', 'z'});
tc.verifyEqual(fReference2.on(), fFlipped2.on(), "AbsTol", 10*eps);
tc.verifyEqual(fReference2.on(), fFlipped3.on(), "AbsTol", 10*eps);
end % testFlipGrid();

function testScalarPlusMinusQuantity(testCase)

z = quantity.Domain('z', linspace(0, 1, 7));
f = quantity.Discrete( ( [2; 1] + zeros(2, 1) .* z.grid' )', z);

testCase.verifyError(@() 1-f-1, '');
testCase.verifyError(@() 1+f+1, '');

end % testScalarPlusMinusQuantity

function testNumericVectorPlusMinusQuantity(testCase)

z = quantity.Domain('z', linspace(0, 1, 7));
f = quantity.Discrete( [1+z.grid, 2+sin(z.grid)], z);

a = ones(size(f));
testCase.verifyEqual(on(a-f), 1-f.on());
testCase.verifyEqual(on(f-a), f.on()-1);
testCase.verifyEqual(on(a+f), f.on()+1);
testCase.verifyEqual(on(f+a), f.on()+1);
end % testNumericVectorPlusMinusQuantity

function testUnitaryPluasAndMinus(testCase)
z = quantity.Domain('z', linspace(0, 1, 7));
zeta = quantity.Domain('zeta', linspace(0, 1, 7));

q = quantity.Discrete(cat(4, ...
	cat(3, 1 + z.grid .* zeta.grid', - z.grid .* zeta.ones'), ...
	cat(3, - z.ones .* zeta.grid', z.grid.^2 .* zeta.ones') ), [z, zeta]) ;

qDoubleArray = q.on();
	
testCase.verifyEqual(on(-q), -qDoubleArray);
testCase.verifyEqual(on(+q), +qDoubleArray);

end

function testConcatenate(testCase)

t = linspace(0, pi, 7)';
T = quantity.Domain("t", t);
A = quantity.Discrete({sin(t); cos(t)}, T);
B = quantity.Discrete({tan(t); exp(t)}, T);

% concatenation of vectors:
AB = [A, B];
AB_ = [A', B'];
ABA = [A, B, A];

testCase.verifyTrue(all(size(AB) == [2, 2]));
testCase.verifyTrue(all(size(AB_) == [1, 4]));
testCase.verifyTrue(all(size(ABA) == [2, 3]));

T2 = quantity.Domain("t", linspace(0, pi, 13)');
C = quantity.Discrete({sin(T2.grid); cos(T2.grid)}, T2);
% concatenation of vectors on different domains
AC = [A; C];
testCase.verifyTrue(all(size(AC) == [4, 1]));

% concatenation of quantities and empty quantites
A_C = [A; []; C];
testCase.verifyEqual( AC.on(), A_C.on());

% concatenation of vectors and numbers
A0s = [A, zeros(2,3)];
testCase.verifyTrue(all(all(all(A0s(:, 2:end).on() == 0))))

% concatenation of empty quantities
O = quantity.Discrete.empty([5, 0]);
O_horz = [O, O];
O_vert = [O; O];
testCase.verifyEqual(size(O_horz, 1), 5);
testCase.verifyEqual(size(O_vert, 1), 10);
testCase.verifyEqual(size([O; zeros(3,0)], 1), 8);
testCase.verifyEqual(size([zeros(3,0); O], 1), 8)
testCase.verifyEqual( size( [zeros(0,1), O'], 2), 6)

% concatenation of quantities on different sorted domains
z = linspace(0, 1, 5);
Z = quantity.Domain("z", z);
D = quantity.Discrete({sin(t * z); cos(t * z)}, [T, Z]);
E = quantity.Discrete({tan(z' * t'); cos(z' * t')}, [Z, T]);
DE = [D, E];
compareDE = quantity.Discrete({sin(t * z), tan(t*z); cos(t * z), cos(t*z)}, [T, Z]);
testCase.verifyEqual(DE.on(), compareDE.on())
ED = [E, D];
compareED = quantity.Discrete({tan(t*z), sin(t * z); cos(t * z), cos(t*z)}, [T, Z]);
testCase.verifyEqual(ED.on(), compareED.on())

% concatenation of a quantity number and a quantity:
C = [5; 2];
const = quantity.Discrete(C, quantity.Domain.empty);
constA = [const, A];
testCase.verifyTrue(all(constA(1,1).on() == C(1)));
testCase.verifyTrue(all(constA(2,1).on() == C(2)));

Aconst = [A, const];
testCase.verifyTrue(all(Aconst(1,2).on() == C(1)));
testCase.verifyTrue(all(Aconst(2,2).on() == C(2)));
end

function testExp(testCase)
% 1 spatial variable
z = quantity.Domain('z', linspace(0, 1, 3));
s1d = quantity.Discrete(1 + z.grid .* z.grid.', z);
testCase.verifyEqual(s1d.exp.on(), exp(s1d.on()));

% diagonal matrix
zeta = quantity.Domain('zeta', linspace(0, 1, 4));
s2dDiag = quantity.Discrete(cat(4, ...
	cat(3, 1 + z.grid .* zeta.grid', zeros(z.n, zeta.n)), ....
	cat(3, zeros(z.n, zeta.n), z.grid.^2 .* zeta.ones')), [z, zeta]);

testCase.verifyEqual(s2dDiag.exp.on(), exp(s2dDiag.on()));
end

function testExpm(testCase)

z = quantity.Domain('z', linspace(0, 1, 3));
zeta = quantity.Domain('zeta', linspace(0, 1, 4));

mat2d = quantity.Discrete(...
	{1 + z.grid .* zeta.grid', 3 * z.ones .* zeta.grid'; ...
	 2 + 5 * z.grid + zeta.grid', z.grid .^2 .* zeta.ones'}, [z, zeta]);

mat2dMat = mat2d.on();
mat2dExpm = 0 * mat2d.on();
for zIdx = 1 : z.n
	for zetaIdx = 1 : zeta.n
		mat2dExpm(zIdx, zetaIdx, :, :) = expm(reshape(mat2dMat(zIdx, zetaIdx, :, :), [2, 2]));
	end
end
testCase.verifyEqual(mat2d.expm.on(), mat2dExpm, "RelTol", 100*eps);

end

function testSqrt(testCase)
% 1 spatial variable
quanScalar1d = quantity.Discrete((linspace(0,2).^2).', quantity.Domain("z", linspace(0, 1)), ...
	'name', "s1d");
quanScalarRoot = quanScalar1d.sqrt();
testCase.verifyEqual(quanScalarRoot.on(), linspace(0, 2).');

% 2 spatial variables
quanScalar2d = quantity.Discrete((linspace(0, 2, 21).' + linspace(0, 2, 41)).^2, ...
	[quantity.Domain("z", linspace(0, 1, 21)), quantity.Domain("zeta", linspace(0, 1, 41))], ...
	'name', 's2d');
quanScalar2dRoot = quanScalar2d.sqrt();
testCase.verifyEqual(quanScalar2dRoot.on(), (linspace(0, 2, 21).' + linspace(0, 2, 41)));

% diagonal matrix
quanDiag = [quanScalar1d, 0*quanScalar1d; 0*quanScalar1d, (2)*quanScalar1d];
quanDiagRoot = quanDiag.sqrt();
testCase.verifyEqual(quanDiagRoot(1,1).on(), quanScalarRoot.on());
testCase.verifyEqual(quanDiagRoot(1,2).on(), 0*quanDiagRoot(1,2).on());
testCase.verifyEqual(quanDiagRoot(2,1).on(), 0*quanDiagRoot(2,1).on());
testCase.verifyEqual(quanDiagRoot(2,2).on(), sqrt(2)*quanDiagRoot(1,1).on(), "AbsTol", 10*eps);

%testCase.verifyEqual(1, 0);
end

function testSqrtm(testCase)
quanScalar1d = quantity.Discrete((linspace(0,2).^2).', quantity.Domain("z",	linspace(0, 1)), ...
	'name', 's1d');
quanScalar2d = quantity.Discrete((linspace(0, 2, 21).' + linspace(0, 2, 41)).^2, ...
	[quantity.Domain("z", linspace(0, 1, 21)), quantity.Domain("zeta", linspace(0, 1, 41))], ...
	'name', 's2d');
% diagonal matrix - 1 spatial variable
quanDiag = [quanScalar1d, 0*quanScalar1d; 0*quanScalar1d, (2)*quanScalar1d];
quanDiagRoot = quanDiag.sqrt(); % only works for diagonal matrices
quanDiagRootMatrix = quanDiag.sqrtm();
testCase.verifyEqual(quanDiagRootMatrix.on(), quanDiagRoot.on());

% full matrix - 1 spatial variable
quanMat1d = [quanScalar1d, 4*quanScalar1d; quanScalar1d, (2)*quanScalar1d];
quanMat1dMat = quanMat1d.on();
quanMat1dReference = 0*quanMat1dMat;
for zIdx = 1:size(quanMat1dReference, 1)
	quanMat1dReference(zIdx, :, :) = sqrt(reshape(quanMat1dMat(zIdx, :), [2, 2]));
end
testCase.verifyEqual(quanMat1d.sqrtm.on(), quanMat1dReference, "AbsTol", 10*eps);

% full matrix - 2 spatial variables
quanMat2d = [quanScalar2d, 4*quanScalar2d; quanScalar2d, (2)*quanScalar2d];
quanMat2dMat = quanMat2d.on();
quanMat2dReference = 0*quanMat2dMat;
for zIdx = 1:size(quanMat2dMat, 1)
	for zetaIdx = 1:size(quanMat2dMat, 2)
		quanMat2dReference(zIdx, zetaIdx, :, :) = ...
			sqrt(reshape(quanMat2dMat(zIdx, zetaIdx, :), [2, 2]));
	end
end
testCase.verifyEqual(quanMat2d.sqrtm.on(), quanMat2dReference, "AbsTol", 10*eps);

end

function testDiag2Vec(testCase)
% quantity.Symbolic
z = testCase.TestData.sym.z;

do = quantity.Domain('z', linspace(0,1,3));
myMatrixSymbolic = quantity.Symbolic([sin(0.5*z*pi)+1, 0; 0, 0.9-z/2], do);
myVectorSymbolic = diag2vec(myMatrixSymbolic);
testCase.verifyEqual(myMatrixSymbolic(1,1).valueContinuous, myVectorSymbolic(1,1).valueContinuous);
testCase.verifyEqual(myMatrixSymbolic(2,2).valueContinuous, myVectorSymbolic(2,1).valueContinuous);
testCase.verifyEqual(numel(myVectorSymbolic), size(myMatrixSymbolic, 1));

% quantity.Discrete
myMatrixDiscrete = quantity.Discrete(myMatrixSymbolic);
myVectorDiscrete = diag2vec(myMatrixDiscrete);
testCase.verifyEqual(myMatrixDiscrete(1,1).on(), myVectorDiscrete(1,1).on());
testCase.verifyEqual(myMatrixDiscrete(2,2).on(), myVectorDiscrete(2,1).on());
testCase.verifyEqual(numel(myVectorDiscrete), size(myMatrixDiscrete, 1));
end

function testVec2Diag(testCase)
% quantity.Discrete
n = 7;
z = linspace(0,1,n)';
Z = quantity.Domain("z", z);
myMatrixDiscrete = quantity.Discrete(...
	{sin(0.5*z*pi)+1, zeros(n,1); zeros(n,1), 0.9-z/2}, Z);
myVectorDiscrete = quantity.Discrete(...
	{sin(0.5*z*pi)+1; 0.9-z/2}, Z);

testCase.verifyTrue( myVectorDiscrete.vec2diag.near(myMatrixDiscrete) );
end

function testInvert(testCase)
x = quantity.Domain("x", linspace(0, 1, 21));
% scalar
fScalar = quantity.Discrete((x.grid).^2, x);
fScalarInverse = fScalar.invert();
testCase.verifyEqual(fScalarInverse.on(fScalar.on()), x.grid),
end

function testSolveAlgebraic(testCase)
x = quantity.Domain("x", linspace(0, 1, 21));
% scalar
fScalar = quantity.Discrete((1+x.grid).^2, x);
solutionScalar = fScalar.solveAlgebraic(2);
testCase.verifyEqual(solutionScalar, sqrt(2)-1, "AbsTol", 1e-3);
end

function testSolveDVariableEqualQuantityConstant(testCase)
%% simple constant case
quan = quantity.Discrete(ones(3, 1), quantity.Domain("z", linspace(0, 1, 3)));
solution = quan.solveDVariableEqualQuantity();
[referenceResult1, referenceResult2] = ndgrid(linspace(0, 1, 3), linspace(0, 1, 3));
testCase.verifyEqual(solution.on(), referenceResult1 + referenceResult2, "AbsTol", 10*eps);
% %% 2x2 constant case
%Lambda = quantity.Discrete(quantity.Symbolic([1+sym("z")^2; sym("z")-2], quantity.Domain("z", linspace(0, 1, 101))));
%Lambda = quantity.Discrete(quantity.Symbolic([2; -1], quantity.Domain("z", linspace(0, 1, 11))));
%solution = Lambda.solveDVariableEqualQuantity();
%[referenceResult1, referenceResult2] = ndgrid(linspace(0, 1, 3), linspace(0, 1, 3));
%testCase.verifyEqual(solution.on(), referenceResult1 + referenceResult2, "AbsTol", 10*eps);
end

function testSolveDVariableEqualQuantityNegative(testCase)
z = testCase.TestData.sym.z;
zeta = testCase.TestData.sym.zeta;

assume(z>0 & z<1); assume(zeta>0 & zeta<1);
Z = quantity.Domain("z", linspace(0, 0.1, 5));
Lambda = quantity.Symbolic(-0.1-z^2, Z, 'name', '\Lambda');

%%
myGrid = linspace(-.2, .2, 11);
sEval = -0.09;
solveThisOdeDiscrete = solveDVariableEqualQuantity(...
	diag2vec(quantity.Discrete(Lambda)), 'variableGrid', myGrid);
solveThisOdeSymbolic = solveDVariableEqualQuantity(...
	diag2vec(Lambda), 'variableGrid', myGrid);

testCase.verifyEqual(solveThisOdeSymbolic.on({sEval, 0.05}), solveThisOdeDiscrete.on({sEval, 0.05}), "RelTol", 5e-3);
end

function testSolveDVariableEqualQuantityComparedToSym(testCase)
%% compare with symbolic implementation
z = testCase.TestData.sym.z;
Z = quantity.Domain("z", linspace(0, 1, 5));
assume(z>0 & z<1);
quanBSym = quantity.Symbolic(1+z, Z);
quanBDiscrete = quantity.Discrete(quanBSym.on(), Z);
solutionBSym = quanBSym.solveDVariableEqualQuantity();
solutionBDiscrete = quanBDiscrete.solveDVariableEqualQuantity();
%solutionBSym.plot(); solutionBDiscrete.plot();
testCase.verifyEqual(solutionBDiscrete.on(), solutionBSym.on(), "RelTol", 1e-6);
end

function testSolveDVariableEqualQuantityAbsolut(testCase)
%% compare with symbolic implementation
z = testCase.TestData.sym.z;
Z = quantity.Domain("z", linspace(0, 1, 11));
assume(z>0 & z<1);
quanBSym = quantity.Symbolic(1+z, Z, "name", "bSym");
quanBDiscrete = quantity.Discrete(quanBSym.on(), Z, "name", "bDiscrete");

solutionBDiscrete = quanBDiscrete.solveDVariableEqualQuantity();
myGrid = solutionBDiscrete.domain(1).grid;
solutionBDiscreteDiff = solutionBDiscrete.diff("s");
quanOfSolutionOfS = zeros(length(myGrid), length(myGrid), 1);
for icIdx = 1 : length(myGrid)
	quanOfSolutionOfS(:, icIdx, :) = quanBSym.on(solutionBDiscrete.on({myGrid, myGrid(icIdx)}));
end
%%
testCase.verifyEqual(solutionBDiscreteDiff.on({myGrid(2:end-1), myGrid}), quanOfSolutionOfS(2:end-1, :), "AbsTol", 1e-2);

end

function testMtimesDifferentGridLength(testCase)
%%
a = quantity.Discrete(ones(11, 1), quantity.Domain("z", linspace(0, 1, 11)), "name", "a");
b = quantity.Discrete(ones(5, 1), quantity.Domain("z", linspace(0, 1, 5)), "name", "b");
ab  = a*b;
%
z = testCase.TestData.sym.z;

c = quantity.Symbolic(1, quantity.Domain("z", linspace(0, 1, 21)), "name", "c");
ac = a*c;

%%
testCase.verifyEqual(ab.on(), ones(11, 1));
testCase.verifyEqual(ac.on(), ones(21, 1));
end

function testDiffNonequidistant(tc)
z = quantity.Domain("z", linspace(0, 1, 11).^2);
quan1 = quantity.Discrete(ones(z.n, 1), z);
quanZsqrt = quantity.Discrete(1+sqrt(z.grid), z);
quanZZ = quantity.Discrete(1+z.grid, z);

tc.verifyEqual(quan1.diff.on(), 0*z.grid);
tc.verifyEqual(quanZZ.diff.on(), 1+0*z.grid, "AbsTol", 10*eps);
end % testDiffNonequidistant()

function testDiff1d(testCase)
%% 1d
constant = quantity.Discrete([2*ones(11, 1), linspace(0, 1, 11).'], ...
	quantity.Domain("z", linspace(0, 1, 11)));

constantDiff = diff(constant);
testCase.verifyEqual(constantDiff.on(), [zeros(11, 1), ones(11, 1)], "AbsTol", 10*eps);

z = quantity.Domain("z", linspace(0,pi)');
sinfun = quantity.Discrete(sin(z.grid), z);

% do the comparison on a smaller grid, because the numerical derivative is
% very bad a the boundarys of the domain.
Z = linspace(0.1, pi-0.1)';
testCase.verifyTrue(numeric.near(sinfun.diff().on(Z), cos(Z), 1e-3));
testCase.verifyTrue(numeric.near(sinfun.diff('z', 2).on(Z), -sin(Z), 1e-3));
testCase.verifyTrue(numeric.near(sinfun.diff('z', 3).on(Z), -cos(Z), 1e-3));

end

function testDiffMat(tc)
z = quantity.Domain("z", linspace(0, 1, 5));
myMat = ones(2, 2) * z.Discrete();
tc.verifyEqual(myMat.diff.on(), ones(5, 2, 2));
end % testDiffMat()

function testDiffConstant2d(tc)
%% 2d
[zNdgrid, zetaNdgrid] = ndgrid(linspace(0, 1, 11), linspace(0, 1, 21));
myQuantity = quantity.Discrete(cat(3, 2*ones(11, 21), zNdgrid, zetaNdgrid), ...
	[quantity.Domain("z", linspace(0, 1, 11)), quantity.Domain("zeta", linspace(0, 1, 21))], ...
	'name', 'constant');
myQuantityDz = diff(myQuantity, "z", 1);
myQuantityDzeta = diff(myQuantity, "zeta", 1);

% verify zero-th derivative
tc.verifyEqual( myQuantity.diff("z", 0).on(), myQuantity.on() );

tc.verifyError( @() diff(myQuantity), ?MException )
tc.verifyError( @() diff(myQuantity, 1, {'z', 'zeta'}), ?MException )
tc.verifyError( @() diff(myQuantity,  {'z', 'zeta'}), ?MException )

% constant
tc.verifyEqual(myQuantityDz(1).on(), zeros(11, 21));
tc.verifyEqual(myQuantityDzeta(1).on(), zeros(11, 21));


% zNdgrid
tc.verifyEqual(myQuantityDz(2).on(), ones(11, 21), "AbsTol", 10*eps);
tc.verifyEqual(myQuantityDzeta(2).on(), zeros(11, 21), "AbsTol", 10*eps);

% zetaNdgrid
tc.verifyEqual(myQuantityDz(3).on(), zeros(11, 21), "AbsTol", 10*eps);
tc.verifyEqual(myQuantityDzeta(3).on(), ones(11, 21), "AbsTol", 10*eps);

end

function testOn(testCase)
% init data
domainVecA = quantity.Domain('z', linspace(0, 1, 27));
domainVecB = quantity.Domain('zeta', linspace(0, 1, 41));
[value] = createTestData(domainVecA.grid, domainVecB.grid);
a = quantity.Discrete(value, [domainVecA, domainVecB], 'name', 'A');

%
testCase.verifyEqual(value, a.on());
testCase.verifyEqual(permute(value, [2, 1, 3, 4]), ...
	a.on([domainVecB, domainVecA]));
testCase.verifyEqual(createTestData(linspace(0, 1, 100), linspace(0, 1, 21)), ...
	a.on({linspace(0, 1, 100), linspace(0, 1, 21)}), "AbsTol", 100*eps);

d24z = quantity.Domain('z', linspace(0, 1, 24));
d21zeta = quantity.Domain('zeta', linspace(0, 1, 21));

testCase.verifyEqual(createTestData(linspace(0, 1, 24), linspace(0, 1, 21)), ...
	a.on( [d24z, d21zeta] ), "AbsTol", 24*eps);

testCase.verifyEqual(permute(createTestData(linspace(0, 1, 21), linspace(0, 1, 24)), [2, 1, 3, 4]), ...
	a.on({linspace(0, 1, 24), linspace(0, 1, 21)}, {'zeta', 'z'}), "AbsTol", 2e-4);

	function [value] = createTestData(gridVecA, gridVecB)
		[zGridA , zetaGridA] = ndgrid(gridVecA, gridVecB);
		value = ones(numel(gridVecA), numel(gridVecB), 2, 3);
		value(:,:,1,1) = 1+zGridA;
		value(:,:,2,1) = 1+zetaGridA;
		value(:,:,2,2) = 1+2*zGridA - zetaGridA.^2;
		value(:,:,1,2) = 1+zGridA + zetaGridA.^2;
		value(:,:,2,3) = 1+zeros(numel(gridVecA), numel(gridVecB));
	end

%% test on on 3-dim domain
z = quantity.Domain('z', 0:3);
t = quantity.Domain('t', 0:4);
x = quantity.Domain('x', 0:5);

Z = quantity.Discrete( z.grid, z);
T = quantity.Discrete( t.grid, t);
X = quantity.Discrete( x.grid, x);

ZTX = Z+T+X;
XTZ = X+T+Z;

testCase.verifyEqual( ZTX.on([t z x]), XTZ.on([t z x]), "AbsTol", 1e-12);

end

function testOn2(testCase)
zeta = quantity.Domain("zeta", linspace(0, 1));
eta = quantity.Domain("eta", linspace(0, 1, 71)');
z = quantity.Domain("z", linspace(0, 1, 51));

[ZETA, ETA, Z] = ndgrid(zeta.grid, eta.grid, z.grid);

a = quantity.Discrete(cat(4, sin(ZETA.*ZETA.*Z)+ETA.*ETA, ETA+cos(ZETA.*ETA.*Z)), [zeta, eta, z]);

testCase.verifyEqual(a.on([zeta, eta, z]), ...
	permute(a.on([eta, zeta, z]), [2, 1, 3, 4]));
testCase.verifyEqual([a(1).on({zeta.grid(2), eta.grid(3), z.grid(4)}); ...
	                  a(2).on({zeta.grid(2), eta.grid(3), z.grid(4)})], ...
	[sin(zeta.grid(2)*zeta.grid(2)*z.grid(4))+eta.grid(3)*eta.grid(3); ...
	 eta.grid(3)+cos(zeta.grid(2)*eta.grid(3)*z.grid(4))]);
end

function testOnDescending(tc)
domainVecA = quantity.Domain('z', linspace(0, 1, 27));
myData = quantity.Discrete(domainVecA.grid+1, domainVecA);
tc.verifyEqual(flip(domainVecA.grid)+1, myData.on(flip(domainVecA.grid)), "AbsTol", 1e3*eps);
tc.verifyEqual(flip(domainVecA.grid)+1, myData.on(flip(domainVecA.grid), 'z'), "AbsTol", 1e3*eps);
tc.verifyEqual(flip(domainVecA.grid)+1, myData.on({flip(domainVecA.grid)}, {'z'}), "AbsTol", 1e3*eps);
end % testOnDescending()

function testMtimesZZeta2x2(testCase)



%%
z11_ = linspace(0, 1, 11);
z11 = quantity.Domain('z', z11_);
zeta11 = quantity.Domain('zeta', z11_);

a = quantity.Discrete(ones([z11.n, z11.n, 2, 2]), [z11, zeta11], 'name', 'A');

zeta = testCase.TestData.sym.zeta;
z10_ = quantity.Domain("zeta", linspace(0, 1, 10));

b = quantity.Symbolic((eye(2, 2)), z10_);

c = a*b;

%
testCase.verifyEqual(c.on(), a.on());
end

function testMTimesPointWise(tc)

z = tc.TestData.sym.z;
zeta = tc.TestData.sym.zeta;

Z = quantity.Domain("z", linspace(0, pi, 51)');
ZETA = quantity.Domain("zeta", linspace(0, pi / 2, 71)');

P = quantity.Symbolic( sin(zeta) * z, [Z, ZETA]);
B = quantity.Symbolic( sin(zeta), ZETA);

PB = P*B;

p = quantity.Discrete(P);
b = quantity.Discrete(B);

pb = p*b;

tc.verifyEqual(MAX(abs(PB-pb)), 0, "AbsTol", 10*eps);
end

function testMldivide(testCase)
%% scalar example
s = quantity.Domain("s", linspace(0, 1, 21));
vL = quantity.Discrete(2*ones(21, 1), s);
vR = quantity.Discrete((linspace(1, 2, 21).').^2, s);
vLdVR = vL \ vR;


%% matrix example
t = quantity.Domain("t", linspace(0, pi));

[T, ~] = ndgrid(t.grid, s.grid);

KL = quantity.Discrete(permute(repmat(2*eye(2), [1, 1, size(T)]), [3, 4, 1, 2]), [t, s]);
KR = quantity.Discrete(permute(repmat(4*eye(2), [1, 1, size(T)]), [3, 4, 1, 2]), [t, s]);
KLdKR = KL \ KR;

%%
testCase.verifyEqual(vLdVR.on(), 2 .\ (linspace(1, 2, 21).').^2);

% TODO #discussion: for the following case the operation KL \ KR seems to change the order of the grid in the old version. As this is very unexpected. I would like to change them to not change the order! 
testCase.verifyEqual(KLdKR.on(), permute(repmat(2*eye(2), [1, 1, size(T)]), [3, 4, 1, 2]));

end

function testMrdivide(testCase)
%% scalar example
s = quantity.Domain("s", linspace(0, 1, 21));
vL = quantity.Discrete(2*ones(21, 1), s);
vR = quantity.Discrete((linspace(1, 2, 21).').^2, s);
vLdVR = vL / vR;


%% matrix example
t = quantity.Domain("t", linspace(0, pi));

[T, ~] = ndgrid(t.grid, s.grid);

KL = quantity.Discrete(permute(repmat(2*eye(2), [1, 1, size(T)]), [3, 4, 1, 2]), [t, s]);
KR = quantity.Discrete(permute(repmat(4*eye(2), [1, 1, size(T)]), [3, 4, 1, 2]), [t, s]);
KLdKR = KL / KR;

%%
testCase.verifyEqual(vLdVR.on(), 2 ./ (linspace(1, 2, 21).').^2);

% TODO #discussion: for the following case the operation KL \ KR seems to change the order of the grid in the old version. As this is very unexpected. I would like to change them to not change the order! 
testCase.verifyEqual(KLdKR.on(),  permute(repmat(0.5*eye(2), [1, 1, size(T)]), [3, 4, 1, 2]));
end

function testInv(testCase)
%% scalar example
s = quantity.Domain("s", linspace(0, 1, 21));
v = quantity.Discrete((linspace(1, 2, 21).').^2, s);
vInv = v.inv();

%% matrix example
t = quantity.Domain("t", linspace(0, pi));
[T, ~] = ndgrid(t.grid, s.grid);

K = quantity.Discrete(permute(repmat(2*eye(2), [1, 1, size(T)]), [3, 4, 1, 2]), [t, s]);
kInv = inv(K);

%%
testCase.verifyEqual(vInv.on(), 1./(linspace(1, 2, 21).').^2)
testCase.verifyEqual(kInv.on(), permute(repmat(0.5*eye(2), [1, 1, size(T)]), [3, 4, 1, 2]))

end

function testCumInt(testCase)

tt = quantity.Domain("t", linspace(pi, 1.1*pi, 11)');
s = quantity.Domain("s", linspace(pi, 1.1*pi, 11)');
[T, S] = ndgrid(tt.grid, tt.grid);

t = testCase.TestData.sym.t;
sy = testCase.TestData.sym.sy;

a = [ 1, sy; t, 1];
b = [ sy; 2*sy];

A = zeros([size(T), size(a)]);
B = zeros([s.n, size(b)]);

for i = 1:size(a,1)
	for j = 1:size(a,2)
		A(:,:,i,j) = subs(a(i,j), {sy, t}, {S, T});
	end
end

for i = 1:size(b,1)
	B(:,i) = subs(b(i), sy, s.grid);
end

%% int_0_t a(t,s) * b(s) ds
% compute symbolic version of the volterra integral
v = int(a*b, sy, tt.grid(1), t);
V = quantity.Symbolic(v, tt);

% compute the numeric version of the volterra integral
k = quantity.Discrete(A, [tt, s]);
x = quantity.Discrete(B, s);

f = cumInt(k * x, "s", s.grid(1), "t");

testCase.verifyEqual(V.on(), f.on(), "AbsTol", 5e-4);

%% int_s_t a(t,s) * b(s) ds
v = int(a*b, sy, sy, t);
V = quantity.Symbolic(subs(v, {t, sy}, {'t', 's'}), [tt, s]);

f = cumInt(k * x, "s", "s", "t");

testCase.verifyEqual( f.on(f(1).domain), V.on(f(1).domain), "AbsTol", 5e-4 );

%% int_s_t a(t,s) * c(t,s) ds
c = [1, sy+1; t+1, 1];
C = zeros([size(T), size(c)]);
for i = 1:numel(c)
	C(:,:,i) = double(subs(c(i), {t sy}, {T S}));
end
y = quantity.Discrete(C, [tt, s]);

v = int(a*c, sy, sy, t);
V = quantity.Symbolic(subs(v, {t, sy}, {'t', 's'}), [tt, s]);
f = cumInt( k * y, "s", "s", "t");

testCase.verifyEqual( f.on(f(1).domain), V.on(f(1).domain), "AbsTol", 5e-5 );

%% testCumIntWithLowerAndUpperBoundSpecified
tt = quantity.Domain("t", linspace(pi, 1.1*pi, 11));
s = quantity.Domain("s", linspace(pi, 1.1*pi, 11));
[T, S] = ndgrid(tt.grid, tt.grid);

sy = testCase.TestData.sym.sy;
t = testCase.TestData.sym.t;

a = [ 1, sy; t, 1];

A = zeros([size(T), size(a)]);

for i = 1:size(a,1)
	for j = 1:size(a,2)
		A(:,:,i,j) = subs(a(i,j), {sy, t}, {S, T});
	end
end

% compute the numeric version of the volterra integral
k = quantity.Discrete(A, [tt, s]);

fCumInt2Bcs = cumInt(k, "s", "zeta", "t");
fCumInt2Cum = cumInt(k, "s", tt.grid(1), "t") ...
	- cumInt(k, "s", tt.grid(1), "zeta");
testCase.verifyEqual(fCumInt2Bcs.on(), fCumInt2Cum.on(), "AbsTol", 10*eps);

%% testCumInt with one independent variable
tt = quantity.Domain("t", linspace(0, pi, 51));

a = [ 1, sin(t); t, 1];

% compute the numeric version of the volterra integral
f = quantity.Symbolic(a, tt);
f = quantity.Discrete(f);

intK = cumInt(f, "t", 0, "t");

K = quantity.Symbolic( int(a, 0, t), tt);

testCase.verifyEqual(intK.on(), K.on(), "AbsTol", 1e-3);


end

function testAtIndex(testCase)

z = linspace(0,1);
a = sin(z * pi);

Z = quantity.Domain("z", z);

A = quantity.Discrete({a}, Z);

testCase.verifyEqual(a(end), A.atIndex(end));
testCase.verifyEqual(a(1), A.atIndex(1));
testCase.verifyEqual(a(23), A.atIndex(23));

y = linspace(0,2,51);
Y = quantity.Domain("y", y);
b1 = sin(z.' * pi * y);
b2 = cos(z.' * pi * y);

B = quantity.Discrete({b1; b2}, [Z, Y]);

B_1_y = B.atIndex(1,1);
b_1_y = [sin(0); cos(0)];
testCase.verifyTrue(numeric.near(B_1_y, b_1_y));

B_z_1 = B.atIndex(':',1);

testCase.verifyTrue(all(B_z_1(:,1,1) == 0));
testCase.verifyTrue(all(B_z_1(:,:,2) == 1));

end

function testMTimes(testCase)
% multiplication of a(z) * a(z)'
% a(z) \in (3, 2), z \in (100)
z = linspace(0, 2*pi)';
a = cat(3, [z*0, z, sin(z)], [z.^2, z.^3, cos(z)]); % dim = (100, 3, 2)
aa = misc.multArray(a, permute(a, [1, 3, 2]), 3, 2, 1);    % dim = (100, 3, 3)

Z = quantity.Domain("z", z);

A = quantity.Discrete(a, Z);
AA = A*A';

testCase.verifyTrue(numeric.near(aa, AA.on()));

z = linspace(0, 2*pi, 31);
t = linspace(0, 1, 13);
v = linspace(0, 1, 5);
[z, t] = ndgrid(z, t);
[v, t2] = ndgrid(v, t(1,:));

T = quantity.Domain("t", t(1,:));
Z = quantity.Domain("z", z(:,1));
V = quantity.Domain("v", v(:,1));

% scalar multiplication of b(z, t) * b(v, t)
b = z .* t;
b1b1 = misc.multArray(b, b, 4, 3, [1, 2]);
B1 = quantity.Discrete(b(:,:,1,1)', [T, Z]);
B1B1 = B1*B1;
testCase.verifyTrue(numeric.near(b1b1', B1B1.on()));

% vector multiplication of b(z, t) * b(v, t)
b2 = cat(3, z .* t, z*0 +1);
b2b2 = misc.multArray(b2, permute(b2, [1 2 4 3]), 4, 3, [1, 2]);
b2b2 = permute(b2b2, [1 2 3 4]);
B2 = quantity.Discrete(b2, [Z, T]);
B2B2 = B2*B2';
testCase.verifyTrue(numeric.near(b2b2, B2B2.on()));

c2 = cat(4, sin(z), sin(t), cos(z));
b2c2 = misc.multArray(b2, c2, 4, 3, [1 2]);
C2 = quantity.Discrete(c2, [Z, T]);
B2C2 = B2 * C2;
testCase.verifyTrue(numeric.near(permute(b2c2, [1 2 3 4]), B2C2.on()));

% matrix b(z,t) * b(z,t)
b = cat(4, cat(3, z .* t, z*0 +1), cat(3, sin( z ), cos( z .* t )));
bb = misc.multArray(b, b, 4, 3, [1, 2]);
bb = permute(bb, [1 2 3 4]);
B = quantity.Discrete(b, [Z, T]);
BB = B*B;
testCase.verifyTrue(numeric.near(bb, BB.on));

% matrix multiplication with one commmon and one distinct domain
c = cat(4, cat(3, v .* t2, v*0 +1), cat(3, sin( v ), cos( v .* t2 )), cat(3, cos( t2 ), tan( v .* t2 )));
bc = misc.multArray(b, c, 4, 3, 2);
bc = permute(bc, [ 1 2 4 3 5 ]);

B = quantity.Discrete(b, [Z, T]);
C = quantity.Discrete(c, [V, T]);
BC = B*C;
testCase.verifyTrue(numeric.near(bc, BC.on()));

%%
z = linspace(0,1).';
Z = quantity.Domain("z", z);
a = quantity.Discrete({sin(z * pi), cos(z* pi)}, Z, "name", "a");
aa = a.' * a;
%%
testCase.verifyTrue( numeric.near( aa(1,1).on() , sin(z * pi) .* sin(z * pi)));
testCase.verifyTrue( numeric.near( aa(1,2).on() , sin(z * pi) .* cos(z * pi)));
testCase.verifyTrue( numeric.near( aa(2,2).on() , cos(z * pi) .* cos(z * pi)));

%% test multiplicatin with constants:
C = [3 0; 0 5];
c = quantity.Discrete(C, quantity.Domain.empty(), "name", "c");
testCase.verifyTrue( numeric.near( squeeze(double(a*c)), [sin(z * pi) * 3, cos(z * pi)  * 5]));
testCase.verifyTrue( numeric.near( squeeze(double(a*[3 0; 0 5])), [sin(z * pi) * 3, cos(z * pi)  * 5]));
testCase.verifyTrue( numeric.near( double(c*c), C*C));

%% test multiplication with a scalar:
s = quantity.Discrete(42, quantity.Domain.empty, "name", "s");
testCase.verifyTrue( numeric.near( squeeze(double(42 * a)), [sin(z * pi) * 42, cos(z * pi) * 42]));
testCase.verifyTrue( numeric.near( squeeze(double(a * 42)), [sin(z * pi) * 42, cos(z * pi) * 42]));

testCase.verifyTrue( numeric.near( squeeze(double(s * a')), [sin(z * pi) * 42, cos(z * pi) * 42]));
testCase.verifyTrue( numeric.near( squeeze(double(a * s)), [sin(z * pi) * 42, cos(z * pi) * 42]));
%% test multiplication with empty quantities
e1 = quantity.Discrete.empty([3, 0]);

testCase.verifyTrue( isempty( e1.' * e1 ) );
testCase.verifyError( @() e1 * e1.', "quantity:Discrete:mTimes" )
testCase.verifyEqual( size(e1 * quantity.Discrete.empty()), [3 0])

testCase.verifyTrue( isempty( 2 * e1 ) );
testCase.verifyTrue( isempty( e1 * 2 ) );


end

function testOnConstant(testCase)

A = [0 1; 2 3];
const = quantity.Discrete(A, quantity.Domain.empty);
C = const.on(1:1:5);

for k = 1:numel(A)
	testCase.verifyTrue( all( A(k) == C(:,k) ));
end

end % testOnConstant()

function testIsNumber(testCase)

C = quantity.Discrete(rand(3,7), quantity.Domain.empty());
testCase.verifyTrue(C.isNumber());

z = linspace(0, pi).';
A = quantity.Discrete(sin(z), quantity.Domain("z", z));
testCase.verifyFalse(A.isNumber());

end % testIsNumber()

function testMTimesConstant(testCase)

myGrid = linspace(0, 2*pi).';
a = cat(3, [myGrid*0, myGrid, sin(myGrid)], [myGrid.^2, myGrid.^3, cos(myGrid)]); % dim = (100, 3, 2)
c = rand(2, 3);  % dim = (2, 3)

A = quantity.Discrete(a, quantity.Domain("z", myGrid));
C = quantity.Discrete(c, quantity.Domain.empty());

ac = misc.multArray(a, c, 3, 1);    % dim = (100, 3, 3)
AC = A*C;
Ac = A*c;

ca = permute(misc.multArray(c, a, 2, 2), [2, 1, 3]);
CA = C*A;
cA = c*A;

verifyTrue(testCase, numeric.near(Ac.on(), ac));
verifyTrue(testCase, numeric.near(AC.on(), ac));
verifyTrue(testCase, numeric.near(cA.on(), ca, 1e-12));
verifyTrue(testCase, numeric.near(CA.on(), ca, 1e-12));

end

function testMPower(tc)
%% scalar case
z = quantity.Domain("z", linspace(0, 1, 11));
A = quantity.Discrete(sin(z.grid * pi), z);
aa = sin(z.grid * pi) .* sin(z.grid * pi);
AA = A^2;

tc.verifyEqual( aa, AA.on());
tc.verifyEqual( on(A^0), ones(z.n, 1));
tc.verifyEqual( on(A^3), on(AA * A));

%% matrix case
zeta = quantity.Domain("zeta", linspace(0, 1, 21));
B = [0*z.Discrete + zeta.Discrete, A + 0*zeta.Discrete; ...
	z.Discrete+zeta.Discrete, A * subs(A, "z", "zeta")];
BBB = B^3;
BBB_alt = B * B * B;
tc.verifyEqual(BBB.on(), BBB_alt.on(), "AbsTol", 1e-14);
tc.verifyEqual(BBB.at({0.5, 0.6}), (B.at({0.5, 0.6}))^3, "AbsTol", 1e-15);
tc.verifyEqual(on(B^0), permute(repmat(eye(2), [1, 1, B(1).domain(1).n, B(1).domain(2).n]), [3, 4, 1, 2]), ...
	"AbsTol", 1e-15);
end % testMPower()

function testPlus(testCase)
%%
z = linspace(0,1,31).';
zeta = linspace(0,1,51);
[zGrid, zetaGrid] = ndgrid(z, zeta);
a = sin(z * pi);
aLr = sin(zeta * pi);
b = cos(z * pi);
C = zeta;
bc = cos(zGrid * pi) + zetaGrid;
azZeta = sin(zGrid * pi) + sin(zetaGrid * pi);
bzZeta = cos(zGrid * pi) + cos(zetaGrid * pi);

Z = quantity.Domain("z", z);
Zeta = quantity.Domain("zeta", zeta);

AB = quantity.Discrete({a, b}, Z.rename("blub"));
A = quantity.Discrete({a}, Z);
ALr = quantity.Discrete({aLr}, Zeta.rename("z"));
B = quantity.Discrete({b}, Z);
C = quantity.Discrete({C}, Zeta);
AZZETA = quantity.Discrete({azZeta}, [Z, Zeta]);
BZZETA = quantity.Discrete({bzZeta}, [Z, Zeta]);

ABAB = AB + AB;
BC = B+C;
ApB = A+B;
ABZZETA = AZZETA + BZZETA;
ALrA = ALr+A;

%%
testCase.verifyEqual(a+a, ABAB(1).on());
testCase.verifyEqual(b+b, ABAB(2).on());
testCase.verifyEqual(a+b, ApB.on());
testCase.verifyTrue(numeric.near(bc, BC.on()));
testCase.verifyEqual(azZeta+bzZeta, ABZZETA.on());
testCase.verifyEqual(a+a, ALrA.on(z), "RelTol", 1e-3);

%% additional test
c = quantity.Discrete(zGrid, [Z, Zeta], "name", "a");
d = quantity.Discrete(zetaGrid, [Z, Zeta], "name", "b");
e = c + d;
eMat = e.on();
eMatReference = zGrid + zetaGrid;
%%
testCase.verifyEqual(numel(eMat), numel(eMatReference));
testCase.verifyEqual(eMat(:), eMatReference(:));

%% addition with constant values
testCase.verifyEqual(permute([a b], [1 3 2]), AB.on());

AB2 = AB' * AB;
tst(:,1,1) = a.^2 + 1;
tst(:,1,2) = a.*b + 2;
tst(:,2,1) = b.*a + 3;
tst(:,2,2) = b.^2 + 4;

AB2c = AB2 + [1 2; 3 4];
cAB2 = [1 2; 3 4] + AB2;

testCase.verifyEqual(AB2c.on(), tst)
testCase.verifyEqual(cAB2.on(), tst)

%% test plus on different domains
z = quantity.Domain("z", 0:3);
t = quantity.Domain("t", 0:4);
x = quantity.Domain("x", 0:5);

T = quantity.Discrete( t.grid, t);
Z = quantity.Discrete( z.grid, z);
X = quantity.Discrete( x.grid, x);

TZX = T+Z+X;

TZ1 = T + Z + 1;
TZX1 = subs(TZX, "x", 1);

testCase.verifyEqual( TZX1.on(), TZ1.on(), "AbsTol", 1e-12 );
testCase.verifyEqual( on( T + 0.5 + X  ), on( subs( TZX, "z", 0.5) ), "AbsTol", 1e-12 );

XTZ = X+T+Z;
testCase.verifyEqual( on( TZX + XTZ ), on( 2 * TZX ), "AbsTol", eps )

end

function testInit(tc)
%%
z = linspace(0,1).';
t = linspace(0,1,101);
v = {sin(z * t * pi); cos(z * t * pi)};
V = cat(3, v{:});

Z = quantity.Domain("z", z);
T = quantity.Domain("t", t);

a = quantity.Discrete(v, [Z, T]);
b = quantity.Discrete(V, [Z, T]);

%
verifyTrue(tc, all(a.on() == b.on(), "all"));

%%
zeta = T.grid;
[zGrid, zetaGrid] = ndgrid(z, zeta);
Z = quantity.Domain("z", z);
Zeta = quantity.Domain("zeta", zeta);

c = quantity.Discrete(zGrid, [Z, Zeta], "name", "a"); % this should work
tc.verifyError(...
	@() quantity.Discrete(zetaGrid.', [Z, Zeta], "name", "d"), ...% this should not work
	''); 

end

function testInt(testCase)
%%

z = linspace(0, 2*pi, 701)';
t = linspace(0, 3*pi, 501);

Z = quantity.Domain("z", z);
T = quantity.Domain("t", t);

F = {@(z,t) sin(z*t), @(z,t) cos(z*t)};

a = quantity.Discrete(cat(3, sin(z*t), cos(z*t)), [Z, T]);

At = int(a, "z");
Anumt = zeros(numel(t), numel(F));
for tau = 1 : numel(t)
	Anumt(tau, :) = [
		integral(@(z)F{1}(z, t(tau)), z(1), z(end)), ...
		integral(@(z)F{2}(z, t(tau)), z(1), z(end))];
end

verifyTrue(testCase, numeric.near(At.on(), Anumt, 1e-3));

Az = int(a, "t");
AnumZ = zeros(numel(z), numel(F));
for zIdx = 1 : numel(z)
	AnumZ(zIdx, :) = [...
		integral(@(t)F{1}(z(zIdx), t), t(1), t(end)), ...
		integral(@(t)F{2}(z(zIdx), t), t(1), t(end))];
end

verifyTrue(testCase, numeric.near(Az.on(), AnumZ, 1e-3));

A = int(a);
Anum = [integral2(@(z,t) sin(z.*t), z(1), z(end), t(1), t(end)); ...
	integral2(@(z,t) cos(z.*t), z(1), z(end), t(1), t(end))];

verifyTrue(testCase, numeric.near(A, Anum, 1e-2));

end

function testValue2Cell(testCase)
v = rand([100, 42, 2, 3]);
Z = quantity.Domain("z", linspace(0,1,100));
T = quantity.Domain("t", linspace(0,1,42));
V = quantity.Discrete( quantity.Discrete.value2cell(v, [100, 42], [2, 3]), [Z, T]);
verifyTrue(testCase, all(v == V.on(), "all"));
end

function testMtimesDifferentGrid(testCase)
%%
zetaA = linspace(0, 1, 21);
zA = linspace(0, 1);
[zetaAGrid, zAGrid] = ndgrid(zetaA, zA);
zX = linspace(0, 1, 21);
tX = linspace(1, 2, 31);
[zXGrid, tXGrid] = ndgrid(zX, tX);


d.a.zeta = quantity.Domain("zeta", zetaA);
d.a.z = quantity.Domain("z", zA);

d.x.z = quantity.Domain("z", zX);
d.x.t = quantity.Domain("t", tX);

a = quantity.Discrete({sin(zetaAGrid * pi), cos(zAGrid* pi)}, [d.a.zeta, d.a.z]);
x = quantity.Discrete({zXGrid; tXGrid}, [d.x.z, d.x.t]);

ax = a * x;
ax2 = x.' * a.';
% calculate reference result with stupid for loops
referenceResult = zeros(numel(zA), numel(zetaA), numel(tX));
aMat = a.on();
xMat = x.on({zA, tX});
for zIdx = 1:numel(zA)
	for zetaIdx = 1:numel(zetaA)
		for tIdx = 1:numel(tX)
			referenceResult(zIdx, zetaIdx, tIdx) = ...
				reshape(aMat(zetaIdx, zIdx, :, :), [1, 2]) ...
				* reshape(xMat(zIdx, tIdx, :), [2, 1]);
		end
	end
end

%%
testCase.verifyEqual(referenceResult, ax.on(), "AbsTol", 1e-12);
testCase.verifyEqual(referenceResult, permute(ax2.on(), [1, 3, 2]), "AbsTol", 1e-12);
%%
end

function testZeros(testCase)

z = quantity.Discrete.zeros([2, 7, 8], ...
	[quantity.Domain("a", linspace(0, 10)), quantity.Domain("b", linspace(0, 23, 11))]);
O = zeros(100, 11, 2, 7, 8);
testCase.verifyEqual(z.on(), O);

% test the creation of empty zeros:
e = quantity.Discrete.zeros([3, 0], quantity.Domain.defaultDomain(10));

testCase.verifyEmpty( e );

end % testZeros()

function testChangeDomain(testCase)

z = quantity.Domain("z", linspace(0, 1, 21).');
t = quantity.Domain("t", linspace(0, 1, 31));

quan = quantity.Discrete({sin(z.grid * t.grid.' * pi); cos(z.grid * t.grid.' * pi)}, [z, t]);
quanCopy = copy(quan);
%
quanSampled = quanCopy.changeDomain( [z, t] );
testCase.verifyEqual(quanSampled.on(), quan.on())

% test the change of two grids:
z2 = quantity.Domain("z", linspace(0, 1, 11));
t2 = quantity.Domain("t", linspace(0, 1, 12));
gridSampled = {z2.grid, t2.grid};
quanSampled = quanCopy.changeDomain( [z2, t2] );
testCase.verifyEqual(quanSampled.on(), quan.on(gridSampled))

% test the change of only one grid:
newZ = quantity.Domain("z",	linspace(0,1,5));
qNewZ = quanCopy.changeDomain( newZ );
testCase.verifyEqual(qNewZ.on(), quan.on([newZ t]));

% test the change of grids in wrong order
quanSampled2 = quan.changeDomain([t z]);
testCase.verifyEqual(quanSampled2.on(), quan.on());

% test the change of a quantity.EquidistantDomain with a quantity.Domain
e = quantity.EquidistantDomain( "z", 0, 1);
d = quantity.Domain("z", linspace(0,1, 3));

E = quantity.Discrete( sin(e.grid), e);
E_ = E.changeDomain(d);
testCase.verifyEqual(E_.on(), E.on(d))
end % testChangeDomain()

function testSubs(tc)
%%
myTestArray = ones(21, 41, 2, 3);
myTestArray(:,1,1,1) = linspace(0, 1, 21);
myTestArray(:,:,2,:) = 2;
z = quantity.Domain("z", linspace(0, 1, 21));
zeta = quantity.Domain("zeta", linspace(0, 1, 41));

quan = quantity.Discrete(myTestArray, [z, zeta]);
quanZetaZ = quan.subs(["z", "zeta"], "zeta", "z");

quan10 = quan.subs(["z", "zeta"], 1, 0);
quan01 = quan.subs(["z", "zeta"], 0, 1);
quant1 = quan.subs(["z", "zeta"], "t", 1);
quanZetaZeta = quan.subs("z", "zeta");
quanPt = quan.subs("z", 1);
quanEta = quan.subs("z", "eta");
%
tc.verifyEqual(quan10, shiftdim(quan.on({1, 0})));
tc.verifyEqual(quan01, shiftdim(quan.on({0, 1})));
tc.verifyEqual(quanZetaZ.on(), quan.on());
tc.verifyEqual(quant1.on(), squeeze(quan.on({quan(1).domain(1).grid, 1})));
tc.verifyEqual([quanZetaZ(1).domain.name], ["zeta", "z"])
tc.verifyEqual([quant1(1).domain.name], "t")

quanZetaZetaReference = misc.diagNd(quan.on({linspace(0, 1, 41), linspace(0, 1, 41)}), [1, 2]);
tc.verifyEqual(quanZetaZeta.on(), quanZetaZetaReference);
tc.verifyEqual([quanEta(1).domain.name], ["eta", "zeta"])
tc.verifyEqual(quanPt.on(), shiftdim(quan.on({1, quan(1).domain(2).grid})));

myQuantity = quanZetaZeta.subs("zeta", quantity.Domain("zeta", linspace(0,1,3)));
tc.verifyEqual(myQuantity(1).domain.n, 3);
tc.verifyEqual(myQuantity.on(), quanZetaZeta.on(linspace(0, 1, 3)));

% 4d-case
myTestArray4d = rand(11, 21, 31, 41, 1, 2);
x1 = quantity.Domain("z", linspace(0, 1, 11));
x2 = quantity.Domain("zeta", linspace(0, 1, 21));
x3 = quantity.Domain("eta", linspace(0, 1, 31));
x4 = quantity.Domain("beta", linspace(0, 1, 41));

quan4d = quantity.Discrete(myTestArray4d, [x1, x2, x3, x4], "name", "fun5d");
quan4dAllEta = quan4d.subs(["z", "zeta", "eta", "beta"], ["eta", "eta", "eta", "eta"]);
tc.verifyEqual(reshape(quan4d.on({1, 1, 1, 1}), size(quan4d)), ...
	reshape(quan4dAllEta.on({1}), size(quan4dAllEta)));
quan4dAllEta = quan4d.subs(["z", "zeta", "eta", "beta"], "eta", "eta", "eta", "eta");
tc.verifyEqual(reshape(quan4d.on({1, 1, 1, 1}), size(quan4d)), ...
	reshape(quan4dAllEta.on({1}), size(quan4dAllEta)));
eta = quantity.Domain("eta", linspace(0, 1, 11));
quan4dAllEta = quan4d.subs(["z", "zeta", "eta", "beta"], eta, [eta, eta], eta);
tc.verifyEqual(reshape(quan4d.on({1, 1, 1, 1}), size(quan4d)), ...
	reshape(quan4dAllEta.on({1}), size(quan4dAllEta)));

%
quan4dArbitrary = quan4d.subs(["z", "zeta", "eta", "beta"], ["zeta", "beta"], "z", 1);
tc.verifyEqual(reshape(quan4d.on({1, 1, 1, 1}), size(quan4d)), ...
	reshape(quan4dArbitrary.on({1, 1, 1}), size(quan4dAllEta)));

% test a substution where a quantity.EquidistantDomain is used:
e = quantity.EquidistantDomain("t", 0, 1, "stepSize", 0.1);
q = Discrete(e);
p = q.subs(e.name, "z");
tc.verifyEqual( p.domain.name, "z")
tc.verifyEqual( on( q.subs(e.name, z.grid) ), q.on(z.grid) )


end

function testZTTimes(testCase)
%%
z = quantity.Domain("z", linspace(0,1).');
t = quantity.Domain("t", linspace(0,1,101));

a = quantity.Discrete({sin(z.grid * pi), cos(z.grid* pi)}, z);
b = quantity.Discrete({sin(t.grid * pi); cos(t.grid * pi)}, t);

AB = a * b;

F = @(z,t) sin(z*pi) .* sin( t * pi) + cos(z*pi) .* cos( t * pi);

%%
verifyTrue(testCase, numeric.near(AB.on(), F(z.grid,t.grid')) );

end


function testZZTTimes(testCase)
%%
z = quantity.Domain("z", linspace(0,1).');
t = quantity.Domain("t", linspace(0,1,101));

a = quantity.Discrete({sin(z.grid * pi), cos(z.grid* pi)}, z);
b = quantity.Discrete({sin(z.grid * t.grid' * pi); cos(z.grid * t.grid' * pi)}, [z, t]);

%%
verifyTrue(testCase, numeric.near(b(1).on(), sin(z.grid * t.grid' * pi)));
verifyTrue(testCase, numeric.near(b(2).on(), cos(z.grid * t.grid' * pi)));

ab = a*b;

F = @(z,t) sin(z*pi) .* sin(z * t * pi) + cos(z*pi) .* cos(z * t * pi);
verifyTrue(testCase, numeric.near(ab.on(), F(z.grid,t.grid')) );
end

function testZZZTmTimes(testCase)

%%
z = linspace(0,1).';
t = linspace(0,2,101);

dom.z = quantity.Domain("z", z);
dom.t = quantity.Domain("t", t);

a = quantity.Discrete({sin(z * pi), cos(z* pi)}, dom.z);
b = quantity.Discrete({sin(z * t * pi); cos(z * t * pi)}, [dom.z, dom.t]);

A = a.' * a;

Anum = cat(3, sin(z * pi).^2 .* sin(z * t * pi) + sin(z * pi) .* cos(z * pi) .* cos(z * t * pi), ...
	sin(z * pi) .* cos(z * pi) .* sin(z * t * pi) + cos(z * pi).^2 .* cos(z * t * pi));
%
% figure(2);clf;
% subplot(211);
% misc.isurf( z, t, Anum(:,:,1))
% subplot(212);
% misc.isurf( z, t, Anum(:,:,2))

Ab = A * b;
verifyTrue(testCase, numeric.near(Ab.on(), Anum, 1e-9))


end

function testIsEmpty(tc)

% create an empty quantity and check if it is empty:
e = quantity.Discrete.empty([5, 0]);
tc.verifyTrue(isempty(e));

% create an empty object by calling the constructor without arguements.
tc.verifyTrue(isempty(quantity.Discrete()));

% create a constant quantity.Discrete. This should not be empty:
c = quantity.Discrete(1, quantity.Domain.empty);
tc.verifyTrue(~isempty(c));
end
