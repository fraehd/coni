function [tests] = testEquidistantDomain()
	tests = functiontests(localfunctions);
end

function setupOnce(testCase)
end


function testInit(testCase)
	dt = 0.123;
	
	d1 = quantity.EquidistantDomain('t', 0, 1, 'stepSize', dt);
	
	testCase.verifyEqual(d1.grid, (0:dt:1)');
	
	d2 = quantity.EquidistantDomain('t', 0, 1, 'stepSize', dt, ...
			'exactBoundary', quantity.DomainBoundary.upper);
	testCase.verifyEqual(d2.grid, flip( ( 1: -dt: 0)' ))
	
	d3 = quantity.EquidistantDomain('t', 0, 1, 'stepNumber', 3);
	testCase.verifyEqual(d3.grid, linspace(0, 1, 3)' )
	
	d4 = quantity.EquidistantDomain('t', 0, 1, 'stepSize', dt, ...
		'exactBoundary', quantity.DomainBoundary.extend);			
	tGrid = (0:dt:1)';
	tGrid = [tGrid; tGrid(end)+dt];
	testCase.verifyEqual(d4.grid, tGrid)
end

function testConcat(testCase)
e = quantity.EquidistantDomain('t', 0, 1);
d = quantity.Domain('z', linspace(0,1, 15));
	
E = [e, d];
D = [d; e];
testCase.verifyEqual(E(1), quantity.Domain(e))
testCase.verifyEqual(D(2), quantity.Domain(e))

EE(1:2) = quantity.Domain();
EE(1) = e;
EE(2) = d;

end

function testSubs(testCase)

f = quantity.Discrete((1:5)', quantity.EquidistantDomain("t", 0, 1, "stepNumber", 5));
testCase.verifyEqual( f.subs("t", 1), f.on(1))
testCase.verifyEqual( f.changeDomain(quantity.Domain("t", linspace(0,1))).on(), linspace(1,5)', 'AbsTol', 1e-15);

f = quantity.Symbolic(sym("t"), quantity.EquidistantDomain("t", 0, 1, "stepNumber", 5));
testCase.verifyEqual( f.subs("t", 1), f.on(1))
testCase.verifyEqual( f.changeDomain(quantity.Domain("t", linspace(0,1))).on(), linspace(0,1)', 'AbsTol', 1e-15);


end