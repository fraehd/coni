function [tests] = nearTest()
tests = functiontests(localfunctions);
end

function testCumIntegral(testCase)

Z = linspace(0,1)';
f = @(z) sin(z * pi);

F = numeric.cumIntegral(f, Z);
F_sym = int( sym(f) );

F_eval = double( subs( F_sym , 'z', Z ) ) - double( subs( F_sym, 'z', 0) );

testCase.verifyEqual(F, F_eval, 'AbsTol', 3e-16);

end