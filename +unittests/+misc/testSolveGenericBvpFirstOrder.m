function [tests] = testSolveGenericBvpFirstOrder()
	% result = runtests('unittests.misc.testSolveGenericBvpFirstOrder')
	tests = functiontests(localfunctions);
end % testSolveGenericBvpFirstOrder()

function testSolveGenericBvpFirstOrderNumerically(tc)
z = quantity.Domain("z", linspace(0, 1, 51));
S = blkdiag([0, 1; -1, 0], [0, 1; 0, 0], [0, 1; -1, 0]);
L = diag([3, -2, 1]) * quantity.Discrete.eye([3, 3], z);
n = 3;
p = 2;
m = 1;
E1 = [1, 0; 0, 0; 0, 1];
E2 = [0; 1; 0];
Az = quantity.Discrete.ones([n, n], z) / 2;
A = quantity.Discrete.ones([n, size(S, 1)], z) / 2;
A0 = quantity.Discrete.ones([n, p], z) / 2;
Q0 = ones(m, p);
B = ones([m, size(S, 1)]);
Cx = model.Output("Cx", "C1", eye([p, n]));
F = ones([p, size(S, 1)]);
Cw = ones([2, 4]);
C = eye(4)*2 + reshape(linspace(-3, 2, 16), [4, 4]);
D = ones([4, size(S, 1)]) * 0.8;
E = ones([4, p]);

% do calculation
[X, W] = misc.solveGenericBvpFirstOrder(S, L, Az, A, A0, Q0, B, Cx, F, ...
	"Cw", Cw, "C", C, "D", D, "E", E);

% verifications
res.ode = L * X.diff() + Az * X + A0 * E1.' * X.at(0) - X * S - A;
tc.verifyEqual(mean(abs(res.ode), "all"), 0, "AbsTol", 9e-4);

res.bc0 = (E2.' - Q0 * E1.') * X.at(0) - B;
tc.verifyEqual(res.bc0, zeros(size(res.bc0)), "AbsTol", 1e2 * eps);

res.bcC = Cx.out(X) + Cw * W - F;
tc.verifyEqual(res.bcC, zeros(size(res.bcC)), "AbsTol", 1e2 * eps);

res.syl = C * W - W * S - D - E * E1.' * X.at(0);
tc.verifyEqual(res.syl, zeros(size(res.syl)), "AbsTol", 1e2 * eps);
end % testSolveGenericBvpFirstOrderNumerically()
