function [tests] = testBackstepping()
%testBackstepping Summary of this function goes here
%   Detailed explanation goes here
tests = functiontests(localfunctions());
end

function testGradient(tc)
syms z zeta
myGrid = linspace(0, 1, 31);
myDomain = [quantity.Domain('z', myGrid), quantity.Domain('zeta', myGrid)];
kernelData = quantity.Discrete(quantity.Symbolic(...
	[z^2/2, sin(z); 1/(2-z), -z] * magic(2) * [-zeta^2/2, sin(2*zeta); 1/(2-zeta), zeta], ...
	myDomain, 'name', 'K'));

% calculate derivatives symbolic on raw data
domainSelector = quantity.Discrete(quantity.Symbolic(zeta<=z, ...
	myDomain, 'name', ''));
kernelData_dz = domainSelector * quantity.Symbolic(...
	diff([z^2/2, sin(z); 1/(2-z), -z] * magic(2) * [-zeta^2/2, sin(2*zeta); 1/(2-zeta), zeta], z), ...
	myDomain, 'name', 'K');
kernelData_dzeta = domainSelector * quantity.Symbolic(...
	diff([z^2/2, sin(z); 1/(2-z), -z] * magic(2) * [-zeta^2/2, sin(2*zeta); 1/(2-zeta), zeta], zeta), ...
	myDomain, 'name', 'K');

% set up kernel data in misc.Backstepping objects
k = misc.Backstepping('kernelInverse', kernelData, ...
	'signOfIntegralTermInverse', +1, 'integralBounds', {0, 'z'});
tc.verifyEqual(domainSelector.on(), k(1).domainSelector.on())

[K_dzeta, K_dz] = gradient(k, 'kernelInverse');

tc.verifyEqual(median(abs(kernelData_dz-K_dz)), zeros(size(K_dz)), 'AbsTol', 1e-3);
tc.verifyEqual(median(abs(kernelData_dzeta-K_dzeta)), zeros(size(K_dz)), 'AbsTol', 1e-3);

end

function testInvert(testCase)
% init random kernel
syms z zeta
myGrid = linspace(0, 1, 21);
myDomain = [quantity.Domain("z", myGrid), quantity.Domain("zeta", myGrid)];
kernelData = quantity.Discrete(quantity.Symbolic(...
	[z^2/2, sin(z); 1/(2-z), -z] * magic(2) * [-zeta^2/2, sin(2*zeta); 1/(2-zeta), zeta], ...
	myDomain, "name", "K"));

% set up kernel data in misc.Backstepping objects
k(1) = misc.Backstepping("kernel", kernelData, ...
	"signOfIntegralTerm", +1, "integralBounds", {0, "z"});
k(2) = misc.Backstepping("kernel", kernelData, ...
	"signOfIntegralTerm", -1, "integralBounds", {0, "z"});
k(3) = misc.Backstepping("kernel", kernelData, ...
	"signOfIntegralTerm", +1, "integralBounds", {"z", 1});
k(4) = misc.Backstepping("kernel", kernelData, ...
	"signOfIntegralTerm", -1, "integralBounds", {"z", 1});
% inverse
k(5) = misc.Backstepping("kernelInverse", kernelData, ...
	"signOfIntegralTermInverse", +1, "integralBounds", {0, "z"});
k(6) = misc.Backstepping("kernelInverse", kernelData, ...
	"signOfIntegralTermInverse", -1, "integralBounds", {0, "z"});
k(7) = misc.Backstepping("kernelInverse", kernelData, ...
	"signOfIntegralTermInverse", +1, "integralBounds", {"z", 1});
k(8) = misc.Backstepping("kernelInverse", kernelData, ...
	"signOfIntegralTermInverse", -1, "integralBounds", {"z", 1});

% invert all
for it = 1 : numel(k)
	k(it).invert(-1, 1e-3, true);
	k(it).verifyInversion(5e-3);
end

% compare to old implementation for int_0^z
myDomain = quantity.Discrete(quantity.Symbolic(zeta<z, myDomain));
zZetaDomain = [quantity.Domain('z', myGrid), quantity.Domain('zeta', myGrid)];
Kref = quantity.Discrete(...
	misc.invertBacksteppingKernel(kernelData.on(zZetaDomain), -1), ...
	zZetaDomain);
k1Diff = myDomain * (Kref - k(1).kernelInverse);
k5Diff = myDomain * (Kref - k(5).kernel);
testCase.verifyEqual(MAX(abs(k1Diff)), 0, 'AbsTol', 2e-4);
testCase.verifyEqual(MAX(abs(k5Diff)), 0, 'AbsTol', 2e-4);

end
