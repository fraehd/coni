function [tests] = testGains()
%TESTGAINS Summary of this function goes here
%   Detailed explanation goes here
tests = functiontests(localfunctions());
end

function testMtimes(tc)
e1 = misc.Gain("in1", ones(3, 2), ...
	"outputType", "bliblublu");
e2 = misc.Gain("in2", ones(3, 2),...
	"outputType", "xyz");
ee = e1 + e2;
a = triu(ones(3));

ae1 = mtimes(e1, a);
ae2 = mtimes(e2, a);
ae1ae2 = ae1 + ae2;

aee = mtimes(ee, a);

tc.verifyEqual(blkdiag(a, a) * ee.value, ae1ae2.value);
tc.verifyEqual(blkdiag(a, a) * ee.value, aee.value);
tc.verifyEqual(ee.outputType, ae1ae2.outputType);
tc.verifyEqual(ee.outputType, aee.outputType);
tc.verifyEqual(ee.inputType, ae1ae2.inputType);
tc.verifyEqual(ee.inputType, aee.inputType);
end % testMtimes

function testBlkdiag(tc)
% misc.Gain
g1 = misc.Gain("control", [2, 3], "outputType", "measurement");
g2 = misc.Gain("control", [3, 4], "outputType", "measurement");

g12 = blkdiag(g1, g2);
tc.verifyEqual(g12.value, blkdiag(g1.value, g2.value));
tc.verifyEqual(g12.inputType, g1.inputType);
tc.verifyEqual(g12.outputType, g1.outputType);

% misc.Gains
G1 = g1 + misc.Gain("disturbance", [0.5], "outputType", "controlOutput") ...
	+ misc.Gain("fault", [1.5], "outputType", "measurement");
G2 = g2 + misc.Gain("disturbance", [-0.5], "outputType", "controlOutput") ...
	+ misc.Gain("fault", [2.5], "outputType", "measurement");
G12 = blkdiag(G1, G2);
control1 = [2; 3];
control2 = [3; 4];
disturbance1 = 2;
disturbance2 = 3;
fault1 = 0.23;
fault2 = 42;
outputG12 = G12.value * [control1; control2; disturbance1; disturbance2; fault1; fault2];
outputG1 = G1.value * [control1; disturbance1; fault1];
outputG2 = G2.value * [control2; disturbance2; fault2];
outputG1mergeG2 = [outputG1(1, :); outputG2(1, :); outputG1(2, :); outputG2(2, :)];
tc.verifyEqual(outputG12, outputG1mergeG2);
tc.verifyEqual(G12.inputType, G1.inputType);
tc.verifyEqual(G12.outputType, G1.outputType);
end % testBlkdiag()

function testRemove(tc)
a = misc.Gain("in1", ones(10),...
	"outputType", ["x", "y", "xy", "X"], ...
	"lengthOutput", [2; 3; 4; 1]);
A = misc.Gains(a);
A.remove("asdf");
tc.verifyEqual(a.value, A.value)
B = copy(A);
tc.verifyTrue(isempty(B.remove("in1")));

e1 = misc.Gain("in1", ones(3, 2), ...
	"outputType", ["bliblublu", "bliblablub"], ...
	"lengthOutput", [2; 1]);
e2 = misc.Gain("in2", ones(4, 2),...
	"outputType", ["xyz", "bliblabla"], ...
	"lengthOutput", [2; 2]);
ee = copy(e1 + e2);
tc.verifyTrue(isequal(ee.remove("in1").gain(1), e2));
end % testRemove()

function testRemoveOutput(tc)
e1 = misc.Gain("in1", ones(3, 2), ...
	"outputType", ["bliblublu", "bliblablub"], ...
	"lengthOutput", [2; 1]);
e2 = misc.Gain("in2", ones(4, 2),...
	"outputType", ["xyz", "bliblabla"], ...
	"lengthOutput", [2; 2]);
ee = copy(e1 + e2);
ee = ee.removeOutput("xyz");
ee = ee.removeOutput("bliblabla");
tc.verifyTrue(isequal(ee, misc.Gains(e1)))
end % testRemove()

function testStrrepOutputType(tc)
e1 = misc.Gain("in1", ones(3, 2), ...
	"outputType", ["bliblublu", "bliblablub"], ...
	"lengthOutput", [2; 1]);
e2 = misc.Gain("in2", ones(4, 2),...
	"outputType", ["xyz", "bliblabla"], ...
	"lengthOutput", [2; 2]);
ee = e1 + e2;
e1BLA = strrepOutputType(copy(e1), "bla", "BLA");
e2BLA = strrepOutputType(copy(e2), "bla", "BLA");
eBLA = strrepOutputType(copy(ee), "bla", "BLA");
tc.verifyEqual(e1BLA.outputType, ["bliblublu"; "bliBLAblub"]);
tc.verifyEqual(e2BLA.outputType, ["xyz"; "bliBLABLA"]);
tc.verifyEqual(eBLA.outputType, ["bliblublu"; "bliBLAblub"; "xyz"; "bliBLABLA"]);
end % testStrrepOutputType()

function testEqual(tc)
a = misc.Gain("in1", ones(10),...
	"outputType", ["x", "y", "xy", "X"], ...
	"lengthOutput", [2; 3; 4; 1]);
b = copy(a);
e1 = misc.Gain("in1", ones(10)+blkdiag(zeros(4), 1, zeros(5)), ...
	"outputType", ["x", "y", "xy", "X"], ...
	"lengthOutput", [2; 3; 4; 1]);
e2 = misc.Gain("in1", ones(10),...
	"outputType", ["x", "y", "xw", "X"], ...
	"lengthOutput", [2; 3; 4; 1]);

tc.verifyTrue(isequal(a, b));
tc.verifyFalse(isequal(a, a, e1));
tc.verifyFalse(isequal(a, e2, a));
tc.verifyTrue(isequal(a+b+e1, a+b+e1));
tc.verifyTrue(isequal(a+b+e1, a+e1+b));

% misc.Gains
e12 = misc.Gain("in2", ones(10)+blkdiag(zeros(4), 1, zeros(5)), ...
	"outputType", ["x", "y", "xy", "X"], ...
	"lengthOutput", [2; 3; 4; 1]);
b2 = misc.Gain("in3", ones(10),...
	"outputType", ["x", "y", "xy", "X"], ...
	"lengthOutput", [2; 3; 4; 1]);
tc.verifyTrue(isequal(a+b2+e12, a+b2+e12));
tc.verifyFalse(isequal(a+b2+e12, a+e12+b2));
end % testEqual()

function testExchange(tc)
a = misc.Gain("in1", ones(10),...
	"outputType", ["x", "y", "xy", "X"], ...
	"lengthOutput", [2; 3; 4; 1]);
newY = misc.Gain("in1", 2*ones(3, 10), "outputType", "y");
a.exchange(newY);
tc.verifyEqual(a.valueOfOutput("y"), 2*ones(3, 10));
end % testExchange()

function testGainParallel(tc)
a = misc.Gain("in", ones(2),...
	"outputType", ["x", "y"], ...
	"lengthOutput", [1; 1]);
b = misc.Gain("in", ones(1, 2),...
	"outputType", ["y"]);
c = parallel(a, b);
tc.verifyEqual(c.valueOfOutput("x"), [1, 1]);
tc.verifyEqual(c.valueOfOutput("y"), 2*[1, 1]);

d = parallel(b, a);
tc.verifyEqual(d.valueOfOutput("x"), [1, 1]);
tc.verifyEqual(d.valueOfOutput("y"), 2*[1, 1]);
%d = parallel(b, a);
end % testGainParallel()


function testGain2Gains(tc)
a = misc.Gain("in1", ones(2),...
	"outputType", ["x", "y"], ...
	"lengthOutput", [1; 1]);
b = misc.Gain("in2", ones(1, 2),...
	"outputType", ["y"]);
c = a + b;
tc.verifyEqual(c.value, [ones(2), [zeros(1, 2); ones(1, 2)]]);
end % testGains2Gains()

function testGains2Gains(tc)
a = misc.Gains(misc.Gain("in", ones(2),...
	"outputType", ["x", "y"], ...
	"lengthOutput", [1; 1]));
b = misc.Gains(misc.Gain("in", ones(1, 2),...
	"outputType", ["y"]));
c = a + b;
tc.verifyEqual(c.value, a.value + [zeros(1, 2); b.value]);
end % testGains2Gains()

function testLengthOfOutput(tc)
a = misc.Gain("in1", ones(10),...
	"outputType", ["x", "y", "xy", "X"], ...
	"lengthOutput", [2; 3; 4; 1]);
tc.verifyEqual(a.lengthOfOutput("x", "y"), [2; 3])
tc.verifyEqual(a.lengthOfOutput("y", "x"), [3; 2])
tc.verifyEqual(a.lengthOfOutput("x", "y", "xy", "X"), [2; 3; 4; 1])
tc.verifyEqual(a.lengthOfOutput("X", "x", "y", "xy"), [1; 2; 3; 4])
end % testLengthOfOutput()