%% test misc.jordanReal.m
function [tests] = testJordanReal()
	% result = runtests('unittests.misc.testJordanReal')
	tests = functiontests(localfunctions);
end

function testDiag2d(tc)
%% todo: fix this bug for multiple complex eigenvalues
mySubSystems = {[0, 1; -1, 0], [0, 1; -1, 0], [0, 1, 0; 0, 0, 1; 0, 0, 0], eye(2)};
A = blkdiag(mySubSystems{:});
[V, J, l] = misc.jordanReal(A);
tc.verifyEqual(l(:), [3; 1; 1; 2; 2]);
tc.verifyEqual(sort(eig(A)), sort(eig(J)));
tc.verifyEqual(A * V, V * J, 'AbsTol', 10*eps);
end