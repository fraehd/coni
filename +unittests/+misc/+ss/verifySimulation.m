classdef verifySimulation < matlab.unittest.TestCase
	% class containing only static methods to be called in other unittests in order
	% to verify simulation results for dps.wave.SystemOde.
	
	methods (Static = true)
		
		function odes(tc, odes, simData, AbsTol)
			mySs = odes.odes;
			% get state
			w = misc.getfield(simData, odes.type{1});
			for it = 2 : odes.numTypes
				w = [w; misc.getfield(simData, odes.type{it})];
			end
			residuum = - w.diff("t", 1) + mySs.A * w;
			
			% get input
			inputNames = misc.ss.removeEnumeration(odes.InputName, true);
			if ~isempty(inputNames)
				u = misc.getfield(simData, inputNames{1});
				for it = 2 : numel(inputNames)
					u = [u; misc.getfield(simData, inputNames{it})];
				end
				residuum = residuum + mySs.B * u;
			end
			
% 			plot(residuum);
% 			figure(); t = w.domain.grid;
% 			plot(t, on(- w.diff("t", 1) + mySs.A * w)); hold on;
% 			plot(t, -on(mySs.B * u));
			
			residuumMat = residuum.abs.on();
 			tc.verifyLessThan(median(residuumMat(2:end-1)), AbsTol, ...
				['misc.ss.odes median fault: ' num2str(median(residuumMat(:)))]);
		end % odes()
		
	end % methods (Static = true)
end % classdef