function [outputArg1,outputArg2] = testMFilter()


tests = functiontests(localfunctions);

end

function testScalarFilter(testCase)

%%
    t = linspace(-pi,pi,100)';
    rng default  %initialize random number generator
    x = sin(t) + 0.25*rand(size(t));
    
    windowSize = 5; 
    b = (1/windowSize)*ones(windowSize, 1);
    
    y = signals.mFilter(b, x);
    
    Y = filter(b, 1, x);
    
    assert(max(abs(y-Y)) == 0);
    
end


function testVectorFilter(testCase)
%%
    t = linspace(-pi,pi,100)';
    rng default  %initialize random number generator
    x = sin(t) + 0.25*rand(size(t));
    
    windowSize = 5; 
    b1 = (1/windowSize)*ones(windowSize, 1) * 0.1;
    b2 = (1/windowSize)*ones(windowSize, 1) * 0.2;
    
    b = [b1, b2];
    
    y = signals.mFilter(b, x);
    
    Y1 = filter(b1, 1, x);
    Y2 = filter(b2, 1, x);

    assert(max(max(y - [Y1, Y2])) == 0)
    
end


function testMatrixFilter(testCase)
%%
    t = linspace(-pi,pi,100)';
    rng default  %initialize random number generator
    x1 = sin(t) + 0.25*rand(size(t));
    x2 = cos(t) + 0.5 * rand(size(t));
    x = [x1, x2];
    
    windowSize = 5; 
    b1 = ones(windowSize,1) * 1;
    b2 = ones(windowSize,1) * 2;
    b3 = ones(windowSize,1) * 3;
    b4 = ones(windowSize,1) * 4;
    
    % b = [b1 b2; b3 b4];
    b = reshape([b1 b3 b2 b4], windowSize, 2, 2)
    
    y = signals.mFilter(b, x);
    
    Y1 = filter(b1, 1, x1) + filter(b2, 1, x2);
    Y2 = filter(b3, 1, x1) + filter(b4, 1, x2);

    assert(max(max(y - Y)) == 0)

end