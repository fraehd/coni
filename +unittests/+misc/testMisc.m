function [tests] = testMisc()
%TESTMISC Summary of this function goes here
%   Detailed explanation goes here
tests = functiontests(localfunctions());
end

function testStructMerge(tc)
newStruct = misc.structMerge(struct("asdf", 42), struct("blub", 1337));
tc.verifyEqual(misc.fieldnames(newStruct), ["asdf"; "blub"]);
tc.verifyEqual(newStruct.asdf, 42);
tc.verifyEqual(newStruct.blub, 1337);
%
newStruct = misc.structMerge(misc.setfield(struct(), "asdf.blub", 42), struct("asdf", 1337), ...
	"noOverwrite", false);
tc.verifyEqual(misc.fieldnames(newStruct), "asdf");
tc.verifyEqual(newStruct.asdf, 1337);
%
fun = @() misc.structMerge(misc.setfield(struct(), "asdf.blub", 42), struct("asdf", 1337), ...
	"noOverwrite", true);
tc.verifyError(fun, "misc:structMerge:overwriteField");
%
newStruct = misc.structMerge(misc.setfield(struct(), "asdf.blub", 42), ...
	misc.setfield(struct(), "asdf.bli", 1337), "noOverwrite", true);
tc.verifyEqual(misc.fieldnames(newStruct), ["asdf.blub"; "asdf.bli"]);
tc.verifyEqual(newStruct.asdf.blub, 42);
tc.verifyEqual(newStruct.asdf.bli, 1337);
end % testStructMerge()

function testFieldnames(tc)
fieldNames = ["asdf", "blub.asdf.moeb", "bli.bla.blub", "tet.ris", "tetris", "ASDF", ...
	"simData.plant.control", "simData.plant.controlOutput", "simData.flatOutput", ...
	"blub.apfelsaft", "bli.blub.bla", "sonnen.schein", "sonne", "blub.asdf.blub.asdf"];

myStruct = struct();
for thisFieldName = fieldNames
	myStruct = misc.setfield(myStruct, thisFieldName, rand(1));
end
fieldNamesFound = misc.fieldnames(myStruct);
tc.verifyEmpty(setdiff(fieldNames, fieldNamesFound));
end % testFieldnames()

function testPlace(tc)
A = magic(2);
B = ones(2, 1);
K = misc.place(A, B, 0*B);
tc.verifyEqual(eig(A - B*K), 0*B, "AbsTol", 1e-6);

A = [zeros(4, 2), eye(4, 2)];
B = flipud(A(:, 3:end));
K = misc.place(A, B, 0*A(:, 1)-1);
tc.verifyEqual(eig(A - B*K), 0*A(:, 1)-1, "AbsTol", 1e-6);

A = [0, 1; -1, 0];
B = eye(2, 1);
K = misc.place(A, B, 0*A(:, 1)-1);
tc.verifyEqual(eig(A - B*K), 0*A(:, 1)-1, "AbsTol", 1e-6);

A =	[0, 0, 1, 0; 0, 0, 0, 1; 0, 0, -sqrt(2), 0; 0, 0, 0, -pi];
B = [0, 0; 0, 0; 0, sqrt(2); exp(1), 0];
K = misc.place(A, B, 0*A(:, 1)-5);
tc.verifyEqual(eig(A - B*K), 0*A(:, 1)-5, "AbsTol", 1e-6);

A = blkdiag([0, 1; -1, 0], [0, 1; -1, 0]);
B = blkdiag(eye(2, 1), eye(2, 1));
K = misc.place(A, B, 0*A(:, 1)-1);
tc.verifyEqual(eig(A - B*K), 0*A(:, 1)-1, "AbsTol", 1e-6);

A = rot90(diag([1, 1, -1, -1]));
K = misc.place(A, B, 0*A(:, 1)-5);
tc.verifyEqual(eig(A - B*K), 0*A(:, 1)-5, "AbsTol", 1e-6);
end % testPlace()

function testReporter(tc)

m = misc.Message("hallo");
r = misc.Reporter();
r.report("test blabalb ablab");
disp("======================");
r.showMessage = false;
r.report("test blabla lbbubl");
r.printMessages()

end % testReporter(tc)

function testStr2cell(tc)
testData = ["a", "sd", "f"; "bli", "asdf", "blub"];
testDataCell = {"a", "sd", "f"; "bli", "asdf", "blub"};
tc.verifyEqual(misc.str2cell(testData), testDataCell);
end % testStr2celll(tc)

function testGetDimensions(tc)

d = quantity.Domain("z", linspace(0, 1, 3));

args.B_1 = quantity.Discrete.zeros( [3, 3], d );
args.B_2 = zeros(3,2);
args.B_3 = signals.PolynomialOperator({ zeros(4,1), zeros(4,1)});
args.B_4 = [];
args.C_1 = zeros(3, 5);

[dims, sizes] = misc.getDimensions(args, ["B_1", "B_2", "B_3", "B_4"], 2);

tc.verifyEqual(dims, 3);
tc.verifyEqual(sizes, (3:-1:0)')

end

function testFunctionArguments(tc)

	% single variable case
	names = misc.functionArguments(@(z) sin(z));
	tc.verifyEqual(names, "z");

	% multi variable case
	names = misc.functionArguments(@(z, zeta, t) z*zeta+t);
	tc.verifyEqual(names, ["z"; "zeta"; "t"]);
	
	% none variable case
	names = misc.functionArguments(@() sin(4));
	tc.verifyEqual(names, "");
		
end % testFunctionArguments()

function testBinomial(testCase)

for k = 1:3
	for n = k:k+3
	% test the integer order
	b1 = nchoosek(n, k);
	b2 = misc.binomial(n, k);
	testCase.verifyEqual(b1, b2);	
	end
end

% test the non integer case - example from wikipedia
testCase.verifyEqual( misc.binomial(2.5, 2), 1.875)
testCase.verifyEqual( misc.binomial(-1, 4), (-1)^4)

for k = -1:3
	% test non integer over zero
	testCase.verifyEqual( misc.binomial( k*0.3, 0), 1);
	% test non integer over one
	testCase.verifyEqual( misc.binomial( k*0.3, 1), k * 0.3, 'AbsTol', 1e-16);
end

end

function testUnique4cells(tc)
Moriginal = {zeros(2), 0, rand(3), [0, 1; 0, 0], "asdf", 0, 'asdf', [0, 1; 0, 0], eps, eps};
Mexpected = {zeros(2), 0, Moriginal{3}, [0, 1; 0, 0], "asdf", eps};
Munique = misc.unique4cells(Moriginal);
tc.verifyEqual(Mexpected, Munique);
end % testUnique4cells()

function testBlkdiagInv(tc)
Moriginal = {zeros(2), 0, rand(3), [0, 1; 0, 0]};
lengthBlock = zeros(size(Moriginal));
for it = 1 : numel(lengthBlock)
	lengthBlock(it) = size(Moriginal{it}, 1);
end % for it = 1 : numel(lengthBlock)
M = blkdiag(Moriginal{:});

Mrestored = misc.blkdiagInv(M, lengthBlock);
tc.verifyEqual(Moriginal, Mrestored);
end % testBlkdiagInv()

function testEnsureString(tc)

a = "a";
b = 'b';

tc.verifyEqual( misc.ensureString(a), a)
tc.verifyEqual( misc.ensureString(b), string(b))
tc.verifyEqual( misc.ensureString({a, b}), [a, string(b)])
tc.verifyEqual( misc.ensureString({b, b}), [string(b), string(b)])

end

function testEyeNd(tc)
n = 2;
tc.verifyEqual(misc.eyeNd([n, n]), eye(n));
example3d = misc.eyeNd([n, n, n]);
for it = 1 : size(example3d, 1)
	for jt = 1 : size(example3d, 2)
		for kt = 1 : size(example3d, 3)
			if (it == jt) && (jt == kt)
				tc.verifyEqual(example3d(it, jt, kt), 1);
			else
				tc.verifyEqual(example3d(it, jt, kt), 0);
			end
		end
	end
end
%
n = 3;
tc.verifyEqual(misc.eyeNd([n, n]), eye(n));
example3d = misc.eyeNd([n, n, n]);
for it = 1 : size(example3d, 1)
	for jt = 1 : size(example3d, 2)
		for kt = 1 : size(example3d, 3)
			if (it == jt) && (jt == kt)
				tc.verifyEqual(example3d(it, jt, kt), 1);
			else
				tc.verifyEqual(example3d(it, jt, kt), 0);
			end
		end
	end
end
end % testEyeNd()

function testNameValuePairRemoveDots(tc)
nameValuePairsCleaned = misc.nameValuePairRemoveDots('asdf', 123', 'bli.bla', 'test');
tc.verifyEqual(nameValuePairsCleaned, {'asdf', 123});
end % testNameValuePairRemoveDots()

function testIsfield(tc)
myStruct = misc.setfield(struct(), 'topLevel.lowerLevel.data', 42);
tc.verifyTrue(misc.isfield(myStruct, 'topLevel'));
tc.verifyEqual(misc.isfield(myStruct, ["topLevel"; "lowerLevel.data"]), [true; false]);
tc.verifyTrue(misc.isfield(myStruct, 'topLevel.lowerLevel'));
tc.verifyTrue(misc.isfield(myStruct, 'topLevel.lowerLevel.data'));
tc.verifyFalse(misc.isfield(myStruct, 'lowerLevel.data'));
tc.verifyFalse(misc.isfield(myStruct, 'abc'));
end

function testGetfield(tc)
myStruct = misc.setfield(struct(), 'topLevel.lowerLevel.data', 42);
result = misc.getfield(myStruct, 'topLevel.lowerLevel.data');
tc.verifyEqual(result, 42);

myStruct = struct('a', 1, 'b', 2, 'c', 3);
tc.verifyEqual(misc.getfield(myStruct, ["a", "c"]), [1, 3]);
end

function testSetfield(tC)
myStruct = struct('asdf', 123);
myStruct = misc.setfield(myStruct, 'topLevel.lowerLevel.data', 42);
myStruct = misc.setfield(myStruct, 'topLevel.asdf', 1337);
myStruct = misc.setfield(myStruct, 'topLevel.lowerLevel.dieter', 3);

tC.verifyEqual(myStruct.asdf, 123);
tC.verifyEqual(myStruct.topLevel.lowerLevel.data, 42);
tC.verifyEqual(myStruct.topLevel.lowerLevel.dieter, 3);
tC.verifyEqual(myStruct.topLevel.asdf, 1337);
end

function testIsunique(testCase)
testCase.verifyTrue(misc.isunique({'bli', 'bla', 'blu'}));
testCase.verifyTrue(misc.isunique([1, 2, 3]));
testCase.verifyFalse(misc.isunique([1, 2, 1]));
testCase.verifyFalse(misc.isunique({'bli', 'bla', 'blu', 'bla'}));
testCase.verifyTrue(misc.isunique({'bli', 'bla', 'blu'}.'));
testCase.verifyTrue(misc.isunique([1, 2, 3].'));
testCase.verifyFalse(misc.isunique([1, 2, 1].'));
testCase.verifyFalse(misc.isunique({'bli', 'bla', 'blu', 'bla'}.'));
end


function testMTimesPointWise(testCase)

z = linspace(0, 2*pi, 1e2)';

A = cat(3, [sin(z), cos(z)],  [tan(z), sinh(z)]);
B = cat(3, [sin(z), cos(z)],  [tan(z), sinh(z)], [z, z.^2]);
C = misc.mTimesPointwise(A, B);

% symbolic computation for verification
Z = sym('z', 'real');
D = [sin(Z), tan(Z); cos(Z), sinh(Z)];
E = [sin(Z), tan(Z) Z; cos(Z) sinh(Z) Z^2];
F = D*E;

testCase.verifyTrue(numeric.near(C, ...
	reshape(double(subs(F, Z, z)), length(z), 2, 3), 1e-9));

end

function testGetInputNamesOfFunctionHandle(testCase)
myFun = @(z, zeta, x) sin(z)*zeta;
inputArgs = misc.getInputNamesOfFunctionHandle(myFun);
testCase.verifyEqual({'z', 'zeta', 'x'}, inputArgs);
end