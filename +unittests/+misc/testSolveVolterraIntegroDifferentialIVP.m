classdef testSolveVolterraIntegroDifferentialIVP < matlab.unittest.TestCase
	% result = runtests('unittests.misc.testSolveVolterraIntegroDifferentialIVP')
	
	properties
		doPlot = false;
		z = quantity.Domain("z", linspace(0, 1, 101));
		zeta;
		ic = [-1, 1, 0; 1, 0, -0.5];
		M;
		A;
		B;
		C;
		D;
		N;
	end % properties
		
	methods(TestClassSetup)
		function tc = initData(tc)
			tc.zeta = quantity.Domain("zeta", tc.z.grid);
			tc.M = quantity.Symbolic([1+sym("z"), sin(sym("z")); 0, 2], tc.z);
			tc.A = quantity.Symbolic([1+sym("z"), 0; -sym("z")^2, 2], tc.z);
			tc.B = quantity.Symbolic([1+sym("z"), 0, 0.1; 0.2, 0, 2; -1, 0.5, 0.5], tc.z);
			tc.C = quantity.Symbolic([1+sym("z"), 0, -1; 0, 2, -1], tc.z);
			tc.D = quantity.Symbolic([1+sym("z"), sym("z")*sym("zeta"); sym("zeta"), 2], [tc.z, tc.zeta]);
			tc.N = misc.solveVolterraIntegroDifferentialIVP(tc.z, tc.ic, ...
				"M", tc.M, "A", tc.A, "B", tc.B, "C", tc.C, "D", tc.D);
		end
	end % methods(TestClassSetup)
	
	methods (Test)
		
		function testSolution(tc)
			% verify initial condition
			tc.verifyEqual(tc.ic, tc.N.at(0), "AbsTol", 10*eps);
			
			% verify ODE
			odeResiduum = tc.M * tc.N.diff("z", 1) + tc.A * tc.N + tc.N * tc.B + tc.C ...
				+ cumInt(tc.D * tc.N.subs("z", "zeta"), "zeta", 0, "z");
			tc.verifyEqual(max(odeResiduum.abs.median(), [], "all"), 0, "AbsTol", 1e-4);
			
			% plot
			if tc.doPlot
				tc.N.plot();
				odeResiduum.plot();
			end
		end % testSolution
		
		function testConvergence(tc)
			if tc.doPlot
				gridPoints = [11, 21, 41];
				Ncell = cell(numel(gridPoints), 1);
				medianDifference = 0 * gridPoints;
				for it = 1 : numel(gridPoints)
					Ncell{it} = misc.solveVolterraIntegroDifferentialIVP(...
						quantity.Domain("z", linspace(0, 1, gridPoints(it))), ...
						tc.ic, "M", tc.M, "A", tc.A, "B", tc.B, "C", tc.C, "D", tc.D);
					medianDifference(it) = max(median(abs(tc.N - Ncell{it})), [], "all");
				end % for it = 1 : numel(gridPoints)
				
				% decay
				figure()
				plot(gridPoints, medianDifference, "x");
				
				% comparison
				figure();
				idx = 1;
				for jt = 1 : size(tc.ic, 1)
					for it = 1 : size(tc.ic, 2)
						subplot(size(tc.ic, 1), size(tc.ic, 2), idx);
						plot(tc.N(1).grid{1}, tc.N(jt, it).on());
						hold on;
						for kt = 1 : numel(gridPoints)
							plot(Ncell{kt}(1).grid{1}, Ncell{kt}(jt, it).on());
						end % for it = 1 : numel(gridPoints)
						idx = idx + 1;
					end
				end
			end
		end % testSolution
		
	end % methods (Test)
	
end % classdef