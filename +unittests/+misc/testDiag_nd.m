%% test misc.diagNd.m
function [tests] = testDiag_nd()
	% result = runtests('unittests.misc.testDiagNd')
	tests = functiontests(localfunctions);
end

function testDiag2d(testCase)
%% Matrix
testData = rand(123, 123);
testCase.verifyEqual(diag(testData), misc.diag_nd(testData));
end

function testDiagNdComplicated(testCase)
test1.data = rand(2, 2, 3, 4, 2, 5);
test1.result = misc.diag_nd(test1.data);
test1.forResult = zeros(2, 3, 4, 2, 5);

for it = 1 : 2
	test1.forResult(it,:,:,:,:) = squeeze(test1.data(it, it, :, :, :, :));
end

testCase.verifyEqual(test1.forResult, test1.result);
end
