classdef testGetMinimalInternalModel < matlab.unittest.TestCase
	
	methods (Test)
		
		function testVariousMatrices(tc)
			ramp = [0, 1; 0, 0];
			sine = [0, 1; -1, 0];
			sinePi = [0, pi; -pi, 0];
			unittests.misc.testGetMinimalInternalModel.verifyFunction(tc, zeros(3), 0);
			unittests.misc.testGetMinimalInternalModel.verifyFunction(tc, ...
				blkdiag(ramp, ramp), ramp);
			unittests.misc.testGetMinimalInternalModel.verifyFunction(tc, ...
				blkdiag(ramp, 0, ramp, 0), blkdiag(ramp));
			unittests.misc.testGetMinimalInternalModel.verifyFunction(tc, ...
				blkdiag(sine, sine), sine);
			unittests.misc.testGetMinimalInternalModel.verifyFunction(tc, ...
				blkdiag(sinePi, sine, -sine, ramp, 0, sinePi), blkdiag(ramp, sine, sinePi));
		end % testVariousMatrices()
		
		function testNumberOfCopies(tc)
			S = blkdiag(0, 0, [0, 1; 0, 0]);
			numberOfCopies = 3;
			Smin = blkdiag([0, 1; 0, 0], [0, 1; 0, 0], [0, 1; 0, 0]);
			myOdes = misc.getMinimalInternalModel(S, "minimal", numberOfCopies);
			mySs = myOdes.ss{1};
			tc.verifyEqual(rank(ctrb(mySs)), size(mySs.A, 1), "not controllable");
			tc.verifyEqual(mySs.A, Smin, "not minimum");
			tc.verifyEqual(size(mySs.B, 2), numberOfCopies);
		end % testNumberOfCopies()
		
	end
	
	methods (Static = true)
		
		function verifyFunction(tc, S, Smin)
			myOdes = misc.getMinimalInternalModel(S);
			mySs = myOdes.ss{1};
			tc.verifyEqual(rank(ctrb(mySs)), size(mySs.A, 1), "not controllable");
			tc.verifyEqual(mySs.A, Smin, 'AbsTol', 10*eps, "not minimum");
		end % verifyFunction()
		
	end % methods (Static = true)
	
end % classdef