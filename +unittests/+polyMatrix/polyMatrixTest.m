function [tests] = polyMatrixTest()
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
tests = functiontests(localfunctions);
end

function cTransposeTest(testCase)

	K = magic(4);
	KT = polyMatrix.cTranspose(K);
	
	testCase.verifyEqual(KT, K');
	
	K = cat(3, K(1:2, :), K(3:4, :));
	
	KT = polyMatrix.cTranspose(K);
	
	testCase.verifyEqual(K(:,:,1)', KT(:,:,1))
	testCase.verifyEqual(K(:,:,2)', -KT(:,:,2))

end

function polynomial2coefficientsTest(testCase)

syms s;
A0 = rand(2);
A1 = rand(2);
A2 = rand(2);
A3 = rand(2);

A = A0 + A1*s + A2*s^2 + A3*s^3;
Ac = double( polyMatrix.polynomial2coefficients(A, s) );

testCase.verifyEqual(Ac(:,:,1), A0);
testCase.verifyEqual(Ac(:,:,2), A1);
testCase.verifyEqual(Ac(:,:,3), A2);
testCase.verifyEqual(Ac(:,:,4), A3);

a = s^5;
ac = double( polyMatrix.polynomial2coefficients(a,s) );

testCase.verifyEqual(ac(:), misc.unitVector(6,6));

end
