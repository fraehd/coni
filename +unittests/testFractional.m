function [tests] = testFractional()
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
tests = functiontests(localfunctions);
end

function testDerivative(testCase)
t = sym("t");
t0 = 0.5;

	for p = [0.1 0.4 0.7 0.9]
		
		% define the analytic solution for the fractional derivative
		[y, m] = fractional.monomialFractionalDerivative( t, t0, 1, p);
		
		% compute the fractional derivative with the function
		f = fractional.derivative(m, p, t0, t);
		
		% evaluate both and compare them
		dt = 0.1;
		% skip the first value because there is a singularity that can not be computed by the
		% default symbolic evaluation. 
		% TODO@ferdinand: allow the quantity.Symbolic to use the limit function for the evaluation
		% at certain points.
		tDomain = quantity.EquidistantDomain("t", t0+dt, t0+1, "stepSize", dt);
		
		Y = quantity.Symbolic( y, tDomain);
		F = quantity.Symbolic( f, tDomain);
		
		testCase.verifyEqual( Y.on(), F.on(), 'AbsTol', 1e-15);
		
 		h = fractional.derivative(m, p, t0, t, 'type', 'C');
		H = quantity.Symbolic( h, tDomain);
 		testCase.verifyEqual( H.on(), Y.on(), 'AbsTol', 1e-15);
	end
end