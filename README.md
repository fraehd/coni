# conI - Control of Infinite-dimensional systems
The library contains Matlab code created for the research of the Infinite-dimensional Systems group at the [Institute of Measurement, Control and Microtechnology](https://www.uni-ulm.de/en/in/institute-of-measurement-control-and-microtechnology/) of Ulm University, Germany.

We work on the backstepping control, output regulation, fault diagnosis and simulation for distributed parameter systems (dps), hence most parts of this library are useful for working with dps.

## Prerequisites
Some parts of our code rely on Matlab toolboxes, for instance the Symbolic Math Toolbox, however there are useful functions and methods that can be used with plain Matlab installations. We recommend Matlab 2020a or more recent versions.

## How to use
Probably the most outstanding contribution is the "+quantity" framework. This enhances working with distributed parameters (for instance, time and/or space dependent matrices), since it combines the speed of numerical calculations with the simple interface of symbolic calculations. When implementing equations, your code will almost look like in your paper, instead of a cascade of plenty for-loops. An illustrative example is given in `+quantity\+example.m`

Besides that, this is a collection of many Matlab functions resulting from years of work in our research group. There is a reasonable chance, that you will find something useful, especially in "+misc" or "+numeric" folders.

We hope to add our newest implementations of backstepping controllers and fault diagnosis soon.

### Folder structure
The folder structure uses the Matlab packages, thus each folder starts with a plus "+" sign. 
*  The folder `+misc` is used for generic or standard Matlab helper functions. 
*  `+mustBe` contains verification functions for parameter of classes, see `doc Validate Property Values` in the Matlab documentation. 
*  The folder `+numeric` contains functions for fem, numerical integration, etc. 
*  In `+quantity` are classes to facilitate calculation with distributed matrices of numerical (`+Discrete`), symbolic (`+Symbolic`), function_handle (`+Function`) type. 
*  `+signals`, obviously, contains signals, for instance gevrey functions. 
*  In `+unittests` there is the same structure of folders, but it contains the unittests.

### Coding Guidelines
For general hints, see [the related wiki-page.](https://gitlab.com/fraehd/coni/-/wikis/Coding-guidelines)

### Contributing
We are looking forward to your feedback, criticism and contributions. Feel free contact us  and don't hesitate to commit changes. We will review all commits to ensure quality.

## Questions & Contact
Feel free to contact [Jakob](https://www.uni-ulm.de/en/in/institute-of-measurement-control-and-microtechnology/institute/staff/wissenschaftliche-mitarbeiter/gabriel-jakob-m-sc/) or [Ferdinand](https://www.uni-ulm.de/en/in/institute-of-measurement-control-and-microtechnology/institute/staff/wissenschaftliche-mitarbeiter/fischer-ferdinand-m-sc/) in case of suggestions or questions.

## License
GNU Lesser General Public License v3.0 (LGPL), see [Licence-file](https://gitlab.cs.fau.de/lrt_infinite_dimensional_systems/coni/blob/master/LICENSE).