function [ x_,y_,z ] = data2dat( filename, x, y, z, N, xRange, yRange )
%DATA2DAT Converts the given data to ".dat" format to use it with Latex
%tikzplot package. 
% Inputparameters:
%
%data2dat( filename, x, y)
%   filename: filename for the new ".dat" file
%   x: x-coordinate of the data as a vactor
%   y: y-coordinate of the data as a vactor
%
%data2dat( filename, x, y, z)
%   filename: filename for the new ".dat" file
%   x: x-coordinate of the data as a matrix
%   y: y-coordinate of the data as a matrix
%   z: z-coordinate of the data as a matrix
%
%data2dat( filename, x, y, z, xRange)
%   filename: filename for the new ".dat" file
%   x: x-coordinate of the data as a vactor
%   y: y-coordinate of the data as a vactor
%   z: has to be empty ~
%   xRange:  export.range of x-coordinate as vector with [x_min x_max]
%
%data2dat( filename, x, y, z, xRange, yRange )
%   filename: filename for the new ".dat" file
%   x: x-coordinate of the data as a matrix
%   y: y-coordinate of the data as a matrix
%   z: z-coordinate of the data as a matrix
%   xRange:  export.range of x-coordinate as vector with [x_min x_max]
%   yRange:  export.range of x-coordinate as vector with [y_min y_max]

if ~exist('xRange', 'var')
   xRange(1)=x(1);
   xRange(2)=x(end);
end

if ~exist('yRange', 'var')
   yRange(1)=y(1);
   yRange(2)=y(end);
end

if ~exist('N', 'var')
    N = 60;
end

if exist('z','var')
    z = squeeze(z);
    
    x_=linspace(xRange(1), xRange(2), N);
    y_=linspace(yRange(1), yRange(2), N);
    [x_,y_]=meshgrid(x_,y_);
    [x,y]=meshgrid(x,y);
    z=interp2(x,y,z',x_,y_);
    out=[x_(:),y_(:),z(:)];
else
    x_=linspace(xRange(1),xRange(2), length(x), N)';
    
    %Vektoren in das richtige Format f�r interp1 bringen:
    y=MakeColumnVectors(y);
    x=MakeColumnVectors(x);
        
    y_=interp1(x,y,x_);    
    out=[x_, y_];
end

if isempty(regexpi(filename,'\w*\.(dat)$'))
    filename=[filename '.dat'];
end
save(filename, 'out',  '-ascii', '-tabs')

