classdef namevalues < export.Data
    %VARS Summary of this class goes here
    %   Detailed explanation goes here
    %
    %   example:
    % --------------------------------------
    %   data = export.namevalues('vars', {'key1', 'key2', 'key3'}, 'values',
    %           [1, 2, 3]);
    %   keys.export();

    properties
        vars;
        values;
        seperator = '=';
    end
    
    properties (Dependent)
       Tab; 
    end
    
    methods
        
        function obj = namevalues(varargin)
           obj@export.Data(); 
           
           for arg=1:2:length(varargin)
               obj.(varargin{arg}) = varargin{arg + 1}; 
           end            
        end
        
        function T = get.Tab(obj)
            
            for k = 1:length(obj.vars)
                if length(obj.vars{k}) == 1
                   obj.vars{k} = ['"' obj.vars{k} '"']; 
                end
            end
           
            seps = repmat({obj.seperator}, size(obj.vars));
            
            T = table(obj.vars(:), seps(:), obj.values(:));
        end
        
        function is = isempty(obj)
            is = isempty(obj.vars) || isempty(obj.values);
        end
        
        function addnamevalue(obj, name, value)
            obj.vars{end+1} = name;
            obj.values(end+1) = value;
        end
        
    end
    
    methods ( Access = protected)
        function innerexport(obj)
              writetable(obj.Tab, obj.path, 'Delimiter', 'space', 'WriteVariableNames', false);             
        end
    end
    
    
end

