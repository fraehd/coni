classdef (Abstract) Data < handle & matlab.mixin.Heterogeneous
    %EXPORTDATA Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        basepath char = '.';
        foldername char = '.';
        filename char = 'data';
        N = 201;        
    end
        
    methods
        
        function obj = Data(varargin)
           for arg=1:2:length(varargin)
               obj.(varargin{arg}) = varargin{arg + 1}; 
           end
        end
        
        function p = path(obj, k)
            if ~exist('k', 'var')
                k = '';
            end
            
           p = [obj.absolutepath filesep obj.addExtension(k)]; 
        end
                
        function p = absolutepath(obj)
            p = [obj.basepath filesep obj.foldername];
        end
        
        function f = addExtension(obj, k)
            
            if ~exist('k', 'var')
                name = obj.filename;
            elseif ischar(k)
                name = [obj.filename k];
            elseif isnumeric(k)
                name = [obj.filename num2str(k)];
            end
            
            if isempty(regexpi(obj.filename, '\w*\.(dat)$'))
                f = [name '.dat'];
            else
                error('Dateiname muss ohne Endung übergebenw erden!');
            end                        
        end
        
    end
    
    methods(Sealed)
        function export(obj)
            for k = 1:length(obj)
               if ~obj(k).isempty
                   
                  if ~isfolder(obj(k).absolutepath)
                     mkdir(obj(k).absolutepath); 
                  end
                   
                  obj(k).innerexport();
                  
                  fprintf(['export: %s \n'], obj(k).path);
                  
               else
                  warning(['Object ' num2str(k) ' is empty']);
               end
            end
        end   
    end 
   
    methods (Abstract, Access = protected)
       innerexport(obj); 
    end
    
    methods (Static)
        function exportAll( exp_struct)
           structfun(@(x)export(x), exp_struct); 
        end
    end
end

